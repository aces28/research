package ph.com.ncs.vanilla.base.security.rest;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import ph.com.ncs.vanilla.audit.service.AuditService;

/**
 * An authentication logout success handler implementation adapted to a REST approach.
 */
public class RestLogoutHandler implements LogoutSuccessHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestLogoutHandler.class);

    @Autowired
    private AuditService auditService;
    
    @Autowired
    private Environment resource;
    
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException, ServletException {
    	
        String userName = request.getParameter("username");
    	
    	if (userName != null) {
    		auditService.saveUserAccessAuditLogs(userName,  Integer.parseInt(resource.getRequiredProperty("module.logout")), request);
            LOGGER.info("onLogoutSuccess : Save User Access History (Logout)");   
    	}
    	
        LOGGER.info("session destroy : START ");
        HttpSession httpSession = request.getSession();
        if (httpSession != null) {
            request.getSession().invalidate();
        }

    }
}
