package ph.com.ncs.vanilla.base.security.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import ph.com.ncs.vanilla.audit.service.AuditService;
import ph.com.ncs.vanilla.user.domain.User;
import ph.com.ncs.vanilla.user.service.UserService;

/**
 * An authentication success handler implementation adapted to a REST approach.
 */
public class RestAuthSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestAuthSuccessHandler.class);
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private AuditService auditService;
    
    @Autowired
    private Environment resource;
    
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {

		Collection<GrantedAuthority> grantedAuthorities = (Collection<GrantedAuthority>) authentication.getAuthorities();
        List<String> roles = new ArrayList<>();

        for (GrantedAuthority grantedAuthority : grantedAuthorities) {
            roles.add(grantedAuthority.getAuthority());
        }
        
        String username = authentication.getName();
        
        LOGGER.info("send user to request : START : user name : " + username + " : " + Arrays.toString(roles.toArray()));
        
        User user = userService.findByUserName(username);
        user.setFailedLoginAttempts(0);
        user.setIsLocked(0);
        userService.save(user);
        auditService.saveUserAccessAuditLogs(username,  Integer.parseInt(resource.getRequiredProperty("module.login")), request);
        LOGGER.info("onAuthenticationSuccess : Failed Login Attempts Reset & Save User Access History (Login)");
        
        response.setStatus(HttpServletResponse.SC_OK);
        response.getWriter().write(authentication.getName() + "," + Arrays.toString(roles.toArray()).toUpperCase());
        response.getWriter().flush();
    }
}