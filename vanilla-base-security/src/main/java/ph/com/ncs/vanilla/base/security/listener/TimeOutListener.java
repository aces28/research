package ph.com.ncs.vanilla.base.security.listener;

import java.util.Date;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@WebListener
public class TimeOutListener implements HttpSessionListener {
	  
	private static final Logger LOGGER = LoggerFactory.getLogger(TimeOutListener.class);
	private static int totalActiveSessions;
	
	public static int getTotalActiveSession(){
		return totalActiveSessions;  
	}
			
	@Override
	public void sessionCreated(HttpSessionEvent event) {
		totalActiveSessions++;
		LOGGER.info("Session ID" + event.getSession().getId() + " created at " + new Date().toString());
	}
	 
	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
		totalActiveSessions--;
		LOGGER.info("Session ID" + event.getSession().getId() + " destroyed at " + new Date().toString());
	}
}