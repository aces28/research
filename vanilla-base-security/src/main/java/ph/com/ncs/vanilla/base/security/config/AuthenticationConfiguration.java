package ph.com.ncs.vanilla.base.security.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;

/**
 * Created by ace on 7/3/16.
 */
@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE)
public class AuthenticationConfiguration extends GlobalAuthenticationConfigurerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(AuthenticationConfiguration.class);

    @Autowired
    private CustomLdapAuthorities ldapAuthorities;
    @Autowired
    private Environment environment;

    @Override
    public void init(AuthenticationManagerBuilder auth) throws Exception {
        logger.info("initialize LDAP configuration : START");

        String managerDn = environment.getRequiredProperty("ldap.uid");
        String managerPassword = environment.getRequiredProperty("ldap.password");
        String userSearchBase = environment.getRequiredProperty("ldap.base");
        String ldapUrl = environment.getRequiredProperty("ldap.url");
        String userDnPatterns = environment.getRequiredProperty("ldap.userSearchDn");
        String userFilter = environment.getRequiredProperty("ldap.user.filter");

        logger.info("userDnPatterns : " + userDnPatterns);
        logger.info("LDAP url : " + ldapUrl);
        logger.info("managerDN : " + managerDn);
        logger.info("userSearchBase : " + userSearchBase);

        //XXX Add configuration to control environment from DEV, STAGING and PROD
//        auth.ldapAuthentication().ldapAuthoritiesPopulator(ldapAuthorities).userDnPatterns("uid={0},ou=people").
//                groupSearchBase("ou=groups").contextSource().ldif("classpath:user.ldif");

        auth.ldapAuthentication().ldapAuthoritiesPopulator(ldapAuthorities)
                .contextSource()
                .url(ldapUrl)
                .managerDn(managerDn)
                .managerPassword(managerPassword)
                .and()
                .userSearchBase(userDnPatterns)
                .userSearchFilter(userFilter);
    }
}
