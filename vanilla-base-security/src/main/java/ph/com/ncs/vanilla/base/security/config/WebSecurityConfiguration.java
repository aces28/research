package ph.com.ncs.vanilla.base.security.config;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.util.matcher.AndRequestMatcher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.NegatedRequestMatcher;

import ph.com.ncs.vanilla.base.security.filters.SecurityFilter;

/**
 * Created by ace on 7/3/16.
 */
@SuppressWarnings("unused")
@Configuration
@EnableWebSecurity
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(WebSecurityConfiguration.class);

    @Resource
    private AuthenticationEntryPoint authenticationEntryPoint;
    @Resource
    private AuthenticationFailureHandler authenticationFailureHandler;
    @Resource
    private AuthenticationSuccessHandler authenticationSuccessHandler;
    @Resource
    private LogoutSuccessHandler logoutSuccessHandler;
    @Resource
    private SecurityFilter securityFilter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().
                antMatchers(HttpMethod.OPTIONS, "/*/**").permitAll().
                antMatchers("/login",  "/login/role").permitAll().
//                antMatchers("/logout", "/mis/**", "/isg/**", "/approver/**").permitAll();
                antMatchers("/logout", "/mis/**", "/isg/**", "/approval/**", "/audit/**").authenticated();

        http.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint);
        http.formLogin().successHandler(authenticationSuccessHandler);
        http.formLogin().failureHandler(authenticationFailureHandler);

        http.logout().logoutUrl("/logout").logoutSuccessHandler(logoutSuccessHandler);

        http.addFilterBefore(securityFilter, ChannelProcessingFilter.class);

        http.csrf().requireCsrfProtectionMatcher(new AndRequestMatcher(
                new NegatedRequestMatcher(new AntPathRequestMatcher("/login*/**", HttpMethod.OPTIONS.toString())),
                new NegatedRequestMatcher(new AntPathRequestMatcher("/login*/**", HttpMethod.POST.toString())),
                new NegatedRequestMatcher(new AntPathRequestMatcher("/login*/**", HttpMethod.GET.toString())),
                new NegatedRequestMatcher(new AntPathRequestMatcher("/mis*/**", HttpMethod.GET.toString())),
                new NegatedRequestMatcher(new AntPathRequestMatcher("/mis*/**", HttpMethod.POST.toString())),
                new NegatedRequestMatcher(new AntPathRequestMatcher("/isg*/**", HttpMethod.GET.toString())),
                new NegatedRequestMatcher(new AntPathRequestMatcher("/isg*/**", HttpMethod.POST.toString())),
                new NegatedRequestMatcher(new AntPathRequestMatcher("/approval*/**", HttpMethod.GET.toString())),
                new NegatedRequestMatcher(new AntPathRequestMatcher("/approval*/**", HttpMethod.POST.toString())),
                new NegatedRequestMatcher(new AntPathRequestMatcher("/audit*/**", HttpMethod.GET.toString())),
                new NegatedRequestMatcher(new AntPathRequestMatcher("/audit*/**", HttpMethod.POST.toString())),
                new NegatedRequestMatcher(new AntPathRequestMatcher("/logout*/**", HttpMethod.OPTIONS.toString())))).disable();

//        http
//        	.sessionManagement()
//        	.maximumSessions(1)
//        	.expiredUrl("/login")
//        	.maxSessionsPreventsLogin(true)
//        	.and()
//        	.sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
//        	.invalidSessionUrl("/");
    }
}
