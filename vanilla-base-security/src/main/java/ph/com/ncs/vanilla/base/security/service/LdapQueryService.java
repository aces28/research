package ph.com.ncs.vanilla.base.security.service;

import ph.com.ncs.vanilla.user.domain.User;

import java.util.List;

/**
 * Created by ace on 7/18/16.
 */
public interface LdapQueryService {

    /**
     * @return
     */
    List<User> getAllUsers();

    /**
     * @param userName
     * @return
     */
    User getUserDetails(String userName);

    /**
     * @param userName
     * @return
     */
    List<User> getLdapUsers(String userName);

    /**
     * @param expiredDate
     * @return list of Expired LDAP Users
     */
    List<User> getExpiredLdapUsers(String expiredDate);

    /**
     * LDAP batch base DN
     */
    String BATCH_BASEDN = "batch.baseDN";

    /**
     * LDAP object class
     */
    String OBJECT_CLASS = "objectClass";

    /**
     * LDAP batch object class
     */
    String BATCH_OBJECT_CLASS = "batch.objectClass";

    /**
     * LDAP user binding
     */
    String USER_BIND = "sAMAccountName";

    /**
     * LDAP batch account expire
     */
    String BATCH_ACCT_EXPIRE = "batch.accountExpires";

    /**
     * LDAP batch never expires
     */
    String BATCH_NEVER_EXPIRE = "batch.neverExpires1";
}
