package ph.com.ncs.vanilla.base.security.config;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.ldap.userdetails.LdapAuthoritiesPopulator;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import ph.com.ncs.vanilla.user.domain.User;
import ph.com.ncs.vanilla.user.service.UserService;

/**
 * Created by ace on 7/8/16.
 */
@Component
public class CustomLdapAuthorities implements LdapAuthoritiesPopulator {

    private static final Logger logger = LoggerFactory.getLogger(CustomLdapAuthorities.class);

    @Autowired
    private UserService userService;

    @Override
    public Collection<? extends GrantedAuthority> getGrantedAuthorities(DirContextOperations dirContextOperations, String userName) {
        Collection<GrantedAuthority> grantedAuthorities = new HashSet<>();

        User user = userService.findByUserName(userName);

        String roleType = "";

        if (user != null) {

            if (user.getFailedLoginAttempts() >= 3) throw new LockedException(userName);

            if (user.getGroup() != null) {
                if (user.getGroup().getRole() != null && !StringUtils.isEmpty(user.getGroup().getRole().getRoleType())) {
                    roleType = user.getGroup().getRole().getRoleType();
                } else if (user.getRole() != null && !StringUtils.isEmpty(user.getRole().getRoleType())) {
                    roleType = user.getRole().getRoleType();
                }
            } else if (user.getRole() != null && !StringUtils.isEmpty(user.getRole().getRoleType())) {
                roleType = user.getRole().getRoleType();
            }

            logger.info("Granted authorities : user : " + userName + " : " + roleType);
            grantedAuthorities.add(new SimpleGrantedAuthority(roleType));
        } else {
            logger.info("User account can not be found or is already locked");
            throw new UsernameNotFoundException(userName);
        }

        return grantedAuthorities;
    }
}
