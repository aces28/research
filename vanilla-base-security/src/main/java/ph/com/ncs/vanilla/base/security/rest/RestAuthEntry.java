package ph.com.ncs.vanilla.base.security.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * An authentication entry point implementation adapted to a REST approach.
 */
public class RestAuthEntry implements AuthenticationEntryPoint {

    private static final Logger logger = LoggerFactory.getLogger(RestAuthEntry.class);

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException)
            throws IOException, ServletException {
        logger.info("commence : START");
//        response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
    }
}
