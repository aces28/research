package ph.com.ncs.vanilla.base.security.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ace on 7/3/16.
 */
public class SecurityFilter implements Filter {

    private static final Logger logger = LoggerFactory.getLogger(SecurityFilter.class);
    // This is to be replaced with a list of domains allowed to access the server
    private final List<String> allowedOrigins = Arrays.asList("http://internal-vmvnlelbpd01-2052517109.ap-southeast-1.elb.amazonaws.com", "http://easyplanadmin.republikatm.ph");
//    		"http://localhost:8080", "http://127.0.0.1:8080",
//            "http://127.0.0.1:33456", "http://10.176.82.69:4502", "http://localhost:4502", "http://localhost:9090",
//            "http://vanillawebtool-preprod.globe.com.ph", "vanillawebtool-preprod.globe.com.ph", "http://10.225.37.116:4502",
//            "http://10.225.36.107:4502", "http://contentstage.globe.com.ph", "http://contentdev.globe.com.ph", "192.168.23.152",
//            "http://192.168.23.179:4502", "192.168.23.179", "192.168.23.152", "http://192.168.23.191:4502", "192.168.23.191", "192.168.23.175",
//            "http://10.243.255.59:4503", "http://10.243.255.73:4503", "10.243.255.51", "10.243.255.87", 
//            "http://10.176.82.138:4502", "http://10.176.82.138:9090",  "http://10.176.82.134:4502", "http://10.176.82.134:9090");

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.info("Initialize filter config : START");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        if (request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
            HttpServletRequest httpServletRequest = (HttpServletRequest) request;
            String origin = httpServletRequest.getHeader("Origin");
            HttpServletResponse httpServletResponse = (HttpServletResponse) response;

            httpServletResponse.setHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, allowedOrigins.contains(origin) ? origin : "");
            httpServletResponse.setHeader(HttpHeaders.VARY, "Origin");
            httpServletResponse.setHeader(HttpHeaders.ACCESS_CONTROL_MAX_AGE, "3600");
            httpServletResponse.setHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS, "true");
            httpServletResponse.setHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_METHODS, "POST, PUT, GET, OPTIONS");
            httpServletResponse.setHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS, "Origin, X-Requested-With, X-Requested-By, " +
                    "Content-Type, Accept, " + "X-CSRF-TOKEN, CSRF-Token");
        }

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }
}
