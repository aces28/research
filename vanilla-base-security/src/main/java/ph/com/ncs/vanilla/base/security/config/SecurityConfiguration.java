package ph.com.ncs.vanilla.base.security.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import ph.com.ncs.vanilla.base.security.filters.SecurityFilter;
import ph.com.ncs.vanilla.base.security.rest.RestAuthEntry;
import ph.com.ncs.vanilla.base.security.rest.RestAuthSuccessHandler;
import ph.com.ncs.vanilla.base.security.rest.RestLogoutHandler;

/**
 * Created by ace on 6/27/16.
 */
@Configuration
public class SecurityConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(SecurityConfiguration.class);

    @Autowired
    private Environment environment;

    @Bean
    public RestAuthEntry authEntry() {
        return new RestAuthEntry();
    }

    @Bean
    public SimpleUrlAuthenticationFailureHandler authenticationFailureHandler() {
        return new BaseAuthenticator();
    }

    @Bean
    public RestAuthSuccessHandler authSuccessHandler() {
        return new RestAuthSuccessHandler();
    }

    @Bean
    public SecurityFilter securityFilter() {
        return new SecurityFilter();
    }

    @Bean
    public LogoutSuccessHandler logoutSuccessHandler() {
        return new RestLogoutHandler();
    }

    @Bean
    public LdapContextSource contextSource() {
        LdapContextSource contextSource = new LdapContextSource();
        String ldapUrl = "ldap://" + environment.getRequiredProperty("ldap.address") + ":" + environment.getRequiredProperty("ldap.port");
        logger.info("ldap connection : " + ldapUrl);
        contextSource.setUrl(ldapUrl);
        contextSource.setBase(environment.getRequiredProperty("ldap.base"));
        contextSource.setUserDn(environment.getRequiredProperty("ldap.uid"));
        contextSource.setPassword(environment.getRequiredProperty("ldap.password"));
        return contextSource;
    }

    @Bean
    public LdapTemplate ldapTemplate() {
        return new LdapTemplate(contextSource());
    }

}
