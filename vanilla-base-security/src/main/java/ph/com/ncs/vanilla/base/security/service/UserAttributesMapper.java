package ph.com.ncs.vanilla.base.security.service;

import org.springframework.ldap.core.AttributesMapper;
import org.springframework.util.StringUtils;
import ph.com.ncs.vanilla.user.domain.User;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;

/**
 * Created by ace on 7/18/16.
 */
public class UserAttributesMapper implements AttributesMapper<User> {

    private User user;

    @Override
    public User mapFromAttributes(Attributes attributes) throws NamingException {
        
    	if (attributes == null)
            return null;
            
        user = new User();
        
        if (attributes.get("sAMAccountName") != null && !StringUtils.isEmpty(attributes.get("sAMAccountName").toString()))
        	user.setUserName(attributes.get("sAMAccountName").get().toString());
        
        if (attributes.get("userPrincipalName") != null && !StringUtils.isEmpty(attributes.get("userPrincipalName").toString()))
        	user.setEmail(attributes.get("userPrincipalName").get().toString());
        else
        	user.setEmail("no email set from the active directory"); //XXX check for other alternative attribute for email
       
        if (attributes.get("givenName") != null && !StringUtils.isEmpty(attributes.get("givenName").toString()))
        	user.setFirstName(attributes.get("givenName").get().toString());
        else
        	user.setFirstName("no first name set from the active directory");
        
        if (attributes.get("sn") != null && !StringUtils.isEmpty(attributes.get("sn").toString()))
        	user.setLastName(attributes.get("sn").get().toString());
        else
        	user.setLastName("no last name set from the active directory");

        return user;
    }
}
