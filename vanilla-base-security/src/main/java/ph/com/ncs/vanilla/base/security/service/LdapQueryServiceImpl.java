package ph.com.ncs.vanilla.base.security.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.query.LdapQueryBuilder;
import org.springframework.stereotype.Service;

import ph.com.ncs.vanilla.user.domain.User;

import javax.naming.directory.SearchControls;

/**
 * Created by ace on 7/18/16.
 */
@Service
public class LdapQueryServiceImpl implements LdapQueryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LdapQueryServiceImpl.class);

    @Autowired
    @Qualifier(value = "ldapTemplate")
    private LdapTemplate ldapTemplate;

    @Autowired
    private Environment resource;

    @Override
    public List<User> getAllUsers() {
    	LOGGER.info("getAllUsers() : START");
        ldapTemplate.setIgnoreSizeLimitExceededException(true);
        List<User> userList = ldapTemplate.search(LdapQueryBuilder.query()
        		.base(resource.getRequiredProperty(BATCH_BASEDN)).where(OBJECT_CLASS)
                .is(resource.getRequiredProperty(BATCH_OBJECT_CLASS)), new UserAttributesMapper());
        return userList;
    }

    @Override
    public User getUserDetails(String userName) {
    	LOGGER.info("getUserDetails() : START");
        List<User> userList = ldapTemplate.search(LdapQueryBuilder.query()
        		.base(resource.getRequiredProperty(BATCH_BASEDN)).where(OBJECT_CLASS)
                .is(resource.getRequiredProperty(BATCH_OBJECT_CLASS))
                .and(USER_BIND).is(userName), new UserAttributesMapper());

        if (userList != null && !userList.isEmpty()) {
            return userList.get(0);
        }

        return null;
    }

    @Override
    public List<User> getLdapUsers(String userName) {
    	LOGGER.info("getLdapUsers() : START");
        ldapTemplate.setIgnoreSizeLimitExceededException(true);
        List<User> userList = ldapTemplate.search(LdapQueryBuilder.query()
        		.base(resource.getRequiredProperty(BATCH_BASEDN)).where(OBJECT_CLASS)
                .is(resource.getRequiredProperty(BATCH_OBJECT_CLASS))
                .and(USER_BIND).is(userName), new UserAttributesMapper());
        return userList;
    }

	@Override
	public List<User> getExpiredLdapUsers(String expiredDate) {
		LOGGER.info("getExpiredLdapUsers() : START");
        ldapTemplate.setIgnoreSizeLimitExceededException(true);
		List<User> userList = ldapTemplate.search(LdapQueryBuilder.query()
				.base(resource.getRequiredProperty(BATCH_BASEDN)).where(OBJECT_CLASS)
				.is(resource.getRequiredProperty(BATCH_OBJECT_CLASS))
				.and(resource.getRequiredProperty(BATCH_ACCT_EXPIRE)).lte(expiredDate)
				.and(resource.getRequiredProperty(BATCH_ACCT_EXPIRE)).not().is(resource.getRequiredProperty(BATCH_NEVER_EXPIRE))
				, new UserAttributesMapper());
	     return userList;
	}
}
