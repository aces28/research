package ph.com.ncs.vanilla.base.security.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import ph.com.ncs.vanilla.user.domain.User;
import ph.com.ncs.vanilla.user.service.UserService;

/**
 * Created by ace on 7/28/16.
 */
public class BaseAuthenticator extends SimpleUrlAuthenticationFailureHandler {

    private static final Logger logger = LoggerFactory.getLogger(BaseAuthenticator.class);

    @Autowired
    private UserService userService;

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {

        logger.info("onAuthenticationFailure : START");
        super.onAuthenticationFailure(request, response, exception);

        String userName = request.getParameter("username");
        User user = userService.findByUserName(userName);
        int failedAttempts = 0;

        if (user != null) {
            failedAttempts = user.getFailedLoginAttempts();
            if (failedAttempts < 3) {
                user.setFailedLoginAttempts(++failedAttempts);
                if (failedAttempts == 3) {
                    logger.info("onAuthenticationFailure : Account Locked");
                    user.setIsLocked(1);
                }
                userService.save(user);
            } else {
                throw new LockedException(userName);
            }
        } else {
            throw new UsernameNotFoundException(userName);
        }

        logger.info("onAuthenticationFailure : END");
    }

}
