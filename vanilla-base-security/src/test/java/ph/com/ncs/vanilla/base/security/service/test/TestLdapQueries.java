package ph.com.ncs.vanilla.base.security.service.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;
import ph.com.ncs.vanilla.base.security.service.LdapQueryService;
import ph.com.ncs.vanilla.user.domain.User;

import java.util.List;

/**
 * Created by ace on 7/18/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = VanillaContextConfig.class)
public class TestLdapQueries {

    private static final Logger logger = LoggerFactory.getLogger(TestLdapQueries.class);

    @Autowired
    private LdapQueryService ldapQueryService;

    @Test
    public void testLdapQueries() {
        User user = ldapQueryService.getUserDetails("zncs0006");
        logger.info("User first name : " + user.getFirstName() + " : e-mail : " + user.getEmail());
    }

    @Test
    public void testLdapQueryList() {
        List<User> userList = ldapQueryService.getAllUsers();
        logger.info("User size : " + userList.size());
    }
}
