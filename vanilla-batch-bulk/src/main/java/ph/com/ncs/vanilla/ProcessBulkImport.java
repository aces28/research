package ph.com.ncs.vanilla;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.support.ResourcePropertySource;
import org.springframework.stereotype.Component;

import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;
import ph.com.ncs.vanilla.group.domain.Group;
import ph.com.ncs.vanilla.role.domain.Role;
import ph.com.ncs.vanilla.user.dao.UserRepository;
import ph.com.ncs.vanilla.user.domain.User;

/**
 * Created by francism on 08/12/16.
 */
@Component
public class ProcessBulkImport implements CommandLineRunner{

	private static final Logger LOGGER = LoggerFactory.getLogger(VanillaContextConfig.class);
	
	private static final SimpleDateFormat DATE_LOG_FORMAT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");

	private static final SimpleDateFormat DATE_FILE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmssSSS");	 	
	
	@Autowired
	private UserRepository userService;
	
	 @Autowired
	 private ConfigurableApplicationContext context;
	
	 @Override
	 public void run(String... args) throws Exception {
			
			ResourcePropertySource propertySource = new ResourcePropertySource("resource",
					"classpath:application-bulk-jobs.properties");

			String logDir = (String) propertySource.getProperty("vanilla.batch.logs_dir");
			LOGGER.info((String) propertySource.getProperty("vanilla.batch.logs_dir"));
			if (!validateDir(logDir)) {
				throw new Exception("Cannot find valid log directory location. ");
			}
			String sourceDir = (String) propertySource.getProperty("vanilla.batch.source_dir");
			if (!validateDir(sourceDir)) {
				throw new Exception("Cannot find valid source directory location. ");
			}
			String backupDir = (String) propertySource.getProperty("vanilla.batch.backup_dir");
			if (!validateDir(backupDir)) {
				throw new Exception("Cannot find valid backup directory location. ");
			}
			String errorDir = (String) propertySource.getProperty("vanilla.batch.error_dir");
			if (!validateDir(errorDir)) {
				throw new Exception("Cannot find valid error directory location. ");
			}
			
			ArrayList<String> logContents = new ArrayList<String>();
			LOGGER.info("Start of Vanilla import batch. Time:" + DATE_LOG_FORMAT.format(new Date()));
			logContents.add("Start of Vanilla import batch. Time:" + DATE_LOG_FORMAT.format(new Date()));
			String[] filenames = new File(sourceDir).list();
			String filename = filenames[0];
			File file = null;
			LOGGER.info("Filename is: " + filename);
			logContents.add("Filename is: " + filename);
			
			file = new File(sourceDir + File.separator + filename);
			
			if (file.isFile()) {
					LOGGER.info("Processing File "+ filename + ".....");						
					processFile(file, logContents, backupDir, errorDir,filename);					
					LOGGER.info("Processing "+ filename + " ended");
					logContents.add("Processing File "+ filename + " ended");

			}
			LOGGER.info("End of Vanilla import batch. Time: " + DATE_LOG_FORMAT.format(new Date()));
			logContents.add("End of Vanilla import batch. Time: " + DATE_LOG_FORMAT.format(new Date()));
			String date = DATE_FILE_FORMAT.format(new Date());
			File location = new File(logDir);
			File logFile = new File(location.getAbsolutePath() + File.separator + "vanilla_bulk_import_" + date + ".log");
			writeFile(logFile, logContents);
			
			context.close();
		}
		
			private static boolean validateDir(String directory) {
			if (directory == null || directory.length() == 0) {
				return false;
			}
			if (directory.endsWith(File.separator)) {
				directory = directory.substring(0, directory.length() - 1);
			}
			return (new File(directory).isDirectory());

		}	
		
		
		private void processFile(File file, ArrayList<String> logContents, String backupDir, String errorDir, String filename) throws IOException {
			LOGGER.info("Start processing file: " + file.getName() + " " + DATE_LOG_FORMAT.format(new Date()));
			logContents.add("Start processing file: " + file.getName() + " " + DATE_LOG_FORMAT.format(new Date()));
				if (checkFile(filename)){
					logContents.add("File Type : csv");	
				List<User> accounts = readFile(file, logContents);
				if (accounts != null) {				
					if (accounts.size() > 0){
						userService.save(accounts);
	
					LOGGER.info("Process Import Accounts: " + accounts.size() + " accounts imported!");
					logContents.add("Process Import Accounts: " + accounts.size() + " accounts imported!");	
					
					LOGGER.info("Records saved: " + accounts.size());
					logContents.add("Records saved: " + accounts.size());
					moveFile(backupDir, file, "backup");
					
					}else{
						LOGGER.info("Account file being imported is empty.");
						logContents.add("Account file being imported is empty.");
						moveFile(errorDir, file, "error");
				}
					
				LOGGER.info("End processing file: " + file.getName() + " " + DATE_LOG_FORMAT.format(new Date()));
				logContents.add("End processing file: " + file.getName() + " " + DATE_LOG_FORMAT.format(new Date()));
				}
			}else{
				LOGGER.info("File Type : Invalid");
				logContents.add("File Type : Invalid");
				LOGGER.info("End processing file: " + file.getName() + " " + DATE_LOG_FORMAT.format(new Date()));
				logContents.add("End processing file: " + file.getName() + " " + DATE_LOG_FORMAT.format(new Date()));
			}
		}
		private static void moveFile(String directory, File file, String type) {
			String date = DATE_FILE_FORMAT.format(new Date());
			File location = new File(directory);
			File destination = new File(
					location.getAbsolutePath() + File.separator + file.getName() + "_" + type + "_" + date);
			file.renameTo(destination);
		}


		public static List<User> readFile(File file, List<String> logContents) throws IOException {
			List<User> accountListToVanillaDB =  Collections.synchronizedList(new ArrayList<User>());;
			int fileCtr = count(file);
			BufferedReader reader = null;
			String cvsSplit =",";
			logContents.add("Starting Reading File "+ file.getName() + ".....");
			
			int ctr = 0;
			try {
				reader = new BufferedReader(new FileReader(file));
				String line = "";
				User[] account = new User[fileCtr];
								
					while ((line = reader.readLine()) != null) {
						String[] userInfo = line.split(cvsSplit);	
						
						logContents.add("");
						logContents.add("Adding Record [" + ctr + "]");
						logContents.add("USERNAME: "+ userInfo[0]);
						logContents.add("FIRST NAME: " + userInfo[1]);
						logContents.add("LAST NAME: " + userInfo[2]);
						logContents.add("EMAIL: " + userInfo[3]);
						logContents.add("ROLE: " + userInfo[4]);
						
						account[ctr] = new User();
						Role role = new Role();
						role.setId(Integer.parseInt(userInfo[4]));
						
						Group grp = new Group();
						grp.setId(0);

						account[ctr].setUserName(userInfo[0]);
						account[ctr].setFirstName(userInfo[1]);
						account[ctr].setLastName(userInfo[2]);
						account[ctr].setEmail(userInfo[3]);	
						account[ctr].setGroup(grp);
						account[ctr].setRole(role);
						account[ctr].setFailedLoginAttempts(0);
						account[ctr].setStatus(1);
						account[ctr].setIsLocked(0);
						account[ctr].setCreatedBy("VanillaTeam");
						account[ctr].setCreatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
						account[ctr].setUpdatedBy("VanillaTeam");
						account[ctr].setUpdatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));						
						accountListToVanillaDB.add(account[ctr]);
						logContents.add("Account added : " +account[ctr].getUserName() + " , " 
								+ account[ctr].getFirstName() + " , " + account[ctr].getLastName() + " , " + account[ctr].getEmail() + " , " + role.getId());
						logContents.add("");
						
						ctr++;
						
					}
					
			} catch (FileNotFoundException e) {
				System.out.println("Error:"+e.getMessage());
			} catch (IOException e) {
				System.out.println("Error:"+e.getMessage());
			} finally {
				if (reader != null) {
					try {
						reader.close();
					} catch (IOException e) {
					}
				}
			}
			logContents.add(".... Reading File "+ file.getName() + " ended");
			return accountListToVanillaDB;

		}	
		
		
		private static int count(File filename) throws IOException {
		    InputStream is = new BufferedInputStream(new FileInputStream(filename));
		    try {
		    byte[] c = new byte[1024];
		    int count = 0;
		    int readChars = 0;
		    boolean empty = true;
		    while ((readChars = is.read(c)) != -1) {
		        empty = false;
		        for (int i = 0; i < readChars; ++i) {
		            if (c[i] == '\n') {
		                ++count;
		            }
		        }
		    }
		    return (count == 0 && !empty) ? 1 : count;
		    } finally {
		    is.close();
		   }
		}
		
		public static void writeFile(File file, ArrayList<String> logContents) {
			try {
				if (!file.exists()) {
					file.createNewFile();
				}
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				for (String log : logContents) {
					bw.write(log + "\n");
				}
				bw.close();
				fw.close();
			} catch (IOException e) {
				System.out.println("Error:"+e.getMessage());
			}
		}
		
		
		public static boolean checkFile(String filename)
		{
			String ext = FilenameUtils.getExtension(filename);
			if(ext.equals("csv")){
				return true;
			}else			
			return false;		
					
		}
	}

