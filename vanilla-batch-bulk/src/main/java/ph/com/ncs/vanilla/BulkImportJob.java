package ph.com.ncs.vanilla;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.test.ImportAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;

/**
 * Created by francism on 08/12/16.
 */

@SpringBootApplication
@ImportAutoConfiguration(VanillaContextConfig.class)
public class BulkImportJob {

    public static void main(String[] args) {
        new SpringApplicationBuilder(BulkImportJob.class).web(false).run(args);
    }

}
