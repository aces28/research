package ph.com.ncs.vanilla.commons.constant.test;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ph.com.ncs.vanilla.commons.constants.VanillaConstants;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;

import java.sql.Date;

/**
 * Created by ace on 6/11/16.
 */
public class TestConstants {

    private static final Logger logger = LoggerFactory.getLogger(TestConstants.class);

    @Test
    public void testConstants() {
        logger.info("User : " + VanillaConstants.SYSTEM_USER);
        Assert.assertEquals("ace", VanillaConstants.SYSTEM_USER);

        Date date = new Date(VanillaDateUtils.longDateFromFormat("07/21/2016"));
        logger.info("date from : " + date);
    }
}
