package ph.com.ncs.vanilla.commons.constant.test;

import org.junit.Test;
import ph.com.ncs.vanilla.commons.utils.PhoneUtils;

/**
 * Created by ace on 6/14/16.
 */
public class TestPhoneNumberUtils {

    @Test
    public void testNumber() {
        System.out.println(PhoneUtils.esbPhoneNumber("09063241954"));
        System.out.println(PhoneUtils.internationalFormat("9063241954"));
    }
}
