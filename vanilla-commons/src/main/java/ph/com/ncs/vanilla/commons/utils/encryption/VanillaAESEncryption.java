package ph.com.ncs.vanilla.commons.utils.encryption;

import java.io.*;
import java.security.*;
import java.security.spec.*;
import java.util.*;

import javax.crypto.*;
import javax.crypto.spec.*;

import org.apache.commons.codec.binary.Base64;

import ph.com.ncs.vanilla.commons.utils.encryption.exceptions.InvalidKeyLengthException;
import ph.com.ncs.vanilla.commons.utils.encryption.exceptions.StrongEncryptionNotAvailableException;

public class VanillaAESEncryption {
	
	private final static String SECRET_KEY = new  String("q2IfiQyifQSSbMV3Sht4jxjWjMwAAAAAAAAAAAAAAAA=");
	private final static String SALT = new String("vhGVN/DQq8wVhnxD4wKNhRV0MuoAAAAAAAAAAAAAAAA=");

	private static byte[] key;
	
	private static String initializationVector = "eaZlpHWXPPCGAjk84MVoWQ==";
//	private static String plainText = &lt;your_plainText&gt;;
	private static int pswdIterations = 65536;
	private static int keySize = 256;
	//private static SecretKeySpec secretKey;
	
	public static String encrypt(String plainText)  {
		
		System.out.println("Starting ecnryption");
		byte[] encryptedTextBytes = null;
		try{
			byte[] saltBytes = SALT.getBytes("UTF-8");
	//		System.out.println("saltBytes " + new String(saltBytes));
			byte[] ivBytes = initializationVector.getBytes("UTF-8");
			
	//		System.out.println("ivBytes " + new String(ivBytes));
			
			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			PBEKeySpec spec = new PBEKeySpec(SECRET_KEY.toCharArray(), saltBytes, pswdIterations, keySize);
			
			SecretKey secretKey = factory.generateSecret(spec);
			SecretKeySpec secret = new SecretKeySpec(secretKey.getEncoded(), "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, secret, new IvParameterSpec(new Base64().decodeBase64(ivBytes)));
			encryptedTextBytes = cipher.doFinal(plainText.getBytes("UTF-8"));
			System.out.println("Encrypted Text = " + new String(Base64.encodeBase64(encryptedTextBytes)));
		} catch(InvalidKeyException e){
			e.printStackTrace();
		}catch(UnsupportedEncodingException e){
			e.printStackTrace();
		}catch(NoSuchAlgorithmException e){
			e.printStackTrace();
		}catch(InvalidKeySpecException e){
			e.printStackTrace();
		}catch(NoSuchPaddingException e){
			e.printStackTrace();
		}catch(InvalidAlgorithmParameterException e){
			e.printStackTrace();
		}catch(IllegalBlockSizeException e){
			e.printStackTrace();
		}catch(BadPaddingException e){
			e.printStackTrace();
		}
		
		return new String(Base64.encodeBase64(encryptedTextBytes));
	}

	public static String decrypt(String plainText) {
		
		System.out.println("Staring decryption");
		byte[] decryptedTextBytes = null;
		try{
			byte[] saltBytes = SALT.getBytes("UTF-8");
	//		System.out.println("saltBytes " + new String(saltBytes));
			byte[] ivBytes = initializationVector.getBytes("UTF-8");
	//		System.out.println("ivBytes " + new String(ivBytes));
			byte[] encryptedTextBytes = new Base64().decodeBase64(plainText.getBytes("UTF-8"));
			
			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			PBEKeySpec spec = new PBEKeySpec(SECRET_KEY.toCharArray(), saltBytes, pswdIterations, keySize);
			
			SecretKey secretKey = factory.generateSecret(spec);
			SecretKeySpec secret = new SecretKeySpec(secretKey.getEncoded(), "AES");
			
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		    cipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(new Base64().decodeBase64(ivBytes)));

			decryptedTextBytes = cipher.doFinal(encryptedTextBytes);
		    System.out.println("Decrypted Text = " + new String(decryptedTextBytes));
		} catch(InvalidKeyException e){
			e.printStackTrace();
		}catch(UnsupportedEncodingException e){
			e.printStackTrace();
		}catch(NoSuchAlgorithmException e){
			e.printStackTrace();
		}catch(InvalidKeySpecException e){
			e.printStackTrace();
		}catch(NoSuchPaddingException e){
			e.printStackTrace();
		}catch(InvalidAlgorithmParameterException e){
			e.printStackTrace();
		}catch(IllegalBlockSizeException e){
			e.printStackTrace();
		}catch(BadPaddingException e){
			e.printStackTrace();
		}
		
	    return new String(decryptedTextBytes);
	}


}
