package ph.com.ncs.vanilla.commons.utils.encryption.exceptions;

public class StrongEncryptionNotAvailableException extends Exception {
	public StrongEncryptionNotAvailableException(int keySize) {
		super(keySize + "-bit AES encryption is not available on this Java platform.");
	}
}
