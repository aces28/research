package ph.com.ncs.vanilla.commons.utils.encryption.exceptions;

public class InvalidAESStreamException extends Exception {
	public InvalidAESStreamException() { super(); };
	public InvalidAESStreamException(Exception e) { super(e); }
}
