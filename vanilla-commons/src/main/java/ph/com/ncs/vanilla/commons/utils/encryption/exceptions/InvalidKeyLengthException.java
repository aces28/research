package ph.com.ncs.vanilla.commons.utils.encryption.exceptions;

public class InvalidKeyLengthException extends Exception {
	public InvalidKeyLengthException(int length) {
		super("Invalid AES key length: " + length);
	}
}
