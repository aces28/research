package ph.com.ncs.vanilla.commons.utils;


import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by ace on 5/29/16.
 */
public class VanillaDateUtils {

    private static final String FORMAT_DATE_MMDDYYYY = "MM/dd/yyyy";
    private static final String FORMAT_SQL_MMDDYYYY = "MM/dd/yyyy";

    private static final String FORMAT_FROM_MMDDYYYY = "MM/dd/yyyy";
    private static final String FORMAT_TO_MMDDYYYY = "MM/dd/yyyy";
    
    private static final String FORMAT_MMDDYYYY = "MMddyyyy";
    
    private VanillaDateUtils() {
    }

    /**
     * @return long
     */
    public static long  longCurrentDate() {
        return (new Date()).getTime();
    }

    /**
     * @param dateVal
     * @return
     */
    public static long longDateFormat(final String dateVal) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(FORMAT_DATE_MMDDYYYY);
        try {
            return dateFormat.parse(dateVal).getTime();
        } catch (ParseException e) {
            return 0L;
        }
    }
    
    /**
     * @param dateVal
     * @return
     */
    public static long formatToDate(final String dateVal) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(FORMAT_MMDDYYYY);
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateFormat.parse(dateVal));
            calendar.set(Calendar.HOUR, 23);
            calendar.set(Calendar.MINUTE, 59);
            return calendar.getTime().getTime();
        } catch (ParseException e) {
            return 0L;
        }
    }
    
    /**
     * @param dateVal
     * @return
     */
    public static long formatFromDate(final String dateVal) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(FORMAT_MMDDYYYY);
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateFormat.parse(dateVal));
            calendar.set(Calendar.HOUR, 00);
            calendar.set(Calendar.MINUTE, 00);
            return calendar.getTime().getTime();
        } catch (ParseException e) {
            return 0L;
        }
    }

    /**
     * @param dateVal
     * @return
     */
    public static long longSqlFormat(final String dateVal) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(FORMAT_SQL_MMDDYYYY);
        try {
            return dateFormat.parse(dateVal).getTime();
        } catch (ParseException e) {
            return 0L;
        }
    }

    /**
     * @param date
     * @return
     */
    public static String formatDate(Date date) {
        return new SimpleDateFormat(FORMAT_SQL_MMDDYYYY).format(date);
    }

    /**
     * @param dateVal
     * @return
     */
    public static long longDateFromFormat(final String dateVal) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(FORMAT_FROM_MMDDYYYY);
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateFormat.parse(dateVal));
            calendar.set(Calendar.HOUR, 00);
            calendar.set(Calendar.MINUTE, 00);
            return calendar.getTime().getTime();
        } catch (ParseException e) {
            return 0L;
        }
    }

    /**
     * @param dateVal
     * @return
     */
    public static long longDateToFormat(final String dateVal) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(FORMAT_TO_MMDDYYYY);
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateFormat.parse(dateVal));
            calendar.set(Calendar.HOUR, 23);
            calendar.set(Calendar.MINUTE, 59);
            return calendar.getTime().getTime();
        } catch (ParseException e) {
            return 0L;
        }
    }

    public static String formatTimestamp(Timestamp timestamp) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(FORMAT_SQL_MMDDYYYY);
        Date date = new Date(timestamp.getTime());
        return dateFormat.format(date);
    }

}
