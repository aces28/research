package ph.com.ncs.vanilla.commons.utils;

/**
 * Created by ace on 6/14/16.
 */
public class PhoneUtils {

    private PhoneUtils() {
    }

    /**
     * Format number to ESB
     *
     * @param msisdn
     * @return {@link String}
     */
    public static String esbPhoneNumber(final String msisdn) {
        if (msisdn.startsWith("0")) {
            return msisdn.substring(1, 11);
        }
        return msisdn;
    }

    /**
     * Format number to international format
     *
     * @param msisdn
     * @return {@link String}
     */
    public static String internationalFormat(final String msisdn) {
        StringBuilder builder = new StringBuilder("63");
        if (msisdn.startsWith("0")) {
            builder.append(msisdn.substring(1, 11));
        } else if (msisdn.length() == 10) {
            builder.append(msisdn);
        }
        return builder.toString();
    }
}
