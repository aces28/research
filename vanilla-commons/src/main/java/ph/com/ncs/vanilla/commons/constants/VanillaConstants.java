package ph.com.ncs.vanilla.commons.constants;

/**
 * Created by ace on 6/11/16.
 */
public interface VanillaConstants {


    /**
     * Default user for all transactions.
     */
    String SYSTEM_USER = System.getProperty("user.name", "VNLADM");
}
