/**
 * SyncTransactionClientService.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:08:57 BST)
 */
package ph.com.ncs.vanilla.nf.ws.transaction.stubs;


/*
 *  SyncTransactionClientService java interface
 */
public interface SyncTransactionClientService {
    /**
     * Auto generated method signature
     *
     * @param execute
     */
    public ph.com.metr.bus.soap.client.ExecuteResponse execute(ph.com.metr.bus.soap.client.Execute execute) throws java.rmi.RemoteException;

    //
}
