/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:09:26 BST)
 */
package ph.com.metr.bus.soap.client;


/**
 *  ExtensionMapper class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class ExtensionMapper {
    public static java.lang.Object getTypeObject(
        java.lang.String namespaceURI, java.lang.String typeName,
        javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
        if ("http://bus.metr.com.ph/soap/objects/".equals(namespaceURI) &&
                "KeyValuePairType".equals(typeName)) {
            return ph.com.metr.bus.soap.objects.KeyValuePairType.Factory.parse(reader);
        }

        if ("http://bus.metr.com.ph/soap/objects/".equals(namespaceURI) &&
                "TransactionRequestType".equals(typeName)) {
            return ph.com.metr.bus.soap.objects.TransactionRequestType.Factory.parse(reader);
        }

        if ("http://bus.metr.com.ph/soap/objects/".equals(namespaceURI) &&
                "CredentialType".equals(typeName)) {
            return ph.com.metr.bus.soap.objects.CredentialType.Factory.parse(reader);
        }

        if ("http://bus.metr.com.ph/soap/client".equals(namespaceURI) &&
                "ExecuteResponseType".equals(typeName)) {
            return ph.com.metr.bus.soap.client.ExecuteResponseType.Factory.parse(reader);
        }

        if ("http://bus.metr.com.ph/soap/objects/".equals(namespaceURI) &&
                "TransactionResponseType".equals(typeName)) {
            return ph.com.metr.bus.soap.objects.TransactionResponseType.Factory.parse(reader);
        }

        if ("http://bus.metr.com.ph/soap/objects/".equals(namespaceURI) &&
                "ParamListType".equals(typeName)) {
            return ph.com.metr.bus.soap.objects.ParamListType.Factory.parse(reader);
        }

        if ("http://bus.metr.com.ph/soap/client".equals(namespaceURI) &&
                "ExecuteRequestType".equals(typeName)) {
            return ph.com.metr.bus.soap.client.ExecuteRequestType.Factory.parse(reader);
        }

        throw new org.apache.axis2.databinding.ADBException("Unsupported type " +
            namespaceURI + " " + typeName);
    }
}
