package ph.com.ncs.vanilla.base.client;

/**
 * Created by ace on 5/25/16.
 */
public interface BaseClient {

    /**
     * Return web service client
     * <p>
     * Client class type.
     *
     * @return T
     */
    <T> T getClient(Class<T> classType) throws IllegalAccessException, InstantiationException;

}
