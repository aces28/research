package ph.com.ncs.vanilla.base.form;


import ph.com.ncs.vanilla.base.listener.BaseListener;

/**
 * Created by ace on 5/29/16.
 */
public abstract class BaseForm<S> {

    public abstract <T extends BaseListener> void setListener(T t);

    public <T> T handleRequest(S s) throws FormException {
        return (T) s;
    }
}
