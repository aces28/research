package ph.com.ncs.vanilla.base.service;

/**
 * Created by ace on 5/27/16.
 */
public interface BaseClientService<S> {

    /**
     * Retrieve result from web service.
     *
     * @param s Class type to be return
     * @return T
     */
    <T> T getResult(S s) throws Exception;

    /**
     * Error message to be displayed for LTP users.
     */
    String LTP_ERROR_MESSAGE = "error.ltp.message";

    /**
     * Error message to be displayed for Postpaid users.
     */
    String POSTPAID_ERROR_MESSAGE = "error.postpaid.message";

    /**
     * Error message to be displayed for Postpaid users.
     */
    String PREPAID_ERROR_MESSAGE = "error.prepaid.message";

    /***
     * Error message to be displayed for Postpaid users.
     */
    String NFBUS_ERROR_MESSAGE = "error.nfbus.message";

    /***
     * Error message to be displayed for Postpaid users.
     */
    String ESB_TIMEOUT_MESSAGE = "error.esb.timeout";

    /***
     * Error message to be displayed for Postpaid users.
     */
    String NFBUS_TIMEOUT_MESSAGE = "error.nfbus.timeout";

    /***
     * Error message to be displayed for Postpaid users.
     */
    String RAVEN_TIMEOUT_MESSAGE = "error.raven.timeout";

    /**
     * LTP resource type
     */
    String PRIMARY_RESOURCE_TYPE = "esb.primary.resource.type";

    /**
     * ESB WSDL URL
     */
    String ESB_WSDL_URL = "esb.wsdl.url";

    /**
     * ESB Generic error message.
     */
    String GENERIC_ESB_ERROR = "error.generic.message";

    String ALLOW_GHP_PREPAID = "esb.allow.ghp.prepaid";

    String ESB_GHP_PREPAID = "GHP-Prepaid";

    String ESB_TM_PREPAID = "TM";

    String ENABLE_BRANDING = "enabled";

    String DISABLE_BRANDING = "disabled";
    
    /**
     * Pending application Error Messages
     */
    
    String REASSIGN_ROLE_ERROR_MESSAGE = "error.user.reassign.role.message";
    
    String REASSIGN_GROUP_ERROR_MESSAGE = "error.user.reassign.group.message";
    
    String REASSIGN_APPROVER_ERROR_MESSAGE = "error.application.reassign.approver.message";
    
    String CHANGE_LOCKED_STATUS_ERROR_MESSAGE = "error.change.locked.status.message";
    
    String UPDATE_GROUP_MEMBERS_ERROR_MESSAGE = "error.update.group.members.message";
}
