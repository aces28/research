package ph.com.ncs.vanilla.base.service;

import ph.com.ncs.vanilla.base.domain.Model;

/**
 * Created by ace on 5/19/16.
 */
public interface BaseService {

    /**
     * Generic object to save data
     *
     * @param data
     */
    <T extends Model> T save(T data);


    /**
     * Generic object to get data
     *
     * @param data
     */
    <T extends Model> T get(T data);

    /**
     * Generic object to get data
     *
     * @param iterable
     */
    <T> T save(Iterable iterable);

}
