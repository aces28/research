package ph.com.ncs.vanilla.base.listener;

/**
 * Created by ace on 5/29/16.
 */
public interface BaseListener<T> {

    /**
     * Asynchronous insert for transactions.
     *
     * @param t
     */
    void onStatusComplete(T t);

}
