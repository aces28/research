package ph.com.ncs.vanilla.base.client;

/**
 * Created by ace on 5/25/16.
 */
public abstract class VanillaDefaultClient implements BaseClient {

    /**
     * Create instance of client
     *
     * @param classType
     * @return T
     */
    public <T> T getClient(Class<T> classType) throws IllegalAccessException, InstantiationException {
        return classType.newInstance();
    }

}
