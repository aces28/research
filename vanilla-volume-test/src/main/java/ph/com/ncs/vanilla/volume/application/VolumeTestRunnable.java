package ph.com.ncs.vanilla.volume.application;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.util.JSONPObject;

/**
 * Created by jomerp on 8/15/2016.
 */
public class VolumeTestRunnable implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(VolumeTestRunnable.class);

    private String msisdn;
    private String domain;
    private String bday;
    private String fname;
    private String mname;
    private String lname;
    private String gender;
    private String email;
    private String address;
    private String momname;
    private String type;
    private String attachment;
    private String agree;
    private String resourceurl;
    

    public VolumeTestRunnable(String msisdn, String domain, String bday, String fname, String mname, String lname,
    							String gender,String email, String address, String momname,String type, String attachment,
    							String agree,String resourceurl) {
        this.msisdn = msisdn;
        this.domain = domain;
        this.bday = bday;
        this.fname = fname;
        this.mname = mname;
        this.lname = lname;
        this.gender = gender;
        this.email = email;
        this.address = address;
        this.momname = momname;
        this.type = type;
        this.attachment = attachment;
        this.agree = agree;
        this.resourceurl = resourceurl;
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    public void run() {
        // String url = domain + "/register/verify/0" + msisdn;
    	// Portal Step 3
        String url = domain + "/subscriber/application";
        logger.info("Create URL : " + url);

        try {
            URI uri = new URI(url);
            
            // Portal Step 3
            Map<String, Object> map = new HashMap<String, Object>();;
            map.put("mobile", msisdn);
            map.put("birthday", bday);
            map.put("firstName", fname);
            map.put("middleName", mname);
            map.put("lastName", lname);
            map.put("gender", gender);
            map.put("emailAdd", email);
            map.put("homeAddress", address);
            map.put("motherName", momname);
            map.put("type", type);
            map.put("attachment", attachment);
            map.put("agree", agree);
            map.put("resourceUrl", resourceurl);
            
            RestTemplate restTemplate = new RestTemplate();
            Object response = restTemplate.postForObject(uri, map, String.class);
            //Object response = restTemplate.postForObject(uri, null, String.class);
            logger.info("Get object instance : " + (new JSONPObject("", response)).getValue());

            // XXX ModifyF necessary value if needed.
            Thread.sleep(0);
        } catch (InterruptedException | URISyntaxException e) {
            logger.error("Unable to send request : ", e);
        }
    }
}