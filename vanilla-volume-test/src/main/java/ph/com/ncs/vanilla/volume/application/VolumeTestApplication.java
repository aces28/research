package ph.com.ncs.vanilla.volume.application;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.test.ImportAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;

/**
 * Created by jomerp on 8/15/2016.
 */
@SpringBootApplication
public class VolumeTestApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(VolumeTestApplication.class).web(false).run(args);
    }
}
