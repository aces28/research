package ph.com.ncs.vanilla.volume.application;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

/**
 * Created by jomerp on 8/15/2016.
 */
@Configuration
@Component
@PropertySource(value = "classpath:application-volume.properties", ignoreResourceNotFound = true)
public class VanillaCommandRunner implements VolumeTestService {

    private static final Logger logger = LoggerFactory.getLogger(VanillaCommandRunner.class);

    @Autowired
    private ResourceLoader resourceLoader;
    @Autowired
    private Environment environment;

    public void run(String... strings) throws Exception {
        logger.info("environment : threads : " + environment.getRequiredProperty(VOLUME_TEST_THREADS) + " : file : " + environment.getRequiredProperty(VOLUME_TEST_PAYLOAD));

        logger.info("File : " + System.getProperty("user.dir") + "/" + environment.getRequiredProperty(VOLUME_TEST_PAYLOAD));
        Resource resource = resourceLoader.getResource("file:///" + System.getProperty("user.dir") + "/" + environment.getRequiredProperty(VOLUME_TEST_PAYLOAD));
        BufferedReader bufferedReader = null;

        ExecutorService executorService = Executors.newFixedThreadPool(Integer.valueOf(environment.getRequiredProperty(VOLUME_TEST_THREADS)));
        try (InputStream inputStream = resource.getInputStream()) {
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String readLine;
            while ((readLine = bufferedReader.readLine()) != null) {
            	String [] data = readLine.split(",");
            	VolumeTestRunnable runnable = new VolumeTestRunnable(data[0], environment.getRequiredProperty(VOLUME_TEST_TARGET),
            			data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8], data[9], data[10], data[11], data[12]);
                executorService.execute(runnable);
                System.gc();
            	/* 
            	VolumeTestRunnable runnable = new VolumeTestRunnable(readLine, environment.getRequiredProperty(VOLUME_TEST_TARGET));
                executorService.execute(runnable);
                System.gc();
                */
            }
            executorService.shutdown();
        } catch (IOException e) {
            logger.error("Unable to read file : ", e);
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                logger.error("Unable to close reader : ", e);
            }
        }
    }
}