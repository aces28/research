package ph.com.ncs.vanilla.volume.application;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * Created by jomerp on 8/15/2016.
 */
@Configuration
public class VolumeTestConfig {

    private static final Logger logger = LoggerFactory.getLogger(VolumeTestConfig.class);

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
        logger.info("load configuration : START");
        return new PropertySourcesPlaceholderConfigurer();
    }
}
