package ph.com.ncs.vanilla.volume.application;

import org.springframework.boot.CommandLineRunner;

/**
 * Created by jomerp on 8/15/2016.
 */
public interface VolumeTestService extends CommandLineRunner {

    /**
     * Number of thread to be used
     */
    String VOLUME_TEST_THREADS = "volume.test.threads";

    /**
     * File containing the msisdn
     */
    String VOLUME_TEST_PAYLOAD = "volume.test.payload";


    /**
     * Target URL for sending request
     */
    String VOLUME_TEST_TARGET = "volume.test.target";
}
