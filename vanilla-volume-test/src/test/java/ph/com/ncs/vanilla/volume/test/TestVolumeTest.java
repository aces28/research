package ph.com.ncs.vanilla.volume.test;

import com.fasterxml.jackson.databind.util.JSONPObject;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by jomerp on 8/12/2016.
 */
public class TestVolumeTest {

    private static final Logger logger = LoggerFactory.getLogger(TestVolumeTest.class);

    @Test
    public void testRestRequest() throws URISyntaxException {

        ExecutorService executorService = Executors.newFixedThreadPool(10);

        while (true) {

            Runnable runnable = new Runnable() {
                long number = 9063241954L;

                public void run() {
                    URI uri = null;
                    try {
                        ++number;
                        String url = "http://192.168.23.169:8080/register/verify/0" + number;
                        uri = new URI(url);
                    } catch (URISyntaxException e) {
                        logger.error("Unable to send request : ", e);
                    }
                    RestTemplate restTemplate = new RestTemplate();
                    Object response = restTemplate.postForObject(uri, null, String.class);
                    logger.info("Get object instance : " + (new JSONPObject("", response)).getValue());
                }
            };

            executorService.execute(runnable);
        }
    }

}
