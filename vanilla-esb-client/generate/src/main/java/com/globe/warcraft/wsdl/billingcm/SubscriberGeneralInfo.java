
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SubscriberGeneralInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubscriberGeneralInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DealerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActWaiveRsnCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrimResourceTp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrimResourceVal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreditClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreditLimit" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="CalculatePaymentCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Language" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LinkPrevSubNo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="LinkNextSubNo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="L9DOB" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubscriberGeneralInfo", propOrder = {
    "effectiveDate",
    "dealerCode",
    "actWaiveRsnCd",
    "primResourceTp",
    "primResourceVal",
    "subPassword",
    "creditClass",
    "creditLimit",
    "calculatePaymentCategory",
    "language",
    "linkPrevSubNo",
    "linkNextSubNo",
    "l9DOB"
})
public class SubscriberGeneralInfo {

    @XmlElement(name = "EffectiveDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effectiveDate;
    @XmlElement(name = "DealerCode")
    protected String dealerCode;
    @XmlElement(name = "ActWaiveRsnCd")
    protected String actWaiveRsnCd;
    @XmlElement(name = "PrimResourceTp")
    protected String primResourceTp;
    @XmlElement(name = "PrimResourceVal")
    protected String primResourceVal;
    @XmlElement(name = "SubPassword")
    protected String subPassword;
    @XmlElement(name = "CreditClass")
    protected String creditClass;
    @XmlElement(name = "CreditLimit")
    protected Double creditLimit;
    @XmlElement(name = "CalculatePaymentCategory")
    protected String calculatePaymentCategory;
    @XmlElement(name = "Language")
    protected String language;
    @XmlElement(name = "LinkPrevSubNo")
    protected int linkPrevSubNo;
    @XmlElement(name = "LinkNextSubNo")
    protected int linkNextSubNo;
    @XmlElement(name = "L9DOB")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar l9DOB;

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDate(XMLGregorianCalendar value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the dealerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerCode() {
        return dealerCode;
    }

    /**
     * Sets the value of the dealerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerCode(String value) {
        this.dealerCode = value;
    }

    /**
     * Gets the value of the actWaiveRsnCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActWaiveRsnCd() {
        return actWaiveRsnCd;
    }

    /**
     * Sets the value of the actWaiveRsnCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActWaiveRsnCd(String value) {
        this.actWaiveRsnCd = value;
    }

    /**
     * Gets the value of the primResourceTp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimResourceTp() {
        return primResourceTp;
    }

    /**
     * Sets the value of the primResourceTp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimResourceTp(String value) {
        this.primResourceTp = value;
    }

    /**
     * Gets the value of the primResourceVal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimResourceVal() {
        return primResourceVal;
    }

    /**
     * Sets the value of the primResourceVal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimResourceVal(String value) {
        this.primResourceVal = value;
    }

    /**
     * Gets the value of the subPassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubPassword() {
        return subPassword;
    }

    /**
     * Sets the value of the subPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubPassword(String value) {
        this.subPassword = value;
    }

    /**
     * Gets the value of the creditClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditClass() {
        return creditClass;
    }

    /**
     * Sets the value of the creditClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditClass(String value) {
        this.creditClass = value;
    }

    /**
     * Gets the value of the creditLimit property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getCreditLimit() {
        return creditLimit;
    }

    /**
     * Sets the value of the creditLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setCreditLimit(Double value) {
        this.creditLimit = value;
    }

    /**
     * Gets the value of the calculatePaymentCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalculatePaymentCategory() {
        return calculatePaymentCategory;
    }

    /**
     * Sets the value of the calculatePaymentCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalculatePaymentCategory(String value) {
        this.calculatePaymentCategory = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Gets the value of the linkPrevSubNo property.
     * 
     */
    public int getLinkPrevSubNo() {
        return linkPrevSubNo;
    }

    /**
     * Sets the value of the linkPrevSubNo property.
     * 
     */
    public void setLinkPrevSubNo(int value) {
        this.linkPrevSubNo = value;
    }

    /**
     * Gets the value of the linkNextSubNo property.
     * 
     */
    public int getLinkNextSubNo() {
        return linkNextSubNo;
    }

    /**
     * Sets the value of the linkNextSubNo property.
     * 
     */
    public void setLinkNextSubNo(int value) {
        this.linkNextSubNo = value;
    }

    /**
     * Gets the value of the l9DOB property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getL9DOB() {
        return l9DOB;
    }

    /**
     * Sets the value of the l9DOB property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setL9DOB(XMLGregorianCalendar value) {
        this.l9DOB = value;
    }

}
