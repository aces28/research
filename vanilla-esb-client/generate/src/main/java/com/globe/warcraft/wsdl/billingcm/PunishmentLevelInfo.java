
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PunishmentLevelInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PunishmentLevelInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PunishmentLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PunishmentLevelInfo", propOrder = {
    "punishmentLevel"
})
public class PunishmentLevelInfo {

    @XmlElement(name = "PunishmentLevel")
    protected String punishmentLevel;

    /**
     * Gets the value of the punishmentLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPunishmentLevel() {
        return punishmentLevel;
    }

    /**
     * Sets the value of the punishmentLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPunishmentLevel(String value) {
        this.punishmentLevel = value;
    }

}
