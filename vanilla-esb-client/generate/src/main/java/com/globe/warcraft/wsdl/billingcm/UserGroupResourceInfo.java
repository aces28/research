
package com.globe.warcraft.wsdl.billingcm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UserGroupResourceInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UserGroupResourceInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UserGroupId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ResourceNameInfoList" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ResourceNameInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}ResourceNameInfo" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ActivityId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserGroupResourceInfo", propOrder = {
    "userGroupId",
    "resourceNameInfoList",
    "activityId"
})
public class UserGroupResourceInfo {

    @XmlElement(name = "UserGroupId")
    protected Integer userGroupId;
    @XmlElement(name = "ResourceNameInfoList")
    protected UserGroupResourceInfo.ResourceNameInfoList resourceNameInfoList;
    @XmlElement(name = "ActivityId")
    protected Integer activityId;

    /**
     * Gets the value of the userGroupId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getUserGroupId() {
        return userGroupId;
    }

    /**
     * Sets the value of the userGroupId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setUserGroupId(Integer value) {
        this.userGroupId = value;
    }

    /**
     * Gets the value of the resourceNameInfoList property.
     * 
     * @return
     *     possible object is
     *     {@link UserGroupResourceInfo.ResourceNameInfoList }
     *     
     */
    public UserGroupResourceInfo.ResourceNameInfoList getResourceNameInfoList() {
        return resourceNameInfoList;
    }

    /**
     * Sets the value of the resourceNameInfoList property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserGroupResourceInfo.ResourceNameInfoList }
     *     
     */
    public void setResourceNameInfoList(UserGroupResourceInfo.ResourceNameInfoList value) {
        this.resourceNameInfoList = value;
    }

    /**
     * Gets the value of the activityId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getActivityId() {
        return activityId;
    }

    /**
     * Sets the value of the activityId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setActivityId(Integer value) {
        this.activityId = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ResourceNameInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}ResourceNameInfo" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "resourceNameInfo"
    })
    public static class ResourceNameInfoList {

        @XmlElement(name = "ResourceNameInfo", required = true)
        protected List<ResourceNameInfo> resourceNameInfo;

        /**
         * Gets the value of the resourceNameInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the resourceNameInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getResourceNameInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ResourceNameInfo }
         * 
         * 
         */
        public List<ResourceNameInfo> getResourceNameInfo() {
            if (resourceNameInfo == null) {
                resourceNameInfo = new ArrayList<ResourceNameInfo>();
            }
            return this.resourceNameInfo;
        }

    }

}
