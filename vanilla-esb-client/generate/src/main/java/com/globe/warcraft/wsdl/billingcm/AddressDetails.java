
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddressDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddressDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AddressType">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="LinkType">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AddressElement1">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AddressElement2">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AddressElement3">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AddressElement4">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AddressElement5">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AddressElement6">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AddressElement7">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AddressElement8">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AddressElement9" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AddressElement10">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AddressElement11">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AddressElement12" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AddressElement13" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AddressElement14" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AddressElement15" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddressDetails", propOrder = {
    "addressType",
    "linkType",
    "addressElement1",
    "addressElement2",
    "addressElement3",
    "addressElement4",
    "addressElement5",
    "addressElement6",
    "addressElement7",
    "addressElement8",
    "addressElement9",
    "addressElement10",
    "addressElement11",
    "addressElement12",
    "addressElement13",
    "addressElement14",
    "addressElement15"
})
public class AddressDetails {

    @XmlElement(name = "AddressType", required = true)
    protected String addressType;
    @XmlElement(name = "LinkType", required = true)
    protected String linkType;
    @XmlElement(name = "AddressElement1", required = true)
    protected String addressElement1;
    @XmlElement(name = "AddressElement2", required = true)
    protected String addressElement2;
    @XmlElement(name = "AddressElement3", required = true)
    protected String addressElement3;
    @XmlElement(name = "AddressElement4", required = true)
    protected String addressElement4;
    @XmlElement(name = "AddressElement5", required = true)
    protected String addressElement5;
    @XmlElement(name = "AddressElement6", required = true)
    protected String addressElement6;
    @XmlElement(name = "AddressElement7", required = true)
    protected String addressElement7;
    @XmlElement(name = "AddressElement8", required = true)
    protected String addressElement8;
    @XmlElement(name = "AddressElement9")
    protected String addressElement9;
    @XmlElement(name = "AddressElement10", required = true)
    protected String addressElement10;
    @XmlElement(name = "AddressElement11", required = true)
    protected String addressElement11;
    @XmlElement(name = "AddressElement12")
    protected String addressElement12;
    @XmlElement(name = "AddressElement13")
    protected String addressElement13;
    @XmlElement(name = "AddressElement14")
    protected String addressElement14;
    @XmlElement(name = "AddressElement15")
    protected String addressElement15;

    /**
     * Gets the value of the addressType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressType() {
        return addressType;
    }

    /**
     * Sets the value of the addressType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressType(String value) {
        this.addressType = value;
    }

    /**
     * Gets the value of the linkType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLinkType() {
        return linkType;
    }

    /**
     * Sets the value of the linkType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLinkType(String value) {
        this.linkType = value;
    }

    /**
     * Gets the value of the addressElement1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement1() {
        return addressElement1;
    }

    /**
     * Sets the value of the addressElement1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement1(String value) {
        this.addressElement1 = value;
    }

    /**
     * Gets the value of the addressElement2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement2() {
        return addressElement2;
    }

    /**
     * Sets the value of the addressElement2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement2(String value) {
        this.addressElement2 = value;
    }

    /**
     * Gets the value of the addressElement3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement3() {
        return addressElement3;
    }

    /**
     * Sets the value of the addressElement3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement3(String value) {
        this.addressElement3 = value;
    }

    /**
     * Gets the value of the addressElement4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement4() {
        return addressElement4;
    }

    /**
     * Sets the value of the addressElement4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement4(String value) {
        this.addressElement4 = value;
    }

    /**
     * Gets the value of the addressElement5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement5() {
        return addressElement5;
    }

    /**
     * Sets the value of the addressElement5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement5(String value) {
        this.addressElement5 = value;
    }

    /**
     * Gets the value of the addressElement6 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement6() {
        return addressElement6;
    }

    /**
     * Sets the value of the addressElement6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement6(String value) {
        this.addressElement6 = value;
    }

    /**
     * Gets the value of the addressElement7 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement7() {
        return addressElement7;
    }

    /**
     * Sets the value of the addressElement7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement7(String value) {
        this.addressElement7 = value;
    }

    /**
     * Gets the value of the addressElement8 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement8() {
        return addressElement8;
    }

    /**
     * Sets the value of the addressElement8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement8(String value) {
        this.addressElement8 = value;
    }

    /**
     * Gets the value of the addressElement9 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement9() {
        return addressElement9;
    }

    /**
     * Sets the value of the addressElement9 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement9(String value) {
        this.addressElement9 = value;
    }

    /**
     * Gets the value of the addressElement10 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement10() {
        return addressElement10;
    }

    /**
     * Sets the value of the addressElement10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement10(String value) {
        this.addressElement10 = value;
    }

    /**
     * Gets the value of the addressElement11 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement11() {
        return addressElement11;
    }

    /**
     * Sets the value of the addressElement11 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement11(String value) {
        this.addressElement11 = value;
    }

    /**
     * Gets the value of the addressElement12 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement12() {
        return addressElement12;
    }

    /**
     * Sets the value of the addressElement12 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement12(String value) {
        this.addressElement12 = value;
    }

    /**
     * Gets the value of the addressElement13 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement13() {
        return addressElement13;
    }

    /**
     * Sets the value of the addressElement13 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement13(String value) {
        this.addressElement13 = value;
    }

    /**
     * Gets the value of the addressElement14 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement14() {
        return addressElement14;
    }

    /**
     * Sets the value of the addressElement14 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement14(String value) {
        this.addressElement14 = value;
    }

    /**
     * Gets the value of the addressElement15 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement15() {
        return addressElement15;
    }

    /**
     * Sets the value of the addressElement15 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement15(String value) {
        this.addressElement15 = value;
    }

}
