
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetNextDsaCustomerResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetNextDsaCustomerResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetNextDsaCustomerResult" type="{http://www.globe.com/warcraft/wsdl/billingcm/}GetNextDsaCustomerResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetNextDsaCustomerResponse", propOrder = {
    "getNextDsaCustomerResult"
})
public class GetNextDsaCustomerResponse {

    @XmlElement(name = "GetNextDsaCustomerResult")
    protected GetNextDsaCustomerResult getNextDsaCustomerResult;

    /**
     * Gets the value of the getNextDsaCustomerResult property.
     * 
     * @return
     *     possible object is
     *     {@link GetNextDsaCustomerResult }
     *     
     */
    public GetNextDsaCustomerResult getGetNextDsaCustomerResult() {
        return getNextDsaCustomerResult;
    }

    /**
     * Sets the value of the getNextDsaCustomerResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetNextDsaCustomerResult }
     *     
     */
    public void setGetNextDsaCustomerResult(GetNextDsaCustomerResult value) {
        this.getNextDsaCustomerResult = value;
    }

}
