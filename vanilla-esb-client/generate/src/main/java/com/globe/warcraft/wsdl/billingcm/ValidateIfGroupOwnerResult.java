
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ValidateIfGroupOwnerResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValidateIfGroupOwnerResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResultCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ResultText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserGroupInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}UserGroupInfo" minOccurs="0"/>
 *         &lt;element name="ResultNamespace" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidateIfGroupOwnerResult", propOrder = {
    "resultCode",
    "resultText",
    "userGroupInfo",
    "resultNamespace"
})
public class ValidateIfGroupOwnerResult {

    @XmlElement(name = "ResultCode")
    protected Integer resultCode;
    @XmlElement(name = "ResultText")
    protected String resultText;
    @XmlElement(name = "UserGroupInfo")
    protected UserGroupInfo userGroupInfo;
    @XmlElement(name = "ResultNamespace")
    protected String resultNamespace;

    /**
     * Gets the value of the resultCode property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getResultCode() {
        return resultCode;
    }

    /**
     * Sets the value of the resultCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setResultCode(Integer value) {
        this.resultCode = value;
    }

    /**
     * Gets the value of the resultText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResultText() {
        return resultText;
    }

    /**
     * Sets the value of the resultText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResultText(String value) {
        this.resultText = value;
    }

    /**
     * Gets the value of the userGroupInfo property.
     * 
     * @return
     *     possible object is
     *     {@link UserGroupInfo }
     *     
     */
    public UserGroupInfo getUserGroupInfo() {
        return userGroupInfo;
    }

    /**
     * Sets the value of the userGroupInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserGroupInfo }
     *     
     */
    public void setUserGroupInfo(UserGroupInfo value) {
        this.userGroupInfo = value;
    }

    /**
     * Gets the value of the resultNamespace property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResultNamespace() {
        return resultNamespace;
    }

    /**
     * Sets the value of the resultNamespace property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResultNamespace(String value) {
        this.resultNamespace = value;
    }

}
