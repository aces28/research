
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetDetailsByMsisdnResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetDetailsByMsisdnResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubscriberHeader" type="{http://www.globe.com/warcraft/wsdl/billingcm/}SubscriberHeader" minOccurs="0"/>
 *         &lt;element name="UnifiedResourceDetails" type="{http://www.globe.com/warcraft/wsdl/billingcm/}UnifiedResourceDetailsInfo" minOccurs="0"/>
 *         &lt;element name="PayChannelHeader" type="{http://www.globe.com/warcraft/wsdl/billingcm/}PayChannelHeader" minOccurs="0"/>
 *         &lt;element name="BillingArrangementHeader" type="{http://www.globe.com/warcraft/wsdl/billingcm/}BillingArrangementHeader" minOccurs="0"/>
 *         &lt;element name="AccountHeader" type="{http://www.globe.com/warcraft/wsdl/billingcm/}AccountHeader" minOccurs="0"/>
 *         &lt;element name="CustomerHeader" type="{http://www.globe.com/warcraft/wsdl/billingcm/}CustomerHeader" minOccurs="0"/>
 *         &lt;element name="ResultNameSpace" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetDetailsByMsisdnResult", propOrder = {
    "subscriberHeader",
    "unifiedResourceDetails",
    "payChannelHeader",
    "billingArrangementHeader",
    "accountHeader",
    "customerHeader",
    "resultNameSpace"
})
public class GetDetailsByMsisdnResult {

    @XmlElement(name = "SubscriberHeader")
    protected SubscriberHeader subscriberHeader;
    @XmlElement(name = "UnifiedResourceDetails")
    protected UnifiedResourceDetailsInfo unifiedResourceDetails;
    @XmlElement(name = "PayChannelHeader")
    protected PayChannelHeader payChannelHeader;
    @XmlElement(name = "BillingArrangementHeader")
    protected BillingArrangementHeader billingArrangementHeader;
    @XmlElement(name = "AccountHeader")
    protected AccountHeader accountHeader;
    @XmlElement(name = "CustomerHeader")
    protected CustomerHeader customerHeader;
    @XmlElement(name = "ResultNameSpace")
    protected String resultNameSpace;

    /**
     * Gets the value of the subscriberHeader property.
     * 
     * @return
     *     possible object is
     *     {@link SubscriberHeader }
     *     
     */
    public SubscriberHeader getSubscriberHeader() {
        return subscriberHeader;
    }

    /**
     * Sets the value of the subscriberHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubscriberHeader }
     *     
     */
    public void setSubscriberHeader(SubscriberHeader value) {
        this.subscriberHeader = value;
    }

    /**
     * Gets the value of the unifiedResourceDetails property.
     * 
     * @return
     *     possible object is
     *     {@link UnifiedResourceDetailsInfo }
     *     
     */
    public UnifiedResourceDetailsInfo getUnifiedResourceDetails() {
        return unifiedResourceDetails;
    }

    /**
     * Sets the value of the unifiedResourceDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnifiedResourceDetailsInfo }
     *     
     */
    public void setUnifiedResourceDetails(UnifiedResourceDetailsInfo value) {
        this.unifiedResourceDetails = value;
    }

    /**
     * Gets the value of the payChannelHeader property.
     * 
     * @return
     *     possible object is
     *     {@link PayChannelHeader }
     *     
     */
    public PayChannelHeader getPayChannelHeader() {
        return payChannelHeader;
    }

    /**
     * Sets the value of the payChannelHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link PayChannelHeader }
     *     
     */
    public void setPayChannelHeader(PayChannelHeader value) {
        this.payChannelHeader = value;
    }

    /**
     * Gets the value of the billingArrangementHeader property.
     * 
     * @return
     *     possible object is
     *     {@link BillingArrangementHeader }
     *     
     */
    public BillingArrangementHeader getBillingArrangementHeader() {
        return billingArrangementHeader;
    }

    /**
     * Sets the value of the billingArrangementHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillingArrangementHeader }
     *     
     */
    public void setBillingArrangementHeader(BillingArrangementHeader value) {
        this.billingArrangementHeader = value;
    }

    /**
     * Gets the value of the accountHeader property.
     * 
     * @return
     *     possible object is
     *     {@link AccountHeader }
     *     
     */
    public AccountHeader getAccountHeader() {
        return accountHeader;
    }

    /**
     * Sets the value of the accountHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountHeader }
     *     
     */
    public void setAccountHeader(AccountHeader value) {
        this.accountHeader = value;
    }

    /**
     * Gets the value of the customerHeader property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerHeader }
     *     
     */
    public CustomerHeader getCustomerHeader() {
        return customerHeader;
    }

    /**
     * Sets the value of the customerHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerHeader }
     *     
     */
    public void setCustomerHeader(CustomerHeader value) {
        this.customerHeader = value;
    }

    /**
     * Gets the value of the resultNameSpace property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResultNameSpace() {
        return resultNameSpace;
    }

    /**
     * Sets the value of the resultNameSpace property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResultNameSpace(String value) {
        this.resultNameSpace = value;
    }

}
