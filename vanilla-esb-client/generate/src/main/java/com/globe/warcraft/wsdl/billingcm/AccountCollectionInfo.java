
package com.globe.warcraft.wsdl.billingcm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountCollectionInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountCollectionInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountCollectionDateInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}AccountCollectionDateInfo" minOccurs="0"/>
 *         &lt;element name="AccountCollectionFixInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}AccountCollectionFixInfo" minOccurs="0"/>
 *         &lt;element name="CollectionStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FullSuspensionIndicator" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="PunishmentLevels" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="PunishmentLevelInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}PunishmentLevelInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="SuspensionType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountCollectionInfo", propOrder = {
    "accountCollectionDateInfo",
    "accountCollectionFixInfo",
    "collectionStatus",
    "fullSuspensionIndicator",
    "punishmentLevels",
    "suspensionType"
})
public class AccountCollectionInfo {

    @XmlElement(name = "AccountCollectionDateInfo")
    protected AccountCollectionDateInfo accountCollectionDateInfo;
    @XmlElement(name = "AccountCollectionFixInfo")
    protected AccountCollectionFixInfo accountCollectionFixInfo;
    @XmlElement(name = "CollectionStatus")
    protected String collectionStatus;
    @XmlElement(name = "FullSuspensionIndicator")
    protected byte fullSuspensionIndicator;
    @XmlElement(name = "PunishmentLevels")
    protected AccountCollectionInfo.PunishmentLevels punishmentLevels;
    @XmlElement(name = "SuspensionType")
    protected String suspensionType;

    /**
     * Gets the value of the accountCollectionDateInfo property.
     * 
     * @return
     *     possible object is
     *     {@link AccountCollectionDateInfo }
     *     
     */
    public AccountCollectionDateInfo getAccountCollectionDateInfo() {
        return accountCollectionDateInfo;
    }

    /**
     * Sets the value of the accountCollectionDateInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountCollectionDateInfo }
     *     
     */
    public void setAccountCollectionDateInfo(AccountCollectionDateInfo value) {
        this.accountCollectionDateInfo = value;
    }

    /**
     * Gets the value of the accountCollectionFixInfo property.
     * 
     * @return
     *     possible object is
     *     {@link AccountCollectionFixInfo }
     *     
     */
    public AccountCollectionFixInfo getAccountCollectionFixInfo() {
        return accountCollectionFixInfo;
    }

    /**
     * Sets the value of the accountCollectionFixInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountCollectionFixInfo }
     *     
     */
    public void setAccountCollectionFixInfo(AccountCollectionFixInfo value) {
        this.accountCollectionFixInfo = value;
    }

    /**
     * Gets the value of the collectionStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCollectionStatus() {
        return collectionStatus;
    }

    /**
     * Sets the value of the collectionStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCollectionStatus(String value) {
        this.collectionStatus = value;
    }

    /**
     * Gets the value of the fullSuspensionIndicator property.
     * 
     */
    public byte getFullSuspensionIndicator() {
        return fullSuspensionIndicator;
    }

    /**
     * Sets the value of the fullSuspensionIndicator property.
     * 
     */
    public void setFullSuspensionIndicator(byte value) {
        this.fullSuspensionIndicator = value;
    }

    /**
     * Gets the value of the punishmentLevels property.
     * 
     * @return
     *     possible object is
     *     {@link AccountCollectionInfo.PunishmentLevels }
     *     
     */
    public AccountCollectionInfo.PunishmentLevels getPunishmentLevels() {
        return punishmentLevels;
    }

    /**
     * Sets the value of the punishmentLevels property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountCollectionInfo.PunishmentLevels }
     *     
     */
    public void setPunishmentLevels(AccountCollectionInfo.PunishmentLevels value) {
        this.punishmentLevels = value;
    }

    /**
     * Gets the value of the suspensionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSuspensionType() {
        return suspensionType;
    }

    /**
     * Sets the value of the suspensionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSuspensionType(String value) {
        this.suspensionType = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="PunishmentLevelInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}PunishmentLevelInfo" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "punishmentLevelInfo"
    })
    public static class PunishmentLevels {

        @XmlElement(name = "PunishmentLevelInfo")
        protected List<PunishmentLevelInfo> punishmentLevelInfo;

        /**
         * Gets the value of the punishmentLevelInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the punishmentLevelInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPunishmentLevelInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link PunishmentLevelInfo }
         * 
         * 
         */
        public List<PunishmentLevelInfo> getPunishmentLevelInfo() {
            if (punishmentLevelInfo == null) {
                punishmentLevelInfo = new ArrayList<PunishmentLevelInfo>();
            }
            return this.punishmentLevelInfo;
        }

    }

}
