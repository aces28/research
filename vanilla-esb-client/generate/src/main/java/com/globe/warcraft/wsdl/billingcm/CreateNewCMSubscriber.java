
package com.globe.warcraft.wsdl.billingcm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreateNewCMSubscriber complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreateNewCMSubscriber">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustomerId">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SubscriberType">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NameInfoList">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="NameInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}NameInfos" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AddressDetailsList">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AddressDetails" type="{http://www.globe.com/warcraft/wsdl/billingcm/}AddressDetails" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="SrvAgrInfoList">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SrvAgrInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}SrvAgrInfo" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="LogicalResourceInfoList">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="LogicalResourceInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}LogicalResourceInfoModel" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="PhysicalResourceInfoList">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="PhysicalResourceInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}PhysicalResourceInfoModel" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ParameterInfoList">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ParameterInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}ParameterInfo" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ActivityReason">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateNewCMSubscriber", propOrder = {
    "customerId",
    "subscriberType",
    "nameInfoList",
    "addressDetailsList",
    "srvAgrInfoList",
    "logicalResourceInfoList",
    "physicalResourceInfoList",
    "parameterInfoList",
    "activityReason"
})
public class CreateNewCMSubscriber {

    @XmlElement(name = "CustomerId")
    protected int customerId;
    @XmlElement(name = "SubscriberType", required = true)
    protected String subscriberType;
    @XmlElement(name = "NameInfoList", required = true)
    protected CreateNewCMSubscriber.NameInfoList nameInfoList;
    @XmlElement(name = "AddressDetailsList", required = true)
    protected CreateNewCMSubscriber.AddressDetailsList addressDetailsList;
    @XmlElement(name = "SrvAgrInfoList", required = true)
    protected CreateNewCMSubscriber.SrvAgrInfoList srvAgrInfoList;
    @XmlElement(name = "LogicalResourceInfoList", required = true)
    protected CreateNewCMSubscriber.LogicalResourceInfoList logicalResourceInfoList;
    @XmlElement(name = "PhysicalResourceInfoList", required = true)
    protected CreateNewCMSubscriber.PhysicalResourceInfoList physicalResourceInfoList;
    @XmlElement(name = "ParameterInfoList", required = true)
    protected CreateNewCMSubscriber.ParameterInfoList parameterInfoList;
    @XmlElement(name = "ActivityReason", required = true)
    protected String activityReason;

    /**
     * Gets the value of the customerId property.
     * 
     */
    public int getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     */
    public void setCustomerId(int value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the subscriberType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberType() {
        return subscriberType;
    }

    /**
     * Sets the value of the subscriberType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberType(String value) {
        this.subscriberType = value;
    }

    /**
     * Gets the value of the nameInfoList property.
     * 
     * @return
     *     possible object is
     *     {@link CreateNewCMSubscriber.NameInfoList }
     *     
     */
    public CreateNewCMSubscriber.NameInfoList getNameInfoList() {
        return nameInfoList;
    }

    /**
     * Sets the value of the nameInfoList property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateNewCMSubscriber.NameInfoList }
     *     
     */
    public void setNameInfoList(CreateNewCMSubscriber.NameInfoList value) {
        this.nameInfoList = value;
    }

    /**
     * Gets the value of the addressDetailsList property.
     * 
     * @return
     *     possible object is
     *     {@link CreateNewCMSubscriber.AddressDetailsList }
     *     
     */
    public CreateNewCMSubscriber.AddressDetailsList getAddressDetailsList() {
        return addressDetailsList;
    }

    /**
     * Sets the value of the addressDetailsList property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateNewCMSubscriber.AddressDetailsList }
     *     
     */
    public void setAddressDetailsList(CreateNewCMSubscriber.AddressDetailsList value) {
        this.addressDetailsList = value;
    }

    /**
     * Gets the value of the srvAgrInfoList property.
     * 
     * @return
     *     possible object is
     *     {@link CreateNewCMSubscriber.SrvAgrInfoList }
     *     
     */
    public CreateNewCMSubscriber.SrvAgrInfoList getSrvAgrInfoList() {
        return srvAgrInfoList;
    }

    /**
     * Sets the value of the srvAgrInfoList property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateNewCMSubscriber.SrvAgrInfoList }
     *     
     */
    public void setSrvAgrInfoList(CreateNewCMSubscriber.SrvAgrInfoList value) {
        this.srvAgrInfoList = value;
    }

    /**
     * Gets the value of the logicalResourceInfoList property.
     * 
     * @return
     *     possible object is
     *     {@link CreateNewCMSubscriber.LogicalResourceInfoList }
     *     
     */
    public CreateNewCMSubscriber.LogicalResourceInfoList getLogicalResourceInfoList() {
        return logicalResourceInfoList;
    }

    /**
     * Sets the value of the logicalResourceInfoList property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateNewCMSubscriber.LogicalResourceInfoList }
     *     
     */
    public void setLogicalResourceInfoList(CreateNewCMSubscriber.LogicalResourceInfoList value) {
        this.logicalResourceInfoList = value;
    }

    /**
     * Gets the value of the physicalResourceInfoList property.
     * 
     * @return
     *     possible object is
     *     {@link CreateNewCMSubscriber.PhysicalResourceInfoList }
     *     
     */
    public CreateNewCMSubscriber.PhysicalResourceInfoList getPhysicalResourceInfoList() {
        return physicalResourceInfoList;
    }

    /**
     * Sets the value of the physicalResourceInfoList property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateNewCMSubscriber.PhysicalResourceInfoList }
     *     
     */
    public void setPhysicalResourceInfoList(CreateNewCMSubscriber.PhysicalResourceInfoList value) {
        this.physicalResourceInfoList = value;
    }

    /**
     * Gets the value of the parameterInfoList property.
     * 
     * @return
     *     possible object is
     *     {@link CreateNewCMSubscriber.ParameterInfoList }
     *     
     */
    public CreateNewCMSubscriber.ParameterInfoList getParameterInfoList() {
        return parameterInfoList;
    }

    /**
     * Sets the value of the parameterInfoList property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateNewCMSubscriber.ParameterInfoList }
     *     
     */
    public void setParameterInfoList(CreateNewCMSubscriber.ParameterInfoList value) {
        this.parameterInfoList = value;
    }

    /**
     * Gets the value of the activityReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityReason() {
        return activityReason;
    }

    /**
     * Sets the value of the activityReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityReason(String value) {
        this.activityReason = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AddressDetails" type="{http://www.globe.com/warcraft/wsdl/billingcm/}AddressDetails" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "addressDetails"
    })
    public static class AddressDetailsList {

        @XmlElement(name = "AddressDetails", required = true)
        protected List<AddressDetails> addressDetails;

        /**
         * Gets the value of the addressDetails property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the addressDetails property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAddressDetails().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AddressDetails }
         * 
         * 
         */
        public List<AddressDetails> getAddressDetails() {
            if (addressDetails == null) {
                addressDetails = new ArrayList<AddressDetails>();
            }
            return this.addressDetails;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="LogicalResourceInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}LogicalResourceInfoModel" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "logicalResourceInfo"
    })
    public static class LogicalResourceInfoList {

        @XmlElement(name = "LogicalResourceInfo", required = true)
        protected List<LogicalResourceInfoModel> logicalResourceInfo;

        /**
         * Gets the value of the logicalResourceInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the logicalResourceInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getLogicalResourceInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link LogicalResourceInfoModel }
         * 
         * 
         */
        public List<LogicalResourceInfoModel> getLogicalResourceInfo() {
            if (logicalResourceInfo == null) {
                logicalResourceInfo = new ArrayList<LogicalResourceInfoModel>();
            }
            return this.logicalResourceInfo;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="NameInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}NameInfos" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nameInfo"
    })
    public static class NameInfoList {

        @XmlElement(name = "NameInfo", required = true)
        protected List<NameInfos> nameInfo;

        /**
         * Gets the value of the nameInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the nameInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getNameInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link NameInfos }
         * 
         * 
         */
        public List<NameInfos> getNameInfo() {
            if (nameInfo == null) {
                nameInfo = new ArrayList<NameInfos>();
            }
            return this.nameInfo;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ParameterInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}ParameterInfo" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "parameterInfo"
    })
    public static class ParameterInfoList {

        @XmlElement(name = "ParameterInfo", required = true)
        protected List<ParameterInfo> parameterInfo;

        /**
         * Gets the value of the parameterInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the parameterInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getParameterInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ParameterInfo }
         * 
         * 
         */
        public List<ParameterInfo> getParameterInfo() {
            if (parameterInfo == null) {
                parameterInfo = new ArrayList<ParameterInfo>();
            }
            return this.parameterInfo;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="PhysicalResourceInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}PhysicalResourceInfoModel" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "physicalResourceInfo"
    })
    public static class PhysicalResourceInfoList {

        @XmlElement(name = "PhysicalResourceInfo", required = true)
        protected List<PhysicalResourceInfoModel> physicalResourceInfo;

        /**
         * Gets the value of the physicalResourceInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the physicalResourceInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPhysicalResourceInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link PhysicalResourceInfoModel }
         * 
         * 
         */
        public List<PhysicalResourceInfoModel> getPhysicalResourceInfo() {
            if (physicalResourceInfo == null) {
                physicalResourceInfo = new ArrayList<PhysicalResourceInfoModel>();
            }
            return this.physicalResourceInfo;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SrvAgrInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}SrvAgrInfo" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "srvAgrInfo"
    })
    public static class SrvAgrInfoList {

        @XmlElement(name = "SrvAgrInfo", required = true)
        protected List<SrvAgrInfo> srvAgrInfo;

        /**
         * Gets the value of the srvAgrInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the srvAgrInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSrvAgrInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SrvAgrInfo }
         * 
         * 
         */
        public List<SrvAgrInfo> getSrvAgrInfo() {
            if (srvAgrInfo == null) {
                srvAgrInfo = new ArrayList<SrvAgrInfo>();
            }
            return this.srvAgrInfo;
        }

    }

}
