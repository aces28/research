
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountCollectionFixInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountCollectionFixInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CollectionFixCsrCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CollectionFixPolicy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CollectionPermanentWaiveInd" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountCollectionFixInfo", propOrder = {
    "collectionFixCsrCd",
    "collectionFixPolicy",
    "collectionPermanentWaiveInd"
})
public class AccountCollectionFixInfo {

    @XmlElement(name = "CollectionFixCsrCd")
    protected String collectionFixCsrCd;
    @XmlElement(name = "CollectionFixPolicy")
    protected String collectionFixPolicy;
    @XmlElement(name = "CollectionPermanentWaiveInd")
    protected byte collectionPermanentWaiveInd;

    /**
     * Gets the value of the collectionFixCsrCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCollectionFixCsrCd() {
        return collectionFixCsrCd;
    }

    /**
     * Sets the value of the collectionFixCsrCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCollectionFixCsrCd(String value) {
        this.collectionFixCsrCd = value;
    }

    /**
     * Gets the value of the collectionFixPolicy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCollectionFixPolicy() {
        return collectionFixPolicy;
    }

    /**
     * Sets the value of the collectionFixPolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCollectionFixPolicy(String value) {
        this.collectionFixPolicy = value;
    }

    /**
     * Gets the value of the collectionPermanentWaiveInd property.
     * 
     */
    public byte getCollectionPermanentWaiveInd() {
        return collectionPermanentWaiveInd;
    }

    /**
     * Sets the value of the collectionPermanentWaiveInd property.
     * 
     */
    public void setCollectionPermanentWaiveInd(byte value) {
        this.collectionPermanentWaiveInd = value;
    }

}
