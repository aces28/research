
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NameInfos complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NameInfos">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NameType">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="LinkType">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NameElement1">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NameElement2">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NameElement3">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NameElement4">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NameElement5">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NameElement6">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NameElement7">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NameElement8">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NameElement9" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NameElement10">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NameInfos", propOrder = {
    "nameType",
    "linkType",
    "nameElement1",
    "nameElement2",
    "nameElement3",
    "nameElement4",
    "nameElement5",
    "nameElement6",
    "nameElement7",
    "nameElement8",
    "nameElement9",
    "nameElement10"
})
public class NameInfos {

    @XmlElement(name = "NameType", required = true)
    protected String nameType;
    @XmlElement(name = "LinkType", required = true)
    protected String linkType;
    @XmlElement(name = "NameElement1", required = true)
    protected String nameElement1;
    @XmlElement(name = "NameElement2", required = true)
    protected String nameElement2;
    @XmlElement(name = "NameElement3", required = true)
    protected String nameElement3;
    @XmlElement(name = "NameElement4", required = true)
    protected String nameElement4;
    @XmlElement(name = "NameElement5", required = true)
    protected String nameElement5;
    @XmlElement(name = "NameElement6", required = true)
    protected String nameElement6;
    @XmlElement(name = "NameElement7", required = true)
    protected String nameElement7;
    @XmlElement(name = "NameElement8", required = true)
    protected String nameElement8;
    @XmlElement(name = "NameElement9")
    protected String nameElement9;
    @XmlElement(name = "NameElement10", required = true)
    protected String nameElement10;

    /**
     * Gets the value of the nameType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameType() {
        return nameType;
    }

    /**
     * Sets the value of the nameType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameType(String value) {
        this.nameType = value;
    }

    /**
     * Gets the value of the linkType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLinkType() {
        return linkType;
    }

    /**
     * Sets the value of the linkType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLinkType(String value) {
        this.linkType = value;
    }

    /**
     * Gets the value of the nameElement1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameElement1() {
        return nameElement1;
    }

    /**
     * Sets the value of the nameElement1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameElement1(String value) {
        this.nameElement1 = value;
    }

    /**
     * Gets the value of the nameElement2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameElement2() {
        return nameElement2;
    }

    /**
     * Sets the value of the nameElement2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameElement2(String value) {
        this.nameElement2 = value;
    }

    /**
     * Gets the value of the nameElement3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameElement3() {
        return nameElement3;
    }

    /**
     * Sets the value of the nameElement3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameElement3(String value) {
        this.nameElement3 = value;
    }

    /**
     * Gets the value of the nameElement4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameElement4() {
        return nameElement4;
    }

    /**
     * Sets the value of the nameElement4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameElement4(String value) {
        this.nameElement4 = value;
    }

    /**
     * Gets the value of the nameElement5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameElement5() {
        return nameElement5;
    }

    /**
     * Sets the value of the nameElement5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameElement5(String value) {
        this.nameElement5 = value;
    }

    /**
     * Gets the value of the nameElement6 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameElement6() {
        return nameElement6;
    }

    /**
     * Sets the value of the nameElement6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameElement6(String value) {
        this.nameElement6 = value;
    }

    /**
     * Gets the value of the nameElement7 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameElement7() {
        return nameElement7;
    }

    /**
     * Sets the value of the nameElement7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameElement7(String value) {
        this.nameElement7 = value;
    }

    /**
     * Gets the value of the nameElement8 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameElement8() {
        return nameElement8;
    }

    /**
     * Sets the value of the nameElement8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameElement8(String value) {
        this.nameElement8 = value;
    }

    /**
     * Gets the value of the nameElement9 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameElement9() {
        return nameElement9;
    }

    /**
     * Sets the value of the nameElement9 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameElement9(String value) {
        this.nameElement9 = value;
    }

    /**
     * Gets the value of the nameElement10 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameElement10() {
        return nameElement10;
    }

    /**
     * Sets the value of the nameElement10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameElement10(String value) {
        this.nameElement10 = value;
    }

}
