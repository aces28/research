
package com.globe.warcraft.wsdl.billingcm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UnifiedResourceCategoryInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UnifiedResourceCategoryInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CategoryType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CategoryTypeId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CategoryValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CategoryValueAttributeList" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CategoryValueAttribute" type="{http://www.globe.com/warcraft/wsdl/billingcm/}AttributesData" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CategoryValueId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UnifiedResourceCategoryInfo", propOrder = {
    "categoryType",
    "categoryTypeId",
    "categoryValue",
    "categoryValueAttributeList",
    "categoryValueId"
})
public class UnifiedResourceCategoryInfo {

    @XmlElement(name = "CategoryType")
    protected String categoryType;
    @XmlElement(name = "CategoryTypeId")
    protected int categoryTypeId;
    @XmlElement(name = "CategoryValue")
    protected String categoryValue;
    @XmlElement(name = "CategoryValueAttributeList")
    protected UnifiedResourceCategoryInfo.CategoryValueAttributeList categoryValueAttributeList;
    @XmlElement(name = "CategoryValueId")
    protected int categoryValueId;

    /**
     * Gets the value of the categoryType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategoryType() {
        return categoryType;
    }

    /**
     * Sets the value of the categoryType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategoryType(String value) {
        this.categoryType = value;
    }

    /**
     * Gets the value of the categoryTypeId property.
     * 
     */
    public int getCategoryTypeId() {
        return categoryTypeId;
    }

    /**
     * Sets the value of the categoryTypeId property.
     * 
     */
    public void setCategoryTypeId(int value) {
        this.categoryTypeId = value;
    }

    /**
     * Gets the value of the categoryValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategoryValue() {
        return categoryValue;
    }

    /**
     * Sets the value of the categoryValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategoryValue(String value) {
        this.categoryValue = value;
    }

    /**
     * Gets the value of the categoryValueAttributeList property.
     * 
     * @return
     *     possible object is
     *     {@link UnifiedResourceCategoryInfo.CategoryValueAttributeList }
     *     
     */
    public UnifiedResourceCategoryInfo.CategoryValueAttributeList getCategoryValueAttributeList() {
        return categoryValueAttributeList;
    }

    /**
     * Sets the value of the categoryValueAttributeList property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnifiedResourceCategoryInfo.CategoryValueAttributeList }
     *     
     */
    public void setCategoryValueAttributeList(UnifiedResourceCategoryInfo.CategoryValueAttributeList value) {
        this.categoryValueAttributeList = value;
    }

    /**
     * Gets the value of the categoryValueId property.
     * 
     */
    public int getCategoryValueId() {
        return categoryValueId;
    }

    /**
     * Sets the value of the categoryValueId property.
     * 
     */
    public void setCategoryValueId(int value) {
        this.categoryValueId = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CategoryValueAttribute" type="{http://www.globe.com/warcraft/wsdl/billingcm/}AttributesData" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "categoryValueAttribute"
    })
    public static class CategoryValueAttributeList {

        @XmlElement(name = "CategoryValueAttribute")
        protected List<AttributesData> categoryValueAttribute;

        /**
         * Gets the value of the categoryValueAttribute property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the categoryValueAttribute property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCategoryValueAttribute().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AttributesData }
         * 
         * 
         */
        public List<AttributesData> getCategoryValueAttribute() {
            if (categoryValueAttribute == null) {
                categoryValueAttribute = new ArrayList<AttributesData>();
            }
            return this.categoryValueAttribute;
        }

    }

}
