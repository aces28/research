
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PayChannelHeader complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PayChannelHeader">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BillingArrangementIdInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}BillingArrangementIdInfo" minOccurs="0"/>
 *         &lt;element name="ExternalIdInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}ExternalIdInfo" minOccurs="0"/>
 *         &lt;element name="PayChannelBusinessEntityIdInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}BusinessEntityIdInfo" minOccurs="0"/>
 *         &lt;element name="PayChannelDescriptionInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}PayChannelDescriptionInfo" minOccurs="0"/>
 *         &lt;element name="PayChannelGeneralInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}PayChannelGeneralInfo" minOccurs="0"/>
 *         &lt;element name="PayChannelIdInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}PayChannelIdInfo" minOccurs="0"/>
 *         &lt;element name="PayChannelNumberInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}PayChannelNumberInfo" minOccurs="0"/>
 *         &lt;element name="PayChannelPaymentCategoryInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}PayChannelPaymentCategoryInfo" minOccurs="0"/>
 *         &lt;element name="PayChannelPaymentMethodInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}PayChannelPaymentMethodInfo" minOccurs="0"/>
 *         &lt;element name="PayChannelStatusInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}PayChannelStatusInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PayChannelHeader", propOrder = {
    "billingArrangementIdInfo",
    "externalIdInfo",
    "payChannelBusinessEntityIdInfo",
    "payChannelDescriptionInfo",
    "payChannelGeneralInfo",
    "payChannelIdInfo",
    "payChannelNumberInfo",
    "payChannelPaymentCategoryInfo",
    "payChannelPaymentMethodInfo",
    "payChannelStatusInfo"
})
public class PayChannelHeader {

    @XmlElement(name = "BillingArrangementIdInfo")
    protected BillingArrangementIdInfo billingArrangementIdInfo;
    @XmlElement(name = "ExternalIdInfo")
    protected ExternalIdInfo externalIdInfo;
    @XmlElement(name = "PayChannelBusinessEntityIdInfo")
    protected BusinessEntityIdInfo payChannelBusinessEntityIdInfo;
    @XmlElement(name = "PayChannelDescriptionInfo")
    protected PayChannelDescriptionInfo payChannelDescriptionInfo;
    @XmlElement(name = "PayChannelGeneralInfo")
    protected PayChannelGeneralInfo payChannelGeneralInfo;
    @XmlElement(name = "PayChannelIdInfo")
    protected PayChannelIdInfo payChannelIdInfo;
    @XmlElement(name = "PayChannelNumberInfo")
    protected PayChannelNumberInfo payChannelNumberInfo;
    @XmlElement(name = "PayChannelPaymentCategoryInfo")
    protected PayChannelPaymentCategoryInfo payChannelPaymentCategoryInfo;
    @XmlElement(name = "PayChannelPaymentMethodInfo")
    protected PayChannelPaymentMethodInfo payChannelPaymentMethodInfo;
    @XmlElement(name = "PayChannelStatusInfo")
    protected PayChannelStatusInfo payChannelStatusInfo;

    /**
     * Gets the value of the billingArrangementIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link BillingArrangementIdInfo }
     *     
     */
    public BillingArrangementIdInfo getBillingArrangementIdInfo() {
        return billingArrangementIdInfo;
    }

    /**
     * Sets the value of the billingArrangementIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillingArrangementIdInfo }
     *     
     */
    public void setBillingArrangementIdInfo(BillingArrangementIdInfo value) {
        this.billingArrangementIdInfo = value;
    }

    /**
     * Gets the value of the externalIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ExternalIdInfo }
     *     
     */
    public ExternalIdInfo getExternalIdInfo() {
        return externalIdInfo;
    }

    /**
     * Sets the value of the externalIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExternalIdInfo }
     *     
     */
    public void setExternalIdInfo(ExternalIdInfo value) {
        this.externalIdInfo = value;
    }

    /**
     * Gets the value of the payChannelBusinessEntityIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link BusinessEntityIdInfo }
     *     
     */
    public BusinessEntityIdInfo getPayChannelBusinessEntityIdInfo() {
        return payChannelBusinessEntityIdInfo;
    }

    /**
     * Sets the value of the payChannelBusinessEntityIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessEntityIdInfo }
     *     
     */
    public void setPayChannelBusinessEntityIdInfo(BusinessEntityIdInfo value) {
        this.payChannelBusinessEntityIdInfo = value;
    }

    /**
     * Gets the value of the payChannelDescriptionInfo property.
     * 
     * @return
     *     possible object is
     *     {@link PayChannelDescriptionInfo }
     *     
     */
    public PayChannelDescriptionInfo getPayChannelDescriptionInfo() {
        return payChannelDescriptionInfo;
    }

    /**
     * Sets the value of the payChannelDescriptionInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link PayChannelDescriptionInfo }
     *     
     */
    public void setPayChannelDescriptionInfo(PayChannelDescriptionInfo value) {
        this.payChannelDescriptionInfo = value;
    }

    /**
     * Gets the value of the payChannelGeneralInfo property.
     * 
     * @return
     *     possible object is
     *     {@link PayChannelGeneralInfo }
     *     
     */
    public PayChannelGeneralInfo getPayChannelGeneralInfo() {
        return payChannelGeneralInfo;
    }

    /**
     * Sets the value of the payChannelGeneralInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link PayChannelGeneralInfo }
     *     
     */
    public void setPayChannelGeneralInfo(PayChannelGeneralInfo value) {
        this.payChannelGeneralInfo = value;
    }

    /**
     * Gets the value of the payChannelIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link PayChannelIdInfo }
     *     
     */
    public PayChannelIdInfo getPayChannelIdInfo() {
        return payChannelIdInfo;
    }

    /**
     * Sets the value of the payChannelIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link PayChannelIdInfo }
     *     
     */
    public void setPayChannelIdInfo(PayChannelIdInfo value) {
        this.payChannelIdInfo = value;
    }

    /**
     * Gets the value of the payChannelNumberInfo property.
     * 
     * @return
     *     possible object is
     *     {@link PayChannelNumberInfo }
     *     
     */
    public PayChannelNumberInfo getPayChannelNumberInfo() {
        return payChannelNumberInfo;
    }

    /**
     * Sets the value of the payChannelNumberInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link PayChannelNumberInfo }
     *     
     */
    public void setPayChannelNumberInfo(PayChannelNumberInfo value) {
        this.payChannelNumberInfo = value;
    }

    /**
     * Gets the value of the payChannelPaymentCategoryInfo property.
     * 
     * @return
     *     possible object is
     *     {@link PayChannelPaymentCategoryInfo }
     *     
     */
    public PayChannelPaymentCategoryInfo getPayChannelPaymentCategoryInfo() {
        return payChannelPaymentCategoryInfo;
    }

    /**
     * Sets the value of the payChannelPaymentCategoryInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link PayChannelPaymentCategoryInfo }
     *     
     */
    public void setPayChannelPaymentCategoryInfo(PayChannelPaymentCategoryInfo value) {
        this.payChannelPaymentCategoryInfo = value;
    }

    /**
     * Gets the value of the payChannelPaymentMethodInfo property.
     * 
     * @return
     *     possible object is
     *     {@link PayChannelPaymentMethodInfo }
     *     
     */
    public PayChannelPaymentMethodInfo getPayChannelPaymentMethodInfo() {
        return payChannelPaymentMethodInfo;
    }

    /**
     * Sets the value of the payChannelPaymentMethodInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link PayChannelPaymentMethodInfo }
     *     
     */
    public void setPayChannelPaymentMethodInfo(PayChannelPaymentMethodInfo value) {
        this.payChannelPaymentMethodInfo = value;
    }

    /**
     * Gets the value of the payChannelStatusInfo property.
     * 
     * @return
     *     possible object is
     *     {@link PayChannelStatusInfo }
     *     
     */
    public PayChannelStatusInfo getPayChannelStatusInfo() {
        return payChannelStatusInfo;
    }

    /**
     * Sets the value of the payChannelStatusInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link PayChannelStatusInfo }
     *     
     */
    public void setPayChannelStatusInfo(PayChannelStatusInfo value) {
        this.payChannelStatusInfo = value;
    }

}
