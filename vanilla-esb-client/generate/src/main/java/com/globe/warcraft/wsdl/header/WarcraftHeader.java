
package com.globe.warcraft.wsdl.header;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WarcraftHeader complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WarcraftHeader">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EsbMessageID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EsbRequestDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="EsbResponseDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="EsbIMLNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EsbOperationName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WarcraftHeader", propOrder = {
    "esbMessageID",
    "esbRequestDateTime",
    "esbResponseDateTime",
    "esbIMLNumber",
    "esbOperationName"
})
public class WarcraftHeader {

    @XmlElementRef(name = "EsbMessageID", type = JAXBElement.class, required = false)
    protected JAXBElement<String> esbMessageID;
    @XmlElementRef(name = "EsbRequestDateTime", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> esbRequestDateTime;
    @XmlElementRef(name = "EsbResponseDateTime", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> esbResponseDateTime;
    @XmlElementRef(name = "EsbIMLNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> esbIMLNumber;
    @XmlElementRef(name = "EsbOperationName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> esbOperationName;

    /**
     * Gets the value of the esbMessageID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEsbMessageID() {
        return esbMessageID;
    }

    /**
     * Sets the value of the esbMessageID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEsbMessageID(JAXBElement<String> value) {
        this.esbMessageID = value;
    }

    /**
     * Gets the value of the esbRequestDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getEsbRequestDateTime() {
        return esbRequestDateTime;
    }

    /**
     * Sets the value of the esbRequestDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setEsbRequestDateTime(JAXBElement<XMLGregorianCalendar> value) {
        this.esbRequestDateTime = value;
    }

    /**
     * Gets the value of the esbResponseDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getEsbResponseDateTime() {
        return esbResponseDateTime;
    }

    /**
     * Sets the value of the esbResponseDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setEsbResponseDateTime(JAXBElement<XMLGregorianCalendar> value) {
        this.esbResponseDateTime = value;
    }

    /**
     * Gets the value of the esbIMLNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEsbIMLNumber() {
        return esbIMLNumber;
    }

    /**
     * Sets the value of the esbIMLNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEsbIMLNumber(JAXBElement<String> value) {
        this.esbIMLNumber = value;
    }

    /**
     * Gets the value of the esbOperationName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEsbOperationName() {
        return esbOperationName;
    }

    /**
     * Sets the value of the esbOperationName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEsbOperationName(JAXBElement<String> value) {
        this.esbOperationName = value;
    }

}
