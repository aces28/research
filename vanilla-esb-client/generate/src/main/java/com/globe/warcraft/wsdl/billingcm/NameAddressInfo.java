
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NameAddressInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NameAddressInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AddressInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}AddressInfo" minOccurs="0"/>
 *         &lt;element name="AddressNameLinkInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}AddressNameLinkInfo" minOccurs="0"/>
 *         &lt;element name="NameInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}NameInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NameAddressInfo", propOrder = {
    "addressInfo",
    "addressNameLinkInfo",
    "nameInfo"
})
public class NameAddressInfo {

    @XmlElement(name = "AddressInfo")
    protected AddressInfo addressInfo;
    @XmlElement(name = "AddressNameLinkInfo")
    protected AddressNameLinkInfo addressNameLinkInfo;
    @XmlElement(name = "NameInfo")
    protected NameInfo nameInfo;

    /**
     * Gets the value of the addressInfo property.
     * 
     * @return
     *     possible object is
     *     {@link AddressInfo }
     *     
     */
    public AddressInfo getAddressInfo() {
        return addressInfo;
    }

    /**
     * Sets the value of the addressInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressInfo }
     *     
     */
    public void setAddressInfo(AddressInfo value) {
        this.addressInfo = value;
    }

    /**
     * Gets the value of the addressNameLinkInfo property.
     * 
     * @return
     *     possible object is
     *     {@link AddressNameLinkInfo }
     *     
     */
    public AddressNameLinkInfo getAddressNameLinkInfo() {
        return addressNameLinkInfo;
    }

    /**
     * Sets the value of the addressNameLinkInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressNameLinkInfo }
     *     
     */
    public void setAddressNameLinkInfo(AddressNameLinkInfo value) {
        this.addressNameLinkInfo = value;
    }

    /**
     * Gets the value of the nameInfo property.
     * 
     * @return
     *     possible object is
     *     {@link NameInfo }
     *     
     */
    public NameInfo getNameInfo() {
        return nameInfo;
    }

    /**
     * Sets the value of the nameInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link NameInfo }
     *     
     */
    public void setNameInfo(NameInfo value) {
        this.nameInfo = value;
    }

}
