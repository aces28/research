
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ValidateIfGroupOwner complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValidateIfGroupOwner">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Resourceinfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}ResourceInfo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidateIfGroupOwner", propOrder = {
    "resourceinfo"
})
public class ValidateIfGroupOwner {

    @XmlElement(name = "Resourceinfo", required = true)
    protected ResourceInfo resourceinfo;

    /**
     * Gets the value of the resourceinfo property.
     * 
     * @return
     *     possible object is
     *     {@link ResourceInfo }
     *     
     */
    public ResourceInfo getResourceinfo() {
        return resourceinfo;
    }

    /**
     * Sets the value of the resourceinfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResourceInfo }
     *     
     */
    public void setResourceinfo(ResourceInfo value) {
        this.resourceinfo = value;
    }

}
