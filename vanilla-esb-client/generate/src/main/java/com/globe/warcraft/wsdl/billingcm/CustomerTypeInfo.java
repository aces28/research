
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerTypeInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerTypeInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustomerType" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="CustomerSubType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerTypeInfo", propOrder = {
    "customerType",
    "customerSubType"
})
public class CustomerTypeInfo {

    @XmlElement(name = "CustomerType")
    protected byte customerType;
    @XmlElement(name = "CustomerSubType")
    protected String customerSubType;

    /**
     * Gets the value of the customerType property.
     * 
     */
    public byte getCustomerType() {
        return customerType;
    }

    /**
     * Sets the value of the customerType property.
     * 
     */
    public void setCustomerType(byte value) {
        this.customerType = value;
    }

    /**
     * Gets the value of the customerSubType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerSubType() {
        return customerSubType;
    }

    /**
     * Sets the value of the customerSubType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerSubType(String value) {
        this.customerSubType = value;
    }

}
