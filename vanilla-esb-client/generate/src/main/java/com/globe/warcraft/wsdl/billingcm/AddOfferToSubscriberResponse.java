
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddOfferToSubscriberResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddOfferToSubscriberResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AddOfferToSubscriberResult" type="{http://www.globe.com/warcraft/wsdl/billingcm/}AddOfferToSubscriberResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddOfferToSubscriberResponse", propOrder = {
    "addOfferToSubscriberResult"
})
public class AddOfferToSubscriberResponse {

    @XmlElement(name = "AddOfferToSubscriberResult")
    protected AddOfferToSubscriberResult addOfferToSubscriberResult;

    /**
     * Gets the value of the addOfferToSubscriberResult property.
     * 
     * @return
     *     possible object is
     *     {@link AddOfferToSubscriberResult }
     *     
     */
    public AddOfferToSubscriberResult getAddOfferToSubscriberResult() {
        return addOfferToSubscriberResult;
    }

    /**
     * Sets the value of the addOfferToSubscriberResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddOfferToSubscriberResult }
     *     
     */
    public void setAddOfferToSubscriberResult(AddOfferToSubscriberResult value) {
        this.addOfferToSubscriberResult = value;
    }

}
