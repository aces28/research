
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountingManagementInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountingManagementInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountingBalancePolicy" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="DocumentType" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="L9AccountType" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="L9CorpId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="L9CorpIdDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="L9CostCenter" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="L9CreditLimit" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="L9PrimaryMSISDN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountingManagementInfo", propOrder = {
    "accountingBalancePolicy",
    "documentType",
    "l9AccountType",
    "l9CorpId",
    "l9CorpIdDesc",
    "l9CostCenter",
    "l9CreditLimit",
    "l9PrimaryMSISDN"
})
public class AccountingManagementInfo {

    @XmlElement(name = "AccountingBalancePolicy")
    protected byte accountingBalancePolicy;
    @XmlElement(name = "DocumentType")
    protected byte documentType;
    @XmlElement(name = "L9AccountType")
    protected byte l9AccountType;
    @XmlElement(name = "L9CorpId")
    protected String l9CorpId;
    @XmlElement(name = "L9CorpIdDesc")
    protected String l9CorpIdDesc;
    @XmlElement(name = "L9CostCenter")
    protected String l9CostCenter;
    @XmlElement(name = "L9CreditLimit")
    protected double l9CreditLimit;
    @XmlElement(name = "L9PrimaryMSISDN")
    protected String l9PrimaryMSISDN;

    /**
     * Gets the value of the accountingBalancePolicy property.
     * 
     */
    public byte getAccountingBalancePolicy() {
        return accountingBalancePolicy;
    }

    /**
     * Sets the value of the accountingBalancePolicy property.
     * 
     */
    public void setAccountingBalancePolicy(byte value) {
        this.accountingBalancePolicy = value;
    }

    /**
     * Gets the value of the documentType property.
     * 
     */
    public byte getDocumentType() {
        return documentType;
    }

    /**
     * Sets the value of the documentType property.
     * 
     */
    public void setDocumentType(byte value) {
        this.documentType = value;
    }

    /**
     * Gets the value of the l9AccountType property.
     * 
     */
    public byte getL9AccountType() {
        return l9AccountType;
    }

    /**
     * Sets the value of the l9AccountType property.
     * 
     */
    public void setL9AccountType(byte value) {
        this.l9AccountType = value;
    }

    /**
     * Gets the value of the l9CorpId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getL9CorpId() {
        return l9CorpId;
    }

    /**
     * Sets the value of the l9CorpId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setL9CorpId(String value) {
        this.l9CorpId = value;
    }

    /**
     * Gets the value of the l9CorpIdDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getL9CorpIdDesc() {
        return l9CorpIdDesc;
    }

    /**
     * Sets the value of the l9CorpIdDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setL9CorpIdDesc(String value) {
        this.l9CorpIdDesc = value;
    }

    /**
     * Gets the value of the l9CostCenter property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getL9CostCenter() {
        return l9CostCenter;
    }

    /**
     * Sets the value of the l9CostCenter property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setL9CostCenter(String value) {
        this.l9CostCenter = value;
    }

    /**
     * Gets the value of the l9CreditLimit property.
     * 
     */
    public double getL9CreditLimit() {
        return l9CreditLimit;
    }

    /**
     * Sets the value of the l9CreditLimit property.
     * 
     */
    public void setL9CreditLimit(double value) {
        this.l9CreditLimit = value;
    }

    /**
     * Gets the value of the l9PrimaryMSISDN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getL9PrimaryMSISDN() {
        return l9PrimaryMSISDN;
    }

    /**
     * Sets the value of the l9PrimaryMSISDN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setL9PrimaryMSISDN(String value) {
        this.l9PrimaryMSISDN = value;
    }

}
