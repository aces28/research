
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetDetailsByMsisdnResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetDetailsByMsisdnResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetDetailsByMsisdnResult" type="{http://www.globe.com/warcraft/wsdl/billingcm/}GetDetailsByMsisdnResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetDetailsByMsisdnResponse", propOrder = {
    "getDetailsByMsisdnResult"
})
public class GetDetailsByMsisdnResponse {

    @XmlElement(name = "GetDetailsByMsisdnResult")
    protected GetDetailsByMsisdnResult getDetailsByMsisdnResult;

    /**
     * Gets the value of the getDetailsByMsisdnResult property.
     * 
     * @return
     *     possible object is
     *     {@link GetDetailsByMsisdnResult }
     *     
     */
    public GetDetailsByMsisdnResult getGetDetailsByMsisdnResult() {
        return getDetailsByMsisdnResult;
    }

    /**
     * Sets the value of the getDetailsByMsisdnResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetDetailsByMsisdnResult }
     *     
     */
    public void setGetDetailsByMsisdnResult(GetDetailsByMsisdnResult value) {
        this.getDetailsByMsisdnResult = value;
    }

}
