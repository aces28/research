
package com.globe.warcraft.wsdl.billingcm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UnifiedResourceDetailsInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UnifiedResourceDetailsInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AttributesInfoList" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AttributesInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}AttributesData" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="UnifiedResourceCategoryInfoList" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="UnifiedResourceCategoryInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}UnifiedResourceCategoryInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ComponentsInfoList" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ComponentsInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}UnifiedResourceInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ParentIdInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}RMEntityIdInfo" minOccurs="0"/>
 *         &lt;element name="StatusInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}StatusInfo" minOccurs="0"/>
 *         &lt;element name="UnifiedResourceIdInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}RMEntityIdInfo" minOccurs="0"/>
 *         &lt;element name="UserId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UnifiedResourceDetailsInfo", propOrder = {
    "attributesInfoList",
    "unifiedResourceCategoryInfoList",
    "componentsInfoList",
    "parentIdInfo",
    "statusInfo",
    "unifiedResourceIdInfo",
    "userId"
})
public class UnifiedResourceDetailsInfo {

    @XmlElement(name = "AttributesInfoList")
    protected UnifiedResourceDetailsInfo.AttributesInfoList attributesInfoList;
    @XmlElement(name = "UnifiedResourceCategoryInfoList")
    protected UnifiedResourceDetailsInfo.UnifiedResourceCategoryInfoList unifiedResourceCategoryInfoList;
    @XmlElement(name = "ComponentsInfoList")
    protected UnifiedResourceDetailsInfo.ComponentsInfoList componentsInfoList;
    @XmlElement(name = "ParentIdInfo")
    protected RMEntityIdInfo parentIdInfo;
    @XmlElement(name = "StatusInfo")
    protected StatusInfo statusInfo;
    @XmlElement(name = "UnifiedResourceIdInfo")
    protected RMEntityIdInfo unifiedResourceIdInfo;
    @XmlElement(name = "UserId")
    protected int userId;

    /**
     * Gets the value of the attributesInfoList property.
     * 
     * @return
     *     possible object is
     *     {@link UnifiedResourceDetailsInfo.AttributesInfoList }
     *     
     */
    public UnifiedResourceDetailsInfo.AttributesInfoList getAttributesInfoList() {
        return attributesInfoList;
    }

    /**
     * Sets the value of the attributesInfoList property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnifiedResourceDetailsInfo.AttributesInfoList }
     *     
     */
    public void setAttributesInfoList(UnifiedResourceDetailsInfo.AttributesInfoList value) {
        this.attributesInfoList = value;
    }

    /**
     * Gets the value of the unifiedResourceCategoryInfoList property.
     * 
     * @return
     *     possible object is
     *     {@link UnifiedResourceDetailsInfo.UnifiedResourceCategoryInfoList }
     *     
     */
    public UnifiedResourceDetailsInfo.UnifiedResourceCategoryInfoList getUnifiedResourceCategoryInfoList() {
        return unifiedResourceCategoryInfoList;
    }

    /**
     * Sets the value of the unifiedResourceCategoryInfoList property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnifiedResourceDetailsInfo.UnifiedResourceCategoryInfoList }
     *     
     */
    public void setUnifiedResourceCategoryInfoList(UnifiedResourceDetailsInfo.UnifiedResourceCategoryInfoList value) {
        this.unifiedResourceCategoryInfoList = value;
    }

    /**
     * Gets the value of the componentsInfoList property.
     * 
     * @return
     *     possible object is
     *     {@link UnifiedResourceDetailsInfo.ComponentsInfoList }
     *     
     */
    public UnifiedResourceDetailsInfo.ComponentsInfoList getComponentsInfoList() {
        return componentsInfoList;
    }

    /**
     * Sets the value of the componentsInfoList property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnifiedResourceDetailsInfo.ComponentsInfoList }
     *     
     */
    public void setComponentsInfoList(UnifiedResourceDetailsInfo.ComponentsInfoList value) {
        this.componentsInfoList = value;
    }

    /**
     * Gets the value of the parentIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link RMEntityIdInfo }
     *     
     */
    public RMEntityIdInfo getParentIdInfo() {
        return parentIdInfo;
    }

    /**
     * Sets the value of the parentIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link RMEntityIdInfo }
     *     
     */
    public void setParentIdInfo(RMEntityIdInfo value) {
        this.parentIdInfo = value;
    }

    /**
     * Gets the value of the statusInfo property.
     * 
     * @return
     *     possible object is
     *     {@link StatusInfo }
     *     
     */
    public StatusInfo getStatusInfo() {
        return statusInfo;
    }

    /**
     * Sets the value of the statusInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusInfo }
     *     
     */
    public void setStatusInfo(StatusInfo value) {
        this.statusInfo = value;
    }

    /**
     * Gets the value of the unifiedResourceIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link RMEntityIdInfo }
     *     
     */
    public RMEntityIdInfo getUnifiedResourceIdInfo() {
        return unifiedResourceIdInfo;
    }

    /**
     * Sets the value of the unifiedResourceIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link RMEntityIdInfo }
     *     
     */
    public void setUnifiedResourceIdInfo(RMEntityIdInfo value) {
        this.unifiedResourceIdInfo = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     */
    public void setUserId(int value) {
        this.userId = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AttributesInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}AttributesData" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "attributesInfo"
    })
    public static class AttributesInfoList {

        @XmlElement(name = "AttributesInfo")
        protected List<AttributesData> attributesInfo;

        /**
         * Gets the value of the attributesInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the attributesInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAttributesInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AttributesData }
         * 
         * 
         */
        public List<AttributesData> getAttributesInfo() {
            if (attributesInfo == null) {
                attributesInfo = new ArrayList<AttributesData>();
            }
            return this.attributesInfo;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ComponentsInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}UnifiedResourceInfo" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "componentsInfo"
    })
    public static class ComponentsInfoList {

        @XmlElement(name = "ComponentsInfo")
        protected List<UnifiedResourceInfo> componentsInfo;

        /**
         * Gets the value of the componentsInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the componentsInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getComponentsInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link UnifiedResourceInfo }
         * 
         * 
         */
        public List<UnifiedResourceInfo> getComponentsInfo() {
            if (componentsInfo == null) {
                componentsInfo = new ArrayList<UnifiedResourceInfo>();
            }
            return this.componentsInfo;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="UnifiedResourceCategoryInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}UnifiedResourceCategoryInfo" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "unifiedResourceCategoryInfo"
    })
    public static class UnifiedResourceCategoryInfoList {

        @XmlElement(name = "UnifiedResourceCategoryInfo")
        protected List<UnifiedResourceCategoryInfo> unifiedResourceCategoryInfo;

        /**
         * Gets the value of the unifiedResourceCategoryInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the unifiedResourceCategoryInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getUnifiedResourceCategoryInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link UnifiedResourceCategoryInfo }
         * 
         * 
         */
        public List<UnifiedResourceCategoryInfo> getUnifiedResourceCategoryInfo() {
            if (unifiedResourceCategoryInfo == null) {
                unifiedResourceCategoryInfo = new ArrayList<UnifiedResourceCategoryInfo>();
            }
            return this.unifiedResourceCategoryInfo;
        }

    }

}
