
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BillingArrangementNumberInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BillingArrangementNumberInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountNo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="BillingArrangmentNo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BillingArrangementNumberInfo", propOrder = {
    "accountNo",
    "billingArrangmentNo"
})
public class BillingArrangementNumberInfo {

    @XmlElement(name = "AccountNo")
    protected int accountNo;
    @XmlElement(name = "BillingArrangmentNo")
    protected int billingArrangmentNo;

    /**
     * Gets the value of the accountNo property.
     * 
     */
    public int getAccountNo() {
        return accountNo;
    }

    /**
     * Sets the value of the accountNo property.
     * 
     */
    public void setAccountNo(int value) {
        this.accountNo = value;
    }

    /**
     * Gets the value of the billingArrangmentNo property.
     * 
     */
    public int getBillingArrangmentNo() {
        return billingArrangmentNo;
    }

    /**
     * Sets the value of the billingArrangmentNo property.
     * 
     */
    public void setBillingArrangmentNo(int value) {
        this.billingArrangmentNo = value;
    }

}
