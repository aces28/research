
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BusinessEntityIdInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BusinessEntityIdInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BusinessEntityId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BusinessEntityIdInfo", propOrder = {
    "businessEntityId"
})
public class BusinessEntityIdInfo {

    @XmlElement(name = "BusinessEntityId")
    protected int businessEntityId;

    /**
     * Gets the value of the businessEntityId property.
     * 
     */
    public int getBusinessEntityId() {
        return businessEntityId;
    }

    /**
     * Sets the value of the businessEntityId property.
     * 
     */
    public void setBusinessEntityId(int value) {
        this.businessEntityId = value;
    }

}
