
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UnifiedResourceInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UnifiedResourceInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StatusInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}StatusInfo" minOccurs="0"/>
 *         &lt;element name="ParentIdInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}RMEntityIdInfo" minOccurs="0"/>
 *         &lt;element name="UserId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="UnifiedResourceIdInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}RMEntityIdInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UnifiedResourceInfo", propOrder = {
    "statusInfo",
    "parentIdInfo",
    "userId",
    "unifiedResourceIdInfo"
})
public class UnifiedResourceInfo {

    @XmlElement(name = "StatusInfo")
    protected StatusInfo statusInfo;
    @XmlElement(name = "ParentIdInfo")
    protected RMEntityIdInfo parentIdInfo;
    @XmlElement(name = "UserId")
    protected Integer userId;
    @XmlElement(name = "UnifiedResourceIdInfo")
    protected RMEntityIdInfo unifiedResourceIdInfo;

    /**
     * Gets the value of the statusInfo property.
     * 
     * @return
     *     possible object is
     *     {@link StatusInfo }
     *     
     */
    public StatusInfo getStatusInfo() {
        return statusInfo;
    }

    /**
     * Sets the value of the statusInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusInfo }
     *     
     */
    public void setStatusInfo(StatusInfo value) {
        this.statusInfo = value;
    }

    /**
     * Gets the value of the parentIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link RMEntityIdInfo }
     *     
     */
    public RMEntityIdInfo getParentIdInfo() {
        return parentIdInfo;
    }

    /**
     * Sets the value of the parentIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link RMEntityIdInfo }
     *     
     */
    public void setParentIdInfo(RMEntityIdInfo value) {
        this.parentIdInfo = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setUserId(Integer value) {
        this.userId = value;
    }

    /**
     * Gets the value of the unifiedResourceIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link RMEntityIdInfo }
     *     
     */
    public RMEntityIdInfo getUnifiedResourceIdInfo() {
        return unifiedResourceIdInfo;
    }

    /**
     * Sets the value of the unifiedResourceIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link RMEntityIdInfo }
     *     
     */
    public void setUnifiedResourceIdInfo(RMEntityIdInfo value) {
        this.unifiedResourceIdInfo = value;
    }

}
