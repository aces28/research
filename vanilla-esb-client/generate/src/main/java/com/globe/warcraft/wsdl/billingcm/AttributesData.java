
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for AttributesData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AttributesData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ApplicationId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AttrDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AttrInd" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="AttrName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AttrQueryInd" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="AttrRscInd" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="AttrUpdateInd" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="AttrValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AttrValuePattern" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DlServiceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DlUpdateDtamp" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="OperatorId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="SysCreationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="SysUpdateDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AttributesData", propOrder = {
    "applicationId",
    "attrDesc",
    "attrInd",
    "attrName",
    "attrQueryInd",
    "attrRscInd",
    "attrUpdateInd",
    "attrValue",
    "attrValuePattern",
    "dlServiceCode",
    "dlUpdateDtamp",
    "operatorId",
    "sysCreationDate",
    "sysUpdateDate"
})
public class AttributesData {

    @XmlElement(name = "ApplicationId")
    protected String applicationId;
    @XmlElement(name = "AttrDesc")
    protected String attrDesc;
    @XmlElement(name = "AttrInd")
    protected byte attrInd;
    @XmlElement(name = "AttrName")
    protected String attrName;
    @XmlElement(name = "AttrQueryInd")
    protected byte attrQueryInd;
    @XmlElement(name = "AttrRscInd")
    protected byte attrRscInd;
    @XmlElement(name = "AttrUpdateInd")
    protected byte attrUpdateInd;
    @XmlElement(name = "AttrValue")
    protected String attrValue;
    @XmlElement(name = "AttrValuePattern")
    protected String attrValuePattern;
    @XmlElement(name = "DlServiceCode")
    protected String dlServiceCode;
    @XmlElement(name = "DlUpdateDtamp")
    protected short dlUpdateDtamp;
    @XmlElement(name = "OperatorId")
    protected int operatorId;
    @XmlElement(name = "SysCreationDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar sysCreationDate;
    @XmlElement(name = "SysUpdateDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar sysUpdateDate;

    /**
     * Gets the value of the applicationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationId() {
        return applicationId;
    }

    /**
     * Sets the value of the applicationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationId(String value) {
        this.applicationId = value;
    }

    /**
     * Gets the value of the attrDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttrDesc() {
        return attrDesc;
    }

    /**
     * Sets the value of the attrDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttrDesc(String value) {
        this.attrDesc = value;
    }

    /**
     * Gets the value of the attrInd property.
     * 
     */
    public byte getAttrInd() {
        return attrInd;
    }

    /**
     * Sets the value of the attrInd property.
     * 
     */
    public void setAttrInd(byte value) {
        this.attrInd = value;
    }

    /**
     * Gets the value of the attrName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttrName() {
        return attrName;
    }

    /**
     * Sets the value of the attrName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttrName(String value) {
        this.attrName = value;
    }

    /**
     * Gets the value of the attrQueryInd property.
     * 
     */
    public byte getAttrQueryInd() {
        return attrQueryInd;
    }

    /**
     * Sets the value of the attrQueryInd property.
     * 
     */
    public void setAttrQueryInd(byte value) {
        this.attrQueryInd = value;
    }

    /**
     * Gets the value of the attrRscInd property.
     * 
     */
    public byte getAttrRscInd() {
        return attrRscInd;
    }

    /**
     * Sets the value of the attrRscInd property.
     * 
     */
    public void setAttrRscInd(byte value) {
        this.attrRscInd = value;
    }

    /**
     * Gets the value of the attrUpdateInd property.
     * 
     */
    public byte getAttrUpdateInd() {
        return attrUpdateInd;
    }

    /**
     * Sets the value of the attrUpdateInd property.
     * 
     */
    public void setAttrUpdateInd(byte value) {
        this.attrUpdateInd = value;
    }

    /**
     * Gets the value of the attrValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttrValue() {
        return attrValue;
    }

    /**
     * Sets the value of the attrValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttrValue(String value) {
        this.attrValue = value;
    }

    /**
     * Gets the value of the attrValuePattern property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttrValuePattern() {
        return attrValuePattern;
    }

    /**
     * Sets the value of the attrValuePattern property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttrValuePattern(String value) {
        this.attrValuePattern = value;
    }

    /**
     * Gets the value of the dlServiceCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDlServiceCode() {
        return dlServiceCode;
    }

    /**
     * Sets the value of the dlServiceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDlServiceCode(String value) {
        this.dlServiceCode = value;
    }

    /**
     * Gets the value of the dlUpdateDtamp property.
     * 
     */
    public short getDlUpdateDtamp() {
        return dlUpdateDtamp;
    }

    /**
     * Sets the value of the dlUpdateDtamp property.
     * 
     */
    public void setDlUpdateDtamp(short value) {
        this.dlUpdateDtamp = value;
    }

    /**
     * Gets the value of the operatorId property.
     * 
     */
    public int getOperatorId() {
        return operatorId;
    }

    /**
     * Sets the value of the operatorId property.
     * 
     */
    public void setOperatorId(int value) {
        this.operatorId = value;
    }

    /**
     * Gets the value of the sysCreationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSysCreationDate() {
        return sysCreationDate;
    }

    /**
     * Sets the value of the sysCreationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSysCreationDate(XMLGregorianCalendar value) {
        this.sysCreationDate = value;
    }

    /**
     * Gets the value of the sysUpdateDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSysUpdateDate() {
        return sysUpdateDate;
    }

    /**
     * Sets the value of the sysUpdateDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSysUpdateDate(XMLGregorianCalendar value) {
        this.sysUpdateDate = value;
    }

}
