
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for AccountBillingInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountBillingInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BillingCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TaxExemptionDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="TaxExemptionIndicator" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountBillingInfo", propOrder = {
    "billingCurrency",
    "taxExemptionDate",
    "taxExemptionIndicator"
})
public class AccountBillingInfo {

    @XmlElement(name = "BillingCurrency")
    protected String billingCurrency;
    @XmlElement(name = "TaxExemptionDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar taxExemptionDate;
    @XmlElement(name = "TaxExemptionIndicator")
    protected byte taxExemptionIndicator;

    /**
     * Gets the value of the billingCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingCurrency() {
        return billingCurrency;
    }

    /**
     * Sets the value of the billingCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingCurrency(String value) {
        this.billingCurrency = value;
    }

    /**
     * Gets the value of the taxExemptionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTaxExemptionDate() {
        return taxExemptionDate;
    }

    /**
     * Sets the value of the taxExemptionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTaxExemptionDate(XMLGregorianCalendar value) {
        this.taxExemptionDate = value;
    }

    /**
     * Gets the value of the taxExemptionIndicator property.
     * 
     */
    public byte getTaxExemptionIndicator() {
        return taxExemptionIndicator;
    }

    /**
     * Sets the value of the taxExemptionIndicator property.
     * 
     */
    public void setTaxExemptionIndicator(byte value) {
        this.taxExemptionIndicator = value;
    }

}
