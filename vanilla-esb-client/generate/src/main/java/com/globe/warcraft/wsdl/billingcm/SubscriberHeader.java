
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubscriberHeader complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubscriberHeader">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubscriberIdInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}SubscriberIdInfo" minOccurs="0"/>
 *         &lt;element name="SubscriberGeneralInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}SubscriberGeneralInfo" minOccurs="0"/>
 *         &lt;element name="SubscriberTypeInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}SubscriberTypeInfo" minOccurs="0"/>
 *         &lt;element name="SubscriberStatusInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}SubscriberStatusInfo" minOccurs="0"/>
 *         &lt;element name="NameInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}NameInfo" minOccurs="0"/>
 *         &lt;element name="AddressInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}AddressInfo" minOccurs="0"/>
 *         &lt;element name="CustomerIdInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}CustomerIdInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubscriberHeader", propOrder = {
    "subscriberIdInfo",
    "subscriberGeneralInfo",
    "subscriberTypeInfo",
    "subscriberStatusInfo",
    "nameInfo",
    "addressInfo",
    "customerIdInfo"
})
public class SubscriberHeader {

    @XmlElement(name = "SubscriberIdInfo")
    protected SubscriberIdInfo subscriberIdInfo;
    @XmlElement(name = "SubscriberGeneralInfo")
    protected SubscriberGeneralInfo subscriberGeneralInfo;
    @XmlElement(name = "SubscriberTypeInfo")
    protected SubscriberTypeInfo subscriberTypeInfo;
    @XmlElement(name = "SubscriberStatusInfo")
    protected SubscriberStatusInfo subscriberStatusInfo;
    @XmlElement(name = "NameInfo")
    protected NameInfo nameInfo;
    @XmlElement(name = "AddressInfo")
    protected AddressInfo addressInfo;
    @XmlElement(name = "CustomerIdInfo")
    protected CustomerIdInfo customerIdInfo;

    /**
     * Gets the value of the subscriberIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link SubscriberIdInfo }
     *     
     */
    public SubscriberIdInfo getSubscriberIdInfo() {
        return subscriberIdInfo;
    }

    /**
     * Sets the value of the subscriberIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubscriberIdInfo }
     *     
     */
    public void setSubscriberIdInfo(SubscriberIdInfo value) {
        this.subscriberIdInfo = value;
    }

    /**
     * Gets the value of the subscriberGeneralInfo property.
     * 
     * @return
     *     possible object is
     *     {@link SubscriberGeneralInfo }
     *     
     */
    public SubscriberGeneralInfo getSubscriberGeneralInfo() {
        return subscriberGeneralInfo;
    }

    /**
     * Sets the value of the subscriberGeneralInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubscriberGeneralInfo }
     *     
     */
    public void setSubscriberGeneralInfo(SubscriberGeneralInfo value) {
        this.subscriberGeneralInfo = value;
    }

    /**
     * Gets the value of the subscriberTypeInfo property.
     * 
     * @return
     *     possible object is
     *     {@link SubscriberTypeInfo }
     *     
     */
    public SubscriberTypeInfo getSubscriberTypeInfo() {
        return subscriberTypeInfo;
    }

    /**
     * Sets the value of the subscriberTypeInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubscriberTypeInfo }
     *     
     */
    public void setSubscriberTypeInfo(SubscriberTypeInfo value) {
        this.subscriberTypeInfo = value;
    }

    /**
     * Gets the value of the subscriberStatusInfo property.
     * 
     * @return
     *     possible object is
     *     {@link SubscriberStatusInfo }
     *     
     */
    public SubscriberStatusInfo getSubscriberStatusInfo() {
        return subscriberStatusInfo;
    }

    /**
     * Sets the value of the subscriberStatusInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubscriberStatusInfo }
     *     
     */
    public void setSubscriberStatusInfo(SubscriberStatusInfo value) {
        this.subscriberStatusInfo = value;
    }

    /**
     * Gets the value of the nameInfo property.
     * 
     * @return
     *     possible object is
     *     {@link NameInfo }
     *     
     */
    public NameInfo getNameInfo() {
        return nameInfo;
    }

    /**
     * Sets the value of the nameInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link NameInfo }
     *     
     */
    public void setNameInfo(NameInfo value) {
        this.nameInfo = value;
    }

    /**
     * Gets the value of the addressInfo property.
     * 
     * @return
     *     possible object is
     *     {@link AddressInfo }
     *     
     */
    public AddressInfo getAddressInfo() {
        return addressInfo;
    }

    /**
     * Sets the value of the addressInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressInfo }
     *     
     */
    public void setAddressInfo(AddressInfo value) {
        this.addressInfo = value;
    }

    /**
     * Gets the value of the customerIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerIdInfo }
     *     
     */
    public CustomerIdInfo getCustomerIdInfo() {
        return customerIdInfo;
    }

    /**
     * Sets the value of the customerIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerIdInfo }
     *     
     */
    public void setCustomerIdInfo(CustomerIdInfo value) {
        this.customerIdInfo = value;
    }

}
