
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PayChannelNumberInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PayChannelNumberInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountNo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="BillingArrangementNo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PayChannelNo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PayChannelNumberInfo", propOrder = {
    "accountNo",
    "billingArrangementNo",
    "payChannelNo"
})
public class PayChannelNumberInfo {

    @XmlElement(name = "AccountNo")
    protected int accountNo;
    @XmlElement(name = "BillingArrangementNo")
    protected int billingArrangementNo;
    @XmlElement(name = "PayChannelNo")
    protected int payChannelNo;

    /**
     * Gets the value of the accountNo property.
     * 
     */
    public int getAccountNo() {
        return accountNo;
    }

    /**
     * Sets the value of the accountNo property.
     * 
     */
    public void setAccountNo(int value) {
        this.accountNo = value;
    }

    /**
     * Gets the value of the billingArrangementNo property.
     * 
     */
    public int getBillingArrangementNo() {
        return billingArrangementNo;
    }

    /**
     * Sets the value of the billingArrangementNo property.
     * 
     */
    public void setBillingArrangementNo(int value) {
        this.billingArrangementNo = value;
    }

    /**
     * Gets the value of the payChannelNo property.
     * 
     */
    public int getPayChannelNo() {
        return payChannelNo;
    }

    /**
     * Sets the value of the payChannelNo property.
     * 
     */
    public void setPayChannelNo(int value) {
        this.payChannelNo = value;
    }

}
