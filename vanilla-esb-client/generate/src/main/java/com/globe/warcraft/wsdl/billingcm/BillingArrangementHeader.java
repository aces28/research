
package com.globe.warcraft.wsdl.billingcm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BillingArrangementHeader complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BillingArrangementHeader">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountIdInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}AccountIdInfo" minOccurs="0"/>
 *         &lt;element name="AddressInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}AddressInfo" minOccurs="0"/>
 *         &lt;element name="NameInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}NameInfo" minOccurs="0"/>
 *         &lt;element name="BillingArrangementBillInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}BillingArrangementBillInfo" minOccurs="0"/>
 *         &lt;element name="BillingArrangementBusinessEntityIdInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}BusinessEntityIdInfo" minOccurs="0"/>
 *         &lt;element name="BillingArrangementGeneralInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}BillingArrangementGeneralInfo" minOccurs="0"/>
 *         &lt;element name="BillingArrangementIdInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}BillingArrangementIdInfo" minOccurs="0"/>
 *         &lt;element name="BillingArrangementNumberInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}BillingArrangementNumberInfo" minOccurs="0"/>
 *         &lt;element name="BillingArrangementStatusInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}BillingArrangementStatusInfo" minOccurs="0"/>
 *         &lt;element name="ExternalIdInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}ExternalIdInfo" minOccurs="0"/>
 *         &lt;element name="NameAddressInfoList" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="NameAddressInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}NameAddressInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BillingArrangementHeader", propOrder = {
    "accountIdInfo",
    "addressInfo",
    "nameInfo",
    "billingArrangementBillInfo",
    "billingArrangementBusinessEntityIdInfo",
    "billingArrangementGeneralInfo",
    "billingArrangementIdInfo",
    "billingArrangementNumberInfo",
    "billingArrangementStatusInfo",
    "externalIdInfo",
    "nameAddressInfoList"
})
public class BillingArrangementHeader {

    @XmlElement(name = "AccountIdInfo")
    protected AccountIdInfo accountIdInfo;
    @XmlElement(name = "AddressInfo")
    protected AddressInfo addressInfo;
    @XmlElement(name = "NameInfo")
    protected NameInfo nameInfo;
    @XmlElement(name = "BillingArrangementBillInfo")
    protected BillingArrangementBillInfo billingArrangementBillInfo;
    @XmlElement(name = "BillingArrangementBusinessEntityIdInfo")
    protected BusinessEntityIdInfo billingArrangementBusinessEntityIdInfo;
    @XmlElement(name = "BillingArrangementGeneralInfo")
    protected BillingArrangementGeneralInfo billingArrangementGeneralInfo;
    @XmlElement(name = "BillingArrangementIdInfo")
    protected BillingArrangementIdInfo billingArrangementIdInfo;
    @XmlElement(name = "BillingArrangementNumberInfo")
    protected BillingArrangementNumberInfo billingArrangementNumberInfo;
    @XmlElement(name = "BillingArrangementStatusInfo")
    protected BillingArrangementStatusInfo billingArrangementStatusInfo;
    @XmlElement(name = "ExternalIdInfo")
    protected ExternalIdInfo externalIdInfo;
    @XmlElement(name = "NameAddressInfoList")
    protected BillingArrangementHeader.NameAddressInfoList nameAddressInfoList;

    /**
     * Gets the value of the accountIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link AccountIdInfo }
     *     
     */
    public AccountIdInfo getAccountIdInfo() {
        return accountIdInfo;
    }

    /**
     * Sets the value of the accountIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountIdInfo }
     *     
     */
    public void setAccountIdInfo(AccountIdInfo value) {
        this.accountIdInfo = value;
    }

    /**
     * Gets the value of the addressInfo property.
     * 
     * @return
     *     possible object is
     *     {@link AddressInfo }
     *     
     */
    public AddressInfo getAddressInfo() {
        return addressInfo;
    }

    /**
     * Sets the value of the addressInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressInfo }
     *     
     */
    public void setAddressInfo(AddressInfo value) {
        this.addressInfo = value;
    }

    /**
     * Gets the value of the nameInfo property.
     * 
     * @return
     *     possible object is
     *     {@link NameInfo }
     *     
     */
    public NameInfo getNameInfo() {
        return nameInfo;
    }

    /**
     * Sets the value of the nameInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link NameInfo }
     *     
     */
    public void setNameInfo(NameInfo value) {
        this.nameInfo = value;
    }

    /**
     * Gets the value of the billingArrangementBillInfo property.
     * 
     * @return
     *     possible object is
     *     {@link BillingArrangementBillInfo }
     *     
     */
    public BillingArrangementBillInfo getBillingArrangementBillInfo() {
        return billingArrangementBillInfo;
    }

    /**
     * Sets the value of the billingArrangementBillInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillingArrangementBillInfo }
     *     
     */
    public void setBillingArrangementBillInfo(BillingArrangementBillInfo value) {
        this.billingArrangementBillInfo = value;
    }

    /**
     * Gets the value of the billingArrangementBusinessEntityIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link BusinessEntityIdInfo }
     *     
     */
    public BusinessEntityIdInfo getBillingArrangementBusinessEntityIdInfo() {
        return billingArrangementBusinessEntityIdInfo;
    }

    /**
     * Sets the value of the billingArrangementBusinessEntityIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessEntityIdInfo }
     *     
     */
    public void setBillingArrangementBusinessEntityIdInfo(BusinessEntityIdInfo value) {
        this.billingArrangementBusinessEntityIdInfo = value;
    }

    /**
     * Gets the value of the billingArrangementGeneralInfo property.
     * 
     * @return
     *     possible object is
     *     {@link BillingArrangementGeneralInfo }
     *     
     */
    public BillingArrangementGeneralInfo getBillingArrangementGeneralInfo() {
        return billingArrangementGeneralInfo;
    }

    /**
     * Sets the value of the billingArrangementGeneralInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillingArrangementGeneralInfo }
     *     
     */
    public void setBillingArrangementGeneralInfo(BillingArrangementGeneralInfo value) {
        this.billingArrangementGeneralInfo = value;
    }

    /**
     * Gets the value of the billingArrangementIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link BillingArrangementIdInfo }
     *     
     */
    public BillingArrangementIdInfo getBillingArrangementIdInfo() {
        return billingArrangementIdInfo;
    }

    /**
     * Sets the value of the billingArrangementIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillingArrangementIdInfo }
     *     
     */
    public void setBillingArrangementIdInfo(BillingArrangementIdInfo value) {
        this.billingArrangementIdInfo = value;
    }

    /**
     * Gets the value of the billingArrangementNumberInfo property.
     * 
     * @return
     *     possible object is
     *     {@link BillingArrangementNumberInfo }
     *     
     */
    public BillingArrangementNumberInfo getBillingArrangementNumberInfo() {
        return billingArrangementNumberInfo;
    }

    /**
     * Sets the value of the billingArrangementNumberInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillingArrangementNumberInfo }
     *     
     */
    public void setBillingArrangementNumberInfo(BillingArrangementNumberInfo value) {
        this.billingArrangementNumberInfo = value;
    }

    /**
     * Gets the value of the billingArrangementStatusInfo property.
     * 
     * @return
     *     possible object is
     *     {@link BillingArrangementStatusInfo }
     *     
     */
    public BillingArrangementStatusInfo getBillingArrangementStatusInfo() {
        return billingArrangementStatusInfo;
    }

    /**
     * Sets the value of the billingArrangementStatusInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillingArrangementStatusInfo }
     *     
     */
    public void setBillingArrangementStatusInfo(BillingArrangementStatusInfo value) {
        this.billingArrangementStatusInfo = value;
    }

    /**
     * Gets the value of the externalIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ExternalIdInfo }
     *     
     */
    public ExternalIdInfo getExternalIdInfo() {
        return externalIdInfo;
    }

    /**
     * Sets the value of the externalIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExternalIdInfo }
     *     
     */
    public void setExternalIdInfo(ExternalIdInfo value) {
        this.externalIdInfo = value;
    }

    /**
     * Gets the value of the nameAddressInfoList property.
     * 
     * @return
     *     possible object is
     *     {@link BillingArrangementHeader.NameAddressInfoList }
     *     
     */
    public BillingArrangementHeader.NameAddressInfoList getNameAddressInfoList() {
        return nameAddressInfoList;
    }

    /**
     * Sets the value of the nameAddressInfoList property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillingArrangementHeader.NameAddressInfoList }
     *     
     */
    public void setNameAddressInfoList(BillingArrangementHeader.NameAddressInfoList value) {
        this.nameAddressInfoList = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="NameAddressInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}NameAddressInfo" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nameAddressInfo"
    })
    public static class NameAddressInfoList {

        @XmlElement(name = "NameAddressInfo")
        protected List<NameAddressInfo> nameAddressInfo;

        /**
         * Gets the value of the nameAddressInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the nameAddressInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getNameAddressInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link NameAddressInfo }
         * 
         * 
         */
        public List<NameAddressInfo> getNameAddressInfo() {
            if (nameAddressInfo == null) {
                nameAddressInfo = new ArrayList<NameAddressInfo>();
            }
            return this.nameAddressInfo;
        }

    }

}
