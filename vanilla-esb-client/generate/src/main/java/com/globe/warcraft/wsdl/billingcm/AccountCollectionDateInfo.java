
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for AccountCollectionDateInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountCollectionDateInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CollectionInd" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="CollectionStartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountCollectionDateInfo", propOrder = {
    "collectionInd",
    "collectionStartDate"
})
public class AccountCollectionDateInfo {

    @XmlElement(name = "CollectionInd")
    protected byte collectionInd;
    @XmlElement(name = "CollectionStartDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar collectionStartDate;

    /**
     * Gets the value of the collectionInd property.
     * 
     */
    public byte getCollectionInd() {
        return collectionInd;
    }

    /**
     * Sets the value of the collectionInd property.
     * 
     */
    public void setCollectionInd(byte value) {
        this.collectionInd = value;
    }

    /**
     * Gets the value of the collectionStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCollectionStartDate() {
        return collectionStartDate;
    }

    /**
     * Sets the value of the collectionStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCollectionStartDate(XMLGregorianCalendar value) {
        this.collectionStartDate = value;
    }

}
