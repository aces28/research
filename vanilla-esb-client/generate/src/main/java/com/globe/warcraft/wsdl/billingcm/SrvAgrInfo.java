
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SrvAgrInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SrvAgrInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Soc">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OfferInstanceId">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SrvAgrInfo", propOrder = {
    "soc",
    "offerInstanceId"
})
public class SrvAgrInfo {

    @XmlElement(name = "Soc", required = true)
    protected String soc;
    @XmlElement(name = "OfferInstanceId")
    protected int offerInstanceId;

    /**
     * Gets the value of the soc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoc() {
        return soc;
    }

    /**
     * Sets the value of the soc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoc(String value) {
        this.soc = value;
    }

    /**
     * Gets the value of the offerInstanceId property.
     * 
     */
    public int getOfferInstanceId() {
        return offerInstanceId;
    }

    /**
     * Sets the value of the offerInstanceId property.
     * 
     */
    public void setOfferInstanceId(int value) {
        this.offerInstanceId = value;
    }

}
