
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreateNewCMSubscriberResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreateNewCMSubscriberResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreateNewCMSubscriberResult" type="{http://www.globe.com/warcraft/wsdl/billingcm/}CreateNewCMSubscriberResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateNewCMSubscriberResponse", propOrder = {
    "createNewCMSubscriberResult"
})
public class CreateNewCMSubscriberResponse {

    @XmlElement(name = "CreateNewCMSubscriberResult")
    protected CreateNewCMSubscriberResult createNewCMSubscriberResult;

    /**
     * Gets the value of the createNewCMSubscriberResult property.
     * 
     * @return
     *     possible object is
     *     {@link CreateNewCMSubscriberResult }
     *     
     */
    public CreateNewCMSubscriberResult getCreateNewCMSubscriberResult() {
        return createNewCMSubscriberResult;
    }

    /**
     * Sets the value of the createNewCMSubscriberResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateNewCMSubscriberResult }
     *     
     */
    public void setCreateNewCMSubscriberResult(CreateNewCMSubscriberResult value) {
        this.createNewCMSubscriberResult = value;
    }

}
