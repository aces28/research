
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for AddressInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddressInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AddressElement1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddressElement2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddressElement3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddressElement4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddressElement5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddressElement6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddressElement7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddressElement8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddressElement9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddressElement10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddressElement11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddressElement12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddressElement13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddressElement14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddressElement15" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddressLine1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddressLine2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddressLine3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddressLine4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddressType" type="{http://www.w3.org/2001/XMLSchema}byte" minOccurs="0"/>
 *         &lt;element name="AddressUpdateType" type="{http://www.w3.org/2001/XMLSchema}byte" minOccurs="0"/>
 *         &lt;element name="EffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ExpirationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="LinkType" type="{http://www.w3.org/2001/XMLSchema}byte" minOccurs="0"/>
 *         &lt;element name="SinceDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ValidityCode" type="{http://www.w3.org/2001/XMLSchema}byte" minOccurs="0"/>
 *         &lt;element name="VerificationInd" type="{http://www.w3.org/2001/XMLSchema}byte" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddressInfo", propOrder = {
    "addressElement1",
    "addressElement2",
    "addressElement3",
    "addressElement4",
    "addressElement5",
    "addressElement6",
    "addressElement7",
    "addressElement8",
    "addressElement9",
    "addressElement10",
    "addressElement11",
    "addressElement12",
    "addressElement13",
    "addressElement14",
    "addressElement15",
    "addressLine1",
    "addressLine2",
    "addressLine3",
    "addressLine4",
    "addressType",
    "addressUpdateType",
    "effectiveDate",
    "expirationDate",
    "linkType",
    "sinceDate",
    "validityCode",
    "verificationInd"
})
public class AddressInfo {

    @XmlElement(name = "AddressElement1")
    protected String addressElement1;
    @XmlElement(name = "AddressElement2")
    protected String addressElement2;
    @XmlElement(name = "AddressElement3")
    protected String addressElement3;
    @XmlElement(name = "AddressElement4")
    protected String addressElement4;
    @XmlElement(name = "AddressElement5")
    protected String addressElement5;
    @XmlElement(name = "AddressElement6")
    protected String addressElement6;
    @XmlElement(name = "AddressElement7")
    protected String addressElement7;
    @XmlElement(name = "AddressElement8")
    protected String addressElement8;
    @XmlElement(name = "AddressElement9")
    protected String addressElement9;
    @XmlElement(name = "AddressElement10")
    protected String addressElement10;
    @XmlElement(name = "AddressElement11")
    protected String addressElement11;
    @XmlElement(name = "AddressElement12")
    protected String addressElement12;
    @XmlElement(name = "AddressElement13")
    protected String addressElement13;
    @XmlElement(name = "AddressElement14")
    protected String addressElement14;
    @XmlElement(name = "AddressElement15")
    protected String addressElement15;
    @XmlElement(name = "AddressLine1")
    protected String addressLine1;
    @XmlElement(name = "AddressLine2")
    protected String addressLine2;
    @XmlElement(name = "AddressLine3")
    protected String addressLine3;
    @XmlElement(name = "AddressLine4")
    protected String addressLine4;
    @XmlElement(name = "AddressType")
    protected Byte addressType;
    @XmlElement(name = "AddressUpdateType")
    protected Byte addressUpdateType;
    @XmlElement(name = "EffectiveDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effectiveDate;
    @XmlElement(name = "ExpirationDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expirationDate;
    @XmlElement(name = "LinkType")
    protected Byte linkType;
    @XmlElement(name = "SinceDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar sinceDate;
    @XmlElement(name = "ValidityCode")
    protected Byte validityCode;
    @XmlElement(name = "VerificationInd")
    protected Byte verificationInd;

    /**
     * Gets the value of the addressElement1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement1() {
        return addressElement1;
    }

    /**
     * Sets the value of the addressElement1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement1(String value) {
        this.addressElement1 = value;
    }

    /**
     * Gets the value of the addressElement2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement2() {
        return addressElement2;
    }

    /**
     * Sets the value of the addressElement2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement2(String value) {
        this.addressElement2 = value;
    }

    /**
     * Gets the value of the addressElement3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement3() {
        return addressElement3;
    }

    /**
     * Sets the value of the addressElement3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement3(String value) {
        this.addressElement3 = value;
    }

    /**
     * Gets the value of the addressElement4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement4() {
        return addressElement4;
    }

    /**
     * Sets the value of the addressElement4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement4(String value) {
        this.addressElement4 = value;
    }

    /**
     * Gets the value of the addressElement5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement5() {
        return addressElement5;
    }

    /**
     * Sets the value of the addressElement5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement5(String value) {
        this.addressElement5 = value;
    }

    /**
     * Gets the value of the addressElement6 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement6() {
        return addressElement6;
    }

    /**
     * Sets the value of the addressElement6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement6(String value) {
        this.addressElement6 = value;
    }

    /**
     * Gets the value of the addressElement7 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement7() {
        return addressElement7;
    }

    /**
     * Sets the value of the addressElement7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement7(String value) {
        this.addressElement7 = value;
    }

    /**
     * Gets the value of the addressElement8 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement8() {
        return addressElement8;
    }

    /**
     * Sets the value of the addressElement8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement8(String value) {
        this.addressElement8 = value;
    }

    /**
     * Gets the value of the addressElement9 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement9() {
        return addressElement9;
    }

    /**
     * Sets the value of the addressElement9 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement9(String value) {
        this.addressElement9 = value;
    }

    /**
     * Gets the value of the addressElement10 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement10() {
        return addressElement10;
    }

    /**
     * Sets the value of the addressElement10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement10(String value) {
        this.addressElement10 = value;
    }

    /**
     * Gets the value of the addressElement11 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement11() {
        return addressElement11;
    }

    /**
     * Sets the value of the addressElement11 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement11(String value) {
        this.addressElement11 = value;
    }

    /**
     * Gets the value of the addressElement12 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement12() {
        return addressElement12;
    }

    /**
     * Sets the value of the addressElement12 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement12(String value) {
        this.addressElement12 = value;
    }

    /**
     * Gets the value of the addressElement13 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement13() {
        return addressElement13;
    }

    /**
     * Sets the value of the addressElement13 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement13(String value) {
        this.addressElement13 = value;
    }

    /**
     * Gets the value of the addressElement14 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement14() {
        return addressElement14;
    }

    /**
     * Sets the value of the addressElement14 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement14(String value) {
        this.addressElement14 = value;
    }

    /**
     * Gets the value of the addressElement15 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressElement15() {
        return addressElement15;
    }

    /**
     * Sets the value of the addressElement15 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressElement15(String value) {
        this.addressElement15 = value;
    }

    /**
     * Gets the value of the addressLine1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressLine1() {
        return addressLine1;
    }

    /**
     * Sets the value of the addressLine1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressLine1(String value) {
        this.addressLine1 = value;
    }

    /**
     * Gets the value of the addressLine2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressLine2() {
        return addressLine2;
    }

    /**
     * Sets the value of the addressLine2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressLine2(String value) {
        this.addressLine2 = value;
    }

    /**
     * Gets the value of the addressLine3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressLine3() {
        return addressLine3;
    }

    /**
     * Sets the value of the addressLine3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressLine3(String value) {
        this.addressLine3 = value;
    }

    /**
     * Gets the value of the addressLine4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressLine4() {
        return addressLine4;
    }

    /**
     * Sets the value of the addressLine4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressLine4(String value) {
        this.addressLine4 = value;
    }

    /**
     * Gets the value of the addressType property.
     * 
     * @return
     *     possible object is
     *     {@link Byte }
     *     
     */
    public Byte getAddressType() {
        return addressType;
    }

    /**
     * Sets the value of the addressType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Byte }
     *     
     */
    public void setAddressType(Byte value) {
        this.addressType = value;
    }

    /**
     * Gets the value of the addressUpdateType property.
     * 
     * @return
     *     possible object is
     *     {@link Byte }
     *     
     */
    public Byte getAddressUpdateType() {
        return addressUpdateType;
    }

    /**
     * Sets the value of the addressUpdateType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Byte }
     *     
     */
    public void setAddressUpdateType(Byte value) {
        this.addressUpdateType = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDate(XMLGregorianCalendar value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the linkType property.
     * 
     * @return
     *     possible object is
     *     {@link Byte }
     *     
     */
    public Byte getLinkType() {
        return linkType;
    }

    /**
     * Sets the value of the linkType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Byte }
     *     
     */
    public void setLinkType(Byte value) {
        this.linkType = value;
    }

    /**
     * Gets the value of the sinceDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSinceDate() {
        return sinceDate;
    }

    /**
     * Sets the value of the sinceDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSinceDate(XMLGregorianCalendar value) {
        this.sinceDate = value;
    }

    /**
     * Gets the value of the validityCode property.
     * 
     * @return
     *     possible object is
     *     {@link Byte }
     *     
     */
    public Byte getValidityCode() {
        return validityCode;
    }

    /**
     * Sets the value of the validityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Byte }
     *     
     */
    public void setValidityCode(Byte value) {
        this.validityCode = value;
    }

    /**
     * Gets the value of the verificationInd property.
     * 
     * @return
     *     possible object is
     *     {@link Byte }
     *     
     */
    public Byte getVerificationInd() {
        return verificationInd;
    }

    /**
     * Sets the value of the verificationInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Byte }
     *     
     */
    public void setVerificationInd(Byte value) {
        this.verificationInd = value;
    }

}
