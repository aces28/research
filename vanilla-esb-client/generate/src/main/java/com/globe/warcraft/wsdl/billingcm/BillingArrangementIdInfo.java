
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BillingArrangementIdInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BillingArrangementIdInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BillingArrangementId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BillingArrangementIdInfo", propOrder = {
    "billingArrangementId"
})
public class BillingArrangementIdInfo {

    @XmlElement(name = "BillingArrangementId")
    protected int billingArrangementId;

    /**
     * Gets the value of the billingArrangementId property.
     * 
     */
    public int getBillingArrangementId() {
        return billingArrangementId;
    }

    /**
     * Sets the value of the billingArrangementId property.
     * 
     */
    public void setBillingArrangementId(int value) {
        this.billingArrangementId = value;
    }

}
