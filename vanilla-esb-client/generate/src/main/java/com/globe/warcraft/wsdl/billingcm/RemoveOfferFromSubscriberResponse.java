
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RemoveOfferFromSubscriberResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RemoveOfferFromSubscriberResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RemoveOfferFromSubscriberResult" type="{http://www.globe.com/warcraft/wsdl/billingcm/}RemoveOfferFromSubscriberResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RemoveOfferFromSubscriberResponse", propOrder = {
    "removeOfferFromSubscriberResult"
})
public class RemoveOfferFromSubscriberResponse {

    @XmlElement(name = "RemoveOfferFromSubscriberResult")
    protected RemoveOfferFromSubscriberResult removeOfferFromSubscriberResult;

    /**
     * Gets the value of the removeOfferFromSubscriberResult property.
     * 
     * @return
     *     possible object is
     *     {@link RemoveOfferFromSubscriberResult }
     *     
     */
    public RemoveOfferFromSubscriberResult getRemoveOfferFromSubscriberResult() {
        return removeOfferFromSubscriberResult;
    }

    /**
     * Sets the value of the removeOfferFromSubscriberResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link RemoveOfferFromSubscriberResult }
     *     
     */
    public void setRemoveOfferFromSubscriberResult(RemoveOfferFromSubscriberResult value) {
        this.removeOfferFromSubscriberResult = value;
    }

}
