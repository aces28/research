
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for CM3AccountStatusInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CM3AccountStatusInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="L3AccountStatus" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="L3StsChngDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="L3StsReasonCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CM3AccountStatusInfo", propOrder = {
    "l3AccountStatus",
    "l3StsChngDate",
    "l3StsReasonCd"
})
public class CM3AccountStatusInfo {

    @XmlElement(name = "L3AccountStatus")
    protected byte l3AccountStatus;
    @XmlElement(name = "L3StsChngDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar l3StsChngDate;
    @XmlElement(name = "L3StsReasonCd")
    protected String l3StsReasonCd;

    /**
     * Gets the value of the l3AccountStatus property.
     * 
     */
    public byte getL3AccountStatus() {
        return l3AccountStatus;
    }

    /**
     * Sets the value of the l3AccountStatus property.
     * 
     */
    public void setL3AccountStatus(byte value) {
        this.l3AccountStatus = value;
    }

    /**
     * Gets the value of the l3StsChngDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getL3StsChngDate() {
        return l3StsChngDate;
    }

    /**
     * Sets the value of the l3StsChngDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setL3StsChngDate(XMLGregorianCalendar value) {
        this.l3StsChngDate = value;
    }

    /**
     * Gets the value of the l3StsReasonCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getL3StsReasonCd() {
        return l3StsReasonCd;
    }

    /**
     * Sets the value of the l3StsReasonCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setL3StsReasonCd(String value) {
        this.l3StsReasonCd = value;
    }

}
