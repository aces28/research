
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.globe.warcraft.wsdl.billingcm package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreateNewCMSubscriberResultResultNameSpace_QNAME = new QName("", "ResultNameSpace");
    private final static QName _UpdateOfferOfSubscriber_QNAME = new QName("http://www.globe.com/warcraft/wsdl/billingcm/", "UpdateOfferOfSubscriber");
    private final static QName _RemoveOfferFromSubscriberResponse_QNAME = new QName("http://www.globe.com/warcraft/wsdl/billingcm/", "RemoveOfferFromSubscriberResponse");
    private final static QName _UpdateOfferOfSubscriberResponse_QNAME = new QName("http://www.globe.com/warcraft/wsdl/billingcm/", "UpdateOfferOfSubscriberResponse");
    private final static QName _ChangeCustomerBillingCycleInfo_QNAME = new QName("http://www.globe.com/warcraft/wsdl/billingcm/", "changeCustomerBillingCycleInfo");
    private final static QName _GetDetailsByMsisdn_QNAME = new QName("http://www.globe.com/warcraft/wsdl/billingcm/", "GetDetailsByMsisdn");
    private final static QName _AddOfferToSubscriber_QNAME = new QName("http://www.globe.com/warcraft/wsdl/billingcm/", "AddOfferToSubscriber");
    private final static QName _CreateNewCMSubscriber_QNAME = new QName("http://www.globe.com/warcraft/wsdl/billingcm/", "CreateNewCMSubscriber");
    private final static QName _RemoveOfferFromSubscriber_QNAME = new QName("http://www.globe.com/warcraft/wsdl/billingcm/", "RemoveOfferFromSubscriber");
    private final static QName _ChangeCustomerBillingCycleInfoResponse_QNAME = new QName("http://www.globe.com/warcraft/wsdl/billingcm/", "ChangeCustomerBillingCycleInfoResponse");
    private final static QName _ValidateIfGroupOwnerResponse_QNAME = new QName("http://www.globe.com/warcraft/wsdl/billingcm/", "ValidateIfGroupOwnerResponse");
    private final static QName _CreateNewCMSubscriberResponse_QNAME = new QName("http://www.globe.com/warcraft/wsdl/billingcm/", "CreateNewCMSubscriberResponse");
    private final static QName _GetDetailsByMsisdnResponse_QNAME = new QName("http://www.globe.com/warcraft/wsdl/billingcm/", "GetDetailsByMsisdnResponse");
    private final static QName _GetNextDsaCustomer_QNAME = new QName("http://www.globe.com/warcraft/wsdl/billingcm/", "GetNextDsaCustomer");
    private final static QName _GetNextDsaCustomerResponse_QNAME = new QName("http://www.globe.com/warcraft/wsdl/billingcm/", "GetNextDsaCustomerResponse");
    private final static QName _UpdateSubscriberResourcesByMsisdn_QNAME = new QName("http://www.globe.com/warcraft/wsdl/billingcm/", "UpdateSubscriberResourcesByMsisdn");
    private final static QName _UpdateSubscriberResourcesByMsisdnResponse_QNAME = new QName("http://www.globe.com/warcraft/wsdl/billingcm/", "UpdateSubscriberResourcesByMsisdnResponse");
    private final static QName _AddOfferToSubscriberResponse_QNAME = new QName("http://www.globe.com/warcraft/wsdl/billingcm/", "AddOfferToSubscriberResponse");
    private final static QName _ValidateIfGroupOwner_QNAME = new QName("http://www.globe.com/warcraft/wsdl/billingcm/", "ValidateIfGroupOwner");
    private final static QName _ResourceInfoType_QNAME = new QName("", "Type");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.globe.warcraft.wsdl.billingcm
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OffersParameterInfo }
     * 
     */
    public OffersParameterInfo createOffersParameterInfo() {
        return new OffersParameterInfo();
    }

    /**
     * Create an instance of {@link UserGroupResourceInfo }
     * 
     */
    public UserGroupResourceInfo createUserGroupResourceInfo() {
        return new UserGroupResourceInfo();
    }

    /**
     * Create an instance of {@link OffersLogicalResourceInfo }
     * 
     */
    public OffersLogicalResourceInfo createOffersLogicalResourceInfo() {
        return new OffersLogicalResourceInfo();
    }

    /**
     * Create an instance of {@link ChangeLogicalResourceInputInfoModel }
     * 
     */
    public ChangeLogicalResourceInputInfoModel createChangeLogicalResourceInputInfoModel() {
        return new ChangeLogicalResourceInputInfoModel();
    }

    /**
     * Create an instance of {@link ParameterInfo }
     * 
     */
    public ParameterInfo createParameterInfo() {
        return new ParameterInfo();
    }

    /**
     * Create an instance of {@link LogicalResourceInfoModel }
     * 
     */
    public LogicalResourceInfoModel createLogicalResourceInfoModel() {
        return new LogicalResourceInfoModel();
    }

    /**
     * Create an instance of {@link ResourceRange }
     * 
     */
    public ResourceRange createResourceRange() {
        return new ResourceRange();
    }

    /**
     * Create an instance of {@link ReplacePhysicalResourceInputInfoModel }
     * 
     */
    public ReplacePhysicalResourceInputInfoModel createReplacePhysicalResourceInputInfoModel() {
        return new ReplacePhysicalResourceInputInfoModel();
    }

    /**
     * Create an instance of {@link UnifiedResourceDetailsInfo }
     * 
     */
    public UnifiedResourceDetailsInfo createUnifiedResourceDetailsInfo() {
        return new UnifiedResourceDetailsInfo();
    }

    /**
     * Create an instance of {@link PhysicalResourceInfoModel }
     * 
     */
    public PhysicalResourceInfoModel createPhysicalResourceInfoModel() {
        return new PhysicalResourceInfoModel();
    }

    /**
     * Create an instance of {@link BillingArrangementHeader }
     * 
     */
    public BillingArrangementHeader createBillingArrangementHeader() {
        return new BillingArrangementHeader();
    }

    /**
     * Create an instance of {@link UnifiedResourceCategoryInfo }
     * 
     */
    public UnifiedResourceCategoryInfo createUnifiedResourceCategoryInfo() {
        return new UnifiedResourceCategoryInfo();
    }

    /**
     * Create an instance of {@link AccountCollectionInfo }
     * 
     */
    public AccountCollectionInfo createAccountCollectionInfo() {
        return new AccountCollectionInfo();
    }

    /**
     * Create an instance of {@link GuidingResourceInfo }
     * 
     */
    public GuidingResourceInfo createGuidingResourceInfo() {
        return new GuidingResourceInfo();
    }

    /**
     * Create an instance of {@link CreateNewCMSubscriber }
     * 
     */
    public CreateNewCMSubscriber createCreateNewCMSubscriber() {
        return new CreateNewCMSubscriber();
    }

    /**
     * Create an instance of {@link RemoveOfferFromSubscriber }
     * 
     */
    public RemoveOfferFromSubscriber createRemoveOfferFromSubscriber() {
        return new RemoveOfferFromSubscriber();
    }

    /**
     * Create an instance of {@link AddOfferToSubscriber }
     * 
     */
    public AddOfferToSubscriber createAddOfferToSubscriber() {
        return new AddOfferToSubscriber();
    }

    /**
     * Create an instance of {@link UpdateOfferOfSubscriber }
     * 
     */
    public UpdateOfferOfSubscriber createUpdateOfferOfSubscriber() {
        return new UpdateOfferOfSubscriber();
    }

    /**
     * Create an instance of {@link UpdateSubscriberResourcesByMsisdn }
     * 
     */
    public UpdateSubscriberResourcesByMsisdn createUpdateSubscriberResourcesByMsisdn() {
        return new UpdateSubscriberResourcesByMsisdn();
    }

    /**
     * Create an instance of {@link CreateNewCMSubscriberResponse }
     * 
     */
    public CreateNewCMSubscriberResponse createCreateNewCMSubscriberResponse() {
        return new CreateNewCMSubscriberResponse();
    }

    /**
     * Create an instance of {@link GetDetailsByMsisdnResponse }
     * 
     */
    public GetDetailsByMsisdnResponse createGetDetailsByMsisdnResponse() {
        return new GetDetailsByMsisdnResponse();
    }

    /**
     * Create an instance of {@link GetNextDsaCustomer }
     * 
     */
    public GetNextDsaCustomer createGetNextDsaCustomer() {
        return new GetNextDsaCustomer();
    }

    /**
     * Create an instance of {@link GetNextDsaCustomerResponse }
     * 
     */
    public GetNextDsaCustomerResponse createGetNextDsaCustomerResponse() {
        return new GetNextDsaCustomerResponse();
    }

    /**
     * Create an instance of {@link UpdateSubscriberResourcesByMsisdnResponse }
     * 
     */
    public UpdateSubscriberResourcesByMsisdnResponse createUpdateSubscriberResourcesByMsisdnResponse() {
        return new UpdateSubscriberResourcesByMsisdnResponse();
    }

    /**
     * Create an instance of {@link AddOfferToSubscriberResponse }
     * 
     */
    public AddOfferToSubscriberResponse createAddOfferToSubscriberResponse() {
        return new AddOfferToSubscriberResponse();
    }

    /**
     * Create an instance of {@link ValidateIfGroupOwner }
     * 
     */
    public ValidateIfGroupOwner createValidateIfGroupOwner() {
        return new ValidateIfGroupOwner();
    }

    /**
     * Create an instance of {@link RemoveOfferFromSubscriberResponse }
     * 
     */
    public RemoveOfferFromSubscriberResponse createRemoveOfferFromSubscriberResponse() {
        return new RemoveOfferFromSubscriberResponse();
    }

    /**
     * Create an instance of {@link UpdateOfferOfSubscriberResponse }
     * 
     */
    public UpdateOfferOfSubscriberResponse createUpdateOfferOfSubscriberResponse() {
        return new UpdateOfferOfSubscriberResponse();
    }

    /**
     * Create an instance of {@link ChangeCustomerBillingCycleInfo }
     * 
     */
    public ChangeCustomerBillingCycleInfo createChangeCustomerBillingCycleInfo() {
        return new ChangeCustomerBillingCycleInfo();
    }

    /**
     * Create an instance of {@link GetDetailsByMsisdn }
     * 
     */
    public GetDetailsByMsisdn createGetDetailsByMsisdn() {
        return new GetDetailsByMsisdn();
    }

    /**
     * Create an instance of {@link ChangeCustomerBillingCycleInfoResponse }
     * 
     */
    public ChangeCustomerBillingCycleInfoResponse createChangeCustomerBillingCycleInfoResponse() {
        return new ChangeCustomerBillingCycleInfoResponse();
    }

    /**
     * Create an instance of {@link ValidateIfGroupOwnerResponse }
     * 
     */
    public ValidateIfGroupOwnerResponse createValidateIfGroupOwnerResponse() {
        return new ValidateIfGroupOwnerResponse();
    }

    /**
     * Create an instance of {@link NameAddressInfo }
     * 
     */
    public NameAddressInfo createNameAddressInfo() {
        return new NameAddressInfo();
    }

    /**
     * Create an instance of {@link CreateNewCMSubscriberResult }
     * 
     */
    public CreateNewCMSubscriberResult createCreateNewCMSubscriberResult() {
        return new CreateNewCMSubscriberResult();
    }

    /**
     * Create an instance of {@link ResourceInfo }
     * 
     */
    public ResourceInfo createResourceInfo() {
        return new ResourceInfo();
    }

    /**
     * Create an instance of {@link AddressDetails }
     * 
     */
    public AddressDetails createAddressDetails() {
        return new AddressDetails();
    }

    /**
     * Create an instance of {@link ChangeCustomerBillingCycleInfoResult }
     * 
     */
    public ChangeCustomerBillingCycleInfoResult createChangeCustomerBillingCycleInfoResult() {
        return new ChangeCustomerBillingCycleInfoResult();
    }

    /**
     * Create an instance of {@link UpdateOfferOfSubscriberResult }
     * 
     */
    public UpdateOfferOfSubscriberResult createUpdateOfferOfSubscriberResult() {
        return new UpdateOfferOfSubscriberResult();
    }

    /**
     * Create an instance of {@link Offer }
     * 
     */
    public Offer createOffer() {
        return new Offer();
    }

    /**
     * Create an instance of {@link AddOfferToSubscriberResult }
     * 
     */
    public AddOfferToSubscriberResult createAddOfferToSubscriberResult() {
        return new AddOfferToSubscriberResult();
    }

    /**
     * Create an instance of {@link StatusInfo }
     * 
     */
    public StatusInfo createStatusInfo() {
        return new StatusInfo();
    }

    /**
     * Create an instance of {@link AccountGeneralInfo }
     * 
     */
    public AccountGeneralInfo createAccountGeneralInfo() {
        return new AccountGeneralInfo();
    }

    /**
     * Create an instance of {@link PayChannelPaymentMethodInfo }
     * 
     */
    public PayChannelPaymentMethodInfo createPayChannelPaymentMethodInfo() {
        return new PayChannelPaymentMethodInfo();
    }

    /**
     * Create an instance of {@link BillingArrangementBillInfo }
     * 
     */
    public BillingArrangementBillInfo createBillingArrangementBillInfo() {
        return new BillingArrangementBillInfo();
    }

    /**
     * Create an instance of {@link GetNextDsaCustomerResult }
     * 
     */
    public GetNextDsaCustomerResult createGetNextDsaCustomerResult() {
        return new GetNextDsaCustomerResult();
    }

    /**
     * Create an instance of {@link PayChannelNumberInfo }
     * 
     */
    public PayChannelNumberInfo createPayChannelNumberInfo() {
        return new PayChannelNumberInfo();
    }

    /**
     * Create an instance of {@link AttributesData }
     * 
     */
    public AttributesData createAttributesData() {
        return new AttributesData();
    }

    /**
     * Create an instance of {@link AccountHeader }
     * 
     */
    public AccountHeader createAccountHeader() {
        return new AccountHeader();
    }

    /**
     * Create an instance of {@link GetDetailsByMsisdnResult }
     * 
     */
    public GetDetailsByMsisdnResult createGetDetailsByMsisdnResult() {
        return new GetDetailsByMsisdnResult();
    }

    /**
     * Create an instance of {@link SubscriberIdInfo }
     * 
     */
    public SubscriberIdInfo createSubscriberIdInfo() {
        return new SubscriberIdInfo();
    }

    /**
     * Create an instance of {@link PunishmentLevelInfo }
     * 
     */
    public PunishmentLevelInfo createPunishmentLevelInfo() {
        return new PunishmentLevelInfo();
    }

    /**
     * Create an instance of {@link BusinessEntityIdInfo }
     * 
     */
    public BusinessEntityIdInfo createBusinessEntityIdInfo() {
        return new BusinessEntityIdInfo();
    }

    /**
     * Create an instance of {@link AccountingManagementInfo }
     * 
     */
    public AccountingManagementInfo createAccountingManagementInfo() {
        return new AccountingManagementInfo();
    }

    /**
     * Create an instance of {@link BillingArrangementGeneralInfo }
     * 
     */
    public BillingArrangementGeneralInfo createBillingArrangementGeneralInfo() {
        return new BillingArrangementGeneralInfo();
    }

    /**
     * Create an instance of {@link AccountIdInfo }
     * 
     */
    public AccountIdInfo createAccountIdInfo() {
        return new AccountIdInfo();
    }

    /**
     * Create an instance of {@link AccountCollectionDateInfo }
     * 
     */
    public AccountCollectionDateInfo createAccountCollectionDateInfo() {
        return new AccountCollectionDateInfo();
    }

    /**
     * Create an instance of {@link PayChannelGeneralInfo }
     * 
     */
    public PayChannelGeneralInfo createPayChannelGeneralInfo() {
        return new PayChannelGeneralInfo();
    }

    /**
     * Create an instance of {@link PayChannelStatusInfo }
     * 
     */
    public PayChannelStatusInfo createPayChannelStatusInfo() {
        return new PayChannelStatusInfo();
    }

    /**
     * Create an instance of {@link EntityLockingInfo }
     * 
     */
    public EntityLockingInfo createEntityLockingInfo() {
        return new EntityLockingInfo();
    }

    /**
     * Create an instance of {@link LockInfo }
     * 
     */
    public LockInfo createLockInfo() {
        return new LockInfo();
    }

    /**
     * Create an instance of {@link AccountCollectionFixInfo }
     * 
     */
    public AccountCollectionFixInfo createAccountCollectionFixInfo() {
        return new AccountCollectionFixInfo();
    }

    /**
     * Create an instance of {@link AddressInfo }
     * 
     */
    public AddressInfo createAddressInfo() {
        return new AddressInfo();
    }

    /**
     * Create an instance of {@link PayChannelHeader }
     * 
     */
    public PayChannelHeader createPayChannelHeader() {
        return new PayChannelHeader();
    }

    /**
     * Create an instance of {@link ExternalIdInfo }
     * 
     */
    public ExternalIdInfo createExternalIdInfo() {
        return new ExternalIdInfo();
    }

    /**
     * Create an instance of {@link RMEntityIdInfo }
     * 
     */
    public RMEntityIdInfo createRMEntityIdInfo() {
        return new RMEntityIdInfo();
    }

    /**
     * Create an instance of {@link CustomerHeader }
     * 
     */
    public CustomerHeader createCustomerHeader() {
        return new CustomerHeader();
    }

    /**
     * Create an instance of {@link BillingArrangementIdInfo }
     * 
     */
    public BillingArrangementIdInfo createBillingArrangementIdInfo() {
        return new BillingArrangementIdInfo();
    }

    /**
     * Create an instance of {@link EventDistributionDetailsInfo }
     * 
     */
    public EventDistributionDetailsInfo createEventDistributionDetailsInfo() {
        return new EventDistributionDetailsInfo();
    }

    /**
     * Create an instance of {@link SrvAgrInfo }
     * 
     */
    public SrvAgrInfo createSrvAgrInfo() {
        return new SrvAgrInfo();
    }

    /**
     * Create an instance of {@link SubscriberHeader }
     * 
     */
    public SubscriberHeader createSubscriberHeader() {
        return new SubscriberHeader();
    }

    /**
     * Create an instance of {@link UserGroupInfo }
     * 
     */
    public UserGroupInfo createUserGroupInfo() {
        return new UserGroupInfo();
    }

    /**
     * Create an instance of {@link CM3AccountStatusInfo }
     * 
     */
    public CM3AccountStatusInfo createCM3AccountStatusInfo() {
        return new CM3AccountStatusInfo();
    }

    /**
     * Create an instance of {@link UpdateSubscriberResourcesByMsisdnResult }
     * 
     */
    public UpdateSubscriberResourcesByMsisdnResult createUpdateSubscriberResourcesByMsisdnResult() {
        return new UpdateSubscriberResourcesByMsisdnResult();
    }

    /**
     * Create an instance of {@link CustomerTypeInfo }
     * 
     */
    public CustomerTypeInfo createCustomerTypeInfo() {
        return new CustomerTypeInfo();
    }

    /**
     * Create an instance of {@link LockDetails }
     * 
     */
    public LockDetails createLockDetails() {
        return new LockDetails();
    }

    /**
     * Create an instance of {@link SubscriberTypeInfo }
     * 
     */
    public SubscriberTypeInfo createSubscriberTypeInfo() {
        return new SubscriberTypeInfo();
    }

    /**
     * Create an instance of {@link ResourceNameInfo }
     * 
     */
    public ResourceNameInfo createResourceNameInfo() {
        return new ResourceNameInfo();
    }

    /**
     * Create an instance of {@link AccountBillingInfo }
     * 
     */
    public AccountBillingInfo createAccountBillingInfo() {
        return new AccountBillingInfo();
    }

    /**
     * Create an instance of {@link CustomerIdInfo }
     * 
     */
    public CustomerIdInfo createCustomerIdInfo() {
        return new CustomerIdInfo();
    }

    /**
     * Create an instance of {@link SubscriberStatusInfo }
     * 
     */
    public SubscriberStatusInfo createSubscriberStatusInfo() {
        return new SubscriberStatusInfo();
    }

    /**
     * Create an instance of {@link PayChannelPaymentCategoryInfo }
     * 
     */
    public PayChannelPaymentCategoryInfo createPayChannelPaymentCategoryInfo() {
        return new PayChannelPaymentCategoryInfo();
    }

    /**
     * Create an instance of {@link NameInfos }
     * 
     */
    public NameInfos createNameInfos() {
        return new NameInfos();
    }

    /**
     * Create an instance of {@link NameInfo }
     * 
     */
    public NameInfo createNameInfo() {
        return new NameInfo();
    }

    /**
     * Create an instance of {@link PayChannelDescriptionInfo }
     * 
     */
    public PayChannelDescriptionInfo createPayChannelDescriptionInfo() {
        return new PayChannelDescriptionInfo();
    }

    /**
     * Create an instance of {@link BillingArrangementStatusInfo }
     * 
     */
    public BillingArrangementStatusInfo createBillingArrangementStatusInfo() {
        return new BillingArrangementStatusInfo();
    }

    /**
     * Create an instance of {@link SubscriberGeneralInfo }
     * 
     */
    public SubscriberGeneralInfo createSubscriberGeneralInfo() {
        return new SubscriberGeneralInfo();
    }

    /**
     * Create an instance of {@link PayChannelIdInfo }
     * 
     */
    public PayChannelIdInfo createPayChannelIdInfo() {
        return new PayChannelIdInfo();
    }

    /**
     * Create an instance of {@link UnifiedResourceInfo }
     * 
     */
    public UnifiedResourceInfo createUnifiedResourceInfo() {
        return new UnifiedResourceInfo();
    }

    /**
     * Create an instance of {@link ValidateIfGroupOwnerResult }
     * 
     */
    public ValidateIfGroupOwnerResult createValidateIfGroupOwnerResult() {
        return new ValidateIfGroupOwnerResult();
    }

    /**
     * Create an instance of {@link BillingArrangementNumberInfo }
     * 
     */
    public BillingArrangementNumberInfo createBillingArrangementNumberInfo() {
        return new BillingArrangementNumberInfo();
    }

    /**
     * Create an instance of {@link RemoveOfferFromSubscriberResult }
     * 
     */
    public RemoveOfferFromSubscriberResult createRemoveOfferFromSubscriberResult() {
        return new RemoveOfferFromSubscriberResult();
    }

    /**
     * Create an instance of {@link ChargeDistributionDetailsInfo }
     * 
     */
    public ChargeDistributionDetailsInfo createChargeDistributionDetailsInfo() {
        return new ChargeDistributionDetailsInfo();
    }

    /**
     * Create an instance of {@link AddressNameLinkInfo }
     * 
     */
    public AddressNameLinkInfo createAddressNameLinkInfo() {
        return new AddressNameLinkInfo();
    }

    /**
     * Create an instance of {@link OffersParameterInfo.Values }
     * 
     */
    public OffersParameterInfo.Values createOffersParameterInfoValues() {
        return new OffersParameterInfo.Values();
    }

    /**
     * Create an instance of {@link UserGroupResourceInfo.ResourceNameInfoList }
     * 
     */
    public UserGroupResourceInfo.ResourceNameInfoList createUserGroupResourceInfoResourceNameInfoList() {
        return new UserGroupResourceInfo.ResourceNameInfoList();
    }

    /**
     * Create an instance of {@link OffersLogicalResourceInfo.Values }
     * 
     */
    public OffersLogicalResourceInfo.Values createOffersLogicalResourceInfoValues() {
        return new OffersLogicalResourceInfo.Values();
    }

    /**
     * Create an instance of {@link ChangeLogicalResourceInputInfoModel.Values }
     * 
     */
    public ChangeLogicalResourceInputInfoModel.Values createChangeLogicalResourceInputInfoModelValues() {
        return new ChangeLogicalResourceInputInfoModel.Values();
    }

    /**
     * Create an instance of {@link ParameterInfo.Values }
     * 
     */
    public ParameterInfo.Values createParameterInfoValues() {
        return new ParameterInfo.Values();
    }

    /**
     * Create an instance of {@link LogicalResourceInfoModel.Values }
     * 
     */
    public LogicalResourceInfoModel.Values createLogicalResourceInfoModelValues() {
        return new LogicalResourceInfoModel.Values();
    }

    /**
     * Create an instance of {@link ResourceRange.Values }
     * 
     */
    public ResourceRange.Values createResourceRangeValues() {
        return new ResourceRange.Values();
    }

    /**
     * Create an instance of {@link ResourceRange.Services }
     * 
     */
    public ResourceRange.Services createResourceRangeServices() {
        return new ResourceRange.Services();
    }

    /**
     * Create an instance of {@link ReplacePhysicalResourceInputInfoModel.Values }
     * 
     */
    public ReplacePhysicalResourceInputInfoModel.Values createReplacePhysicalResourceInputInfoModelValues() {
        return new ReplacePhysicalResourceInputInfoModel.Values();
    }

    /**
     * Create an instance of {@link UnifiedResourceDetailsInfo.AttributesInfoList }
     * 
     */
    public UnifiedResourceDetailsInfo.AttributesInfoList createUnifiedResourceDetailsInfoAttributesInfoList() {
        return new UnifiedResourceDetailsInfo.AttributesInfoList();
    }

    /**
     * Create an instance of {@link UnifiedResourceDetailsInfo.UnifiedResourceCategoryInfoList }
     * 
     */
    public UnifiedResourceDetailsInfo.UnifiedResourceCategoryInfoList createUnifiedResourceDetailsInfoUnifiedResourceCategoryInfoList() {
        return new UnifiedResourceDetailsInfo.UnifiedResourceCategoryInfoList();
    }

    /**
     * Create an instance of {@link UnifiedResourceDetailsInfo.ComponentsInfoList }
     * 
     */
    public UnifiedResourceDetailsInfo.ComponentsInfoList createUnifiedResourceDetailsInfoComponentsInfoList() {
        return new UnifiedResourceDetailsInfo.ComponentsInfoList();
    }

    /**
     * Create an instance of {@link PhysicalResourceInfoModel.Values }
     * 
     */
    public PhysicalResourceInfoModel.Values createPhysicalResourceInfoModelValues() {
        return new PhysicalResourceInfoModel.Values();
    }

    /**
     * Create an instance of {@link BillingArrangementHeader.NameAddressInfoList }
     * 
     */
    public BillingArrangementHeader.NameAddressInfoList createBillingArrangementHeaderNameAddressInfoList() {
        return new BillingArrangementHeader.NameAddressInfoList();
    }

    /**
     * Create an instance of {@link UnifiedResourceCategoryInfo.CategoryValueAttributeList }
     * 
     */
    public UnifiedResourceCategoryInfo.CategoryValueAttributeList createUnifiedResourceCategoryInfoCategoryValueAttributeList() {
        return new UnifiedResourceCategoryInfo.CategoryValueAttributeList();
    }

    /**
     * Create an instance of {@link AccountCollectionInfo.PunishmentLevels }
     * 
     */
    public AccountCollectionInfo.PunishmentLevels createAccountCollectionInfoPunishmentLevels() {
        return new AccountCollectionInfo.PunishmentLevels();
    }

    /**
     * Create an instance of {@link GuidingResourceInfo.ResourceNames }
     * 
     */
    public GuidingResourceInfo.ResourceNames createGuidingResourceInfoResourceNames() {
        return new GuidingResourceInfo.ResourceNames();
    }

    /**
     * Create an instance of {@link CreateNewCMSubscriber.NameInfoList }
     * 
     */
    public CreateNewCMSubscriber.NameInfoList createCreateNewCMSubscriberNameInfoList() {
        return new CreateNewCMSubscriber.NameInfoList();
    }

    /**
     * Create an instance of {@link CreateNewCMSubscriber.AddressDetailsList }
     * 
     */
    public CreateNewCMSubscriber.AddressDetailsList createCreateNewCMSubscriberAddressDetailsList() {
        return new CreateNewCMSubscriber.AddressDetailsList();
    }

    /**
     * Create an instance of {@link CreateNewCMSubscriber.SrvAgrInfoList }
     * 
     */
    public CreateNewCMSubscriber.SrvAgrInfoList createCreateNewCMSubscriberSrvAgrInfoList() {
        return new CreateNewCMSubscriber.SrvAgrInfoList();
    }

    /**
     * Create an instance of {@link CreateNewCMSubscriber.LogicalResourceInfoList }
     * 
     */
    public CreateNewCMSubscriber.LogicalResourceInfoList createCreateNewCMSubscriberLogicalResourceInfoList() {
        return new CreateNewCMSubscriber.LogicalResourceInfoList();
    }

    /**
     * Create an instance of {@link CreateNewCMSubscriber.PhysicalResourceInfoList }
     * 
     */
    public CreateNewCMSubscriber.PhysicalResourceInfoList createCreateNewCMSubscriberPhysicalResourceInfoList() {
        return new CreateNewCMSubscriber.PhysicalResourceInfoList();
    }

    /**
     * Create an instance of {@link CreateNewCMSubscriber.ParameterInfoList }
     * 
     */
    public CreateNewCMSubscriber.ParameterInfoList createCreateNewCMSubscriberParameterInfoList() {
        return new CreateNewCMSubscriber.ParameterInfoList();
    }

    /**
     * Create an instance of {@link RemoveOfferFromSubscriber.OfferList }
     * 
     */
    public RemoveOfferFromSubscriber.OfferList createRemoveOfferFromSubscriberOfferList() {
        return new RemoveOfferFromSubscriber.OfferList();
    }

    /**
     * Create an instance of {@link RemoveOfferFromSubscriber.GuidingResourceInfoList }
     * 
     */
    public RemoveOfferFromSubscriber.GuidingResourceInfoList createRemoveOfferFromSubscriberGuidingResourceInfoList() {
        return new RemoveOfferFromSubscriber.GuidingResourceInfoList();
    }

    /**
     * Create an instance of {@link RemoveOfferFromSubscriber.UserGroupResourceInfoList }
     * 
     */
    public RemoveOfferFromSubscriber.UserGroupResourceInfoList createRemoveOfferFromSubscriberUserGroupResourceInfoList() {
        return new RemoveOfferFromSubscriber.UserGroupResourceInfoList();
    }

    /**
     * Create an instance of {@link RemoveOfferFromSubscriber.ResourceRangeList }
     * 
     */
    public RemoveOfferFromSubscriber.ResourceRangeList createRemoveOfferFromSubscriberResourceRangeList() {
        return new RemoveOfferFromSubscriber.ResourceRangeList();
    }

    /**
     * Create an instance of {@link AddOfferToSubscriber.OfferList }
     * 
     */
    public AddOfferToSubscriber.OfferList createAddOfferToSubscriberOfferList() {
        return new AddOfferToSubscriber.OfferList();
    }

    /**
     * Create an instance of {@link AddOfferToSubscriber.OffersLogicalResourceInfoList }
     * 
     */
    public AddOfferToSubscriber.OffersLogicalResourceInfoList createAddOfferToSubscriberOffersLogicalResourceInfoList() {
        return new AddOfferToSubscriber.OffersLogicalResourceInfoList();
    }

    /**
     * Create an instance of {@link AddOfferToSubscriber.OffersParameterInfoList }
     * 
     */
    public AddOfferToSubscriber.OffersParameterInfoList createAddOfferToSubscriberOffersParameterInfoList() {
        return new AddOfferToSubscriber.OffersParameterInfoList();
    }

    /**
     * Create an instance of {@link AddOfferToSubscriber.ChargeDistributionDetailsInfoList }
     * 
     */
    public AddOfferToSubscriber.ChargeDistributionDetailsInfoList createAddOfferToSubscriberChargeDistributionDetailsInfoList() {
        return new AddOfferToSubscriber.ChargeDistributionDetailsInfoList();
    }

    /**
     * Create an instance of {@link AddOfferToSubscriber.EventDistributionDetailsInfoList }
     * 
     */
    public AddOfferToSubscriber.EventDistributionDetailsInfoList createAddOfferToSubscriberEventDistributionDetailsInfoList() {
        return new AddOfferToSubscriber.EventDistributionDetailsInfoList();
    }

    /**
     * Create an instance of {@link AddOfferToSubscriber.GuidingResourceInfoList }
     * 
     */
    public AddOfferToSubscriber.GuidingResourceInfoList createAddOfferToSubscriberGuidingResourceInfoList() {
        return new AddOfferToSubscriber.GuidingResourceInfoList();
    }

    /**
     * Create an instance of {@link AddOfferToSubscriber.UserGroupResourceInfoList }
     * 
     */
    public AddOfferToSubscriber.UserGroupResourceInfoList createAddOfferToSubscriberUserGroupResourceInfoList() {
        return new AddOfferToSubscriber.UserGroupResourceInfoList();
    }

    /**
     * Create an instance of {@link AddOfferToSubscriber.ResourceRangeList }
     * 
     */
    public AddOfferToSubscriber.ResourceRangeList createAddOfferToSubscriberResourceRangeList() {
        return new AddOfferToSubscriber.ResourceRangeList();
    }

    /**
     * Create an instance of {@link UpdateOfferOfSubscriber.UpdateParameterInfoList }
     * 
     */
    public UpdateOfferOfSubscriber.UpdateParameterInfoList createUpdateOfferOfSubscriberUpdateParameterInfoList() {
        return new UpdateOfferOfSubscriber.UpdateParameterInfoList();
    }

    /**
     * Create an instance of {@link UpdateSubscriberResourcesByMsisdn.ChangeLogicalResourceInputInfoList }
     * 
     */
    public UpdateSubscriberResourcesByMsisdn.ChangeLogicalResourceInputInfoList createUpdateSubscriberResourcesByMsisdnChangeLogicalResourceInputInfoList() {
        return new UpdateSubscriberResourcesByMsisdn.ChangeLogicalResourceInputInfoList();
    }

    /**
     * Create an instance of {@link UpdateSubscriberResourcesByMsisdn.ReplacePhysicalResourceInputInfoList }
     * 
     */
    public UpdateSubscriberResourcesByMsisdn.ReplacePhysicalResourceInputInfoList createUpdateSubscriberResourcesByMsisdnReplacePhysicalResourceInputInfoList() {
        return new UpdateSubscriberResourcesByMsisdn.ReplacePhysicalResourceInputInfoList();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ResultNameSpace", scope = CreateNewCMSubscriberResult.class)
    public JAXBElement<String> createCreateNewCMSubscriberResultResultNameSpace(String value) {
        return new JAXBElement<String>(_CreateNewCMSubscriberResultResultNameSpace_QNAME, String.class, CreateNewCMSubscriberResult.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateOfferOfSubscriber }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.globe.com/warcraft/wsdl/billingcm/", name = "UpdateOfferOfSubscriber")
    public JAXBElement<UpdateOfferOfSubscriber> createUpdateOfferOfSubscriber(UpdateOfferOfSubscriber value) {
        return new JAXBElement<UpdateOfferOfSubscriber>(_UpdateOfferOfSubscriber_QNAME, UpdateOfferOfSubscriber.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveOfferFromSubscriberResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.globe.com/warcraft/wsdl/billingcm/", name = "RemoveOfferFromSubscriberResponse")
    public JAXBElement<RemoveOfferFromSubscriberResponse> createRemoveOfferFromSubscriberResponse(RemoveOfferFromSubscriberResponse value) {
        return new JAXBElement<RemoveOfferFromSubscriberResponse>(_RemoveOfferFromSubscriberResponse_QNAME, RemoveOfferFromSubscriberResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateOfferOfSubscriberResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.globe.com/warcraft/wsdl/billingcm/", name = "UpdateOfferOfSubscriberResponse")
    public JAXBElement<UpdateOfferOfSubscriberResponse> createUpdateOfferOfSubscriberResponse(UpdateOfferOfSubscriberResponse value) {
        return new JAXBElement<UpdateOfferOfSubscriberResponse>(_UpdateOfferOfSubscriberResponse_QNAME, UpdateOfferOfSubscriberResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeCustomerBillingCycleInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.globe.com/warcraft/wsdl/billingcm/", name = "changeCustomerBillingCycleInfo")
    public JAXBElement<ChangeCustomerBillingCycleInfo> createChangeCustomerBillingCycleInfo(ChangeCustomerBillingCycleInfo value) {
        return new JAXBElement<ChangeCustomerBillingCycleInfo>(_ChangeCustomerBillingCycleInfo_QNAME, ChangeCustomerBillingCycleInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDetailsByMsisdn }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.globe.com/warcraft/wsdl/billingcm/", name = "GetDetailsByMsisdn")
    public JAXBElement<GetDetailsByMsisdn> createGetDetailsByMsisdn(GetDetailsByMsisdn value) {
        return new JAXBElement<GetDetailsByMsisdn>(_GetDetailsByMsisdn_QNAME, GetDetailsByMsisdn.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddOfferToSubscriber }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.globe.com/warcraft/wsdl/billingcm/", name = "AddOfferToSubscriber")
    public JAXBElement<AddOfferToSubscriber> createAddOfferToSubscriber(AddOfferToSubscriber value) {
        return new JAXBElement<AddOfferToSubscriber>(_AddOfferToSubscriber_QNAME, AddOfferToSubscriber.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateNewCMSubscriber }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.globe.com/warcraft/wsdl/billingcm/", name = "CreateNewCMSubscriber")
    public JAXBElement<CreateNewCMSubscriber> createCreateNewCMSubscriber(CreateNewCMSubscriber value) {
        return new JAXBElement<CreateNewCMSubscriber>(_CreateNewCMSubscriber_QNAME, CreateNewCMSubscriber.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveOfferFromSubscriber }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.globe.com/warcraft/wsdl/billingcm/", name = "RemoveOfferFromSubscriber")
    public JAXBElement<RemoveOfferFromSubscriber> createRemoveOfferFromSubscriber(RemoveOfferFromSubscriber value) {
        return new JAXBElement<RemoveOfferFromSubscriber>(_RemoveOfferFromSubscriber_QNAME, RemoveOfferFromSubscriber.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeCustomerBillingCycleInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.globe.com/warcraft/wsdl/billingcm/", name = "ChangeCustomerBillingCycleInfoResponse")
    public JAXBElement<ChangeCustomerBillingCycleInfoResponse> createChangeCustomerBillingCycleInfoResponse(ChangeCustomerBillingCycleInfoResponse value) {
        return new JAXBElement<ChangeCustomerBillingCycleInfoResponse>(_ChangeCustomerBillingCycleInfoResponse_QNAME, ChangeCustomerBillingCycleInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateIfGroupOwnerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.globe.com/warcraft/wsdl/billingcm/", name = "ValidateIfGroupOwnerResponse")
    public JAXBElement<ValidateIfGroupOwnerResponse> createValidateIfGroupOwnerResponse(ValidateIfGroupOwnerResponse value) {
        return new JAXBElement<ValidateIfGroupOwnerResponse>(_ValidateIfGroupOwnerResponse_QNAME, ValidateIfGroupOwnerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateNewCMSubscriberResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.globe.com/warcraft/wsdl/billingcm/", name = "CreateNewCMSubscriberResponse")
    public JAXBElement<CreateNewCMSubscriberResponse> createCreateNewCMSubscriberResponse(CreateNewCMSubscriberResponse value) {
        return new JAXBElement<CreateNewCMSubscriberResponse>(_CreateNewCMSubscriberResponse_QNAME, CreateNewCMSubscriberResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDetailsByMsisdnResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.globe.com/warcraft/wsdl/billingcm/", name = "GetDetailsByMsisdnResponse")
    public JAXBElement<GetDetailsByMsisdnResponse> createGetDetailsByMsisdnResponse(GetDetailsByMsisdnResponse value) {
        return new JAXBElement<GetDetailsByMsisdnResponse>(_GetDetailsByMsisdnResponse_QNAME, GetDetailsByMsisdnResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetNextDsaCustomer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.globe.com/warcraft/wsdl/billingcm/", name = "GetNextDsaCustomer")
    public JAXBElement<GetNextDsaCustomer> createGetNextDsaCustomer(GetNextDsaCustomer value) {
        return new JAXBElement<GetNextDsaCustomer>(_GetNextDsaCustomer_QNAME, GetNextDsaCustomer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetNextDsaCustomerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.globe.com/warcraft/wsdl/billingcm/", name = "GetNextDsaCustomerResponse")
    public JAXBElement<GetNextDsaCustomerResponse> createGetNextDsaCustomerResponse(GetNextDsaCustomerResponse value) {
        return new JAXBElement<GetNextDsaCustomerResponse>(_GetNextDsaCustomerResponse_QNAME, GetNextDsaCustomerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateSubscriberResourcesByMsisdn }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.globe.com/warcraft/wsdl/billingcm/", name = "UpdateSubscriberResourcesByMsisdn")
    public JAXBElement<UpdateSubscriberResourcesByMsisdn> createUpdateSubscriberResourcesByMsisdn(UpdateSubscriberResourcesByMsisdn value) {
        return new JAXBElement<UpdateSubscriberResourcesByMsisdn>(_UpdateSubscriberResourcesByMsisdn_QNAME, UpdateSubscriberResourcesByMsisdn.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateSubscriberResourcesByMsisdnResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.globe.com/warcraft/wsdl/billingcm/", name = "UpdateSubscriberResourcesByMsisdnResponse")
    public JAXBElement<UpdateSubscriberResourcesByMsisdnResponse> createUpdateSubscriberResourcesByMsisdnResponse(UpdateSubscriberResourcesByMsisdnResponse value) {
        return new JAXBElement<UpdateSubscriberResourcesByMsisdnResponse>(_UpdateSubscriberResourcesByMsisdnResponse_QNAME, UpdateSubscriberResourcesByMsisdnResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddOfferToSubscriberResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.globe.com/warcraft/wsdl/billingcm/", name = "AddOfferToSubscriberResponse")
    public JAXBElement<AddOfferToSubscriberResponse> createAddOfferToSubscriberResponse(AddOfferToSubscriberResponse value) {
        return new JAXBElement<AddOfferToSubscriberResponse>(_AddOfferToSubscriberResponse_QNAME, AddOfferToSubscriberResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateIfGroupOwner }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.globe.com/warcraft/wsdl/billingcm/", name = "ValidateIfGroupOwner")
    public JAXBElement<ValidateIfGroupOwner> createValidateIfGroupOwner(ValidateIfGroupOwner value) {
        return new JAXBElement<ValidateIfGroupOwner>(_ValidateIfGroupOwner_QNAME, ValidateIfGroupOwner.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Type", scope = ResourceInfo.class)
    public JAXBElement<String> createResourceInfoType(String value) {
        return new JAXBElement<String>(_ResourceInfoType_QNAME, String.class, ResourceInfo.class, value);
    }

}
