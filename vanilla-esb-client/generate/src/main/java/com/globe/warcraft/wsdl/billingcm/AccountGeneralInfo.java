
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for AccountGeneralInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountGeneralInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="L3AccountStatusInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}CM3AccountStatusInfo" minOccurs="0"/>
 *         &lt;element name="LastUpdateDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="OpenDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountGeneralInfo", propOrder = {
    "l3AccountStatusInfo",
    "lastUpdateDate",
    "openDate"
})
public class AccountGeneralInfo {

    @XmlElement(name = "L3AccountStatusInfo")
    protected CM3AccountStatusInfo l3AccountStatusInfo;
    @XmlElement(name = "LastUpdateDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastUpdateDate;
    @XmlElement(name = "OpenDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar openDate;

    /**
     * Gets the value of the l3AccountStatusInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CM3AccountStatusInfo }
     *     
     */
    public CM3AccountStatusInfo getL3AccountStatusInfo() {
        return l3AccountStatusInfo;
    }

    /**
     * Sets the value of the l3AccountStatusInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CM3AccountStatusInfo }
     *     
     */
    public void setL3AccountStatusInfo(CM3AccountStatusInfo value) {
        this.l3AccountStatusInfo = value;
    }

    /**
     * Gets the value of the lastUpdateDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastUpdateDate() {
        return lastUpdateDate;
    }

    /**
     * Sets the value of the lastUpdateDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastUpdateDate(XMLGregorianCalendar value) {
        this.lastUpdateDate = value;
    }

    /**
     * Gets the value of the openDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOpenDate() {
        return openDate;
    }

    /**
     * Sets the value of the openDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOpenDate(XMLGregorianCalendar value) {
        this.openDate = value;
    }

}
