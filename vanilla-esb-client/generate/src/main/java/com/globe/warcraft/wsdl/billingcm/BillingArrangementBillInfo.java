
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BillingArrangementBillInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BillingArrangementBillInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BillProductionFrequency" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="BillProductionIndicator" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="ItemizedTaxIndicator" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BillingArrangementBillInfo", propOrder = {
    "billProductionFrequency",
    "billProductionIndicator",
    "itemizedTaxIndicator"
})
public class BillingArrangementBillInfo {

    @XmlElement(name = "BillProductionFrequency")
    protected short billProductionFrequency;
    @XmlElement(name = "BillProductionIndicator")
    protected byte billProductionIndicator;
    @XmlElement(name = "ItemizedTaxIndicator")
    protected byte itemizedTaxIndicator;

    /**
     * Gets the value of the billProductionFrequency property.
     * 
     */
    public short getBillProductionFrequency() {
        return billProductionFrequency;
    }

    /**
     * Sets the value of the billProductionFrequency property.
     * 
     */
    public void setBillProductionFrequency(short value) {
        this.billProductionFrequency = value;
    }

    /**
     * Gets the value of the billProductionIndicator property.
     * 
     */
    public byte getBillProductionIndicator() {
        return billProductionIndicator;
    }

    /**
     * Sets the value of the billProductionIndicator property.
     * 
     */
    public void setBillProductionIndicator(byte value) {
        this.billProductionIndicator = value;
    }

    /**
     * Gets the value of the itemizedTaxIndicator property.
     * 
     */
    public byte getItemizedTaxIndicator() {
        return itemizedTaxIndicator;
    }

    /**
     * Sets the value of the itemizedTaxIndicator property.
     * 
     */
    public void setItemizedTaxIndicator(byte value) {
        this.itemizedTaxIndicator = value;
    }

}
