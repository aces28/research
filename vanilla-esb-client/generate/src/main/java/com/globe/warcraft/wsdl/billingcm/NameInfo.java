
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for NameInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NameInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ExpirationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="LinkType" type="{http://www.w3.org/2001/XMLSchema}byte" minOccurs="0"/>
 *         &lt;element name="NameElement1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NameElement2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NameElement3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NameElement4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NameElement5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NameElement6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NameElement7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NameElement8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NameElement9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NameElement10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NameLine1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NameLine2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NameType" type="{http://www.w3.org/2001/XMLSchema}byte" minOccurs="0"/>
 *         &lt;element name="NameUpdateType" type="{http://www.w3.org/2001/XMLSchema}byte" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NameInfo", propOrder = {
    "effectiveDate",
    "expirationDate",
    "linkType",
    "nameElement1",
    "nameElement2",
    "nameElement3",
    "nameElement4",
    "nameElement5",
    "nameElement6",
    "nameElement7",
    "nameElement8",
    "nameElement9",
    "nameElement10",
    "nameLine1",
    "nameLine2",
    "nameType",
    "nameUpdateType"
})
public class NameInfo {

    @XmlElement(name = "EffectiveDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effectiveDate;
    @XmlElement(name = "ExpirationDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expirationDate;
    @XmlElement(name = "LinkType")
    protected Byte linkType;
    @XmlElement(name = "NameElement1")
    protected String nameElement1;
    @XmlElement(name = "NameElement2")
    protected String nameElement2;
    @XmlElement(name = "NameElement3")
    protected String nameElement3;
    @XmlElement(name = "NameElement4")
    protected String nameElement4;
    @XmlElement(name = "NameElement5")
    protected String nameElement5;
    @XmlElement(name = "NameElement6")
    protected String nameElement6;
    @XmlElement(name = "NameElement7")
    protected String nameElement7;
    @XmlElement(name = "NameElement8")
    protected String nameElement8;
    @XmlElement(name = "NameElement9")
    protected String nameElement9;
    @XmlElement(name = "NameElement10")
    protected String nameElement10;
    @XmlElement(name = "NameLine1")
    protected String nameLine1;
    @XmlElement(name = "NameLine2")
    protected String nameLine2;
    @XmlElement(name = "NameType")
    protected Byte nameType;
    @XmlElement(name = "NameUpdateType")
    protected Byte nameUpdateType;

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDate(XMLGregorianCalendar value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the linkType property.
     * 
     * @return
     *     possible object is
     *     {@link Byte }
     *     
     */
    public Byte getLinkType() {
        return linkType;
    }

    /**
     * Sets the value of the linkType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Byte }
     *     
     */
    public void setLinkType(Byte value) {
        this.linkType = value;
    }

    /**
     * Gets the value of the nameElement1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameElement1() {
        return nameElement1;
    }

    /**
     * Sets the value of the nameElement1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameElement1(String value) {
        this.nameElement1 = value;
    }

    /**
     * Gets the value of the nameElement2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameElement2() {
        return nameElement2;
    }

    /**
     * Sets the value of the nameElement2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameElement2(String value) {
        this.nameElement2 = value;
    }

    /**
     * Gets the value of the nameElement3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameElement3() {
        return nameElement3;
    }

    /**
     * Sets the value of the nameElement3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameElement3(String value) {
        this.nameElement3 = value;
    }

    /**
     * Gets the value of the nameElement4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameElement4() {
        return nameElement4;
    }

    /**
     * Sets the value of the nameElement4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameElement4(String value) {
        this.nameElement4 = value;
    }

    /**
     * Gets the value of the nameElement5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameElement5() {
        return nameElement5;
    }

    /**
     * Sets the value of the nameElement5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameElement5(String value) {
        this.nameElement5 = value;
    }

    /**
     * Gets the value of the nameElement6 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameElement6() {
        return nameElement6;
    }

    /**
     * Sets the value of the nameElement6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameElement6(String value) {
        this.nameElement6 = value;
    }

    /**
     * Gets the value of the nameElement7 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameElement7() {
        return nameElement7;
    }

    /**
     * Sets the value of the nameElement7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameElement7(String value) {
        this.nameElement7 = value;
    }

    /**
     * Gets the value of the nameElement8 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameElement8() {
        return nameElement8;
    }

    /**
     * Sets the value of the nameElement8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameElement8(String value) {
        this.nameElement8 = value;
    }

    /**
     * Gets the value of the nameElement9 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameElement9() {
        return nameElement9;
    }

    /**
     * Sets the value of the nameElement9 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameElement9(String value) {
        this.nameElement9 = value;
    }

    /**
     * Gets the value of the nameElement10 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameElement10() {
        return nameElement10;
    }

    /**
     * Sets the value of the nameElement10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameElement10(String value) {
        this.nameElement10 = value;
    }

    /**
     * Gets the value of the nameLine1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameLine1() {
        return nameLine1;
    }

    /**
     * Sets the value of the nameLine1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameLine1(String value) {
        this.nameLine1 = value;
    }

    /**
     * Gets the value of the nameLine2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameLine2() {
        return nameLine2;
    }

    /**
     * Sets the value of the nameLine2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameLine2(String value) {
        this.nameLine2 = value;
    }

    /**
     * Gets the value of the nameType property.
     * 
     * @return
     *     possible object is
     *     {@link Byte }
     *     
     */
    public Byte getNameType() {
        return nameType;
    }

    /**
     * Sets the value of the nameType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Byte }
     *     
     */
    public void setNameType(Byte value) {
        this.nameType = value;
    }

    /**
     * Gets the value of the nameUpdateType property.
     * 
     * @return
     *     possible object is
     *     {@link Byte }
     *     
     */
    public Byte getNameUpdateType() {
        return nameUpdateType;
    }

    /**
     * Sets the value of the nameUpdateType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Byte }
     *     
     */
    public void setNameUpdateType(Byte value) {
        this.nameUpdateType = value;
    }

}
