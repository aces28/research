
package com.globe.warcraft.wsdl.billingcm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for RemoveOfferFromSubscriber complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RemoveOfferFromSubscriber">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MSISDN">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="12"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PrimaryResourceType" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OfferList">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Offer" type="{http://www.globe.com/warcraft/wsdl/billingcm/}Offer" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="GuidingResourceInfoList" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="GuidingResourceInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}GuidingResourceInfo" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="UserGroupResourceInfoList" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="UserGroupResourceInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}UserGroupResourceInfo" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ResourceRangeList" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ResourceRange" type="{http://www.globe.com/warcraft/wsdl/billingcm/}ResourceRange" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ActivityDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ActivityReason">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RemoveOfferFromSubscriber", propOrder = {
    "msisdn",
    "primaryResourceType",
    "offerList",
    "guidingResourceInfoList",
    "userGroupResourceInfoList",
    "resourceRangeList",
    "activityDate",
    "activityReason"
})
public class RemoveOfferFromSubscriber {

    @XmlElement(name = "MSISDN", required = true)
    protected String msisdn;
    @XmlElement(name = "PrimaryResourceType")
    protected String primaryResourceType;
    @XmlElement(name = "OfferList", required = true)
    protected RemoveOfferFromSubscriber.OfferList offerList;
    @XmlElement(name = "GuidingResourceInfoList")
    protected RemoveOfferFromSubscriber.GuidingResourceInfoList guidingResourceInfoList;
    @XmlElement(name = "UserGroupResourceInfoList")
    protected RemoveOfferFromSubscriber.UserGroupResourceInfoList userGroupResourceInfoList;
    @XmlElement(name = "ResourceRangeList")
    protected RemoveOfferFromSubscriber.ResourceRangeList resourceRangeList;
    @XmlElement(name = "ActivityDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar activityDate;
    @XmlElement(name = "ActivityReason", required = true)
    protected String activityReason;

    /**
     * Gets the value of the msisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSISDN() {
        return msisdn;
    }

    /**
     * Sets the value of the msisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSISDN(String value) {
        this.msisdn = value;
    }

    /**
     * Gets the value of the primaryResourceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryResourceType() {
        return primaryResourceType;
    }

    /**
     * Sets the value of the primaryResourceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryResourceType(String value) {
        this.primaryResourceType = value;
    }

    /**
     * Gets the value of the offerList property.
     * 
     * @return
     *     possible object is
     *     {@link RemoveOfferFromSubscriber.OfferList }
     *     
     */
    public RemoveOfferFromSubscriber.OfferList getOfferList() {
        return offerList;
    }

    /**
     * Sets the value of the offerList property.
     * 
     * @param value
     *     allowed object is
     *     {@link RemoveOfferFromSubscriber.OfferList }
     *     
     */
    public void setOfferList(RemoveOfferFromSubscriber.OfferList value) {
        this.offerList = value;
    }

    /**
     * Gets the value of the guidingResourceInfoList property.
     * 
     * @return
     *     possible object is
     *     {@link RemoveOfferFromSubscriber.GuidingResourceInfoList }
     *     
     */
    public RemoveOfferFromSubscriber.GuidingResourceInfoList getGuidingResourceInfoList() {
        return guidingResourceInfoList;
    }

    /**
     * Sets the value of the guidingResourceInfoList property.
     * 
     * @param value
     *     allowed object is
     *     {@link RemoveOfferFromSubscriber.GuidingResourceInfoList }
     *     
     */
    public void setGuidingResourceInfoList(RemoveOfferFromSubscriber.GuidingResourceInfoList value) {
        this.guidingResourceInfoList = value;
    }

    /**
     * Gets the value of the userGroupResourceInfoList property.
     * 
     * @return
     *     possible object is
     *     {@link RemoveOfferFromSubscriber.UserGroupResourceInfoList }
     *     
     */
    public RemoveOfferFromSubscriber.UserGroupResourceInfoList getUserGroupResourceInfoList() {
        return userGroupResourceInfoList;
    }

    /**
     * Sets the value of the userGroupResourceInfoList property.
     * 
     * @param value
     *     allowed object is
     *     {@link RemoveOfferFromSubscriber.UserGroupResourceInfoList }
     *     
     */
    public void setUserGroupResourceInfoList(RemoveOfferFromSubscriber.UserGroupResourceInfoList value) {
        this.userGroupResourceInfoList = value;
    }

    /**
     * Gets the value of the resourceRangeList property.
     * 
     * @return
     *     possible object is
     *     {@link RemoveOfferFromSubscriber.ResourceRangeList }
     *     
     */
    public RemoveOfferFromSubscriber.ResourceRangeList getResourceRangeList() {
        return resourceRangeList;
    }

    /**
     * Sets the value of the resourceRangeList property.
     * 
     * @param value
     *     allowed object is
     *     {@link RemoveOfferFromSubscriber.ResourceRangeList }
     *     
     */
    public void setResourceRangeList(RemoveOfferFromSubscriber.ResourceRangeList value) {
        this.resourceRangeList = value;
    }

    /**
     * Gets the value of the activityDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getActivityDate() {
        return activityDate;
    }

    /**
     * Sets the value of the activityDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setActivityDate(XMLGregorianCalendar value) {
        this.activityDate = value;
    }

    /**
     * Gets the value of the activityReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityReason() {
        return activityReason;
    }

    /**
     * Sets the value of the activityReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityReason(String value) {
        this.activityReason = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="GuidingResourceInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}GuidingResourceInfo" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "guidingResourceInfo"
    })
    public static class GuidingResourceInfoList {

        @XmlElement(name = "GuidingResourceInfo", required = true)
        protected List<GuidingResourceInfo> guidingResourceInfo;

        /**
         * Gets the value of the guidingResourceInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the guidingResourceInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getGuidingResourceInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link GuidingResourceInfo }
         * 
         * 
         */
        public List<GuidingResourceInfo> getGuidingResourceInfo() {
            if (guidingResourceInfo == null) {
                guidingResourceInfo = new ArrayList<GuidingResourceInfo>();
            }
            return this.guidingResourceInfo;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Offer" type="{http://www.globe.com/warcraft/wsdl/billingcm/}Offer" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "offer"
    })
    public static class OfferList {

        @XmlElement(name = "Offer", required = true)
        protected List<Offer> offer;

        /**
         * Gets the value of the offer property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the offer property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getOffer().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Offer }
         * 
         * 
         */
        public List<Offer> getOffer() {
            if (offer == null) {
                offer = new ArrayList<Offer>();
            }
            return this.offer;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ResourceRange" type="{http://www.globe.com/warcraft/wsdl/billingcm/}ResourceRange" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "resourceRange"
    })
    public static class ResourceRangeList {

        @XmlElement(name = "ResourceRange", required = true)
        protected List<ResourceRange> resourceRange;

        /**
         * Gets the value of the resourceRange property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the resourceRange property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getResourceRange().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ResourceRange }
         * 
         * 
         */
        public List<ResourceRange> getResourceRange() {
            if (resourceRange == null) {
                resourceRange = new ArrayList<ResourceRange>();
            }
            return this.resourceRange;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="UserGroupResourceInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}UserGroupResourceInfo" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "userGroupResourceInfo"
    })
    public static class UserGroupResourceInfoList {

        @XmlElement(name = "UserGroupResourceInfo", required = true)
        protected List<UserGroupResourceInfo> userGroupResourceInfo;

        /**
         * Gets the value of the userGroupResourceInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the userGroupResourceInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getUserGroupResourceInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link UserGroupResourceInfo }
         * 
         * 
         */
        public List<UserGroupResourceInfo> getUserGroupResourceInfo() {
            if (userGroupResourceInfo == null) {
                userGroupResourceInfo = new ArrayList<UserGroupResourceInfo>();
            }
            return this.userGroupResourceInfo;
        }

    }

}
