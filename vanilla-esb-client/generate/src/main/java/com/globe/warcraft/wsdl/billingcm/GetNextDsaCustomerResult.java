
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetNextDsaCustomerResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetNextDsaCustomerResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="BillingArrangementId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PayChannelId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CustomerType" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="CustomerSubType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResultNameSpace" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetNextDsaCustomerResult", propOrder = {
    "accountId",
    "billingArrangementId",
    "payChannelId",
    "customerType",
    "customerSubType",
    "customerId",
    "status",
    "resultNameSpace"
})
public class GetNextDsaCustomerResult {

    @XmlElement(name = "AccountId")
    protected int accountId;
    @XmlElement(name = "BillingArrangementId")
    protected int billingArrangementId;
    @XmlElement(name = "PayChannelId")
    protected int payChannelId;
    @XmlElement(name = "CustomerType")
    protected byte customerType;
    @XmlElement(name = "CustomerSubType")
    protected String customerSubType;
    @XmlElement(name = "CustomerId")
    protected int customerId;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "ResultNameSpace")
    protected String resultNameSpace;

    /**
     * Gets the value of the accountId property.
     * 
     */
    public int getAccountId() {
        return accountId;
    }

    /**
     * Sets the value of the accountId property.
     * 
     */
    public void setAccountId(int value) {
        this.accountId = value;
    }

    /**
     * Gets the value of the billingArrangementId property.
     * 
     */
    public int getBillingArrangementId() {
        return billingArrangementId;
    }

    /**
     * Sets the value of the billingArrangementId property.
     * 
     */
    public void setBillingArrangementId(int value) {
        this.billingArrangementId = value;
    }

    /**
     * Gets the value of the payChannelId property.
     * 
     */
    public int getPayChannelId() {
        return payChannelId;
    }

    /**
     * Sets the value of the payChannelId property.
     * 
     */
    public void setPayChannelId(int value) {
        this.payChannelId = value;
    }

    /**
     * Gets the value of the customerType property.
     * 
     */
    public byte getCustomerType() {
        return customerType;
    }

    /**
     * Sets the value of the customerType property.
     * 
     */
    public void setCustomerType(byte value) {
        this.customerType = value;
    }

    /**
     * Gets the value of the customerSubType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerSubType() {
        return customerSubType;
    }

    /**
     * Sets the value of the customerSubType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerSubType(String value) {
        this.customerSubType = value;
    }

    /**
     * Gets the value of the customerId property.
     * 
     */
    public int getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     */
    public void setCustomerId(int value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the resultNameSpace property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResultNameSpace() {
        return resultNameSpace;
    }

    /**
     * Sets the value of the resultNameSpace property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResultNameSpace(String value) {
        this.resultNameSpace = value;
    }

}
