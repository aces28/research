
package com.globe.warcraft.wsdl.header;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.globe.warcraft.wsdl.header package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _WarcraftHeader_QNAME = new QName("http://www.globe.com/warcraft/wsdl/header/", "WarcraftHeader");
    private final static QName _WarcraftHeaderEsbMessageID_QNAME = new QName("", "EsbMessageID");
    private final static QName _WarcraftHeaderEsbResponseDateTime_QNAME = new QName("", "EsbResponseDateTime");
    private final static QName _WarcraftHeaderEsbIMLNumber_QNAME = new QName("", "EsbIMLNumber");
    private final static QName _WarcraftHeaderEsbRequestDateTime_QNAME = new QName("", "EsbRequestDateTime");
    private final static QName _WarcraftHeaderEsbOperationName_QNAME = new QName("", "EsbOperationName");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.globe.warcraft.wsdl.header
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link WarcraftHeader }
     * 
     */
    public WarcraftHeader createWarcraftHeader() {
        return new WarcraftHeader();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WarcraftHeader }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.globe.com/warcraft/wsdl/header/", name = "WarcraftHeader")
    public JAXBElement<WarcraftHeader> createWarcraftHeader(WarcraftHeader value) {
        return new JAXBElement<WarcraftHeader>(_WarcraftHeader_QNAME, WarcraftHeader.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EsbMessageID", scope = WarcraftHeader.class)
    public JAXBElement<String> createWarcraftHeaderEsbMessageID(String value) {
        return new JAXBElement<String>(_WarcraftHeaderEsbMessageID_QNAME, String.class, WarcraftHeader.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EsbResponseDateTime", scope = WarcraftHeader.class)
    public JAXBElement<XMLGregorianCalendar> createWarcraftHeaderEsbResponseDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_WarcraftHeaderEsbResponseDateTime_QNAME, XMLGregorianCalendar.class, WarcraftHeader.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EsbIMLNumber", scope = WarcraftHeader.class)
    public JAXBElement<String> createWarcraftHeaderEsbIMLNumber(String value) {
        return new JAXBElement<String>(_WarcraftHeaderEsbIMLNumber_QNAME, String.class, WarcraftHeader.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EsbRequestDateTime", scope = WarcraftHeader.class)
    public JAXBElement<XMLGregorianCalendar> createWarcraftHeaderEsbRequestDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_WarcraftHeaderEsbRequestDateTime_QNAME, XMLGregorianCalendar.class, WarcraftHeader.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EsbOperationName", scope = WarcraftHeader.class)
    public JAXBElement<String> createWarcraftHeaderEsbOperationName(String value) {
        return new JAXBElement<String>(_WarcraftHeaderEsbOperationName_QNAME, String.class, WarcraftHeader.class, value);
    }

}
