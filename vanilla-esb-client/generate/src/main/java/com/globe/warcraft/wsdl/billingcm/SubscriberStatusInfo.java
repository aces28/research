
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SubscriberStatusInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubscriberStatusInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubStatus" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="SubStatusDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="SubStsIssueDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="SubStsLastAct" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubStsRsnCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubscriberStatusInfo", propOrder = {
    "subStatus",
    "subStatusDate",
    "subStsIssueDate",
    "subStsLastAct",
    "subStsRsnCd"
})
public class SubscriberStatusInfo {

    @XmlElement(name = "SubStatus")
    protected byte subStatus;
    @XmlElement(name = "SubStatusDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar subStatusDate;
    @XmlElement(name = "SubStsIssueDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar subStsIssueDate;
    @XmlElement(name = "SubStsLastAct")
    protected String subStsLastAct;
    @XmlElement(name = "SubStsRsnCd")
    protected String subStsRsnCd;

    /**
     * Gets the value of the subStatus property.
     * 
     */
    public byte getSubStatus() {
        return subStatus;
    }

    /**
     * Sets the value of the subStatus property.
     * 
     */
    public void setSubStatus(byte value) {
        this.subStatus = value;
    }

    /**
     * Gets the value of the subStatusDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSubStatusDate() {
        return subStatusDate;
    }

    /**
     * Sets the value of the subStatusDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSubStatusDate(XMLGregorianCalendar value) {
        this.subStatusDate = value;
    }

    /**
     * Gets the value of the subStsIssueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSubStsIssueDate() {
        return subStsIssueDate;
    }

    /**
     * Sets the value of the subStsIssueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSubStsIssueDate(XMLGregorianCalendar value) {
        this.subStsIssueDate = value;
    }

    /**
     * Gets the value of the subStsLastAct property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubStsLastAct() {
        return subStsLastAct;
    }

    /**
     * Sets the value of the subStsLastAct property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubStsLastAct(String value) {
        this.subStsLastAct = value;
    }

    /**
     * Gets the value of the subStsRsnCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubStsRsnCd() {
        return subStsRsnCd;
    }

    /**
     * Sets the value of the subStsRsnCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubStsRsnCd(String value) {
        this.subStsRsnCd = value;
    }

}
