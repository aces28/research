
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ValidateIfGroupOwnerResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValidateIfGroupOwnerResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ValidateIfGroupOwnerResult" type="{http://www.globe.com/warcraft/wsdl/billingcm/}ValidateIfGroupOwnerResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidateIfGroupOwnerResponse", propOrder = {
    "validateIfGroupOwnerResult"
})
public class ValidateIfGroupOwnerResponse {

    @XmlElement(name = "ValidateIfGroupOwnerResult")
    protected ValidateIfGroupOwnerResult validateIfGroupOwnerResult;

    /**
     * Gets the value of the validateIfGroupOwnerResult property.
     * 
     * @return
     *     possible object is
     *     {@link ValidateIfGroupOwnerResult }
     *     
     */
    public ValidateIfGroupOwnerResult getValidateIfGroupOwnerResult() {
        return validateIfGroupOwnerResult;
    }

    /**
     * Sets the value of the validateIfGroupOwnerResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidateIfGroupOwnerResult }
     *     
     */
    public void setValidateIfGroupOwnerResult(ValidateIfGroupOwnerResult value) {
        this.validateIfGroupOwnerResult = value;
    }

}
