
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PayChannelIdInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PayChannelIdInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PayChannelId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PayChannelIdInfo", propOrder = {
    "payChannelId"
})
public class PayChannelIdInfo {

    @XmlElement(name = "PayChannelId")
    protected int payChannelId;

    /**
     * Gets the value of the payChannelId property.
     * 
     */
    public int getPayChannelId() {
        return payChannelId;
    }

    /**
     * Sets the value of the payChannelId property.
     * 
     */
    public void setPayChannelId(int value) {
        this.payChannelId = value;
    }

}
