
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UpdateOfferOfSubscriberResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateOfferOfSubscriberResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UpdateOfferOfSubscriberResult" type="{http://www.globe.com/warcraft/wsdl/billingcm/}UpdateOfferOfSubscriberResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateOfferOfSubscriberResponse", propOrder = {
    "updateOfferOfSubscriberResult"
})
public class UpdateOfferOfSubscriberResponse {

    @XmlElement(name = "UpdateOfferOfSubscriberResult")
    protected UpdateOfferOfSubscriberResult updateOfferOfSubscriberResult;

    /**
     * Gets the value of the updateOfferOfSubscriberResult property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateOfferOfSubscriberResult }
     *     
     */
    public UpdateOfferOfSubscriberResult getUpdateOfferOfSubscriberResult() {
        return updateOfferOfSubscriberResult;
    }

    /**
     * Sets the value of the updateOfferOfSubscriberResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateOfferOfSubscriberResult }
     *     
     */
    public void setUpdateOfferOfSubscriberResult(UpdateOfferOfSubscriberResult value) {
        this.updateOfferOfSubscriberResult = value;
    }

}
