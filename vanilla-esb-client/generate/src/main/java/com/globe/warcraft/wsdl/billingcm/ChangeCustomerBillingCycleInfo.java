
package com.globe.warcraft.wsdl.billingcm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ChangeCustomerBillingCycleInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChangeCustomerBillingCycleInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustomerNo">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BillCycleNo">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}short">
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CycleProductionFrequency" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}short">
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ActivityReason" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="UserText" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="WaiveReason" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="WaiveIndicator" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;pattern value="[Y]|[y]|[N]|[n]"/>
 *               &lt;maxLength value="1"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ActivityFee" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}double">
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="LockInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}LockInfo" minOccurs="0"/>
 *         &lt;element name="ActivityPcn" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="EntityLockingInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}EntityLockingInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ClientLogicalDate" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}dateTime">
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ClientMessageLogID" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DummyInd" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;pattern value="[Y]|[y]|[N]|[n]"/>
 *               &lt;maxLength value="1"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="GeneralFieldNum" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="GeneralFieldString" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ActivityOrigin" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ActivityID" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DistributionType" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeCustomerBillingCycleInfo", propOrder = {
    "customerNo",
    "billCycleNo",
    "cycleProductionFrequency",
    "activityReason",
    "userText",
    "waiveReason",
    "waiveIndicator",
    "activityFee",
    "lockInfo",
    "activityPcn",
    "entityLockingInfo",
    "clientLogicalDate",
    "clientMessageLogID",
    "dummyInd",
    "generalFieldNum",
    "generalFieldString",
    "activityOrigin",
    "activityID",
    "distributionType"
})
public class ChangeCustomerBillingCycleInfo {

    @XmlElement(name = "CustomerNo")
    protected int customerNo;
    @XmlElement(name = "BillCycleNo")
    protected short billCycleNo;
    @XmlElement(name = "CycleProductionFrequency")
    protected Short cycleProductionFrequency;
    @XmlElement(name = "ActivityReason")
    protected String activityReason;
    @XmlElement(name = "UserText")
    protected String userText;
    @XmlElement(name = "WaiveReason")
    protected String waiveReason;
    @XmlElement(name = "WaiveIndicator")
    protected String waiveIndicator;
    @XmlElement(name = "ActivityFee")
    protected Double activityFee;
    @XmlElement(name = "LockInfo")
    protected LockInfo lockInfo;
    @XmlElement(name = "ActivityPcn")
    protected Integer activityPcn;
    @XmlElement(name = "EntityLockingInfo")
    protected List<EntityLockingInfo> entityLockingInfo;
    @XmlElement(name = "ClientLogicalDate")
    protected XMLGregorianCalendar clientLogicalDate;
    @XmlElement(name = "ClientMessageLogID")
    protected String clientMessageLogID;
    @XmlElement(name = "DummyInd")
    protected String dummyInd;
    @XmlElement(name = "GeneralFieldNum")
    protected Integer generalFieldNum;
    @XmlElement(name = "GeneralFieldString")
    protected String generalFieldString;
    @XmlElement(name = "ActivityOrigin")
    protected String activityOrigin;
    @XmlElement(name = "ActivityID")
    protected Integer activityID;
    @XmlElement(name = "DistributionType")
    protected String distributionType;

    /**
     * Gets the value of the customerNo property.
     * 
     */
    public int getCustomerNo() {
        return customerNo;
    }

    /**
     * Sets the value of the customerNo property.
     * 
     */
    public void setCustomerNo(int value) {
        this.customerNo = value;
    }

    /**
     * Gets the value of the billCycleNo property.
     * 
     */
    public short getBillCycleNo() {
        return billCycleNo;
    }

    /**
     * Sets the value of the billCycleNo property.
     * 
     */
    public void setBillCycleNo(short value) {
        this.billCycleNo = value;
    }

    /**
     * Gets the value of the cycleProductionFrequency property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getCycleProductionFrequency() {
        return cycleProductionFrequency;
    }

    /**
     * Sets the value of the cycleProductionFrequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setCycleProductionFrequency(Short value) {
        this.cycleProductionFrequency = value;
    }

    /**
     * Gets the value of the activityReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityReason() {
        return activityReason;
    }

    /**
     * Sets the value of the activityReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityReason(String value) {
        this.activityReason = value;
    }

    /**
     * Gets the value of the userText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserText() {
        return userText;
    }

    /**
     * Sets the value of the userText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserText(String value) {
        this.userText = value;
    }

    /**
     * Gets the value of the waiveReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWaiveReason() {
        return waiveReason;
    }

    /**
     * Sets the value of the waiveReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWaiveReason(String value) {
        this.waiveReason = value;
    }

    /**
     * Gets the value of the waiveIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWaiveIndicator() {
        return waiveIndicator;
    }

    /**
     * Sets the value of the waiveIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWaiveIndicator(String value) {
        this.waiveIndicator = value;
    }

    /**
     * Gets the value of the activityFee property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getActivityFee() {
        return activityFee;
    }

    /**
     * Sets the value of the activityFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setActivityFee(Double value) {
        this.activityFee = value;
    }

    /**
     * Gets the value of the lockInfo property.
     * 
     * @return
     *     possible object is
     *     {@link LockInfo }
     *     
     */
    public LockInfo getLockInfo() {
        return lockInfo;
    }

    /**
     * Sets the value of the lockInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link LockInfo }
     *     
     */
    public void setLockInfo(LockInfo value) {
        this.lockInfo = value;
    }

    /**
     * Gets the value of the activityPcn property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getActivityPcn() {
        return activityPcn;
    }

    /**
     * Sets the value of the activityPcn property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setActivityPcn(Integer value) {
        this.activityPcn = value;
    }

    /**
     * Gets the value of the entityLockingInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the entityLockingInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEntityLockingInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EntityLockingInfo }
     * 
     * 
     */
    public List<EntityLockingInfo> getEntityLockingInfo() {
        if (entityLockingInfo == null) {
            entityLockingInfo = new ArrayList<EntityLockingInfo>();
        }
        return this.entityLockingInfo;
    }

    /**
     * Gets the value of the clientLogicalDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getClientLogicalDate() {
        return clientLogicalDate;
    }

    /**
     * Sets the value of the clientLogicalDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setClientLogicalDate(XMLGregorianCalendar value) {
        this.clientLogicalDate = value;
    }

    /**
     * Gets the value of the clientMessageLogID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientMessageLogID() {
        return clientMessageLogID;
    }

    /**
     * Sets the value of the clientMessageLogID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientMessageLogID(String value) {
        this.clientMessageLogID = value;
    }

    /**
     * Gets the value of the dummyInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDummyInd() {
        return dummyInd;
    }

    /**
     * Sets the value of the dummyInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDummyInd(String value) {
        this.dummyInd = value;
    }

    /**
     * Gets the value of the generalFieldNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGeneralFieldNum() {
        return generalFieldNum;
    }

    /**
     * Sets the value of the generalFieldNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGeneralFieldNum(Integer value) {
        this.generalFieldNum = value;
    }

    /**
     * Gets the value of the generalFieldString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeneralFieldString() {
        return generalFieldString;
    }

    /**
     * Sets the value of the generalFieldString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeneralFieldString(String value) {
        this.generalFieldString = value;
    }

    /**
     * Gets the value of the activityOrigin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityOrigin() {
        return activityOrigin;
    }

    /**
     * Sets the value of the activityOrigin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityOrigin(String value) {
        this.activityOrigin = value;
    }

    /**
     * Gets the value of the activityID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getActivityID() {
        return activityID;
    }

    /**
     * Sets the value of the activityID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setActivityID(Integer value) {
        this.activityID = value;
    }

    /**
     * Gets the value of the distributionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistributionType() {
        return distributionType;
    }

    /**
     * Sets the value of the distributionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistributionType(String value) {
        this.distributionType = value;
    }

}
