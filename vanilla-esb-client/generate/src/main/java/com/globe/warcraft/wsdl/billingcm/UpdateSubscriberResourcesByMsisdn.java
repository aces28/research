
package com.globe.warcraft.wsdl.billingcm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for UpdateSubscriberResourcesByMsisdn complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateSubscriberResourcesByMsisdn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MSISDN">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="12"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ChangeLogicalResourceInputInfoList">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ChangeLogicalResourceInputInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}ChangeLogicalResourceInputInfoModel" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ReplacePhysicalResourceInputInfoList">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ReplacePhysicalResourceInputInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}ReplacePhysicalResourceInputInfoModel" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ActivityDate" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}dateTime">
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ActivityReason">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *               &lt;minLength value="1"/>
 *               &lt;whiteSpace value="collapse"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateSubscriberResourcesByMsisdn", propOrder = {
    "msisdn",
    "changeLogicalResourceInputInfoList",
    "replacePhysicalResourceInputInfoList",
    "activityDate",
    "activityReason"
})
public class UpdateSubscriberResourcesByMsisdn {

    @XmlElement(name = "MSISDN", required = true)
    protected String msisdn;
    @XmlElement(name = "ChangeLogicalResourceInputInfoList", required = true)
    protected UpdateSubscriberResourcesByMsisdn.ChangeLogicalResourceInputInfoList changeLogicalResourceInputInfoList;
    @XmlElement(name = "ReplacePhysicalResourceInputInfoList", required = true)
    protected UpdateSubscriberResourcesByMsisdn.ReplacePhysicalResourceInputInfoList replacePhysicalResourceInputInfoList;
    @XmlElement(name = "ActivityDate")
    protected XMLGregorianCalendar activityDate;
    @XmlElement(name = "ActivityReason", required = true)
    protected String activityReason;

    /**
     * Gets the value of the msisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSISDN() {
        return msisdn;
    }

    /**
     * Sets the value of the msisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSISDN(String value) {
        this.msisdn = value;
    }

    /**
     * Gets the value of the changeLogicalResourceInputInfoList property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateSubscriberResourcesByMsisdn.ChangeLogicalResourceInputInfoList }
     *     
     */
    public UpdateSubscriberResourcesByMsisdn.ChangeLogicalResourceInputInfoList getChangeLogicalResourceInputInfoList() {
        return changeLogicalResourceInputInfoList;
    }

    /**
     * Sets the value of the changeLogicalResourceInputInfoList property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateSubscriberResourcesByMsisdn.ChangeLogicalResourceInputInfoList }
     *     
     */
    public void setChangeLogicalResourceInputInfoList(UpdateSubscriberResourcesByMsisdn.ChangeLogicalResourceInputInfoList value) {
        this.changeLogicalResourceInputInfoList = value;
    }

    /**
     * Gets the value of the replacePhysicalResourceInputInfoList property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateSubscriberResourcesByMsisdn.ReplacePhysicalResourceInputInfoList }
     *     
     */
    public UpdateSubscriberResourcesByMsisdn.ReplacePhysicalResourceInputInfoList getReplacePhysicalResourceInputInfoList() {
        return replacePhysicalResourceInputInfoList;
    }

    /**
     * Sets the value of the replacePhysicalResourceInputInfoList property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateSubscriberResourcesByMsisdn.ReplacePhysicalResourceInputInfoList }
     *     
     */
    public void setReplacePhysicalResourceInputInfoList(UpdateSubscriberResourcesByMsisdn.ReplacePhysicalResourceInputInfoList value) {
        this.replacePhysicalResourceInputInfoList = value;
    }

    /**
     * Gets the value of the activityDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getActivityDate() {
        return activityDate;
    }

    /**
     * Sets the value of the activityDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setActivityDate(XMLGregorianCalendar value) {
        this.activityDate = value;
    }

    /**
     * Gets the value of the activityReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityReason() {
        return activityReason;
    }

    /**
     * Sets the value of the activityReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityReason(String value) {
        this.activityReason = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ChangeLogicalResourceInputInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}ChangeLogicalResourceInputInfoModel" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "changeLogicalResourceInputInfo"
    })
    public static class ChangeLogicalResourceInputInfoList {

        @XmlElement(name = "ChangeLogicalResourceInputInfo", required = true)
        protected List<ChangeLogicalResourceInputInfoModel> changeLogicalResourceInputInfo;

        /**
         * Gets the value of the changeLogicalResourceInputInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the changeLogicalResourceInputInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getChangeLogicalResourceInputInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ChangeLogicalResourceInputInfoModel }
         * 
         * 
         */
        public List<ChangeLogicalResourceInputInfoModel> getChangeLogicalResourceInputInfo() {
            if (changeLogicalResourceInputInfo == null) {
                changeLogicalResourceInputInfo = new ArrayList<ChangeLogicalResourceInputInfoModel>();
            }
            return this.changeLogicalResourceInputInfo;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ReplacePhysicalResourceInputInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}ReplacePhysicalResourceInputInfoModel" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "replacePhysicalResourceInputInfo"
    })
    public static class ReplacePhysicalResourceInputInfoList {

        @XmlElement(name = "ReplacePhysicalResourceInputInfo", required = true)
        protected List<ReplacePhysicalResourceInputInfoModel> replacePhysicalResourceInputInfo;

        /**
         * Gets the value of the replacePhysicalResourceInputInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the replacePhysicalResourceInputInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getReplacePhysicalResourceInputInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ReplacePhysicalResourceInputInfoModel }
         * 
         * 
         */
        public List<ReplacePhysicalResourceInputInfoModel> getReplacePhysicalResourceInputInfo() {
            if (replacePhysicalResourceInputInfo == null) {
                replacePhysicalResourceInputInfo = new ArrayList<ReplacePhysicalResourceInputInfoModel>();
            }
            return this.replacePhysicalResourceInputInfo;
        }

    }

}
