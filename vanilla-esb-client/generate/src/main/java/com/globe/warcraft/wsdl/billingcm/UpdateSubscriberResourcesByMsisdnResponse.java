
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UpdateSubscriberResourcesByMsisdnResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateSubscriberResourcesByMsisdnResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UpdateSubscriberResourcesByMsisdnResult" type="{http://www.globe.com/warcraft/wsdl/billingcm/}UpdateSubscriberResourcesByMsisdnResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateSubscriberResourcesByMsisdnResponse", propOrder = {
    "updateSubscriberResourcesByMsisdnResult"
})
public class UpdateSubscriberResourcesByMsisdnResponse {

    @XmlElement(name = "UpdateSubscriberResourcesByMsisdnResult")
    protected UpdateSubscriberResourcesByMsisdnResult updateSubscriberResourcesByMsisdnResult;

    /**
     * Gets the value of the updateSubscriberResourcesByMsisdnResult property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateSubscriberResourcesByMsisdnResult }
     *     
     */
    public UpdateSubscriberResourcesByMsisdnResult getUpdateSubscriberResourcesByMsisdnResult() {
        return updateSubscriberResourcesByMsisdnResult;
    }

    /**
     * Sets the value of the updateSubscriberResourcesByMsisdnResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateSubscriberResourcesByMsisdnResult }
     *     
     */
    public void setUpdateSubscriberResourcesByMsisdnResult(UpdateSubscriberResourcesByMsisdnResult value) {
        this.updateSubscriberResourcesByMsisdnResult = value;
    }

}
