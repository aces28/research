
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountHeader complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountHeader">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountBillingInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}AccountBillingInfo" minOccurs="0"/>
 *         &lt;element name="AccountBusinessEntityIdInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}BusinessEntityIdInfo" minOccurs="0"/>
 *         &lt;element name="AccountCollectionInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}AccountCollectionInfo" minOccurs="0"/>
 *         &lt;element name="AccountGeneralInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}AccountGeneralInfo" minOccurs="0"/>
 *         &lt;element name="AccountingManagementInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}AccountingManagementInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountHeader", propOrder = {
    "accountBillingInfo",
    "accountBusinessEntityIdInfo",
    "accountCollectionInfo",
    "accountGeneralInfo",
    "accountingManagementInfo"
})
public class AccountHeader {

    @XmlElement(name = "AccountBillingInfo")
    protected AccountBillingInfo accountBillingInfo;
    @XmlElement(name = "AccountBusinessEntityIdInfo")
    protected BusinessEntityIdInfo accountBusinessEntityIdInfo;
    @XmlElement(name = "AccountCollectionInfo")
    protected AccountCollectionInfo accountCollectionInfo;
    @XmlElement(name = "AccountGeneralInfo")
    protected AccountGeneralInfo accountGeneralInfo;
    @XmlElement(name = "AccountingManagementInfo")
    protected AccountingManagementInfo accountingManagementInfo;

    /**
     * Gets the value of the accountBillingInfo property.
     * 
     * @return
     *     possible object is
     *     {@link AccountBillingInfo }
     *     
     */
    public AccountBillingInfo getAccountBillingInfo() {
        return accountBillingInfo;
    }

    /**
     * Sets the value of the accountBillingInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountBillingInfo }
     *     
     */
    public void setAccountBillingInfo(AccountBillingInfo value) {
        this.accountBillingInfo = value;
    }

    /**
     * Gets the value of the accountBusinessEntityIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link BusinessEntityIdInfo }
     *     
     */
    public BusinessEntityIdInfo getAccountBusinessEntityIdInfo() {
        return accountBusinessEntityIdInfo;
    }

    /**
     * Sets the value of the accountBusinessEntityIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessEntityIdInfo }
     *     
     */
    public void setAccountBusinessEntityIdInfo(BusinessEntityIdInfo value) {
        this.accountBusinessEntityIdInfo = value;
    }

    /**
     * Gets the value of the accountCollectionInfo property.
     * 
     * @return
     *     possible object is
     *     {@link AccountCollectionInfo }
     *     
     */
    public AccountCollectionInfo getAccountCollectionInfo() {
        return accountCollectionInfo;
    }

    /**
     * Sets the value of the accountCollectionInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountCollectionInfo }
     *     
     */
    public void setAccountCollectionInfo(AccountCollectionInfo value) {
        this.accountCollectionInfo = value;
    }

    /**
     * Gets the value of the accountGeneralInfo property.
     * 
     * @return
     *     possible object is
     *     {@link AccountGeneralInfo }
     *     
     */
    public AccountGeneralInfo getAccountGeneralInfo() {
        return accountGeneralInfo;
    }

    /**
     * Sets the value of the accountGeneralInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountGeneralInfo }
     *     
     */
    public void setAccountGeneralInfo(AccountGeneralInfo value) {
        this.accountGeneralInfo = value;
    }

    /**
     * Gets the value of the accountingManagementInfo property.
     * 
     * @return
     *     possible object is
     *     {@link AccountingManagementInfo }
     *     
     */
    public AccountingManagementInfo getAccountingManagementInfo() {
        return accountingManagementInfo;
    }

    /**
     * Sets the value of the accountingManagementInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountingManagementInfo }
     *     
     */
    public void setAccountingManagementInfo(AccountingManagementInfo value) {
        this.accountingManagementInfo = value;
    }

}
