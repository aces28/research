
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for PayChannelPaymentMethodInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PayChannelPaymentMethodInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BankAccountNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BankAccountType" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="BankBranchNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BankCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreditCardExpirationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="CreditCardNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreditCardType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IssueDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="PaymentMeansOwnerDetails" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaymentMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaymentType" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="Pin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RecurringAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="RecurringFrequency" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="RecurringFrequencyValue" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="L9ChargeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="L9PostpaidFA" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="L9PostpaidBA" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PayChannelPaymentMethodInfo", propOrder = {
    "bankAccountNo",
    "bankAccountType",
    "bankBranchNo",
    "bankCode",
    "creditCardExpirationDate",
    "creditCardNo",
    "creditCardType",
    "issueDate",
    "paymentMeansOwnerDetails",
    "paymentMethod",
    "paymentType",
    "pin",
    "recurringAmount",
    "recurringFrequency",
    "recurringFrequencyValue",
    "l9ChargeType",
    "l9PostpaidFA",
    "l9PostpaidBA"
})
public class PayChannelPaymentMethodInfo {

    @XmlElement(name = "BankAccountNo")
    protected String bankAccountNo;
    @XmlElement(name = "BankAccountType")
    protected byte bankAccountType;
    @XmlElement(name = "BankBranchNo")
    protected String bankBranchNo;
    @XmlElement(name = "BankCode")
    protected String bankCode;
    @XmlElement(name = "CreditCardExpirationDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar creditCardExpirationDate;
    @XmlElement(name = "CreditCardNo")
    protected String creditCardNo;
    @XmlElement(name = "CreditCardType")
    protected String creditCardType;
    @XmlElement(name = "IssueDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar issueDate;
    @XmlElement(name = "PaymentMeansOwnerDetails")
    protected String paymentMeansOwnerDetails;
    @XmlElement(name = "PaymentMethod")
    protected String paymentMethod;
    @XmlElement(name = "PaymentType")
    protected byte paymentType;
    @XmlElement(name = "Pin")
    protected String pin;
    @XmlElement(name = "RecurringAmount")
    protected double recurringAmount;
    @XmlElement(name = "RecurringFrequency")
    protected byte recurringFrequency;
    @XmlElement(name = "RecurringFrequencyValue")
    protected short recurringFrequencyValue;
    @XmlElement(name = "L9ChargeType")
    protected String l9ChargeType;
    @XmlElement(name = "L9PostpaidFA")
    protected int l9PostpaidFA;
    @XmlElement(name = "L9PostpaidBA")
    protected int l9PostpaidBA;

    /**
     * Gets the value of the bankAccountNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankAccountNo() {
        return bankAccountNo;
    }

    /**
     * Sets the value of the bankAccountNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankAccountNo(String value) {
        this.bankAccountNo = value;
    }

    /**
     * Gets the value of the bankAccountType property.
     * 
     */
    public byte getBankAccountType() {
        return bankAccountType;
    }

    /**
     * Sets the value of the bankAccountType property.
     * 
     */
    public void setBankAccountType(byte value) {
        this.bankAccountType = value;
    }

    /**
     * Gets the value of the bankBranchNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankBranchNo() {
        return bankBranchNo;
    }

    /**
     * Sets the value of the bankBranchNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankBranchNo(String value) {
        this.bankBranchNo = value;
    }

    /**
     * Gets the value of the bankCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankCode() {
        return bankCode;
    }

    /**
     * Sets the value of the bankCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankCode(String value) {
        this.bankCode = value;
    }

    /**
     * Gets the value of the creditCardExpirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreditCardExpirationDate() {
        return creditCardExpirationDate;
    }

    /**
     * Sets the value of the creditCardExpirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreditCardExpirationDate(XMLGregorianCalendar value) {
        this.creditCardExpirationDate = value;
    }

    /**
     * Gets the value of the creditCardNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditCardNo() {
        return creditCardNo;
    }

    /**
     * Sets the value of the creditCardNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditCardNo(String value) {
        this.creditCardNo = value;
    }

    /**
     * Gets the value of the creditCardType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditCardType() {
        return creditCardType;
    }

    /**
     * Sets the value of the creditCardType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditCardType(String value) {
        this.creditCardType = value;
    }

    /**
     * Gets the value of the issueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getIssueDate() {
        return issueDate;
    }

    /**
     * Sets the value of the issueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setIssueDate(XMLGregorianCalendar value) {
        this.issueDate = value;
    }

    /**
     * Gets the value of the paymentMeansOwnerDetails property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentMeansOwnerDetails() {
        return paymentMeansOwnerDetails;
    }

    /**
     * Sets the value of the paymentMeansOwnerDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentMeansOwnerDetails(String value) {
        this.paymentMeansOwnerDetails = value;
    }

    /**
     * Gets the value of the paymentMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Sets the value of the paymentMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentMethod(String value) {
        this.paymentMethod = value;
    }

    /**
     * Gets the value of the paymentType property.
     * 
     */
    public byte getPaymentType() {
        return paymentType;
    }

    /**
     * Sets the value of the paymentType property.
     * 
     */
    public void setPaymentType(byte value) {
        this.paymentType = value;
    }

    /**
     * Gets the value of the pin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPin() {
        return pin;
    }

    /**
     * Sets the value of the pin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPin(String value) {
        this.pin = value;
    }

    /**
     * Gets the value of the recurringAmount property.
     * 
     */
    public double getRecurringAmount() {
        return recurringAmount;
    }

    /**
     * Sets the value of the recurringAmount property.
     * 
     */
    public void setRecurringAmount(double value) {
        this.recurringAmount = value;
    }

    /**
     * Gets the value of the recurringFrequency property.
     * 
     */
    public byte getRecurringFrequency() {
        return recurringFrequency;
    }

    /**
     * Sets the value of the recurringFrequency property.
     * 
     */
    public void setRecurringFrequency(byte value) {
        this.recurringFrequency = value;
    }

    /**
     * Gets the value of the recurringFrequencyValue property.
     * 
     */
    public short getRecurringFrequencyValue() {
        return recurringFrequencyValue;
    }

    /**
     * Sets the value of the recurringFrequencyValue property.
     * 
     */
    public void setRecurringFrequencyValue(short value) {
        this.recurringFrequencyValue = value;
    }

    /**
     * Gets the value of the l9ChargeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getL9ChargeType() {
        return l9ChargeType;
    }

    /**
     * Sets the value of the l9ChargeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setL9ChargeType(String value) {
        this.l9ChargeType = value;
    }

    /**
     * Gets the value of the l9PostpaidFA property.
     * 
     */
    public int getL9PostpaidFA() {
        return l9PostpaidFA;
    }

    /**
     * Sets the value of the l9PostpaidFA property.
     * 
     */
    public void setL9PostpaidFA(int value) {
        this.l9PostpaidFA = value;
    }

    /**
     * Gets the value of the l9PostpaidBA property.
     * 
     */
    public int getL9PostpaidBA() {
        return l9PostpaidBA;
    }

    /**
     * Sets the value of the l9PostpaidBA property.
     * 
     */
    public void setL9PostpaidBA(int value) {
        this.l9PostpaidBA = value;
    }

}
