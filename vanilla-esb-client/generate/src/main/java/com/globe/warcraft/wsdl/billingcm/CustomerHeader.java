
package com.globe.warcraft.wsdl.billingcm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerHeader complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerHeader">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustomerTypeInfo" type="{http://www.globe.com/warcraft/wsdl/billingcm/}CustomerTypeInfo" minOccurs="0"/>
 *         &lt;element name="LockDetails" type="{http://www.globe.com/warcraft/wsdl/billingcm/}LockDetails" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerHeader", propOrder = {
    "customerTypeInfo",
    "lockDetails"
})
public class CustomerHeader {

    @XmlElement(name = "CustomerTypeInfo")
    protected CustomerTypeInfo customerTypeInfo;
    @XmlElement(name = "LockDetails")
    protected LockDetails lockDetails;

    /**
     * Gets the value of the customerTypeInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerTypeInfo }
     *     
     */
    public CustomerTypeInfo getCustomerTypeInfo() {
        return customerTypeInfo;
    }

    /**
     * Sets the value of the customerTypeInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerTypeInfo }
     *     
     */
    public void setCustomerTypeInfo(CustomerTypeInfo value) {
        this.customerTypeInfo = value;
    }

    /**
     * Gets the value of the lockDetails property.
     * 
     * @return
     *     possible object is
     *     {@link LockDetails }
     *     
     */
    public LockDetails getLockDetails() {
        return lockDetails;
    }

    /**
     * Sets the value of the lockDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link LockDetails }
     *     
     */
    public void setLockDetails(LockDetails value) {
        this.lockDetails = value;
    }

}
