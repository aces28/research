package ph.com.ncs.vanilla.esb.client.wsdl.service;

import com.globe.warcraft.wsdl.billingcm.*;
import com.globe.warcraft.wsdl.header.WarcraftHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ph.com.ncs.vanilla.esb.client.wsdl.exceptions.ESBClientException;

import javax.xml.ws.Holder;

public class ESBClientService {

    private static final Logger logger = LoggerFactory.getLogger(ESBClientService.class);


    public String getDetailsByMsisdn(String msisdn, String primaryResourceType) {

        logger.info("getDetailsByMsisdn: Starting Process");
        logger.info("MSISDN: " + msisdn);
        logger.info("Primary Resource Type: " + primaryResourceType);

        try {
            validateParameters(msisdn, primaryResourceType);
        } catch (ESBClientException e) {
            e.printStackTrace();
        }


        BillingCMService service = new BillingCMService();

        BillingCMProxyService client = service.getBillingCMServicePort();

        //GetDetailsByMsisdn Model generated from wsdl
        GetDetailsByMsisdn getDetailsByMsisdnParameters = new GetDetailsByMsisdn();
        getDetailsByMsisdnParameters.setMSISDN(msisdn);
        getDetailsByMsisdnParameters.setPrimaryResourceType(primaryResourceType);

        Holder<GetDetailsByMsisdnResponse> holderGetDetailsByMsisdnResponse = new Holder<GetDetailsByMsisdnResponse>();
        Holder<WarcraftHeader> holderWarcraftHeader = new Holder<WarcraftHeader>();

        client.getDetailsByMsisdn(getDetailsByMsisdnParameters, holderGetDetailsByMsisdnResponse, holderWarcraftHeader);

        GetDetailsByMsisdnResponse getDetailsByMsisdnResponse = holderGetDetailsByMsisdnResponse.value;

        GetDetailsByMsisdnResult getDetailsByMsisdnResult = getDetailsByMsisdnResponse.getGetDetailsByMsisdnResult();

        SubscriberHeader subscriberHeader = new SubscriberHeader();
        subscriberHeader = getDetailsByMsisdnResult.getSubscriberHeader();

        SubscriberGeneralInfo subscriberGeneralInfo = new SubscriberGeneralInfo();

        subscriberGeneralInfo = subscriberHeader.getSubscriberGeneralInfo();

        String subscriberTypeResult = subscriberGeneralInfo.getPrimResourceTp();

        return subscriberTypeResult;

    }

    private void validateParameters(String msisdn, String primaryResourceType) throws ESBClientException {
        //validate msisdn if contains alphabet
        if (!msisdn.matches("[0-9]+")) {
            throw new ESBClientException("Invalid MSISDN! MSISDN is only numeric: " + msisdn);
        }

        //validate msisdn if length is less than 11
        if (msisdn.length() != 11) {
            throw new ESBClientException("Invalid MSISDN length! MSISDN Should only be 11 digits: " + msisdn);
        }

        //validate msisdn if null or empty string
        if (msisdn.equals("") && msisdn == null) {
            throw new ESBClientException("MSISDN Parameter is null");
        }

        //primaryResourceType msisdn if null or empty string
        if (primaryResourceType.equals("") && primaryResourceType == null) {
            throw new ESBClientException("Primary Resource Type Parameter is Null");
        }
    }
}
