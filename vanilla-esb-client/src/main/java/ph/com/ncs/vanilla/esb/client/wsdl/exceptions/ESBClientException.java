package ph.com.ncs.vanilla.esb.client.wsdl.exceptions;

public class ESBClientException extends Exception {

    public ESBClientException() {
    }

    public ESBClientException(String message) {
        super(message);
    }
}
