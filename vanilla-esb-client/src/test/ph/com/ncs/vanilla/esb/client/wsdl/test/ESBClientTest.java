package ph.com.ncs.vanilla.esb.client.wsdl.test;

import org.junit.Test;
import ph.com.ncs.vanilla.esb.client.wsdl.service.ESBClientService;

import static org.junit.Assert.assertEquals;

public class ESBClientTest {

    @Test
    public void testGetDetailsByMsisdnPositiveResult() {
        System.out.println("Start testGetDetailsByMsisdnPositiveResult");
        ESBClientService esbClientService = new ESBClientService();

        String result = esbClientService.getDetailsByMsisdn("09158031084", "LTP");
        System.out.println(result);
        assertEquals("Prepaid", "Prepaid");
        System.out.println("END testGetDetailsByMsisdnPositiveResult");
    }

}
