package ph.com.ncs.vanilla.approval.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import ph.com.ncs.vanilla.application.domain.SubsApplication;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by edjohna on 6/24/2016.
 */
@Transactional
public interface ApproverRepository extends JpaRepository<SubsApplication, Integer> {


    @Query(value = "select application from SubsApplication application where application.user.userName = ?1 and (application.status between 1 and 2)")
    Page<SubsApplication> listForApprovalApprover(String userName, Pageable request);

    @Query(value = "select application from SubsApplication application where application.user.userName = ?1 and application.status = 3")
    Page<SubsApplication> listApprovedApprover(String userName, Pageable request);

    @Query(value = "select application from SubsApplication application where application.user.userName = ?1 and application.status = 4")
    Page<SubsApplication> listDisapprovedApprover(String userName, Pageable request);

    @Query(value = "select application from SubsApplication application " +
            "where application.user.userName = ?1 and (application.status between 1 and 2) and application.applicationDate between ?3 and ?2")
    Page<SubsApplication> filterForApprovalApprover(String userName, Timestamp to, Timestamp from, Pageable request);

    @Query(value = "select application from SubsApplication application " +
            "where application.user.userName = ?1 and application.status = 3 and application.applicationDate between ?3 and ?2")
    Page<SubsApplication> filterApprovedApprover(String userName, Timestamp to, Timestamp from, Pageable request);

    @Query(value = "select application from SubsApplication application " +
            "where application.user.userName = ?1 and application.status = 4 and application.applicationDate between ?3 and ?2")
    Page<SubsApplication> filterDisapprovedApprover(String userName, Timestamp to, Timestamp from, Pageable request);

    @Query(value = "select appl from SubsApplication appl " +
            "where  (appl.user.userName = :uid and appl.status between 1 and 2) and (appl.subscriberMsisdn like concat('%', :keyword, '%') or appl.userEmail like concat('%', :keyword, '%') " +
            "or appl.attachment like concat('%', :keyword, '%') or appl.termsConditions like concat('%', :keyword, '%') or appl.accountType like concat('%', :keyword, '%')" +
            "or appl.subscriber.lastName like concat('%', :keyword, '%') or appl.subscriber.firstName like concat('%', :keyword, '%') " +
            "or appl.subscriber.address like concat('%', :keyword, '%') or appl.subscriber.mothersName like concat('%', :keyword, '%') " +
            "or appl.subscriber.gender like concat('%', :keyword, '%') or appl.subscriber.middleName like concat('%', :keyword, '%'))")
    Page<SubsApplication> searchForApprovalString(@Param("keyword")String keyword, @Param("uid") String userName, Pageable pageable);

    // appl.status between 2 and 5
    @Query(value = "select appl from SubsApplication appl " +
            "where  (appl.user.userName = :uid and appl.status = 3) and (appl.subscriberMsisdn like concat('%', :keyword, '%') or appl.userEmail like concat('%', :keyword, '%') " +
            "or appl.attachment like concat('%', :keyword, '%') or appl.termsConditions like concat('%', :keyword, '%') or appl.accountType like concat('%', :keyword, '%')" +
            "or appl.subscriber.lastName like concat('%', :keyword, '%') or appl.subscriber.firstName like concat('%', :keyword, '%') " +
            "or appl.subscriber.address like concat('%', :keyword, '%') or appl.subscriber.mothersName like concat('%', :keyword, '%') " +
            "or appl.subscriber.gender like concat('%', :keyword, '%') or appl.subscriber.middleName like concat('%', :keyword, '%'))")
    Page<SubsApplication> searchApprovedString(@Param("keyword")String keyword, @Param("uid") String userName, Pageable pageable);

    @Query(value = "select appl from SubsApplication appl " +
            "where  (appl.user.userName = :uid and appl.status = 4) and (appl.subscriberMsisdn like concat('%', :keyword, '%') or appl.userEmail like concat('%', :keyword, '%') " +
            "or appl.attachment like concat('%', :keyword, '%') or appl.termsConditions like concat('%', :keyword, '%') or appl.accountType like concat('%', :keyword, '%') " +
            "or appl.subscriber.lastName like concat('%', :keyword, '%') or appl.subscriber.firstName like concat('%', :keyword, '%') " +
            "or appl.subscriber.address like concat('%', :keyword, '%') or appl.subscriber.mothersName like concat('%', :keyword, '%') " +
            "or appl.subscriber.gender like concat('%', :keyword, '%') or appl.subscriber.middleName like concat('%', :keyword, '%') " +
            "or appl.rejection.reasonName like concat('%', :keyword, '%') or appl.rejection.reasonDescription like concat('%', :keyword, '%'))")
    Page<SubsApplication> searchDispprovedString(@Param("keyword")String keyword, @Param("uid") String userName, Pageable pageable);

}
