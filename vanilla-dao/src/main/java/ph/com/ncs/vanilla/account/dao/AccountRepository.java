package ph.com.ncs.vanilla.account.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import ph.com.ncs.vanilla.subscriber.domain.Account;

import javax.transaction.Transactional;

/**
 * @author allango 7/13/2016
 */
@Transactional
public interface AccountRepository extends JpaRepository<Account, Integer> {

    @Query(value = "select count(account) from Account account where account.msisdn = ?1 and account.status = 1")
    Integer msisdnLookup(String msisdn);

}
