package ph.com.ncs.vanilla.transaction.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.scheduling.annotation.Async;
import ph.com.ncs.vanilla.base.domain.Model;
import ph.com.ncs.vanilla.transaction.domain.Transaction;

import javax.transaction.Transactional;

/**
 * Created by ace on 5/19/16.
 */
@Transactional
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

    @Async
    @Query(value = "select txn from Transaction txn where txn.code = ?1 and txn.msisdn = ?2 and txn.status = 2")
    <T extends Model> T findByCodeAndMsisdn(int code, String msisdn);

    @Async
    @Query(value = "select txn from Transaction txn where txn.msisdn = ?1 and txn.status <> -1")
    <T extends Model> T findByFailedMsisdn(String msisdn);


    @Query(value = "select txn from Transaction txn where txn.msisdn = ?1 and txn.status = 2")
    <T extends Model> T findBySuccessMsisdn(String msisdn);
}
