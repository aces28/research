package ph.com.ncs.vanilla.role.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import ph.com.ncs.vanilla.role.domain.Role;

/**
 * Created by ace on 7/11/16.
 */
@Transactional
public interface RoleRepository extends JpaRepository<Role, Integer> {
	
    @Query(value = "select role from Role role where role.id = ?1")
    Role findRoleById(int roleId);
}
