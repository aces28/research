package ph.com.ncs.vanilla.group.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import ph.com.ncs.vanilla.group.domain.Group;

import java.util.List;

/**
 * Created by edjohna on 6/29/2016.
 */
@Transactional
public interface GroupRepository extends JpaRepository<Group, Integer> {

    @Query(value = "select grp from Group grp")
    Page<Group> listGroup(Pageable request);

    @Query(value = "select grp from Group grp where grp.status = 1")
    List<Group> listAvailableGroup();

    @Query(value = "select grp from Group grp " + "where grp.groupName like concat('%', :keyword, '%') or grp.role.roleType  like concat('%', :keyword, '%')")
    Page<Group> searchGroup(@Param("keyword") String keyword, Pageable request);

    @Query(value = "select grp from Group grp " + "where grp.groupName like concat('%', :keyword, '%') or grp.role.roleType  like concat('%', :keyword, '%')")
    Page<Group> extractGroup(@Param("keyword") String keyword, Pageable request);

    @Query(value = "select grp from Group grp where grp.groupName = ?1")
    Group findGroupByName(String groupName);

}