package ph.com.ncs.vanilla.rejection.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import org.springframework.transaction.annotation.Transactional;
import ph.com.ncs.vanilla.rejection.domain.Rejection;

@Transactional
public interface RejectionRepository extends JpaRepository<Rejection, Integer> {
}
