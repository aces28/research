package ph.com.ncs.vanilla.module.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ph.com.ncs.vanilla.audit.domain.AuditModule;

/**
 * Created by jernardob on 08/08/2016.
 */
@Transactional
public interface ModuleRepository extends JpaRepository<AuditModule, Integer> {

    @Query(value = "select module from AuditModule module where module.moduleId = ?1")
    AuditModule findByModuleId(int moduleId);
}
