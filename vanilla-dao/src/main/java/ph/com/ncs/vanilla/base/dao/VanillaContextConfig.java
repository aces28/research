package ph.com.ncs.vanilla.base.dao;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ph.com.ncs.vanilla.email.domain.Email;

/**
 * Created by ace on 5/18/16.
 */
@Configuration
@ComponentScan(basePackages = {"ph.com.ncs.vanilla"})
@EnableAutoConfiguration(exclude = {JpaRepositoriesAutoConfiguration.class})
@EnableTransactionManagement
@EntityScan(basePackages = {"ph.com.ncs.vanilla.*.domain"})
@PropertySource(value = {"classpath:application-mysql-auth.properties", "classpath:application-webservice.properties", "classpath:application-email.properties",
						"classpath:application-message.properties", "classpath:application-ldap.properties", "classpath:application-batch.properties", 
						"classpath:application-aws.properties", "classpath:application-encrypt.properties", "classpath:application-audit.properties"})
@EnableJpaRepositories(basePackages = {"ph.com.ncs.vanilla.*.dao"})
public class VanillaContextConfig {

    private static final Logger logger = LoggerFactory.getLogger(VanillaContextConfig.class);

    @Autowired
    private Environment resource;

    @Value("${cloud.aws.credentials.accessKey}")
    private String accessKey;

    @Value("${cloud.aws.credentials.secretKey}")
    private String secretKey;

    @Value("${cloud.aws.region}")
    private String region;

    @Value("${cloud.aws.credentials.enable}")
    private boolean enabled;

    @Bean
    protected Email setEmail() {
        Email email = new Email();
        email.setFrom(resource.getRequiredProperty("email.from"));
        email.setSubject(resource.getRequiredProperty("email.subject"));
        email.setBody(resource.getRequiredProperty("email.body"));
        email.setHost(resource.getRequiredProperty("email.server"));
        email.setSmtpUsername(resource.getRequiredProperty("email.user"));
        email.setSmtpPassword(resource.getRequiredProperty("email.password"));
        email.setPort(Integer.parseInt(resource.getRequiredProperty("email.port.25")));
        email.setProtocol(resource.getRequiredProperty("email.protocol"));
        email.setAuthEnabled(resource.getRequiredProperty("email.auth.enable"));
        email.setTlsEnabled(resource.getRequiredProperty("email.tls.enable"));
        email.setTlsRequired(resource.getRequiredProperty("email.tls.required"));
        return email;
    }

    @Bean
    public BasicAWSCredentials basicAWSCredentials() {
        return new BasicAWSCredentials(accessKey, secretKey);
    }

    @Bean
    public AmazonS3Client amazonS3Client() {
        AmazonS3Client amazonS3;
        if (enabled) {
            amazonS3 = new AmazonS3Client(basicAWSCredentials());
        } else {
            amazonS3 = new AmazonS3Client();
        }

        return amazonS3;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
        logger.info("load configuration : START");
        return new PropertySourcesPlaceholderConfigurer();
    }


}
