package ph.com.ncs.vanilla.mis.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import ph.com.ncs.vanilla.application.domain.SubsApplication;

import java.sql.Timestamp;

/**
 * Created by edjohna on 6/28/2016.
 */
@Transactional
public interface MISRepository extends JpaRepository<SubsApplication, Integer> {

    @Query(value = "select application from SubsApplication application where application.status = 0")
    Page<SubsApplication> listForAssignmentMIS(Pageable request);

    @Query(value = "select application from SubsApplication application where application.status between 1 and 2")
    Page<SubsApplication> listForApprovalMIS(Pageable request);

    @Query(value = "select application from SubsApplication application where application.status = 3")
    Page<SubsApplication> listApprovedMIS(Pageable request);

    @Query(value = "select application from SubsApplication application where application.status = 4")
    Page<SubsApplication> listDisapprovedMIS(Pageable request);

    @Query(value = "select application from SubsApplication application where (application.status = 0) and (application.applicationDate between ?2 and ?1)")
    Page<SubsApplication> filterForAssignmentMIS(Timestamp to, Timestamp from, Pageable request);

    @Query(value = "select application from SubsApplication application where (application.status between 1 and 2) and (application.applicationDate between ?2 and ?1)")
    Page<SubsApplication> filterForApprovalMIS(Timestamp to, Timestamp from, Pageable request);

    @Query(value = "select application from SubsApplication application where (application.status = 3) and (application.applicationDate between ?2 and ?1)")
    Page<SubsApplication> filterApprovedMIS(Timestamp to, Timestamp from, Pageable request);

    @Query(value = "select application from SubsApplication application where (application.status = 4) and (application.applicationDate between ?2 and ?1)")
    Page<SubsApplication> filterDisapprovedMIS(Timestamp to, Timestamp from, Pageable request);

    @Query(value = "select appl from SubsApplication appl " +
            "where  (appl.status = 0) and (appl.subscriberMsisdn like concat('%', :keyword, '%') or appl.userEmail like concat('%', :keyword, '%') " +
            "or appl.attachment like concat('%', :keyword, '%') or appl.termsConditions like concat('%', :keyword, '%') or appl.accountType like concat('%', :keyword, '%') " +
            "or appl.subscriber.lastName like concat('%', :keyword, '%') or appl.subscriber.firstName like concat('%', :keyword, '%') " +
            "or appl.subscriber.address like concat('%', :keyword, '%') or appl.subscriber.mothersName like concat('%', :keyword, '%') " +
            "or appl.subscriber.gender like concat('%', :keyword, '%') or appl.subscriber.middleName like concat('%', :keyword, '%'))")
    Page<SubsApplication> searchForAssignmentString(@Param("keyword") String keyword, Pageable pageable);

    @Query(value = "select appl from SubsApplication appl " +
            "where  (appl.status between 1 and 2) and (appl.subscriberMsisdn like concat('%', :keyword, '%') or appl.userEmail like concat('%', :keyword, '%') " +
            "or appl.attachment like concat('%', :keyword, '%') or appl.termsConditions like concat('%', :keyword, '%') or appl.accountType like concat('%', :keyword, '%') " +
            "or appl.subscriber.lastName like concat('%', :keyword, '%') or appl.subscriber.firstName like concat('%', :keyword, '%') " +
            "or appl.subscriber.address like concat('%', :keyword, '%') or appl.subscriber.mothersName like concat('%', :keyword, '%') " +
            "or appl.subscriber.gender like concat('%', :keyword, '%') or appl.subscriber.middleName like concat('%', :keyword, '%') " +
            "or appl.user.userName like concat('%', :keyword, '%'))")
    Page<SubsApplication> searchForApprovalString(@Param("keyword") String keyword, Pageable pageable);

    @Query(value = "select appl from SubsApplication appl " +
            "where  (appl.status = 3) and (appl.subscriberMsisdn like concat('%', :keyword, '%') or appl.userEmail like concat('%', :keyword, '%') " +
            "or appl.attachment like concat('%', :keyword, '%') or appl.termsConditions like concat('%', :keyword, '%') or appl.accountType like concat('%', :keyword, '%') " +
            "or appl.subscriber.lastName like concat('%', :keyword, '%') or appl.subscriber.firstName like concat('%', :keyword, '%') " +
            "or appl.subscriber.address like concat('%', :keyword, '%') or appl.subscriber.mothersName like concat('%', :keyword, '%') " +
            "or appl.subscriber.gender like concat('%', :keyword, '%') or appl.subscriber.middleName like concat('%', :keyword, '%') " +
            "or appl.user.userName like concat('%', :keyword, '%'))")
    Page<SubsApplication> searchApprovedString(@Param("keyword") String keyword, Pageable pageable);

    @Query(value = "select appl from SubsApplication appl " +
            "where  (appl.status = 4) and (appl.subscriberMsisdn like concat('%', :keyword, '%') or appl.userEmail like concat('%', :keyword, '%') " +
            "or appl.attachment like concat('%', :keyword, '%') or appl.termsConditions like concat('%', :keyword, '%') or appl.accountType like concat('%', :keyword, '%') " +
            "or appl.subscriber.lastName like concat('%', :keyword, '%') or appl.subscriber.firstName like concat('%', :keyword, '%') " +
            "or appl.subscriber.address like concat('%', :keyword, '%') or appl.subscriber.mothersName like concat('%', :keyword, '%') " +
            "or appl.subscriber.gender like concat('%', :keyword, '%') or appl.subscriber.middleName like concat('%', :keyword, '%') " +
            "or appl.user.userName like concat('%', :keyword, '%') or appl.rejection.reasonName like concat('%', :keyword, '%') " +
            "or appl.rejection.reasonDescription like concat('%', :keyword, '%'))")
    Page<SubsApplication> searchDispprovedString(@Param("keyword") String keyword, Pageable pageable);

    @Query(value = "select count(application) from SubsApplication application where (application.user.id = ?1) and (application.status = 1 or application.status = 2)")
    Long approverOngoing(Integer id);

    @Query(value = "select count(application) from SubsApplication application where (application.user.id = ?1) and (application.status = 3 or application.status = 4)")
    Long approverCompleted(Integer id);


}
