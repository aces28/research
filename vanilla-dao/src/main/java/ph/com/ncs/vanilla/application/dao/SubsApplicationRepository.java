package ph.com.ncs.vanilla.application.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ph.com.ncs.vanilla.application.domain.SubsApplication;
import ph.com.ncs.vanilla.base.domain.Model;

import javax.transaction.Transactional;

import java.util.List;

/**
 * @author edjohna 5/26/2016
 */
@Transactional
public interface SubsApplicationRepository extends JpaRepository<SubsApplication, Integer> {

    /**
     * @param msisdn
     * @return
     */
    @Query(value = "select application from SubsApplication application where application.subscriberMsisdn = ?1 " +
            "order by application.applicationDate desc")
    List<SubsApplication> findApplicationByMsisdn(String msisdn);
    
    /**
     * 
     * @param status
     * @param userId
     * @return
     */
    @Query(value = "select count(application) from SubsApplication application where application.status = ?1 and application.user.id = ?2")
    Long countApplicationUpdatedByApprover(Integer status, Integer userId);
}
