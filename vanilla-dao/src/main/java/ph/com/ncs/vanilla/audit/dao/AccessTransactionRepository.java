package ph.com.ncs.vanilla.audit.dao;

import java.sql.Timestamp;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ph.com.ncs.vanilla.audit.domain.AuditAccessTransaction;

/**
 * Created by edjohna on 7/17/2016.
 */
@Transactional
public interface AccessTransactionRepository extends JpaRepository<AuditAccessTransaction, Integer> {

    @Query(value = "select audit from AuditAccessTransaction audit where audit.module.auditType = 1")
    Page<AuditAccessTransaction> listUserAccessHistory(Pageable request);

    @Query(value = "select audit from AuditAccessTransaction audit where audit.module.auditType = 2")
    Page<AuditAccessTransaction> listTransactionLogs(Pageable request);

    @Query(value = "select audit from AuditAccessTransaction audit where audit.module.auditType = 1 and audit.module.moduleId = ?1")
    Page<AuditAccessTransaction> filterByModuleUserAccessHistory(int moduleId, Pageable request);

    @Query(value = "select audit from AuditAccessTransaction audit where audit.module.auditType = 2 and audit.module.moduleId = ?1")
    Page<AuditAccessTransaction> filterByModuleTransactionLogs(int moduleId, Pageable request);

    @Query(value = "select audit from AuditAccessTransaction audit where audit.module.auditType = 1 and audit.dateAccessed between ?1 and ?2")
    Page<AuditAccessTransaction> filterByDateUserAccessHistory(Timestamp from, Timestamp to, Pageable request);

    @Query(value = "select audit from AuditAccessTransaction audit where audit.module.auditType = 2 and audit.dateAccessed between ?1 and ?2")
    Page<AuditAccessTransaction> filterByDateTransactionLogs(Timestamp from, Timestamp to, Pageable request);

    @Query(value = "select audit from AuditAccessTransaction audit where audit.module.auditType = 1 and (audit.user.userName like concat('%', :keyword, '%') " +
            "or audit.user.lastName like concat('%', :keyword, '%') or audit.user.firstName like concat('%', :keyword, '%') " +
            "or audit.ipAddress like concat('%', :keyword, '%'))")
    Page<AuditAccessTransaction> searchUserAccessHistory(@Param("keyword") String keyword, Pageable pageable);

    @Query(value = "select audit from AuditAccessTransaction audit where audit.module.auditType = 2 and (audit.user.userName like concat('%', :keyword, '%') " +
            "or audit.user.lastName like concat('%', :keyword, '%') or audit.user.firstName like concat('%', :keyword, '%') " +
            "or audit.ipAddress like concat('%', :keyword, '%'))")
    Page<AuditAccessTransaction> searchTransactionLogs(@Param("keyword") String keyword, Pageable pageable);
    
    @Query(value = "select audit from AuditAccessTransaction audit where audit.dateAccessed in (select max(audit2.dateAccessed) "
    		+ "from AuditAccessTransaction audit2 where audit2.module.moduleId = 1 group by audit2.user.id)")
    List<AuditAccessTransaction> generateLAR();
}
