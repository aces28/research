package ph.com.ncs.vanilla.resource.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import ph.com.ncs.vanilla.resource.domain.Resource;

import javax.transaction.Transactional;

@Transactional
public interface ResourceRepository extends JpaRepository<Resource, Integer> {
}
