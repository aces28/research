package ph.com.ncs.vanilla.user.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import ph.com.ncs.vanilla.user.domain.User;

import java.util.List;

/**
 * Created by edjohna on 6/29/2016.
 */
@Transactional
public interface UserRepository extends JpaRepository<User, Integer> {

    @Query(value = "select user from User user where user.status <= 1")
    Page<User> listUser(Pageable request);

    @Query(value = "select user from User user where user.role.id = 3 and user.status <= 1")
    Page<User> listApprover(Pageable request);

    @Query(value = "select user from User user where user.role.id = 3 and user.status = 1 and user.group.groupName =?1")
    List<User> listApproverForAssignment(String groupName);

    @Query(value = "select user from User user where user.group.groupName =?1 and user.status <= 1")
    Page<User> filterByGroupName(String groupName, Pageable request);

    @Query(value = "select user from User user where user.group.groupName =?1 and user.status <= 1")
    Page<User> extractByGroupName(String groupName, Pageable request);

    @Query(value = "select user from User user where user.status = ?1 and user.role.id = 3")
    Page<User> filterByApproverAvailability(Integer availability, Pageable request);

    @Query(value = "select user from User user where user.status = ?1 and user.role.id = 3")
    Page<User> extractByApproverAvailability(int availability, Pageable request);

    @Query(value = "select user from User user where user.isLocked = ?1")
    Page<User> filterByAvailability(int availability, Pageable request);

    @Query(value = "select user from User user where user.status = ?1 and user.status <= 1")
    Page<User> extractByAvailability(int availability, Pageable request);

    @Query(value = "select user from User user where user.role.roleType = ?1 and user.status <= 1")
    Page<User> filterByRole(String roleType, Pageable request);

    @Query(value = "select user from User user where user.role.roleType = ?1 and user.status <= 1")
    Page<User> extractByRole(String roleType, Pageable request);

    @Query(value = "select user from User user where user.role.id = 3 and user.group.id = 0 and user.isLocked < 1")
    List<User> getApproversNoGroup();

    @Query(value = "select user from User user where user.role.id = 3 and user.group.id = ?1 and user.isLocked < 1")
    List<User> getMembersOfGroup(int groupId);

    @Query(value = "select user from User user where user.userName = ?1 and user.status <= 1")
    User findByUserName(String username);

    @Query(value = "select user from User user " +
            "where user.userName like concat('%', :keyword, '%') or user.firstName like concat('%', :keyword, '%') " +
            "or user.lastName like concat('%', :keyword, '%')")
    Page<User> searchUser(@Param("keyword") String keyword, Pageable request);

    @Query(value = "select user from User user " +
            "where (user.userName like concat('%', :keyword, '%') or user.firstName like concat('%', :keyword, '%') " +
            "or user.lastName like concat('%', :keyword, '%')) and (user.status <= 1)")
    Page<User> extractUser(@Param("keyword") String keyword, Pageable request);

    @Query(value = "select user from User user " +
            "where (user.userName like concat('%', :keyword, '%') or user.firstName like concat('%', :keyword, '%') " +
            "or user.lastName like concat('%', :keyword, '%')) " +
            "and (user.role.id = 3 and user.status <= 1)")
    Page<User> searchApprover(@Param("keyword") String keyword, Pageable request);

    @Query(value = "select user from User user " +
            "where (user.userName like concat('%', :keyword, '%') or user.firstName like concat('%', :keyword, '%') " +
            "or user.lastName like concat('%', :keyword, '%')) and (user.role.id = 3 and user.status <= 1)")
    Page<User> extractApprover(@Param("keyword") String keyword, Pageable request);

    @Query(value = "select user from User user where user.role.id = 3 and user.status <= 1")
    Page<User> extractAllApprover(Pageable request);

    @Query(value = "select user from User user where user.status <= 1")
    Page<User> extractAllUser(Pageable request);

}
