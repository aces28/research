package ph.com.ncs.vanilla.subscriber.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import org.springframework.transaction.annotation.Transactional;
import ph.com.ncs.vanilla.subscriber.domain.Subscriber;

/**
 * @author edjohna 5/24/2016
 */
@Transactional
public interface SubscriberRepository extends JpaRepository<Subscriber, Integer> {
}
