package ph.com.ncs.vanilla.transaction.dao.test;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;
import ph.com.ncs.vanilla.transaction.dao.TransactionRepository;
import ph.com.ncs.vanilla.transaction.domain.Transaction;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by ace on 5/19/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = VanillaContextConfig.class)
public class TestTransactionDao {

    private static final Logger logger = LoggerFactory.getLogger(TestTransactionDao.class);

    private static Transaction transaction;
    private static Transaction transactionResult;

    @Autowired
    private TransactionRepository transactionRepository;


    @BeforeClass
    public static void beforeTest() {

        transaction = new Transaction();
        transaction.setMsisdn("09158031084");
        transaction.setCreatedBy("ej_user");
        transaction.setCreatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        transaction.setCode(0);
        transaction.setStatus(1);
        transaction.setType("GH");
        transaction.setUpdatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        transaction.setUpdatedBy("ej_user");
    }

    @Test
    public void testInsert() {
        transactionResult = transactionRepository.save(transaction);
        logger.info("transaction id : " + transactionResult.getId());
        Assert.assertNotNull(transactionResult);
    }

    @Test
    public void testUpdate() {
        transactionResult = transactionRepository.findOne(transactionResult.getId());
        transactionResult.setCode(2);
        transactionRepository.save(transactionResult);
    }

//    @Test
//    public void testFindByCode() {
////        Transaction transaction = transactionRepository.findByCodeAndMsisdn(2);
//        logger.info("verification data : " + transaction.getMsisdn());
//        Assert.assertEquals("09158031084", transaction.getMsisdn());
//    }
//
//    @Test
//    public void testFindByMsisdn() {
//        Transaction transaction = transactionRepository.findByFailedMsisdn("09063241958");
//        logger.info("verification data : " + transaction.getMsisdn());
//        Assert.assertEquals("09063241958", transaction.getMsisdn());
//    }
}
