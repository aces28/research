package ph.com.ncs.vanilla.mis.dao.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ph.com.ncs.vanilla.application.domain.SubsApplication;
import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;
import ph.com.ncs.vanilla.group.domain.Group;
import ph.com.ncs.vanilla.mis.dao.MISRepository;
import ph.com.ncs.vanilla.role.domain.Role;
import ph.com.ncs.vanilla.user.dao.UserRepository;
import ph.com.ncs.vanilla.user.domain.User;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by edjohna on 6/28/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = VanillaContextConfig.class)
public class TestMisDao {
    private static final int PAGE_SIZE = 10;

    @Autowired
    private MISRepository misRepository;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void search(){

        PageRequest request = new PageRequest(1 - 1, PAGE_SIZE, Sort.Direction.ASC, "applicationDate");
        Page<SubsApplication> page = misRepository.searchForAssignmentString("Arbie", request);

        List<SubsApplication> total = page.getContent();
        System.out.println("TOTAL PAGE :::::::::: " + total.size());
        for(SubsApplication subsApplication : page){
            System.out.println("SUBSAPPLICATION FOR Assignment::: " + subsApplication.toString());
        }
        Assert.assertNotNull(page);

    }

//    @Test
    public void searchUser(){

        PageRequest request = new PageRequest(1 - 1, PAGE_SIZE);
        Page<User> page = userRepository.searchUser("mis2", request);

        List<User> total = page.getContent();
        System.out.println("TOTAL PAGE :::::::::: " + total.size());
        for(User user : page){
            System.out.println("User::: " + user.toString());
        }
        Assert.assertNotNull(page);

    }

//    @Test
    public void addUser(){
        User user = new User();
        Group group = new Group();
        Role role = new Role();
        group.setId(1);
        role.setId(2);
//        user.setGroup(group);
        user.setRole(role);
        user.setUserName("credit1");
        user.setFirstName("Credit");
        user.setLastName("Tester 1");
        user.setEmail("credittester1@globe.com.ph");
        user.setStatus(1);
        user.setCreatedBy("VanillaTeam");
        user.setCreatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        user.setUpdatedBy("VanillaTeam");
        user.setUpdatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));

        user = userRepository.save(user);


        System.out.println("User::: " + user.toString());
        Assert.assertNotNull(user);

    }

//    @Test
    public void login(){
        User user = new User();

        user.setUserName("approver1");
        System.out.println("UserName::: " + user.getUserName());

        User userLoggedIn = userRepository.findByUserName(user.getUserName());
        System.out.println("User::: " + userLoggedIn.toString());

        Assert.assertNotNull(userLoggedIn);

    }

//    @Test
    public void loginAfterSelect(){
        User user = new User();
//        List<User> uesrList = new ArrayList<User>();

//        PageRequest request = new PageRequest(1 - 1, PAGE_SIZE);
//        Page<User> page = userRepository.listUser(request);
        List<User> uesrList = userRepository.findAll();

        user.setUserName("approver1");
        System.out.println("UserName::: " + user.getUserName());


        //User userLoggedIn = userRepository.findByUserName(user.getUserName());


        for(User users : uesrList){
            System.out.println(users.getUserName());
            if(user.getUserName().equals(users.getUserName())){
                user = users;
                System.out.println("Found!");
                break;
            }
        }
        System.out.println("User::: " + user.toString());

        Assert.assertNotNull(user);

    }


//    @Test
//    public void getForAssignment(){
//        PageRequest request = new PageRequest(1 - 1, PAGE_SIZE, Sort.Direction.ASC, "applicationDate");
//
//        Page<SubsApplication> page = misRepository.listDisapprovedMIS(request);
//
//        List<SubsApplication> total = page.getContent();
//        System.out.println("TOTAL PAGE :::::::::: " + total.size());
//        for(SubsApplication subsApplication : page){
//            System.out.println("SUBSAPPLICATION FOR Assignment::: " + subsApplication.toString());
//        }
//        Assert.assertNotNull(page);
//
//    }

//    @Test
//    public void getForApproval(){
//        PageRequest request = new PageRequest(1 - 1, PAGE_SIZE, Sort.Direction.ASC, "applicationDate");
//
//        Page<SubsApplication> page = misRepository.listForApprovalMIS(request);
//
//        List<SubsApplication> total = page.getContent();
//        System.out.println("TOTAL PAGE :::::::::: " + total.size());
//        for(SubsApplication subsApplication : page){
//            System.out.println("SUBSAPPLICATION FOR APPROVAL::: " + subsApplication.toString());
//        }
//        Assert.assertNotNull(page);
//
//    }
//
//    @Test
//    public void getApproved(){
//        PageRequest request = new PageRequest(1 - 1, PAGE_SIZE, Sort.Direction.ASC, "applicationDate");
//
//        Page<SubsApplication> page = misRepository.listApprovedMIS(request);
//
//        List<SubsApplication> total = page.getContent();
//        System.out.println("TOTAL PAGE :::::::::: " + total.size());
//        for(SubsApplication subsApplication : page){
//            System.out.println("SUBSAPPLICATION FOR APPROVED::: " + subsApplication.toString());
//        }
//        Assert.assertNotNull(page);
//
//    }
//
//    @Test
//    public void getDisapproved(){
//        PageRequest request = new PageRequest(1 - 1, PAGE_SIZE, Sort.Direction.ASC, "applicationDate");
//
//        Page<SubsApplication> page = misRepository.listDisapprovedMIS(request);
//
//        List<SubsApplication> total = page.getContent();
//        System.out.println("TOTAL PAGE :::::::::: " + total.size());
//        for(SubsApplication subsApplication : page){
//            System.out.println("SUBSAPPLICATION FOR DISAPPROVED::: " + subsApplication.toString());
//        }
//        Assert.assertNotNull(page);
//
//    }
}
