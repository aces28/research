package ph.com.ncs.vanilla.transaction.dao.test;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;
import ph.com.ncs.vanilla.commons.utils.encryption.VanillaAESEncryption;
import ph.com.ncs.vanilla.transaction.dao.TransactionRepository;
import ph.com.ncs.vanilla.transaction.domain.Transaction;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Timestamp;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = VanillaContextConfig.class)
public class TestTransactionEncryptionDao {

    private static final Logger logger = LoggerFactory.getLogger(TestTransactionEncryptionDao.class);

    private static Transaction transaction;
    private static Transaction transactionResult;

    @Autowired
    private TransactionRepository transactionRepository;


    @BeforeClass
    public static void beforeTest() throws InvalidKeyException, UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {


        transaction = new Transaction();
        transaction.setMsisdn("09157657234");
        transaction.setCreatedBy("encrypt_testuser");
        transaction.setCreatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        transaction.setCode(0);
        transaction.setStatus(1);
        transaction.setType("GH");
        transaction.setUpdatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        transaction.setUpdatedBy("encrypt_testuser");
    }


    @Test
    public void testInsert() throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidAlgorithmParameterException, UnsupportedEncodingException {
        VanillaAESEncryption vanillaAESEncryption = new VanillaAESEncryption();
        transactionResult = transactionRepository.save(transaction);
        logger.info("transaction id : " + transactionResult.getId());
        String decryptedMsisdn = vanillaAESEncryption.decrypt(transactionResult.getMsisdn());
        logger.info("transaction msisdn : " + decryptedMsisdn);
        Assert.assertNotNull(transactionResult);
        Assert.assertEquals("09157657234", decryptedMsisdn);
    }


}
