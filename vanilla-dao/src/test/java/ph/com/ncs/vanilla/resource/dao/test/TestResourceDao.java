package ph.com.ncs.vanilla.resource.dao.test;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ph.com.ncs.vanilla.application.domain.SubsApplication;
import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;
import ph.com.ncs.vanilla.resource.dao.ResourceRepository;
import ph.com.ncs.vanilla.resource.domain.Resource;

import java.sql.Timestamp;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = VanillaContextConfig.class)
public class TestResourceDao {

    private static final Logger logger = LoggerFactory.getLogger(TestResourceDao.class);

    @Autowired
    private ResourceRepository repository;

    private static Resource resource;
    private static Resource resourceResult;

    @BeforeClass
    public static void beforeTest() {

        resource = new Resource();

        SubsApplication subsApplication = new SubsApplication();
        subsApplication.setId(1);

        resource.setSubsApplication(subsApplication);
        resource.setResourceUrl("http://blahblah.com");
        resource.setCreatedBy("vanilla_user");
        resource.setCreatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        resource.setUpdatedBy("vanilla user");
    }

    @Test
    public void testInsert() {
        resourceResult = repository.save(resource);
        logger.info("Resource Insert:::::::::::" + resourceResult.toString());
        Assert.assertNotNull(resourceResult);
    }

    @Test
    public void testUpdate() {
        resourceResult = repository.findOne(resourceResult.getId());
        resourceResult.setResourceUrl("http://amazon.com");
        repository.save(resourceResult);
    }

}
