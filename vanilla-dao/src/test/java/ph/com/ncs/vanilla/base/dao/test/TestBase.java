package ph.com.ncs.vanilla.base.dao.test;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;
import ph.com.ncs.vanilla.email.domain.Email;
import ph.com.ncs.vanilla.transaction.domain.Transaction;

import javax.annotation.Resource;

/**
 * Created by ace on 5/19/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = VanillaContextConfig.class)
public class TestBase {

    private static final Logger logger = LoggerFactory.getLogger(TestBase.class);
    private static Transaction transaction;


    @Value(value = "${esb.wsdl.url}")
    private String wsdlUrl;

    @Autowired
    private Email email;
    @Resource
    private Environment environment;

//    @BeforeClass
    public static void beforeTest() {
        transaction = new Transaction();
        transaction.setMsisdn("09063241954");
    }

//    @Test
//    public void testBaseModel() {
//        logger.info(transaction.getMsisdn());
//        Assert.assertEquals("09063241954", transaction.getMsisdn());
//    }

    //    @Test
    public void testDbConfig() {
//        logger.info(dbDriver);
        logger.info(environment.getRequiredProperty("db.url"));
        logger.info(environment.getRequiredProperty("db.driver"));
        logger.info(environment.getRequiredProperty("db.user"));
        logger.info(environment.getRequiredProperty("db.password"));
        Assert.assertEquals("jdbc:mysql://192.168.23.175:3306/VNLDEV01", environment.getRequiredProperty("db.url"));
    }

//    @Test
    public void testWebProperties() {
        logger.info("ESB Value : " + wsdlUrl);
        Assert.assertEquals("http://10.225.10.4:8111/billing-cm", wsdlUrl);
    }

    @Test
    public void testEmail(){
        email.setTo("ed@gmial.com");
        logger.info(email.toString());
    }

}
