package ph.com.ncs.vanilla.application.dao.test;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ph.com.ncs.vanilla.application.dao.SubsApplicationRepository;
import ph.com.ncs.vanilla.application.domain.SubsApplication;
import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;
import ph.com.ncs.vanilla.subscriber.domain.Subscriber;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * @author edjohna 5/24/2016
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = VanillaContextConfig.class)
public class TestSubsApplicationDao {

    private static final Logger logger = LoggerFactory.getLogger(TestSubsApplicationDao.class);

    @Autowired
    private SubsApplicationRepository subsApplicationRepository;

    private static SubsApplication subsApplication;
    private static SubsApplication subsApplicationResult;

    @BeforeClass
    public static void beforeTest() {
        subsApplication = new SubsApplication();

        Subscriber subscriber = new Subscriber();
        subscriber.setId(6);

        subsApplication.setSubscriber(subscriber);
        subsApplication.setSubscriberMsisdn("09158031084");
        subsApplication.setAccountType("Account");
        subsApplication.setUserEmail("testArbie@email.com");
        subsApplication.setAttachment("ArbieUrl");
        subsApplication.setTermsConditions("ArbieUrlTandC");
        subsApplication.setApprovalDate(new Date(VanillaDateUtils.longCurrentDate()));
        subsApplication.setStatus(1);
        subsApplication.setCreatedBy("vanilla_user");
        subsApplication.setCreatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        subsApplication.setUpdatedBy("vanilla user");
    }

//    @Test
    public void testInsert() {
        subsApplicationResult = subsApplicationRepository.save(subsApplication);
        logger.info("subscriber id : " + subsApplicationResult.getId());
        Assert.assertNotNull(subsApplicationResult);

    }

//    @Test
    public void testUpdate() {
        subsApplicationResult = subsApplicationRepository.findOne(subsApplicationResult.getId());
        subsApplicationResult.setSubscriberMsisdn("09158031084");
        subsApplicationRepository.save(subsApplicationResult);
    }

//    @Test
    public void testDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        Timestamp timestamp = new Timestamp(VanillaDateUtils.longCurrentDate());
        logger.info(String.valueOf(new Date(timestamp.getTime())) + " : formatted : " + simpleDateFormat.format(new Date(timestamp.getTime())));
    }

    @Test
    public void testRetrieve(){
        subsApplication = subsApplicationRepository.findOne(4);
        logger.info("Subs App : " +  subsApplication.toString());
        Assert.assertNotNull(subsApplication);
    }

}
