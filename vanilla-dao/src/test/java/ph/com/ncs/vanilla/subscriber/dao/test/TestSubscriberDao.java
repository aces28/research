package ph.com.ncs.vanilla.subscriber.dao.test;

import java.sql.Date;
import java.sql.Timestamp;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;
import ph.com.ncs.vanilla.subscriber.dao.SubscriberRepository;
import ph.com.ncs.vanilla.subscriber.domain.Subscriber;

/**
 * @author edjohna 5/24/2016
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = VanillaContextConfig.class)
public class TestSubscriberDao {

    private static final Logger logger = LoggerFactory.getLogger(TestSubscriberDao.class);

    @Autowired
    private SubscriberRepository subscriberRepository;

    private static Subscriber subscriber;
    private static Subscriber subscriberResult;

    @BeforeClass
    public static void beforeTest() {
        subscriber = new Subscriber();

        subscriber.setFirstName("Arbie");
        subscriber.setLastName("Diaz");
        subscriber.setMiddleName("Montesines");
        subscriber.setMothersName("Mom");
        subscriber.setGender("Female");
        subscriber.setAddress("Manila");
        subscriber.setBirthday(new Date(VanillaDateUtils.longCurrentDate()));
        logger.info("Birthday : " + subscriber.getBirthday());
        subscriber.setCreatedBy("ferdinand");
        subscriber.setCreatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        subscriber.setUpdatedBy("ferdinand");

    }

    @Test
    public void testInsert() {
        subscriberResult = subscriberRepository.save(subscriber);
        logger.info("subscriber id : " + subscriberResult.getId());
        System.out.println(subscriber.toString());
        Assert.assertNotNull(subscriberResult);
    }

//    @Test
    public void testUpdate() {
        subscriberResult = subscriberRepository.findOne(subscriberResult.getId());
        subscriberResult.setAddress("Ilocos Norte");
        subscriberRepository.save(subscriberResult);
    }

//    @Test
    public void testRetrieve(){
        subscriber = new Subscriber();
        subscriber.setId(4);
        subscriberResult = subscriberRepository.findOne(subscriber.getId());
        System.out.println(subscriberResult.toString());
        Assert.assertNotNull(subscriberResult);
    }

}
