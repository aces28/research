package ph.com.ncs.vanilla.account.dao.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ph.com.ncs.vanilla.account.dao.AccountRepository;
import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;

/**
 * Created by edjohna on 7/14/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = VanillaContextConfig.class)
public class TestAccountDao {

    @Autowired
    private AccountRepository accountRepository;

    @Test
    public void testMsisdnLookup(){
        Integer msisdnExist = accountRepository.msisdnLookup("09271021092");
        System.out.println(msisdnExist);
        Assert.assertEquals(1,1);
    }
}
