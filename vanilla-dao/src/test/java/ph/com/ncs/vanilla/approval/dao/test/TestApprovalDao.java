package ph.com.ncs.vanilla.approval.dao.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ph.com.ncs.vanilla.application.domain.SubsApplication;
import ph.com.ncs.vanilla.approval.dao.ApproverRepository;
import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;
import ph.com.ncs.vanilla.group.dao.GroupRepository;
import ph.com.ncs.vanilla.group.domain.Group;
import ph.com.ncs.vanilla.user.dao.UserRepository;
import ph.com.ncs.vanilla.user.domain.User;

import java.util.List;

/**
 * Created by edjohna on 6/25/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = VanillaContextConfig.class)
public class TestApprovalDao {
    private static final int PAGE_SIZE = 10;

    @Autowired
    private ApproverRepository approverRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GroupRepository groupRepository;
//
////    @Test
//    public void getForApproval() {
//        PageRequest request = new PageRequest(1 - 1, PAGE_SIZE, Sort.Direction.ASC, "applicationDate");
//        User user = new User();
//        user.setId(1);
//        Page<SubsApplication> page = approverRepository.listForApprovalApprover("approver1", request);
//
//        List<SubsApplication> total = page.getContent();
//        System.out.println("TOTAL PAGE :::::::::: " + total.size());
//        for(SubsApplication subsApplication : page){
//            System.out.println("TOTAL PAGE ::::::::::");
//            System.out.println("SUBSAPPLICATION FOR APPROVAL::: " + subsApplication.toString());
//        }
//        Assert.assertNotNull(page);
//
//    }
//
////    @Test
//    public void getApproved() {
//        PageRequest request = new PageRequest(1 - 1, PAGE_SIZE, Sort.Direction.ASC, "applicationDate");
//        User user = new User();
//        user.setId(1);
//        Page<SubsApplication> page = approverRepository.listApprovedApprover("approver1", request);
//
//        List<SubsApplication> total = page.getContent();
//        System.out.println("TOTAL PAGE :::::::::: " + total.size());
//        for(SubsApplication subsApplication : page){
//            System.out.println("TOTAL PAGE ::::::::::");
//            System.out.println("SUBSAPPLICATION APPROVED ::: " + subsApplication.toString());
//        }
//        Assert.assertNotNull(page);
//
//    }
//
////    @Test
//    public void getDisapproved() {
//        PageRequest request = new PageRequest(1 - 1, PAGE_SIZE, Sort.Direction.ASC, "applicationDate");
//        User user = new User();
//        user.setId(1);
//        Page<SubsApplication> page = approverRepository.listDisapprovedApprover("approver1",request);
//
//        List<SubsApplication> total = page.getContent();
//        System.out.println("TOTAL PAGE :::::::::: " + total.size());
//        for(SubsApplication subsApplication : page){
//            System.out.println("TOTAL PAGE ::::::::::");
//            System.out.println("SUBSAPPLICATION DISAPPROVED ::: " + subsApplication.toString());
//        }
//        Assert.assertNotNull(page);
//
//    }

    @Test
    public void searchForApprovalString(){
        Page<SubsApplication> result = approverRepository.searchDispprovedString("9", "approver1", new PageRequest(0, 10));

        for (SubsApplication subsApplication : result){
            System.out.println(subsApplication.toString());
        }

        Assert.assertNotNull(result);
    }

//    @Test
    public void searchUsers(){
        Page<User> result = userRepository.searchApprover("app",new PageRequest(0, 10));

        System.out.println(result.getTotalElements());
        for(User user : result){
            System.out.println(user.toString());
        }

        Assert.assertNotNull(result);
    }

    @Test
    public void searchGroup(){
        Page<Group> result = groupRepository.searchGroup("app", new PageRequest(0, 10));

        for(Group group : result){
            System.out.println(group.toString());
        }

        Assert.assertNotNull(result);

    }
}
