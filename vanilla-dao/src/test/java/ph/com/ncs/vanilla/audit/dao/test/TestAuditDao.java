package ph.com.ncs.vanilla.audit.dao.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ph.com.ncs.vanilla.audit.dao.AccessTransactionRepository;
import ph.com.ncs.vanilla.audit.domain.AuditAccessTransaction;
import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;

import java.util.List;

/**
 * Created by edjohna on 7/19/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = VanillaContextConfig.class)
public class TestAuditDao {

    @Autowired
    AccessTransactionRepository auditAccessRepository;

//    @Test
    public void testAudit(){
        PageRequest request = new PageRequest(1 - 1, 5000, Sort.Direction.DESC, "dateAccessed");
        Page<AuditAccessTransaction> auditPage =  auditAccessRepository.listUserAccessHistory(request);

        List<AuditAccessTransaction> auditAccessList = auditPage.getContent();

        for (AuditAccessTransaction auditAccess : auditAccessList){
            System.out.println(auditAccess.toString());
        }

        Assert.assertNotNull(auditAccessList);
    }

//    @Test
    public void testSearchAudit() {
        int module = 14;
        PageRequest request = new PageRequest(1 - 1, 10, Sort.Direction.DESC, "dateAccessed");
        Page<AuditAccessTransaction> auditPage = auditAccessRepository.filterByModuleTransactionLogs(module, request);

        List<AuditAccessTransaction> auditAccessList = auditPage.getContent();

        for (AuditAccessTransaction auditAccess : auditAccessList){
            System.out.println(auditAccess.toString());
        }
        Assert.assertNotNull(auditAccessList);
    }

    @Test
    public void testSearchKeywordAudit() {
        String keyword = "Tester 12";
        PageRequest request = new PageRequest(1 - 1, 10, Sort.Direction.DESC, "dateAccessed");
        Page<AuditAccessTransaction> auditPage = auditAccessRepository.searchUserAccessHistory(keyword, request);

        List<AuditAccessTransaction> auditAccessList = auditPage.getContent();

        for (AuditAccessTransaction auditAccess : auditAccessList){
            System.out.println(auditAccess.toString());
        }
        Assert.assertNotNull(auditAccessList);
    }


}
