package ph.com.ncs.vanilla.user.dao.test;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;
import ph.com.ncs.vanilla.group.domain.Group;
import ph.com.ncs.vanilla.role.domain.Role;
import ph.com.ncs.vanilla.user.dao.UserRepository;
import ph.com.ncs.vanilla.user.domain.User;

import java.sql.Timestamp;

/**
 * @author arbiem on 8/7/2016
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = VanillaContextConfig.class)
public class TestUserEncryptionDao {

    private static final Logger logger = LoggerFactory.getLogger(TestUserEncryptionDao.class);

    private static User user;
    private static User userResult;
    private static Role role;
    private static Group group;

    @Autowired
    private UserRepository userRepository;

    @BeforeClass
    public static void beforeTest() {
        role = new Role();
        role.setId(1);

        group = new Group();
        group.setId(0);

        user = new User();
        user.setUserName("isg1");
        user.setFirstName("isg1");
        user.setLastName("isg1");
        user.setEmail("isg1@globe.com.ph");
        user.setFailedLoginAttempts(0);
        user.setRole(role);
        user.setGroup(group);
        user.setStatus(1);
        user.setIsLocked(0);

        user.setCreatedBy("VanillaTeam");
        user.setCreatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        user.setUpdatedBy("VanillaTeam");
        user.setUpdatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
    }

    @Test
    public void addNewUser() {
        userResult = userRepository.save(user);
        logger.info("user id : " + userResult.getId());
        Assert.assertNotNull(userResult);

    }

    //    @Test
    public void findUserByUserName() {
        userResult = userRepository.findByUserName("mis1");
        Assert.assertNotNull(userResult);
    }

}
