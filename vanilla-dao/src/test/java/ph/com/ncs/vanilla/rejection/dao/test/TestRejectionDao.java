package ph.com.ncs.vanilla.rejection.dao.test;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ph.com.ncs.vanilla.application.domain.SubsApplication;
import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;
import ph.com.ncs.vanilla.commons.constants.VanillaConstants;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;
import ph.com.ncs.vanilla.rejection.dao.RejectionRepository;
import ph.com.ncs.vanilla.rejection.domain.Rejection;

import java.sql.Timestamp;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = VanillaContextConfig.class)
public class TestRejectionDao {

    private static final Logger logger = LoggerFactory.getLogger(TestRejectionDao.class);

    @Autowired
    private RejectionRepository rejectionRepository;

    private static Rejection rejection;
    private static Rejection rejectionResult;

    @BeforeClass
    public static void beforeTest() {
        rejection = new Rejection();

        SubsApplication subsApplication = new SubsApplication();

        subsApplication.setId(1);

        rejection.setReasonName("Requirements");
        rejection.setReasonDescription("Invalid Requirements");
        rejection.setCreatedBy(VanillaConstants.SYSTEM_USER);
        rejection.setCreatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        rejection.setUpdatedBy(VanillaConstants.SYSTEM_USER);
    }

    @Test
    public void testInsert() {
        rejectionResult = rejectionRepository.save(rejection);
        logger.info("Test Insert Result :::::::: " + rejectionResult.toString());
        Assert.assertNotNull(rejectionResult);
    }

    @Test
    public void testUpdate() {
        rejectionResult = rejectionRepository.findOne(rejectionResult.getId());
        logger.info("ID :::: " + rejectionResult.getId());
        rejectionResult.setReasonName("Missing Requirements");
        rejectionRepository.save(rejectionResult);
    }

}
