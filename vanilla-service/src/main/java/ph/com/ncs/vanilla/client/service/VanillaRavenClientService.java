package ph.com.ncs.vanilla.client.service;

import com.danateq.soap.link.pnm.PushNotification;
import com.danateq.soap.link.pnm.PushNotificationResp;
import com.danateq.soap.link.pnm.TNotifParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ph.com.ncs.vanilla.base.service.BaseClientService;
import ph.com.ncs.vanilla.commons.utils.PhoneUtils;
import ph.com.ncs.vanilla.raven.ws.push.stubs.SyncPushNotificationStub;
import ph.com.ncs.vanilla.transaction.domain.Transaction;

import java.rmi.RemoteException;

/**
 * Created by ace on 5/27/16.
 */
@Component
public class VanillaRavenClientService implements BaseClientService<Transaction> {

    private static final Logger logger = LoggerFactory.getLogger(VanillaRavenClientService.class);

    @Value(value = "${raven.wsdl.url}")
    private String WSDL_RAVEN_URL;
    @Value(value = "${raven.user.identity.type}")
    private String IDENTITY_TYPE;
    @Value(value = "${raven.notif.svc.tm}")
    private String NOTIF_ID_SVC_TM;
    @Value(value = "${raven.svc.message}")
    private String SVCMESSAGE;
    @Value(value = "${raven.notif.submit.tm}")
    private String NOTIF_ID_SUBMIT_TM;
    @Value(value = "${raven.notif.decline.tm}")
    private String NOTIF_ID_DECLINE_TM;
    @Value(value = "${raven.notif.approved.tm.plan1}")
    private String NOTIF_ID_APPROVED_TM_PLAN1;
    @Value(value = "${raven.notif.approved.tm.plan2}")
    private String NOTIF_ID_APPROVED_TM_PLAN2;

    public <T> T getResult(Transaction transaction) throws RavenClientException {
        logger.info("sending push notification to raven : START");
        SyncPushNotificationStub syncPushNotificationStub;
        int responseCode;

        try {
            syncPushNotificationStub = new SyncPushNotificationStub(WSDL_RAVEN_URL);
            PushNotification pushNotification = new PushNotification();
            TNotifParam[] tNotifParamArray = new TNotifParam[]{};

            logger.info("sending verification code : mode : " + transaction.getMode());

            if (transaction.getMode() < 5) {
                TNotifParam tNotifParam = new TNotifParam();
                tNotifParam.setName(SVCMESSAGE);
                tNotifParam.setValue(transaction.getCode().toString());
                logger.info("send subscriber verification code : " + transaction.getCode());
                tNotifParamArray = new TNotifParam[]{tNotifParam};
            }

            pushNotification.setUserIdentity(PhoneUtils.internationalFormat(transaction.getMsisdn()));
            pushNotification.setUserIdentityType(Integer.parseInt(IDENTITY_TYPE));

            switch (transaction.getMode()) {
                case 5:
                    logger.info("sending notification to raven : 1221 : " + transaction.getMsisdn());
                    pushNotification.setNotifPatternId(Integer.parseInt(NOTIF_ID_SUBMIT_TM));
                    break;
                case 6:
                    logger.info("sending notification to raven : 1223 : " + transaction.getMsisdn());
                    pushNotification.setNotifPatternId(Integer.parseInt(NOTIF_ID_DECLINE_TM));
                    break;
                case 7:
                    logger.info("sending notification to raven : 1230 : " + transaction.getMsisdn());
                    pushNotification.setNotifPatternId(Integer.parseInt(NOTIF_ID_APPROVED_TM_PLAN1));
                    break;
                case 8:
                    logger.info("sending notification to raven : 1231 : " + transaction.getMsisdn());
                    pushNotification.setNotifPatternId(Integer.parseInt(NOTIF_ID_APPROVED_TM_PLAN2));
                    break;
                default:
                    logger.info("sending notification to raven : 1225 : " + transaction.getMsisdn());
                    pushNotification.setNotifPatternId(Integer.parseInt(NOTIF_ID_SVC_TM));
                    pushNotification.setNotifParam(tNotifParamArray);
                    break;
            }

            PushNotificationResp pushNotificationResp = syncPushNotificationStub.pushNotification(pushNotification);
            responseCode = pushNotificationResp.getPushNotifResult();
            logger.info("sending subscriber verification code : " + transaction.getCode() + " : response " + responseCode);
            if (responseCode == 0) {
                return (T) Integer.valueOf(responseCode);
            } else {
                return (T) Integer.valueOf(responseCode);
            }
        } catch (Exception e) {
            logger.error("RemoteException ", e);
            throw new RavenClientException(e.getCause());
        }

    }
}
