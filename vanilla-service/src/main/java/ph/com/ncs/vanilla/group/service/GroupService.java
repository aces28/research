package ph.com.ncs.vanilla.group.service;

import java.util.List;

import org.springframework.data.domain.Page;

import ph.com.ncs.vanilla.base.service.BaseService;
import ph.com.ncs.vanilla.group.domain.Group;

/**
 * Created by edjohna on 6/29/2016.
 */
public interface GroupService extends BaseService {

    Page<Group> listGroup(Integer pageNumber);

    List<Group> listAvailableGroup();

    Page<Group> searchGroup(String keyword, Integer pageNumber);

    List<Group> extractGroup(String keyword);

    List<Group> extractAllGroup();

    List<Group> findAll();

    Group findGroupByName(String groupName);
}
