package ph.com.ncs.vanilla.module.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ph.com.ncs.vanilla.audit.domain.AuditModule;
import ph.com.ncs.vanilla.base.domain.Model;
import ph.com.ncs.vanilla.module.dao.ModuleRepository;

/**
 * Created by jernardob on 08/08/16.
 */
@Service
public class ModuleServiceImpl implements ModuleService {

    @Autowired
    private ModuleRepository moduleRepository;

    @Override
    public <T extends Model> T save(T data) {
        return null;
    }

    @Override
    public <T extends Model> T get(T data) {
        return (T) moduleRepository.findOne(((AuditModule) data).getId());
    }

    @Override
    public <T> T save(Iterable iterable) {
        return null;
    }
    
    @Override
    public AuditModule findByModuleId(int moduleId) {
        return moduleRepository.findByModuleId(moduleId);
    }
}
