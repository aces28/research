package ph.com.ncs.vanilla.transaction.service;

import ph.com.ncs.vanilla.base.domain.Model;
import ph.com.ncs.vanilla.base.service.BaseService;

/**
 * Created by ace on 5/20/16.
 */
public interface TransactionService extends BaseService {

    /**
     * Save transaction during request on other systems
     *
     * @param data
     * @return {@link ph.com.ncs.vanilla.transaction.domain.Transaction}
     */
    <T extends Model> T updateTransaction(T data) throws TransactionServiceException;

    /**
     * Retrieve transaction using svc
     *
     * @param code   Subscriber verification code.
     * @param msisdn Subscriber msisdn.
     * @return {@link ph.com.ncs.vanilla.transaction.domain.Transaction}
     */
    <T extends Model> T findByCodeAndMsisdn(int code, String msisdn) throws TransactionServiceException;

    /**
     * Retrieve transaction with expired svc
     *
     * @param msisdn Subscriber msisdn.
     * @return {@link ph.com.ncs.vanilla.transaction.domain.Transaction}
     */
    <T extends Model> T findByFailedMsisdn(String msisdn) throws TransactionServiceException;

    /**
     * Retrieve transaction using svc
     *
     * @param msisdn Subscriber msisdn.
     * @return {@link ph.com.ncs.vanilla.transaction.domain.Transaction}
     */
    <T extends Model> T findBySuccessMsisdn(String msisdn) throws TransactionServiceException;
}
