package ph.com.ncs.vanilla.user.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import ph.com.ncs.vanilla.base.domain.Model;
import ph.com.ncs.vanilla.user.dao.UserRepository;
import ph.com.ncs.vanilla.user.domain.User;

/**
 * Created by edjohna on 6/29/2016.
 */
@Service
public class UserServiceImpl implements UserService {

    private static final int PAGE_SIZE = 10;
    @Autowired
    private UserRepository userRepository;


    @Override
    public Page<User> listUser(Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE);
        return userRepository.listUser(request);
    }

    @Override
    public Page<User> listApprover(Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE);
        return userRepository.listApprover(request);
    }

    @Override
    public List<User> listApproverForAssignment(String groupName) {
        return userRepository.listApproverForAssignment(groupName);
    }

    @Override
    public Page<User> filterByGroupName(String groupName, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE);
        return userRepository.filterByGroupName(groupName, request);
    }

    @Override
    public List<User> extractByGroupName(String groupName) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize);
        Page<User> userPage = userRepository.extractByGroupName(groupName, request);
        List<User> userList = userPage.getContent();
        return userList;
    }

    @Override
    public Page<User> filterByAvailability(int availability, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE);
        return userRepository.filterByAvailability(availability, request);
    }

    @Override
    public List<User> extractByAvailability(int availability) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize);
        Page<User> userPage = userRepository.extractByAvailability(availability, request);
        List<User> userList = userPage.getContent();
        return userList;
    }

    @Override
    public Page<User> filterByRole(String roleType, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE);
        return userRepository.filterByRole(roleType, request);
    }

    @Override
    public List<User> extractByRole(String keyword) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize);
        Page<User> userPage = userRepository.extractByRole(keyword, request);
        List<User> userList = userPage.getContent();
        return userList;
    }

    @Override
    public Page<User> filterByStatus(int status, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE);
        return userRepository.filterByApproverAvailability(status, request);
    }

    @Override
    public List<User> extractByStatus(int status) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize);
        Page<User> userPage = userRepository.extractByApproverAvailability(status, request);
        List<User> userList = userPage.getContent();
        return userList;
    }

    @Override
    public List<User> getApproversNoGroup() {
        return userRepository.getApproversNoGroup();
    }

    @Override
    public List<User> getMembersOfGroup(int groupId) {
        return userRepository.getMembersOfGroup(groupId);
    }

    @Override
    public Page<User> searchUser(String keyword, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE);
        return userRepository.searchUser(keyword, request);
    }

    @Override
    public List<User> extractUser(String keyword) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize);
        Page<User> userPage = userRepository.extractUser(keyword, request);
        List<User> userList = userPage.getContent();
        return userList;
    }

    @Override
    public Page<User> searchApprover(String keyword, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE);
        return userRepository.searchApprover(keyword, request);
    }

    @Override
    public List<User> extractApprover(String keyword) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize);
        Page<User> userPage = userRepository.extractApprover(keyword, request);
        List<User> userList = userPage.getContent();
        return userList;
    }

    @Override
    public List<User> findAll(Iterable iterable) {
        return userRepository.findAll(iterable);
    }

    @Override
    public List<User> extractAllApprover() {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize);
        Page<User> userPage = userRepository.extractAllApprover(request);
        List<User> userList = userPage.getContent();
        return userList;
    }

    @Override
    public List<User> extractAllUser() {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize);
        Page<User> userPage = userRepository.extractAllUser(request);
        List<User> userList = userPage.getContent();
        return userList;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public <T extends Model> T save(T data) {
        return (T) userRepository.save((User) data);
    }

    @Override
    public <T extends Model> T get(T data) {
        return (T) userRepository.findOne(((User) data).getId());
    }

    @Override
    public <T> T save(Iterable iterable) {
        return (T) userRepository.save(iterable);
    }

    @Override
    public User findByUserName(String userName) {
        return userRepository.findByUserName(userName);
    }
}
