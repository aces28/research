package ph.com.ncs.vanilla.subscriber.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ph.com.ncs.vanilla.base.domain.Model;
import ph.com.ncs.vanilla.subscriber.dao.SubscriberRepository;
import ph.com.ncs.vanilla.subscriber.domain.Subscriber;

@Service
public class SubscriberServiceImpl implements SubscriberService {

    @Autowired
    private SubscriberRepository subscriberRepository;

    @Override
    public <T extends Model> T save(T data) {
        return (T) subscriberRepository.save((Subscriber) data);
    }

    @Override
    public <T extends Model> T get(T data) {
        return (T) subscriberRepository.findOne(((Subscriber) data).getId());
    }

    @Override
    public <T> T save(Iterable iterable) {
        return (T) subscriberRepository.save(iterable);
    }

}
