package ph.com.ncs.vanilla.client.service;

import org.apache.axis2.databinding.types.UnsignedLong;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ph.com.metr.bus.soap.client.Execute;
import ph.com.metr.bus.soap.client.ExecuteRequestType;
import ph.com.metr.bus.soap.client.ExecuteResponse;
import ph.com.metr.bus.soap.client.ExecuteResponseType;
import ph.com.metr.bus.soap.objects.*;
import ph.com.ncs.vanilla.base.service.BaseClientService;
import ph.com.ncs.vanilla.commons.utils.PhoneUtils;
import ph.com.ncs.vanilla.nf.ws.transaction.stubs.SyncTransactionClientServiceStub;
import ph.com.ncs.vanilla.transaction.domain.Transaction;

/**
 * Created by ace on 5/27/16.
 */
@Component
public class VanillaNFClientService implements BaseClientService<Transaction> {

    private static final Logger logger = LoggerFactory.getLogger(VanillaNFClientService.class);

    @Value(value = "${nf.wsdl.url}")
    private String WSDL_NF_URL;
    @Value(value = "${nf.user}")
    private String NF_USER;
    @Value(value = "${nf.password}")
    private String NF_PASSWORD;
    @Value(value = "${nf.mobtel}")
    private String NF_MOBTEL;
    @Value(value = "${nf.plan.id}")
    private String NF_PLAN_ID;
    @Value(value = "${nf.plan1.val}")
    private String NF_PLAN1_VAL;
    @Value(value = "${nf.plan2.val}")
    private String NF_PLAN2_VAL;
    @Value(value = "${nf.op.type}")
    private String NF_OP_TYPE;
    @Value(value = "${nf.op.id.register}")
    private String NF_OP_ID_REGISTER;
    @Value(value = "${nf.op.id.get.status}")
    private String NF_OP_ID_STATUS;

    @Override
    public <T> T getResult(Transaction transaction) throws NfClientException {
        String response;
        try {
            logger.info("STARTING NF CLIENT : WSDL= " + WSDL_NF_URL);
            SyncTransactionClientServiceStub clientStub = new SyncTransactionClientServiceStub(WSDL_NF_URL);
            Execute execute = new Execute();
            ExecuteRequestType executeRequestType = new ExecuteRequestType();
            CredentialType credentialType = new CredentialType();
            credentialType.setUser(NF_USER);
            credentialType.setPassword(NF_PASSWORD);

            executeRequestType.setCredential(credentialType);

            TransactionRequestType transactionRequestType = new TransactionRequestType();
            UnsignedLong serviceID = new UnsignedLong(1);
            UnsignedLong transactionID = new UnsignedLong(transaction.getId());
            transactionRequestType.setId(transactionID);
            transactionRequestType.setOpType(NF_OP_TYPE);

            if (transaction.getMode() != 0) {
                transactionRequestType.setOpId(NF_OP_ID_REGISTER);
                logger.info("NF OP ID: " + transactionRequestType.getOpId());
            } else {
                transactionRequestType.setOpId(NF_OP_ID_STATUS);
                logger.info("NF OP ID: " + transactionRequestType.getOpId());
            }

            transactionRequestType.setServiceId(serviceID);

            ParamListType paramListType = new ParamListType();

            KeyValuePairType mobTelPairType = new KeyValuePairType();
            mobTelPairType.setName(NF_MOBTEL);
            mobTelPairType.setValue(PhoneUtils.internationalFormat(transaction.getMsisdn()));

            KeyValuePairType planIdPairType = new KeyValuePairType();
            if (transaction.getMode() == 5) {
                logger.info("SET NF PLAN1 ID AND PLAN1 VAL");
                planIdPairType.setName(NF_PLAN_ID);
                planIdPairType.setValue(NF_PLAN1_VAL);
            } else if (transaction.getMode() == 6) {
                logger.info("SET NF PLAN2 ID AND PLAN2 VAL");
                planIdPairType.setName(NF_PLAN_ID);
                planIdPairType.setValue(NF_PLAN2_VAL);
            }

            KeyValuePairType[] valuePairType;
            if (transaction.getMode() != 0) {
                valuePairType = new KeyValuePairType[]{mobTelPairType, planIdPairType};
            } else {
                valuePairType = new KeyValuePairType[]{mobTelPairType};
            }

            paramListType.setParam(valuePairType);
            transactionRequestType.setParams(paramListType);
            executeRequestType.setTransaction(transactionRequestType);
            execute.setExecuteRequest(executeRequestType);
            ExecuteResponse executeResponse = clientStub.execute(execute);
            ExecuteResponseType executeResponseType = executeResponse.getExecuteResponse();
            TransactionResponseType transactionResponseType = executeResponseType.getResponse();

            if (validateResponse(transactionResponseType)) {
                if (transaction.getMode() != 0) {
                    logger.info("INSIDE IF OF SUCCESS");
                    response = validateGetStatusPlanResponse(transactionResponseType);
                    return (T) response;
                } else {
                    logger.info("INSIDE ELSE OF SUCCESS");
                    response = validateGetStatusPlanResponse(transactionResponseType);
                    return (T) response;
                }

            } else {
                logger.info("INSIDE ELSE FOR FAILED");
                logger.info("Transaction Response Type: " + transactionResponseType.getResult());
                response = "FAILED";
                return (T) response;
            }

        } catch (Exception e) {
            logger.error("Unable to connect ", e);
            throw new NfClientException(e.getCause());
        }
    }

    private boolean validateResponse(TransactionResponseType transactionResponseType) {
        if ("SUCCESS".equalsIgnoreCase(transactionResponseType.getResult())) {
            return true;
        }
        return false;
    }

    private String validateGetStatusPlanResponse(TransactionResponseType transactionResponseType) {
        logger.info("Starting validateGetStatusPlanResponse");
        ParamListType params = transactionResponseType.getParams();
        KeyValuePairType[] keyValuePairTypes = params.getParam();
        for (KeyValuePairType keyValuePairType : keyValuePairTypes) {
            if ("vanilla_plan_status".equalsIgnoreCase(keyValuePairType.getName())) {
                logger.info("Param Name: " + keyValuePairType.getName() + " Param Value: " + keyValuePairType.getValue());
                if ("5".equalsIgnoreCase(keyValuePairType.getValue()) || "6".equalsIgnoreCase(keyValuePairType.getValue())) {
                    return "FAILED";
                }
            }
        }
        return "SUCCESS";
    }
}
