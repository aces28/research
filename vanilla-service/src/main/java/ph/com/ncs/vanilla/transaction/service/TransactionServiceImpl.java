package ph.com.ncs.vanilla.transaction.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ph.com.ncs.vanilla.base.domain.Model;
import ph.com.ncs.vanilla.transaction.dao.TransactionRepository;
import ph.com.ncs.vanilla.transaction.domain.Transaction;

/**
 * Created by ace on 5/20/16.
 */
@Service
public class TransactionServiceImpl implements TransactionService {

    private static final Logger logger = LoggerFactory.getLogger(TransactionServiceImpl.class);

    @Autowired
    private TransactionRepository transactionRepository;

    public <T extends Model> T save(T data) {
        return (T) transactionRepository.save((Transaction) data);
    }

    public <T extends Model> T get(T data) {
        return (T) transactionRepository.findOne(((Transaction) data).getId());
    }

    @Override
    public <T> T save(Iterable iterable) {
        return (T) transactionRepository.save(iterable);
    }

    public <T extends Model> T updateTransaction(T data) {
        return (T) transactionRepository.save(get((Transaction) data));
    }

    @Override
    public <T extends Model> T findByCodeAndMsisdn(int code, String msisdn) {
        return transactionRepository.findByCodeAndMsisdn(code, msisdn);
    }

    @Override
    public <T extends Model> T findByFailedMsisdn(String msisdn) {
        return transactionRepository.findByFailedMsisdn(msisdn);
    }

    @Override
    public <T extends Model> T findBySuccessMsisdn(String msisdn) {
        return transactionRepository.findBySuccessMsisdn(msisdn);
    }
}
