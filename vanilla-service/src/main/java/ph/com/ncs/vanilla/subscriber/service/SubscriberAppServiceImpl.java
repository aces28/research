package ph.com.ncs.vanilla.subscriber.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ph.com.ncs.vanilla.application.dao.SubsApplicationRepository;
import ph.com.ncs.vanilla.application.domain.SubsApplication;
import ph.com.ncs.vanilla.base.domain.Model;

import java.util.List;

@Service
public class SubscriberAppServiceImpl implements SubscriberAppService {

    @Autowired
    private SubsApplicationRepository subsApplicationRepository;

    @Override
    public <T extends Model> T save(T data) {
        return (T) subsApplicationRepository.save((SubsApplication) data);
    }

    @Override
    public <T extends Model> T get(T data) {
        return (T) subsApplicationRepository.findOne(data.getId());
    }

    @Override
    public <T> T save(Iterable iterable) {
        return (T) subsApplicationRepository.save(iterable);
    }

    @Override
    public List<SubsApplication> findApplicationByMsisdn(String msisdn) {
        return subsApplicationRepository.findApplicationByMsisdn(msisdn);
    }

	@Override
	public Long countApplicationUpdatedByApprover(Integer status, Integer userId) {
		return subsApplicationRepository.countApplicationUpdatedByApprover(status, userId);
	}
}


