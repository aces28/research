package ph.com.ncs.vanilla.module.service;


import ph.com.ncs.vanilla.audit.domain.AuditModule;
import ph.com.ncs.vanilla.base.service.BaseService;

/**
 * Created by jernardob on 08/08/16.
 */
public interface ModuleService extends BaseService {

    AuditModule findByModuleId(int moduleId);
}
