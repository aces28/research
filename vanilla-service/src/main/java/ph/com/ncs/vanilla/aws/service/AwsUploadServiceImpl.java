package ph.com.ncs.vanilla.aws.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

import javax.servlet.http.Part;

import org.apache.commons.io.IOUtils;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Created by jomerp on 8/4/2016.
 */
@Service
public class AwsUploadServiceImpl implements AwsUploadService {

	private static final Logger logger = LoggerFactory.getLogger(AwsUploadServiceImpl.class);

	@Autowired
	private AmazonS3Client amazonS3Client;

	@Autowired
	private Environment resource;

	/**
	 * @param multipartFiles
	 * @return
	 */
	@Override
	public Map<String, String> uploadMultipart(Collection<Part> multipartFiles) { // MultipartFile[]
		Map<String, String> returnObj = new WeakHashMap<>();
		logger.info("uploadMultipart : START : " + multipartFiles.size());
		
		boolean isImages = false;
		boolean isPDf = false;
		
		try {
			Document document = null;
	    	ByteArrayOutputStream baos = null;
	    	PDFMergerUtility mergePdf = null;
	    	
	    	for (Part multipartFile : multipartFiles) {
				String keyname = multipartFile.getName();
				String fileId = multipartFile.getSubmittedFileName();
				
				if(keyname.equalsIgnoreCase("tandc")) {
					uploadToBucket(multipartFile.getSize(), multipartFile.getInputStream(), fileId, "application/pdf");
					returnObj.put(keyname, fileId);
                } else if(keyname.contains("pdf")) {
                	if(!isPDf) {
                		mergePdf = new PDFMergerUtility();
                		baos = new ByteArrayOutputStream();
                	}
                	
                	mergePdf.addSource(multipartFile.getInputStream());
                    
                	if(multipartFiles.size() > 2) {
	                    if(isPDf) {
	                    	mergePdf.setDestinationStream(baos);
	                    	mergePdf.mergeDocuments(MemoryUsageSetting.setupTempFileOnly());
	                    	InputStream stream = new ByteArrayInputStream(baos.toByteArray());
	                    	uploadToBucket(baos.size(), stream, fileId, "application/pdf");
	                    	returnObj.put("poid", fileId);
	                    }
	                    isPDf = true;
                	} else {
                		mergePdf.setDestinationStream(baos);
                    	mergePdf.mergeDocuments(MemoryUsageSetting.setupTempFileOnly());
                    	InputStream stream = new ByteArrayInputStream(baos.toByteArray());
                    	uploadToBucket(baos.size(), stream, fileId, "application/pdf");
                    	returnObj.put("poid", fileId);
                	}
                	
                } else if(keyname.contains("jpeg") || keyname.contains("jpg") || keyname.contains("png") || keyname.contains("tiff")) {
                	if(!isImages) {
                		document = new Document(PageSize.A4);
                		baos = new ByteArrayOutputStream();
                		PdfWriter.getInstance(document, baos);
//                		PdfWriter.getInstance(document, new FileOutputStream("C:/temp/" + fileId));
                		document.open();
                	}
                	
                	Image img = Image.getInstance(IOUtils.toByteArray(multipartFile.getInputStream()));
                    
                    float width = img.getWidth();
                    if(img.getWidth() > PageSize.A4.getWidth() / 2) {
                    	width = PageSize.A4.getWidth() / 2;
                    }
                    
                    float height = img.getHeight();
                    if(img.getHeight() > PageSize.A4.getHeight() / 2) {
                    	height = PageSize.A4.getHeight() / 2;
                    }
                    
                    img.scaleToFit(width, height);                    
                    document.add(img);
                    document.add(new Paragraph("\n\n"));
                	
                    if(multipartFiles.size() > 2) {
                    	if(isImages) {
                        	document.close();
                        	InputStream stream = new ByteArrayInputStream(baos.toByteArray());
                        	uploadToBucket(baos.size(), stream, fileId, "application/pdf");
                        	returnObj.put("poid", fileId);
                        }
                    	isImages = true;
                	} else {
                		document.close();
                    	InputStream stream = new ByteArrayInputStream(baos.toByteArray());
                    	uploadToBucket(baos.size(), stream, fileId, "application/pdf");
                    	returnObj.put("poid", fileId);
                	}
                } else if(keyname.contains("msword") || keyname.contains("officedocument")) {
                	logger.info("Content type :: " + multipartFile.getContentType());
                	uploadToBucket(multipartFile.getSize(), multipartFile.getInputStream(), fileId, multipartFile.getContentType());
					returnObj.put("poid", fileId);
                }
			}

		} catch (Exception e) {
			logger.error("unable to upload files : ", e);
		}

		return returnObj;
	}
	
	private void uploadToBucket(long fileSize, InputStream is, String fileId, String contenType) {
		
		logger.info("uploadMultipart : file upload : " + fileId + ", size : " + fileSize);
		
		ObjectMetadata objectMetadata = new ObjectMetadata();
		objectMetadata.setContentLength(fileSize);
		objectMetadata.setContentType(contenType);
		PutObjectRequest objectRequest = new PutObjectRequest(resource.getRequiredProperty(BUCKET_NAME), fileId,
				is, objectMetadata);
		// TransferManager transferManager = new
		// TransferManager(amazonS3Client);
		// transferManager.upload(objectRequest);
		logger.info("uploadMultipart : file upload request : START");
		objectRequest.withCannedAcl(CannedAccessControlList.PublicRead);
		objectRequest.setCannedAcl(CannedAccessControlList.PublicRead);
		amazonS3Client.putObject(objectRequest);
		// String url =
		// amazonS3Client.getUrl(resource.getRequiredProperty(BUCKET_NAME),
		// fileId).toString();
		logger.info("uploadMultipart : file uploaded : END :: " + fileId);
		
	}

	@Override
	public Map<String, String> getUrl(String key) {
		Map<String, String> returnObj = new HashMap<>();
		logger.info("Get S3 Url : START : " + returnObj);

		String url = amazonS3Client.getUrl(resource.getRequiredProperty(BUCKET_NAME), key).toString();

		returnObj.put("fileUrl", url);
		return returnObj;
	}

}
