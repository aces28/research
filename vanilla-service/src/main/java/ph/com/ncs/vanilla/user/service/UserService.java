package ph.com.ncs.vanilla.user.service;

import org.springframework.data.domain.Page;
import ph.com.ncs.vanilla.base.service.BaseService;
import ph.com.ncs.vanilla.user.domain.User;

import java.util.List;

/**
 * Created by edjohna on 6/29/2016.
 */
public interface UserService extends BaseService {

    User findByUserName(String userName);

    Page<User> listUser(Integer pageNumber);

    Page<User> listApprover(Integer pageNumber);

    List<User> listApproverForAssignment(String groupName);

    Page<User> filterByGroupName(String groupName, Integer pageNumber);

    List<User> extractByGroupName(String groupName);

    Page<User> filterByAvailability(int availability, Integer pageNumber);

    List<User> extractByAvailability(int availability);

    Page<User> filterByRole(String roleType, Integer pageNumber);

    List<User> extractByRole(String keyword);

    Page<User> filterByStatus(int status, Integer pageNumber);

    List<User> extractByStatus(int status);

    List<User> getApproversNoGroup();

    List<User> getMembersOfGroup(int groupId);

    Page<User> searchUser(String keyword, Integer pageNumber);

    List<User> extractUser(String keyword);

    Page<User> searchApprover(String keyword, Integer pageNumber);

    List<User> extractApprover(String keyword);

    List<User> findAll(Iterable iterable);

    List<User> extractAllApprover();

    List<User> extractAllUser();

    List<User> findAll();
}
