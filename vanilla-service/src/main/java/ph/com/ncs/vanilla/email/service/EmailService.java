package ph.com.ncs.vanilla.email.service;

/**
 * Created by ace on 7/6/16.
 */
public interface EmailService {

    /***
     * @param recipient
     */
    void sendEmail(String recipient);

    /**
     * E-mail property protocol
     */
    String MAIL_PROTOCOL = "mail.transport.protocol";

    /**
     * E-mail property host
     */
    String MAIL_HOSTS = "mail.smtps.host";

    /**
     * E-mail property port
     */
    String MAIL_PORT = "mail.smtps.port";

    /**
     * E-mail property authentication
     */
    String MAIL_AUTH = "mail.smtps.auth";

    /**
     * E-mail property TLS
     */
    String MAIL_TLS = "mail.smtps.starttls.enable";

    /**
     * E-mail property TLS required
     */
    String MAIL_TLS_REQUIRED = "mail.smtps.starttls.required";

}
