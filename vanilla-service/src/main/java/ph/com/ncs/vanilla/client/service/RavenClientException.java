package ph.com.ncs.vanilla.client.service;

/**
 * Created by ace on 7/10/16.
 */
public class RavenClientException extends Exception {

    public RavenClientException() {
        super();
    }

    public RavenClientException(String message) {
        super(message);
    }

    public RavenClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public RavenClientException(Throwable cause) {
        super(cause);
    }
}
