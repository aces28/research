package ph.com.ncs.vanilla.mis.service;

import org.springframework.data.domain.Page;
import ph.com.ncs.vanilla.application.domain.SubsApplication;
import ph.com.ncs.vanilla.base.service.BaseService;

import java.sql.Timestamp;
import java.util.List;


/**
 * Created by edjohna on 6/28/2016.
 */
public interface MISService extends BaseService {

    Page<SubsApplication> listForAssignmentMIS(Integer pageNumber);

    Page<SubsApplication> listForApprovalMIS(Integer pageNumber);

    Page<SubsApplication> listApprovedMIS(Integer pageNumber);

    Page<SubsApplication> listDisapprovedMIS(Integer pageNumber);

    Page<SubsApplication> filterForAssignmentMIS(Timestamp to, Timestamp from, Integer pageNumber);

    List<SubsApplication> extractForAssignmentMIS(Timestamp to, Timestamp from);

    Page<SubsApplication> filterForApprovalMIS(Timestamp to, Timestamp from, Integer pageNumber);

    List<SubsApplication> extractForApprovalMIS(Timestamp to, Timestamp from);

    Page<SubsApplication> filterApprovedMIS(Timestamp to, Timestamp from, Integer pageNumber);

    List<SubsApplication> extractApprovedMIS(Timestamp to, Timestamp from);

    Page<SubsApplication> filterDisapprovedMIS(Timestamp to, Timestamp from, Integer pageNumber);

    List<SubsApplication> extractDisapprovedMIS(Timestamp to, Timestamp from);

    Page<SubsApplication> searchForAssignmentString(String keyword, Integer pageNumber);

    List<SubsApplication> extractForAssignmentString(String keyword);

    Page<SubsApplication> searchForApprovalString(String keyword, Integer pageNumber);

    List<SubsApplication> extractForApprovalString(String keyword);

    Page<SubsApplication> searchApprovedString(String keyword, Integer pageNumber);

    List<SubsApplication> extractApprovedString(String keyword);

    Page<SubsApplication> searchDisapprovedString(String keyword, Integer pageNumber);

    List<SubsApplication> extractDisapprovedString(String keyword);

    List<SubsApplication> extractAllDisapproved();

    List<SubsApplication> extractAllApproved();

    List<SubsApplication> extractAllForApproval();

    List<SubsApplication> extractAllForAssignment();

    Long approverOngoing(Integer id);

    Long approverCompleted(Integer id);
}
