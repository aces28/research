package ph.com.ncs.vanilla.client.service;

/**
 * Created by ace on 7/10/16.
 */
public class EsbClientException extends Exception {

    public EsbClientException() {
        super();
    }

    public EsbClientException(String message) {
        super(message);
    }

    public EsbClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public EsbClientException(Throwable cause) {
        super(cause);
    }
}
