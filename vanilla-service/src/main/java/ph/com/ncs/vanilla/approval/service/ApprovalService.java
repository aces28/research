package ph.com.ncs.vanilla.approval.service;

import org.springframework.data.domain.Page;
import ph.com.ncs.vanilla.application.domain.SubsApplication;
import ph.com.ncs.vanilla.base.service.BaseService;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by edjohna on 6/25/2016.
 */
public interface ApprovalService extends BaseService {

    Page<SubsApplication> listForApprovalApprover(String userName, Integer pageNumber);

    Page<SubsApplication> listApprovedApprover(String userName, Integer pageNumber);

    Page<SubsApplication> listDisapprovedApprover(String userName, Integer pageNumber);

    Page<SubsApplication> filterForApprovalApprover(String userName, Timestamp to, Timestamp from, Integer pageNumber);

    List<SubsApplication> extractForApprovalApprover(String userName, Timestamp to, Timestamp from);

    Page<SubsApplication> filterApprovedApprover(String userName, Timestamp to, Timestamp from, Integer pageNumber);

    List<SubsApplication> extractApprovedApprover(String userName, Timestamp to, Timestamp from);

    Page<SubsApplication> filterDisapprovedApprover(String userName, Timestamp to, Timestamp from, Integer pageNumber);

    List<SubsApplication> extractDisapprovedApprover(String userName, Timestamp to, Timestamp from);

    Page<SubsApplication> searchForApprovalString(String keyword, String userName, Integer pageNumber);

    List<SubsApplication> extractForApprovalString(String keyword, String userName);

    Page<SubsApplication> searchApprovedString(String keyword, String userName, Integer pageNumber);

    List<SubsApplication> extractApprovedString(String keyword, String userName);

    Page<SubsApplication> searchDispprovedString(String keyword, String userName, Integer pageNumber);

    List<SubsApplication> extractDispprovedString(String keyword, String userName);

    List<SubsApplication> extractAllForApproval(String userName);

    List<SubsApplication> extractAllApproved(String userName);

    List<SubsApplication> extractAllDisapproved(String userName);

}
