package ph.com.ncs.vanilla.email.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import ph.com.ncs.vanilla.email.domain.Email;

import javax.annotation.PostConstruct;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * Created by ace on 7/6/16.
 */
@Service
public class EmailServiceImpl implements EmailService {

    private static final Logger logger = LoggerFactory.getLogger(EmailServiceImpl.class);

    @Autowired
    private Email email;
    @Autowired
    private ResourceLoader resourceLoader;

    @Override
    public void sendEmail(String recipient) {
        logger.info("send e-mail : " + recipient);
        Properties properties = System.getProperties();
        properties.put(MAIL_PROTOCOL, email.getProtocol());
        properties.put(MAIL_HOSTS, email.getHost());
        properties.put(MAIL_PORT, email.getPort());

        properties.put(MAIL_AUTH, email.getAuthEnabled());
        properties.put(MAIL_TLS, email.getTlsEnabled());
        properties.put(MAIL_TLS_REQUIRED, email.getTlsRequired());

        Session session = Session.getDefaultInstance(properties);
        session.setDebug(true);

        MimeMessage mimeMessage = new MimeMessage(session);
        Transport transport = null;

        try {
            mimeMessage.setFrom(new InternetAddress(email.getFrom()));
            mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
            mimeMessage.setSubject(email.getSubject());
            mimeMessage.setContent(loadBody(), "text/plain");
            transport = session.getTransport();

            transport.connect(email.getHost(), email.getSmtpUsername(), email.getSmtpPassword());

            logger.info("AWS SES end-point : " + email.getHost() + " : " + email.getSmtpUsername() + " : " + email.getSmtpPassword());
            transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
        } catch (MessagingException e) {
            logger.error("Unable to send e-mail : ", e);
        } finally {
            try {
                if (transport != null) {
                    transport.close();
                }
            } catch (MessagingException message) {
                logger.error("Unable to close connection : ", message);
            }
        }

    }

    /**
     * @return
     */
    private String loadBody() {
        Resource resource = resourceLoader.getResource("classpath:termsncondition.txt");
        BufferedReader bufferedReader = null;
        StringBuffer stringBuffer = new StringBuffer();
        try (InputStream inputStream = resource.getInputStream()) {
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String readLine;
            while ((readLine = bufferedReader.readLine()) != null) {
                stringBuffer.append(readLine).append("\n");
            }
        } catch (IOException e) {
            logger.error("Unable to read file : ", e);
        } finally {
            try {
                bufferedReader.close();
            } catch (IOException e) {
                logger.error("Unable to close reader : ", e);
            }
        }
        return stringBuffer.toString();
    }
}
