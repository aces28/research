package ph.com.ncs.vanilla.rejection.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ph.com.ncs.vanilla.base.domain.Model;
import ph.com.ncs.vanilla.rejection.dao.RejectionRepository;
import ph.com.ncs.vanilla.rejection.domain.Rejection;

@Service
public class RejectionServiceImpl implements RejectionService {

    @Autowired
    private RejectionRepository rejectionRepository;

    @Override
    public <T extends Model> T save(T data) {
        return (T) rejectionRepository.save((Rejection) data);
    }

    @Override
    public <T extends Model> T get(T data) {
        return (T) rejectionRepository.findOne(((Rejection) data).getId());
    }

    @Override
    public <T> T save(Iterable iterable) {
        return (T) rejectionRepository.save(iterable);
    }

}
