package ph.com.ncs.vanilla.subscriber.service;

import ph.com.ncs.vanilla.application.domain.SubsApplication;
import ph.com.ncs.vanilla.base.service.BaseService;

import java.util.List;

public interface SubscriberAppService extends BaseService {

    List<SubsApplication> findApplicationByMsisdn(String msisdn);
    
    Long countApplicationUpdatedByApprover(Integer status, Integer userId);  
}
