package ph.com.ncs.vanilla.role.service;

import ph.com.ncs.vanilla.base.service.BaseService;
import ph.com.ncs.vanilla.role.domain.Role;

/**
 * Created by ace on 7/11/16.
 */
public interface RoleService extends BaseService {
	
    Role findRoleById(int roleId);
}
