package ph.com.ncs.vanilla.aws.service;

import java.util.Collection;
import java.util.Map;

import javax.servlet.http.Part;

/**
 * Created by jomerp on 8/4/2016.
 */
public interface AwsUploadService {

    /**
     * @param
     * @return
     */
    //List<PutObjectResult> uploadMultipart(MultipartFile[] multipartFiles);


    /**
     * Bucket name of AWS S3
     */
    String BUCKET_NAME = "cloud.aws.s3.bucket";


    Map<String, String> uploadMultipart(Collection<Part> multipartFiles);
    
    Map<String, String> getUrl(String key);
}
