package ph.com.ncs.vanilla.audit.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ph.com.ncs.vanilla.audit.dao.AccessTransactionRepository;
import ph.com.ncs.vanilla.audit.domain.AuditAccessTransaction;
import ph.com.ncs.vanilla.audit.domain.AuditModule;
import ph.com.ncs.vanilla.base.domain.Model;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;
import ph.com.ncs.vanilla.module.service.ModuleService;
import ph.com.ncs.vanilla.user.domain.User;
import ph.com.ncs.vanilla.user.service.UserService;

/**
 * Created by edjohna on 7/17/2016.
 */
@Service
public class AuditServiceImpl implements AuditService {

    private static final int PAGE_SIZE = 10;
    
    private static final Logger LOGGER = LoggerFactory.getLogger(AuditServiceImpl.class);

    @Autowired
    private AccessTransactionRepository auditRepository;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private ModuleService moduleService;
    
    @Override
    public <T extends Model> T save(T data) {
        return (T) auditRepository.save((AuditAccessTransaction) data);
    }

    @Override
    public <T extends Model> T get(T data) {
        return (T) auditRepository.findOne(data.getId());
    }

    @Override
    public <T> T save(Iterable iterable) {
        return null;
    }

    @Override
    public Page<AuditAccessTransaction> listUserAccessHistory(Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.DESC, "dateAccessed");
        return auditRepository.listUserAccessHistory(request);

    }

    @Override
    public Page<AuditAccessTransaction> listTransactionLogs(Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.DESC, "dateAccessed");
        return auditRepository.listTransactionLogs(request);
    }

    @Override
    public Page<AuditAccessTransaction> filterByModuleUserAccessHistory(int moduleId, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.DESC, "dateAccessed");
        return auditRepository.filterByModuleUserAccessHistory(moduleId, request);
    }

    @Override
    public Page<AuditAccessTransaction> filterByModuleTransactionLogs(int moduleId, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.DESC, "dateAccessed");
        return auditRepository.filterByModuleTransactionLogs(moduleId, request);
    }

    @Override
    public Page<AuditAccessTransaction> filterByDateUserAccessHistory(Timestamp from, Timestamp to, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.DESC, "dateAccessed");
        return auditRepository.filterByDateUserAccessHistory(from, to, request);
    }

    @Override
    public Page<AuditAccessTransaction> filterByDateTransactionLogs(Timestamp from, Timestamp to, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.DESC, "dateAccessed");
        return auditRepository.filterByDateTransactionLogs(from, to, request);
    }

    @Override
    public Page<AuditAccessTransaction> searchUserAccessHistory(String keyword, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.DESC, "dateAccessed");
        return auditRepository.searchUserAccessHistory(keyword, request);
    }

    @Override
    public Page<AuditAccessTransaction> searchTransactionLogs(String keyword, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.DESC, "dateAccessed");
        return auditRepository.searchTransactionLogs(keyword, request);
    }

    @Override
    public List<AuditAccessTransaction> extractUserAccessHistory(String keyword) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize, Sort.Direction.DESC, "dateAccessed");
    	List<AuditAccessTransaction> auditAccessList = Collections.synchronizedList(new ArrayList<AuditAccessTransaction>());

        if (keyword.equalsIgnoreCase("all")) {
        	Page<AuditAccessTransaction> auditPage = auditRepository.listUserAccessHistory(request);
            auditAccessList = auditPage.getContent();
        } else {
        	Page<AuditAccessTransaction> auditPage = auditRepository.searchUserAccessHistory(keyword, request);
            auditAccessList = auditPage.getContent();
        }
        return auditAccessList;
        
    }

    @Override
    public List<AuditAccessTransaction> extractTransactionLogs(String keyword) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize, Sort.Direction.DESC, "dateAccessed");
        Page<AuditAccessTransaction> auditPage = auditRepository.searchTransactionLogs(keyword, request);
        List<AuditAccessTransaction> auditAccessList = auditPage.getContent();
        return auditAccessList;
    }

    @Override
    public List<AuditAccessTransaction> extractTransactionLogsByModule(int moduleId) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize, Sort.Direction.DESC, "dateAccessed");
        Page<AuditAccessTransaction> auditPage = auditRepository.filterByModuleTransactionLogs(moduleId, request);
        List<AuditAccessTransaction> auditAccessList = auditPage.getContent();
        return auditAccessList;
    }

    @Override
    public List<AuditAccessTransaction> extractUserAccessHistoryByModule(int moduleId) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize, Sort.Direction.DESC, "dateAccessed");
        Page<AuditAccessTransaction> auditPage = auditRepository.filterByModuleUserAccessHistory(moduleId, request);
        List<AuditAccessTransaction> auditAccessList = auditPage.getContent();
        return auditAccessList;
    }

    @Override
    public List<AuditAccessTransaction> extractTransactionByDate(Timestamp from, Timestamp to) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize, Sort.Direction.DESC, "dateAccessed");
        Page<AuditAccessTransaction> auditPage = auditRepository.filterByDateTransactionLogs(from, to, request);
        List<AuditAccessTransaction> auditAccessList = auditPage.getContent();
        return auditAccessList;
    }

    @Override
    public List<AuditAccessTransaction> extractUserAccessHistoryByDate(Timestamp from, Timestamp to) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize, Sort.Direction.DESC, "dateAccessed");
        Page<AuditAccessTransaction> auditPage = auditRepository.filterByDateUserAccessHistory(from, to, request);
        List<AuditAccessTransaction> auditAccessList = auditPage.getContent();
        return auditAccessList;
    }
    
    @Override
    public void saveUserAccessAuditLogs(String userName, int moduleId, HttpServletRequest request){
        AuditAccessTransaction auditAccess = new AuditAccessTransaction();
        User user = userService.findByUserName(userName);
        auditAccess.setUser(user);
        auditAccess.setIpAddress(request.getRemoteAddr());
        AuditModule auditModule = moduleService.findByModuleId(moduleId);
        auditAccess.setModule(auditModule);
        auditAccess.setDateAccessed(new Timestamp(VanillaDateUtils.longCurrentDate()));
        auditAccess.setCreatedBy(userName);
        auditAccess.setCreatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        auditAccess.setUpdatedBy(userName);
        auditAccess.setUpdatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        this.save(auditAccess);
    }

	@Override
	public List<AuditAccessTransaction> generateLAR() {
		return auditRepository.generateLAR();
	}
}
