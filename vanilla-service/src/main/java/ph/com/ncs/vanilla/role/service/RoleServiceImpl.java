package ph.com.ncs.vanilla.role.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ph.com.ncs.vanilla.base.domain.Model;
import ph.com.ncs.vanilla.role.dao.RoleRepository;
import ph.com.ncs.vanilla.role.domain.Role;

/**
 * Created by ace on 7/11/16.
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public <T extends Model> T save(T data) {
        return null;
    }

    @Override
    public <T extends Model> T get(T data) {
        return (T) roleRepository.findOne(((Role) data).getId());
    }

    @Override
    public <T> T save(Iterable iterable) {
        return null;
    }

	@Override
	public Role findRoleById(int roleId) {
		return roleRepository.findRoleById(roleId);
	}
}
