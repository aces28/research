package ph.com.ncs.vanilla.client.service;

/**
 * Created by ace on 7/10/16.
 */
public class NfClientException extends Exception {

    public NfClientException() {
        super();
    }

    public NfClientException(String message) {
        super(message);
    }

    public NfClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public NfClientException(Throwable cause) {
        super(cause);
    }
}
