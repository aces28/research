package ph.com.ncs.vanilla.approval.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ph.com.ncs.vanilla.application.domain.SubsApplication;
import ph.com.ncs.vanilla.approval.dao.ApproverRepository;
import ph.com.ncs.vanilla.base.domain.Model;
import ph.com.ncs.vanilla.user.domain.User;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by edjohna on 6/25/2016.
 */
@Service
public class ApprovalServiceImpl implements ApprovalService {

    private static final int PAGE_SIZE = 10;
    @Autowired
    private ApproverRepository approverRepository;

    @Override
    public Page<SubsApplication> listForApprovalApprover(String userName, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.ASC, "applicationDate");
        return approverRepository.listForApprovalApprover(userName, request);
    }

    @Override
    public Page<SubsApplication> listApprovedApprover(String userName, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.ASC, "applicationDate");
        return approverRepository.listApprovedApprover(userName, request);
    }

    @Override
    public Page<SubsApplication> listDisapprovedApprover(String userName, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.ASC, "applicationDate");
        return approverRepository.listDisapprovedApprover(userName, request);
    }

    @Override
    public Page<SubsApplication> filterForApprovalApprover(String userName, Timestamp to, Timestamp from, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.ASC, "applicationDate");
        return approverRepository.filterForApprovalApprover(userName, to, from, request);
    }

    @Override
    public List<SubsApplication> extractForApprovalApprover(String userName, Timestamp to, Timestamp from) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize, Sort.Direction.DESC, "applicationDate");
        Page<SubsApplication> subsApplicationsPage = approverRepository.filterForApprovalApprover(userName, to, from, request);
        List<SubsApplication> subsApplicationList = subsApplicationsPage.getContent();
        return subsApplicationList;
    }

    @Override
    public Page<SubsApplication> filterApprovedApprover(String userName, Timestamp to, Timestamp from, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.ASC, "applicationDate");
        return approverRepository.filterApprovedApprover(userName, to, from, request);
    }

    @Override
    public List<SubsApplication> extractApprovedApprover(String userName, Timestamp to, Timestamp from) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize, Sort.Direction.DESC, "applicationDate");
        Page<SubsApplication> subsApplicationsPage = approverRepository.filterApprovedApprover(userName, to, from, request);
        List<SubsApplication> subsApplicationList = subsApplicationsPage.getContent();
        return subsApplicationList;
    }

    @Override
    public Page<SubsApplication> filterDisapprovedApprover(String userName, Timestamp to, Timestamp from, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.ASC, "applicationDate");
        return approverRepository.filterDisapprovedApprover(userName, to, from, request);
    }

    @Override
    public List<SubsApplication> extractDisapprovedApprover(String userName, Timestamp to, Timestamp from) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize, Sort.Direction.DESC, "applicationDate");
        Page<SubsApplication> subsApplicationsPage = approverRepository.filterDisapprovedApprover(userName, to, from, request);
        List<SubsApplication> subsApplicationList = subsApplicationsPage.getContent();
        return subsApplicationList;
    }

    @Override
    public Page<SubsApplication> searchForApprovalString(String keyword, String userName, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.ASC, "applicationDate");
        return approverRepository.searchForApprovalString(keyword, userName, request);
    }

    @Override
    public List<SubsApplication> extractForApprovalString(String keyword, String userName) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize, Sort.Direction.DESC, "applicationDate");
        Page<SubsApplication> subsApplicationsPage = approverRepository.searchForApprovalString(keyword, userName, request);
        List<SubsApplication> subsApplicationList = subsApplicationsPage.getContent();
        return subsApplicationList;
    }

    @Override
    public Page<SubsApplication> searchApprovedString(String keyword, String userName, Integer pageNumber) {

        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.ASC, "applicationDate");
        return approverRepository.searchApprovedString(keyword, userName, request);
    }

    @Override
    public List<SubsApplication> extractApprovedString(String keyword, String userName) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize, Sort.Direction.DESC, "applicationDate");
        Page<SubsApplication> subsApplicationsPage = approverRepository.searchApprovedString(keyword, userName, request);
        List<SubsApplication> subsApplicationList = subsApplicationsPage.getContent();
        return subsApplicationList;
    }

    @Override
    public Page<SubsApplication> searchDispprovedString(String keyword, String userName, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.ASC, "applicationDate");
        return approverRepository.searchDispprovedString(keyword, userName, request);
    }

    @Override
    public List<SubsApplication> extractDispprovedString(String keyword, String userName) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize, Sort.Direction.DESC, "applicationDate");
        Page<SubsApplication> subsApplicationsPage = approverRepository.searchDispprovedString(keyword, userName, request);
        List<SubsApplication> subsApplicationList = subsApplicationsPage.getContent();
        return subsApplicationList;
    }

    @Override
    public List<SubsApplication> extractAllForApproval(String userName) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize, Sort.Direction.DESC, "applicationDate");
        Page<SubsApplication> subsApplicationsPage = approverRepository.listForApprovalApprover(userName, request);
        List<SubsApplication> subsApplicationList = subsApplicationsPage.getContent();
        return subsApplicationList;
    }

    @Override
    public List<SubsApplication> extractAllApproved(String userName) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize, Sort.Direction.DESC, "applicationDate");
        Page<SubsApplication> subsApplicationsPage = approverRepository.listApprovedApprover(userName, request);
        List<SubsApplication> subsApplicationList = subsApplicationsPage.getContent();
        return subsApplicationList;
    }

    @Override
    public List<SubsApplication> extractAllDisapproved(String userName) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize, Sort.Direction.DESC, "applicationDate");
        Page<SubsApplication> subsApplicationsPage = approverRepository.listDisapprovedApprover(userName, request);
        List<SubsApplication> subsApplicationList = subsApplicationsPage.getContent();
        return subsApplicationList;
    }

    @Override
    public <T extends Model> T save(T data) {
        return null;
    }

    @Override
    public <T extends Model> T get(T data) {
        return null;
    }

    @Override
    public <T> T save(Iterable iterable) {
        return null;
    }
}
