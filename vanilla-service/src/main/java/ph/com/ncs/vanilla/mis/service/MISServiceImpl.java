package ph.com.ncs.vanilla.mis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ph.com.ncs.vanilla.application.domain.SubsApplication;
import ph.com.ncs.vanilla.base.domain.Model;
import ph.com.ncs.vanilla.mis.dao.MISRepository;

import java.sql.Timestamp;
import java.util.List;


/**
 * Created by edjohna on 6/28/2016.
 */
@Service
public class MISServiceImpl implements MISService {

    private static final int PAGE_SIZE = 10;

    @Autowired
    private MISRepository misRepository;

    @Override
    public Page<SubsApplication> listForAssignmentMIS(Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.DESC, "applicationDate");
        return misRepository.listForAssignmentMIS(request);
    }

    @Override
    public Page<SubsApplication> listForApprovalMIS(Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.DESC, "applicationDate");
        return misRepository.listForApprovalMIS(request);
    }

    @Override
    public Page<SubsApplication> listApprovedMIS(Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.DESC, "applicationDate");
        return misRepository.listApprovedMIS(request);
    }

    @Override
    public Page<SubsApplication> listDisapprovedMIS(Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.DESC, "applicationDate");
        return misRepository.listDisapprovedMIS(request);
    }

    @Override
    public Page<SubsApplication> filterForAssignmentMIS(Timestamp to, Timestamp from, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.DESC, "applicationDate");
        return misRepository.filterForAssignmentMIS(to, from, request);
    }

    @Override
    public List<SubsApplication> extractForAssignmentMIS(Timestamp to, Timestamp from) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize, Sort.Direction.DESC, "applicationDate");
        Page<SubsApplication> subsApplicationsPage = misRepository.filterForAssignmentMIS(to, from, request);
        List<SubsApplication> subsApplicationList = subsApplicationsPage.getContent();
        return subsApplicationList;
    }


    @Override
    public Page<SubsApplication> filterForApprovalMIS(Timestamp to, Timestamp from, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.DESC, "applicationDate");
        return misRepository.filterForApprovalMIS(to, from, request);
    }

    @Override
    public List<SubsApplication> extractForApprovalMIS(Timestamp to, Timestamp from) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize, Sort.Direction.DESC, "applicationDate");
        Page<SubsApplication> subsApplicationsPage = misRepository.filterForApprovalMIS(to, from, request);
        List<SubsApplication> subsApplicationList = subsApplicationsPage.getContent();
        return subsApplicationList;
    }

    @Override
    public Page<SubsApplication> filterApprovedMIS(Timestamp to, Timestamp from, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.DESC, "applicationDate");
        return misRepository.filterApprovedMIS(to, from, request);
    }

    @Override
    public List<SubsApplication> extractApprovedMIS(Timestamp to, Timestamp from) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize, Sort.Direction.DESC, "applicationDate");
        Page<SubsApplication> subsApplicationsPage = misRepository.filterApprovedMIS(to, from, request);
        List<SubsApplication> subsApplicationList = subsApplicationsPage.getContent();
        return subsApplicationList;
    }

    @Override
    public Page<SubsApplication> filterDisapprovedMIS(Timestamp to, Timestamp from, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.DESC, "applicationDate");
        return misRepository.filterDisapprovedMIS(to, from, request);
    }

    @Override
    public List<SubsApplication> extractDisapprovedMIS(Timestamp to, Timestamp from) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize, Sort.Direction.DESC, "applicationDate");
        Page<SubsApplication> subsApplicationsPage = misRepository.filterDisapprovedMIS(to, from, request);
        List<SubsApplication> subsApplicationList = subsApplicationsPage.getContent();
        return subsApplicationList;
    }

    @Override
    public Page<SubsApplication> searchForAssignmentString(String keyword, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.DESC, "applicationDate");
        return misRepository.searchForAssignmentString(keyword, request);
    }

    @Override
    public List<SubsApplication> extractForAssignmentString(String keyword) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize, Sort.Direction.DESC, "applicationDate");
        Page<SubsApplication> subsApplicationsPage = misRepository.searchForAssignmentString(keyword, request);
        List<SubsApplication> subsApplicationList = subsApplicationsPage.getContent();
        return subsApplicationList;
    }

    @Override
    public Page<SubsApplication> searchForApprovalString(String keyword, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.DESC, "applicationDate");
        return misRepository.searchForApprovalString(keyword, request);
    }

    @Override
    public List<SubsApplication> extractForApprovalString(String keyword) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize, Sort.Direction.DESC, "applicationDate");
        Page<SubsApplication> subsApplicationsPage = misRepository.searchForApprovalString(keyword, request);
        List<SubsApplication> subsApplicationList = subsApplicationsPage.getContent();
        return subsApplicationList;
    }

    @Override
    public Page<SubsApplication> searchApprovedString(String keyword, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.DESC, "applicationDate");
        return misRepository.searchApprovedString(keyword, request);
    }

    @Override
    public List<SubsApplication> extractApprovedString(String keyword) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize, Sort.Direction.DESC, "applicationDate");
        Page<SubsApplication> subsApplicationsPage = misRepository.searchApprovedString(keyword, request);
        List<SubsApplication> subsApplicationList = subsApplicationsPage.getContent();
        return subsApplicationList;
    }

    @Override
    public Page<SubsApplication> searchDisapprovedString(String keyword, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.DESC, "applicationDate");
        return misRepository.searchDispprovedString(keyword, request);
    }

    @Override
    public List<SubsApplication> extractDisapprovedString(String keyword) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize, Sort.Direction.DESC, "applicationDate");
        Page<SubsApplication> subsApplicationsPage = misRepository.searchDispprovedString(keyword, request);
        List<SubsApplication> subsApplicationList = subsApplicationsPage.getContent();
        return subsApplicationList;
    }

    @Override
    public List<SubsApplication> extractAllDisapproved() {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize, Sort.Direction.DESC, "applicationDate");
        Page<SubsApplication> subsApplicationsPage = misRepository.listDisapprovedMIS(request);
        List<SubsApplication> subsApplicationList = subsApplicationsPage.getContent();
        return subsApplicationList;
    }

    @Override
    public List<SubsApplication> extractAllApproved() {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize, Sort.Direction.DESC, "applicationDate");
        Page<SubsApplication> subsApplicationsPage = misRepository.listApprovedMIS(request);
        List<SubsApplication> subsApplicationList = subsApplicationsPage.getContent();
        return subsApplicationList;
    }

    @Override
    public List<SubsApplication> extractAllForApproval() {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize, Sort.Direction.DESC, "applicationDate");
        Page<SubsApplication> subsApplicationsPage = misRepository.listForApprovalMIS(request);
        List<SubsApplication> subsApplicationList = subsApplicationsPage.getContent();
        return subsApplicationList;
    }

    @Override
    public List<SubsApplication> extractAllForAssignment() {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize, Sort.Direction.DESC, "applicationDate");
        Page<SubsApplication> subsApplicationsPage = misRepository.listForAssignmentMIS(request);
        List<SubsApplication> subsApplicationList = subsApplicationsPage.getContent();
        return subsApplicationList;
    }

    @Override
    public Long approverOngoing(Integer id) {
        return misRepository.approverOngoing(id);
    }

    @Override
    public Long approverCompleted(Integer id) {
        return misRepository.approverCompleted(id);
    }


    @Override
    public <T extends Model> T save(T data) {
        return null;
    }

    @Override
    public <T extends Model> T get(T data) {
        return null;
    }

    @Override
    public <T> T save(Iterable iterable) {
        return (T) misRepository.save(iterable);
    }
}
