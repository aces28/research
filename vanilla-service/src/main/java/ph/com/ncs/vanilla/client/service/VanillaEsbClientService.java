package ph.com.ncs.vanilla.client.service;

import com.globe.warcraft.wsdl.billingcm.*;
import com.globe.warcraft.wsdl.billingcm.UnifiedResourceDetailsInfo.AttributesInfoList;
import com.globe.warcraft.wsdl.header.WarcraftHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import ph.com.ncs.vanilla.base.service.BaseClientService;
import ph.com.ncs.vanilla.commons.utils.PhoneUtils;
import ph.com.ncs.vanilla.transaction.domain.Transaction;

import javax.xml.ws.Holder;
import java.net.URL;
import java.util.List;

/**
 * Created by ace on 5/27/16.
 */
@Component
public class VanillaEsbClientService implements BaseClientService<Transaction> {

    private static final Logger logger = LoggerFactory.getLogger(VanillaEsbClientService.class);

    @Autowired
    private Environment environment;

    @Override
    public <T> T getResult(Transaction transaction) throws EsbClientException {
        try {
            logger.info("sending request to ESB : " + environment.getRequiredProperty(ESB_WSDL_URL));
            String enabled = environment.getRequiredProperty(ALLOW_GHP_PREPAID);
            SubscriberHeader subscriberHeader = null;
            SubscriberGeneralInfo subscriberGeneralInfo;
            AttributesInfoList attributesInfoList = null;
            List<AttributesData> listAttributesData = null;
            UnifiedResourceDetailsInfo unifiedResourceDetailsInfo = null;

            URL url = new URL(environment.getRequiredProperty(ESB_WSDL_URL));
            BillingCMService cmService = new BillingCMService(url);
            BillingCMProxyService cmProxyService = cmService.getBillingCMServicePort();

            ObjectFactory factory = new ObjectFactory();

            GetDetailsByMsisdn detailsByMsisdn = factory.createGetDetailsByMsisdn();

            String msisdn = PhoneUtils.esbPhoneNumber(transaction.getMsisdn());
            detailsByMsisdn.setMSISDN(msisdn);
            detailsByMsisdn.setPrimaryResourceType(environment.getRequiredProperty(PRIMARY_RESOURCE_TYPE));

            Holder<GetDetailsByMsisdnResponse> detailsByMsisdnResponseHolder = new Holder<>();
            Holder<WarcraftHeader> headerHolder = new Holder<>();

//            logger.info("header : " + headerHolder.value);
//            logger.info("message id : " + headerHolder.value.getEsbMessageID() + " operation name : " + headerHolder.value.getEsbOperationName());

            cmProxyService.getDetailsByMsisdn(detailsByMsisdn, detailsByMsisdnResponseHolder, headerHolder);
            GetDetailsByMsisdnResult detailsByMsisdnResult = detailsByMsisdnResponseHolder.value.getGetDetailsByMsisdnResult();

            if (detailsByMsisdnResult.getUnifiedResourceDetails() != null) {
                logger.info("unifiedResourceDetailsInfo is not null");
                unifiedResourceDetailsInfo = detailsByMsisdnResult.getUnifiedResourceDetails();
            } else {
                transaction.setReferenceKey(environment.getRequiredProperty(GENERIC_ESB_ERROR));
            }

            if (unifiedResourceDetailsInfo != null && unifiedResourceDetailsInfo.getAttributesInfoList() != null) {
                logger.info("attributesInfoList is not null");
                attributesInfoList = unifiedResourceDetailsInfo.getAttributesInfoList();
            } else {
                transaction.setReferenceKey(environment.getRequiredProperty(GENERIC_ESB_ERROR));
            }

            if (attributesInfoList != null && attributesInfoList.getAttributesInfo() != null) {
                listAttributesData = attributesInfoList.getAttributesInfo();
                logger.info("listAttributesData is not null " + listAttributesData.size());
            } else {
                transaction.setReferenceKey(environment.getRequiredProperty(GENERIC_ESB_ERROR));
            }

            if (detailsByMsisdnResult.getSubscriberHeader() != null) {
                subscriberHeader = detailsByMsisdnResult.getSubscriberHeader();
                logger.info("subscriberHeader is not null");
            } else {
                transaction.setReferenceKey(environment.getRequiredProperty(GENERIC_ESB_ERROR));
            }

            if (subscriberHeader.getSubscriberGeneralInfo() != null) {
                subscriberGeneralInfo = subscriberHeader.getSubscriberGeneralInfo();
                logger.info("subscriberGeneralInfo is not null");
                if (subscriberGeneralInfo.getPrimResourceTp() != null && "LTP".equalsIgnoreCase(subscriberGeneralInfo.getPrimResourceTp())) {
                    logger.info("subscriber is LTP : " + subscriberGeneralInfo.getPrimResourceTp());
                    transaction.setReferenceKey(environment.getRequiredProperty(LTP_ERROR_MESSAGE));
                } else {
                    logger.info("=========== INSIDE ELSE OF General Info Not Null=========== ");
                    if (listAttributesData != null) {
                        for (AttributesData attributesData : listAttributesData) {
                            logger.info("=========== FOR LOOP =========== ");
                            if (attributesData.getAttrName() != null && "BRAND".equalsIgnoreCase(attributesData.getAttrName())) {
                                logger.info("=========== ATTR NAME IF =========== ");
                                if (attributesData.getAttrValue() != null && !"".equalsIgnoreCase(attributesData.getAttrValue())) {
                                    logger.info("=========== ATTR VALUE IF WITHOUT CONFIG =========== " + attributesData.getAttrValue());
                                    if (ESB_GHP_PREPAID.equalsIgnoreCase(attributesData.getAttrValue()) || ESB_TM_PREPAID.equalsIgnoreCase(attributesData.getAttrValue())) {
                                        logger.info("=========== GHP-Prepaid/TM VALUE IF =========== " + attributesData.getAttrValue());

                                        if (unifiedResourceDetailsInfo.getStatusInfo() != null) {
                                            StatusInfo statusInfo = unifiedResourceDetailsInfo.getStatusInfo();
                                            if (!"active".equalsIgnoreCase(statusInfo.getStatus())) {
                                                transaction.setReferenceKey(environment.getRequiredProperty(GENERIC_ESB_ERROR));
                                            } else {
                                                transaction.setType(attributesData.getAttrValue());
                                                transaction.setStatus(0);
                                            }
                                        }
                                    } else {
                                        if (attributesData.getAttrValue().matches("(?s).*\\bPrepaid\\b.*")) {
                                            transaction.setReferenceKey(environment.getRequiredProperty(PREPAID_ERROR_MESSAGE));
                                        } else {
                                            transaction.setReferenceKey(environment.getRequiredProperty(POSTPAID_ERROR_MESSAGE));
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        transaction.setReferenceKey(environment.getRequiredProperty(GENERIC_ESB_ERROR));
                    }
                }
            } else {
                logger.info("=========== INSIDE ELSE General Info Null =========== ");
                if (listAttributesData != null) {
                    for (AttributesData attributesData : listAttributesData) {
                        if (attributesData.getAttrName() != null && "BRAND".equalsIgnoreCase(attributesData.getAttrName())) {
                            logger.info("=========== ATTR NAME IF =========== ");
                            if (attributesData.getAttrValue() != null && !"".equalsIgnoreCase(attributesData.getAttrValue())) {
                                logger.info("=========== ATTR VALUE WITH CONFIG =========== " + attributesData.getAttrValue());
                                if (ENABLE_BRANDING.equalsIgnoreCase(enabled)) {
                                    if (ESB_GHP_PREPAID.equalsIgnoreCase(attributesData.getAttrValue()) || ESB_TM_PREPAID.equalsIgnoreCase(attributesData.getAttrValue())) {
                                        logger.info("=========== GHP-Prepaid/TM VALUE IF =========== " + attributesData.getAttrValue());
                                        transaction.setType(attributesData.getAttrValue());
                                        transaction.setStatus(0);
                                    } else {
                                        transaction.setReferenceKey(environment.getRequiredProperty(POSTPAID_ERROR_MESSAGE));
                                    }
                                } else if (DISABLE_BRANDING.equalsIgnoreCase(enabled)) {
                                    if (ESB_TM_PREPAID.equalsIgnoreCase(attributesData.getAttrValue())) {
                                        logger.info("=========== TM VALUE IF =========== " + attributesData.getAttrValue());
                                        if (unifiedResourceDetailsInfo.getStatusInfo() != null) {
                                            StatusInfo statusInfo = unifiedResourceDetailsInfo.getStatusInfo();
                                            if (!"active".equalsIgnoreCase(statusInfo.getStatus())) {
                                                transaction.setReferenceKey(environment.getRequiredProperty(GENERIC_ESB_ERROR));
                                            } else {
                                                transaction.setType(attributesData.getAttrValue());
                                                transaction.setStatus(0);
                                            }
                                        }
                                    } else {
                                        if (attributesData.getAttrValue().matches("(?s).*\\bPrepaid\\b.*")) {
                                            transaction.setReferenceKey(environment.getRequiredProperty(PREPAID_ERROR_MESSAGE));
                                        } else {
                                            transaction.setReferenceKey(environment.getRequiredProperty(POSTPAID_ERROR_MESSAGE));
                                        }
                                    }
                                }

                            }
                        }
                    }
                } else {
                    transaction.setReferenceKey(environment.getRequiredProperty(GENERIC_ESB_ERROR));
                }
            }

            logger.info("Get transaction value : " + transaction.getType());
        } catch (Exception e) {
            logger.error("Unable to continue process to ESB : ", e);
            throw new EsbClientException(e.getCause());
        }

        return (T) transaction;
    }

}
