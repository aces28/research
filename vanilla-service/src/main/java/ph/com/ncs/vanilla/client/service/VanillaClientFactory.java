package ph.com.ncs.vanilla.client.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ph.com.ncs.vanilla.base.client.VanillaDefaultClient;

/**
 * Created by ace on 5/26/16.
 * <p>
 * Add client for each web service integration
 */
@Component
public class VanillaClientFactory extends VanillaDefaultClient {

    @Autowired
    private VanillaEsbClientService esbClientService;

    @Autowired
    private VanillaNFClientService nfClientService;

    @Autowired
    private VanillaRavenClientService ravenClientService;

    /**
     * @return {@link VanillaEsbClientService}
     */
    public VanillaEsbClientService getEsbService() throws InstantiationException, IllegalAccessException {
        return esbClientService;
    }

    /**
     * @return {@link VanillaRavenClientService}
     */
    public VanillaRavenClientService getRavenService() throws InstantiationException, IllegalAccessException {
        return ravenClientService;
    }


    /**
     * @return {@link VanillaNFClientService}
     */
    public VanillaNFClientService getNFService() throws InstantiationException, IllegalAccessException {
        return nfClientService;
    }
}
