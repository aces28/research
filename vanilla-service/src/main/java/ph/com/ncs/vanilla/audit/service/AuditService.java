package ph.com.ncs.vanilla.audit.service;

import java.sql.Timestamp;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.Page;

import ph.com.ncs.vanilla.audit.domain.AuditAccessTransaction;
import ph.com.ncs.vanilla.base.service.BaseService;

/**
 * Created by edjohna on 7/17/2016.
 */
public interface AuditService extends BaseService {
	
	/**
	 * Methods For User Access History
	 */
    Page<AuditAccessTransaction> listUserAccessHistory(Integer pageNumber);

    Page<AuditAccessTransaction> filterByDateUserAccessHistory(Timestamp from, Timestamp to, Integer pageNumber);

    Page<AuditAccessTransaction> filterByModuleUserAccessHistory(int moduleId, Integer pageNumber);    
    
    Page<AuditAccessTransaction> searchUserAccessHistory(String keyword, Integer pageNumber);

    List<AuditAccessTransaction> extractUserAccessHistory(String keyword);
    
    List<AuditAccessTransaction> extractUserAccessHistoryByDate(Timestamp from, Timestamp to);

    List<AuditAccessTransaction> extractUserAccessHistoryByModule(int moduleId);
    
	/**
	 * Methods For Transaction Logs
	 */
    Page<AuditAccessTransaction> listTransactionLogs(Integer pageNumber);
    
    Page<AuditAccessTransaction> filterByDateTransactionLogs(Timestamp from, Timestamp to, Integer pageNumber);
    
    Page<AuditAccessTransaction> filterByModuleTransactionLogs(int moduleId, Integer pageNumber);
    
    Page<AuditAccessTransaction> searchTransactionLogs(String keyword, Integer pageNumber);

    List<AuditAccessTransaction> extractTransactionLogs(String keyword);
    
    List<AuditAccessTransaction> extractTransactionByDate(Timestamp from, Timestamp to);
    
    List<AuditAccessTransaction> extractTransactionLogsByModule(int moduleId);
    
    void saveUserAccessAuditLogs(String userName, int module, HttpServletRequest request);
    
    /**
     * For LAR
     */
    List<AuditAccessTransaction> generateLAR();
}
