package ph.com.ncs.vanilla.account.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ph.com.ncs.vanilla.base.domain.Model;
import ph.com.ncs.vanilla.account.dao.AccountRepository;
import ph.com.ncs.vanilla.subscriber.domain.Account;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public <T extends Model> T save(T data) {
        return (T) accountRepository.save((Account) data);
    }

    @Override
    public <T extends Model> T get(T data) {
        return (T) accountRepository.findOne(((Account) data).getId());
    }

    @Override
    public <T> T save(Iterable iterable) {
        return (T) accountRepository.save(iterable);
    }

    @Override
    public boolean msisdnLookup(String msisdn) {
        if(accountRepository.msisdnLookup(msisdn) >= 1 ){
            return true;
        }else{
            return false;
        }
    }
}
