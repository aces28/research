package ph.com.ncs.vanilla.resource.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ph.com.ncs.vanilla.base.domain.Model;
import ph.com.ncs.vanilla.resource.dao.ResourceRepository;
import ph.com.ncs.vanilla.resource.domain.Resource;

@Service
public class ResourceServiceImpl implements ResourceService {

    @Autowired
    private ResourceRepository resourceRepository;

    @Override
    public <T extends Model> T save(T data) {
        return (T) resourceRepository.save((Resource) data);
    }

    @Override
    public <T extends Model> T get(T data) {
        return (T) resourceRepository.findOne(((Resource) data).getId());
    }

    @Override
    public <T> T save(Iterable iterable) {
        return (T) resourceRepository.save(iterable);
    }

}
