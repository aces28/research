package ph.com.ncs.vanilla.account.service;

import ph.com.ncs.vanilla.base.service.BaseService;

/**
 * @author allango 7/13/2016
 */
public interface AccountService extends BaseService {

    boolean msisdnLookup(String msisdn);
}
