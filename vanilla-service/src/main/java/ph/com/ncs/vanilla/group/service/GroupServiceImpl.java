package ph.com.ncs.vanilla.group.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import ph.com.ncs.vanilla.base.domain.Model;
import ph.com.ncs.vanilla.group.dao.GroupRepository;
import ph.com.ncs.vanilla.group.domain.Group;

/**
 * Created by edjohna on 6/29/2016.
 */
@Service
public class GroupServiceImpl implements GroupService {

    private static final int PAGE_SIZE = 10;

    @Autowired
    private GroupRepository groupRepository;

    @Override
    public Page<Group> listGroup(Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE);
        return groupRepository.listGroup(request);
    }

    @Override
    public List<Group> listAvailableGroup() {
        return groupRepository.listAvailableGroup();
    }

    @Override
    public Page<Group> searchGroup(String keyword, Integer pageNumber) {
        PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE);
        return groupRepository.searchGroup(keyword, request);
    }

    @Override
    public List<Group> extractGroup(String keyword) {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize);
        Page<Group> groupPage = groupRepository.extractGroup(keyword, request);
        List<Group> groupList = groupPage.getContent();
        return groupList;
    }

    @Override
    public List<Group> extractAllGroup() {
        int pageSize = 5000;
        PageRequest request = new PageRequest(1 - 1, pageSize);
        Page<Group> groupPage = groupRepository.listGroup(request);
        List<Group> groupList = groupPage.getContent();
        return groupList;
    }

    @Override
    public List<Group> findAll() {
        return groupRepository.findAll();
    }

    @Override
    public Group findGroupByName(String groupName) {
        return groupRepository.findGroupByName(groupName);
    }

    @Override
    public <T extends Model> T save(T data) {
        return (T) groupRepository.save((Group) data);
    }

    @Override
    public <T extends Model> T get(T data) {
        return (T) groupRepository.findOne(((Group) data).getId());
    }

    @Override
    public <T> T save(Iterable iterable) {
        return (T) groupRepository.save(iterable);
    }
}