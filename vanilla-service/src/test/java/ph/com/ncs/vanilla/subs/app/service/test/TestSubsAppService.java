package ph.com.ncs.vanilla.subs.app.service.test;

import java.sql.Date;
import java.sql.Timestamp;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;
import ph.com.ncs.vanilla.application.domain.SubsApplication;
import ph.com.ncs.vanilla.commons.constants.VanillaConstants;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;
import ph.com.ncs.vanilla.subscriber.service.SubscriberAppService;
import ph.com.ncs.vanilla.subscriber.domain.Subscriber;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = VanillaContextConfig.class)
@ComponentScan(basePackages = {"ph.com.ncs.vanilla.*.*.service"})
public class TestSubsAppService {

    private static final Logger logger = LoggerFactory.getLogger(TestSubsAppService.class);

    private static SubsApplication subsApplication;
    private static SubsApplication subsApplicationResult;

    @Autowired
    private SubscriberAppService subscriberAppService;

    @BeforeClass
    public static void beforeTest() {
        subsApplication = new SubsApplication();

        Subscriber subscriber = new Subscriber();
        subscriber.setId(1);

        subsApplication.setSubscriber(subscriber);
        subsApplication.setSubscriberMsisdn("09151234567");
        subsApplication.setAccountType("Account");
        subsApplication.setUserEmail("test@email.com");
        subsApplication.setAttachment("Attachment");
        subsApplication.setTermsConditions("TermsCondition");
        subsApplication.setApprovalDate(new Date(VanillaDateUtils.longCurrentDate()));
        subsApplication.setStatus(1);
        subsApplication.setCreatedBy(VanillaConstants.SYSTEM_USER);
        subsApplication.setCreatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        subsApplication.setUpdatedBy(VanillaConstants.SYSTEM_USER);
    }

    @Test
    public void testInsert() {
        subsApplicationResult = subscriberAppService.save(subsApplication);
        logger.info("SubsAppService Test Insert::::::::::" + subsApplicationResult.toString());
        Assert.assertNotNull(subsApplicationResult);
    }

}
