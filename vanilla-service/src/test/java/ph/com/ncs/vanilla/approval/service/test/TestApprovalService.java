package ph.com.ncs.vanilla.approval.service.test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ph.com.ncs.vanilla.approval.service.ApprovalService;
import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;

/**
 * Created by edjohna on 6/28/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = VanillaContextConfig.class)
@ComponentScan(basePackages = {"ph.com.ncs.vanilla.*.service"})
public class TestApprovalService {

    @Autowired
    private ApprovalService approvalService;

//    @Test
//    public void getForApproval(){
//        Page<SubsApplication> page = approvalService.listForApprovalApprover(1, 1);
//
//        for (SubsApplication subs : page){
//            System.out.println("SUBS FOR APPROVAL:: " + subs.toString());
//        }
//
//        Assert.assertNotNull(page);
//    }
//
//    @Test
//    public void getApproved(){
//        Page<SubsApplication> page = approvalService.listApprovedApprover(1, 1);
//
//        for (SubsApplication subs : page){
//            System.out.println("SUBS APPROVED:: " + subs.toString());
//        }
//
//        Assert.assertNotNull(page);
//
//    }
//
//    @Test
//    public void getDisapproved(){
//        Page<SubsApplication> page = approvalService.listDisapprovedApprover(1,1);
//
//        for (SubsApplication subs : page){
//            System.out.println("SUBS DISAPPROVED:: " + subs.toString());
//        }
//
//        Assert.assertNotNull(page);
//
//    }
}
