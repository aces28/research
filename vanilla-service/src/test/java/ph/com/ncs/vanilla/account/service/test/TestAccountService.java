package ph.com.ncs.vanilla.account.service.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ph.com.ncs.vanilla.account.service.AccountService;
import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;

/**
 * Created by edjohna on 7/14/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = VanillaContextConfig.class)
@ComponentScan(basePackages = {"ph.com.ncs.vanilla.*.service"})

public class TestAccountService {

    @Autowired
    private AccountService accountService;

    @Test
    public void testMsisdLookup(){
        boolean isExist = accountService.msisdnLookup("09271030857");
        System.out.print(isExist);
        Assert.assertTrue(isExist);
    }
}
