package ph.com.ncs.vanilla.client.service.test;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ph.com.ncs.vanilla.client.service.VanillaClientFactory;
//import ph.com.ncs.vanilla.client.service.VanillaNFClientService;
import ph.com.ncs.vanilla.transaction.domain.Transaction;

import java.sql.Timestamp;

/**
 * Created by ace on 6/13/16.
 */
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = VanillaContextConfig.class)
//@ComponentScan(basePackages = {"ph.com.ncs.vanilla.*.service"})
public class TestNfClientService {

    private static final Logger logger = LoggerFactory.getLogger(TestNfClientService.class);
//
//    private static Transaction transaction;
//    private VanillaClientFactory clientFactory;
//
//    private VanillaNFClientService clientService;
//
//    private static String[] phoneNumberPrefix = {"917", "977", "905", "906", "915", "916", "925", "926", "927", "935", "936", "937", "996", "997", "0975"};
//
//    private static final String WSDL_NF_URL = "http://10.225.38.147:8080/vanilla-nf-client/services/bustransaction?wsdl";
//    private static final String NF_USER = "vanilla_user";
//    private static final String NF_PASSWORD = "vanilla_user";
//    private static final String NF_MOBTEL = "vanilla_mobtel";
//    private static final String NF_PLAN_ID = "vanilla_plan_id";
//    private static final String NF_PLAN_VAL = "101";
//    private static final String NF_OP_TYPE = "Vanilla";
//    private static final String NF_OP_ID = "Register Subscriber";
//
//    @BeforeClass
//    public static void beforeTest() {
//        transaction = new Transaction();
//
//        transaction.setMsisdn("639063241954");
//        transaction.setCreatedBy("vanilla_user");
//        transaction.setCreatedDate(new Timestamp(System.currentTimeMillis()));
//        transaction.setCode(0);
//        transaction.setStatus(1);
//        transaction.setType("LTP");
//        transaction.setUpdatedDate(new Timestamp(System.currentTimeMillis()));
//        transaction.setUpdatedBy("vanilla_user");
//    }
//
//    @Test
//    public void testNfFactoryTypes() throws IllegalAccessException, InstantiationException {
//
////        ObjectFactory objectFactory = new ObjectFactory();
////
////        ParamListType paramListType = objectFactory.createParamListType();
////        KeyValuePairType mobTelPairType = objectFactory.createKeyValuePairType();
////        mobTelPairType.setName(NF_MOBTEL);
////        mobTelPairType.setValue(transaction.getMsisdn());
////        KeyValuePairType planIdPairType = objectFactory.createKeyValuePairType();
////        planIdPairType.setName(NF_PLAN_ID);
////        planIdPairType.setValue(NF_PLAN_VAL);
////
////        List<KeyValuePairType> keyValuePairTypes = new ArrayList<>();
////        keyValuePairTypes.add(mobTelPairType);
////        keyValuePairTypes.add(planIdPairType);
////
////        paramListType.setParam(keyValuePairTypes);
////
////        List<KeyValuePairType> pairTypes = paramListType.getParam();
////        logger.info("List size : " + pairTypes.size() + " : name : " + pairTypes.get(0).getName() + " : value : " + pairTypes.get(0).getValue());
////
////
////        if (transaction.getMsisdn().startsWith("63")) {
////            String prefix = transaction.getMsisdn().substring(2, 5);
////            if (Arrays.toString(phoneNumberPrefix).contains(prefix)) {
////                logger.info("check number validity : " + prefix + " : " + transaction.getMsisdn().indexOf("63"));
////            }
////        }
//    }
//
//    @Test
//    public void testNfClientService() throws IllegalAccessException, InstantiationException {
//        clientFactory = new VanillaClientFactory();
////        clientService = clientFactory.getNFService();
//        String result = clientService.getResult(transaction);
//        logger.info("NF service result : " + result);
//        Assert.assertEquals("SUCCESS", result);
//    }

}
