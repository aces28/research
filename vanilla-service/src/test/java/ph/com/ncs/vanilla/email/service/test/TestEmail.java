package ph.com.ncs.vanilla.email.service.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;
import ph.com.ncs.vanilla.email.domain.Email;
import ph.com.ncs.vanilla.user.domain.User;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

/**
 * Created by ace on 7/6/16.
 */
public class TestEmail {

    private static final Logger logger = LoggerFactory.getLogger(TestEmail.class);

    @Autowired
    private Email email;
    @Autowired
    private ResourceLoader resourceLoader;

    @Test
    public void testEmailDetails() {
        Assert.assertEquals("email-smtp.us-west-2.amazonaws.com", email.getHost());
    }

    @Test
    public void testEmailBody() throws IOException {
        Resource resource = resourceLoader.getResource("classpath:termsncondition.txt");
        BufferedReader bufferedReader = null;
        StringBuffer stringBuffer = new StringBuffer();
        try (InputStream inputStream = resource.getInputStream()) {
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String readLine;
            while ((readLine = bufferedReader.readLine()) != null) {
                stringBuffer.append(readLine).append("\n");
            }
            logger.info(stringBuffer.toString());
        } catch (IOException e) {
            logger.error("Unale to read file : ", e);
        } finally {
            bufferedReader.close();
        }
    }


    @Test
    public void sendEmail() throws MessagingException {
        Properties properties = System.getProperties();
        properties.put("mail.transport.protocol", "smtps");
        properties.put("mail.smtps.host", email.getHost());
        properties.put("mail.smtps.port", email.getPort());

        properties.put("mail.smtps.auth", "true");
        properties.put("mail.smtps.starttls.enable", "true");
        properties.put("mail.smtps.starttls.required", "true");

        Session session = Session.getDefaultInstance(properties);
        session.setDebug(true);

        MimeMessage msg = new MimeMessage(session);
        Transport transport = null;
        try {

            if (email != null) {
                email.setTo("aces28@gmail.com");
            }

            msg.setFrom(new InternetAddress(email.getFrom()));
            msg.setRecipient(Message.RecipientType.TO, new InternetAddress(email.getTo()));
            msg.setSubject(email.getSubject());
            msg.setContent(email.getBody(), "text/plain");
            transport = session.getTransport();

            logger.info("AWS SES end-point : " + email.getHost() + " : " + email.getSmtpUsername() + " : " + email.getSmtpPassword());
            transport.connect(email.getHost(), email.getSmtpUsername(), email.getSmtpPassword());

            transport.sendMessage(msg, msg.getAllRecipients());
            logger.info("Email sent!");
        } catch (MessagingException e) {
            logger.error("Unable to send e-mail : ", e);
        } finally {
            if (transport != null) {
                transport.close();
            }
        }

    }
}
