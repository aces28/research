package ph.com.ncs.vanilla.transaction.service.test;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;
import ph.com.ncs.vanilla.commons.constants.VanillaConstants;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;
import ph.com.ncs.vanilla.transaction.domain.Transaction;
import ph.com.ncs.vanilla.transaction.service.TransactionService;

import java.sql.Timestamp;

/**
 * Created by ace on 5/20/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = VanillaContextConfig.class)
@ComponentScan(basePackages = {"ph.com.ncs.vanilla.*.service"})
public class TestTransactionService {

    private static final Logger logger = LoggerFactory.getLogger(TestTransactionService.class);

    private static Transaction transaction;
    private static Transaction transactionResult;

    @Autowired
    private TransactionService transactionService;


    @BeforeClass
    public static void beforeTest() {
        transaction = new Transaction();

        transaction.setMsisdn("09753241954");
        transaction.setCreatedBy(VanillaConstants.SYSTEM_USER);
        transaction.setCreatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        transaction.setCode(0);
        transaction.setStatus(1);
        transaction.setType("GH");
        transaction.setUpdatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        transaction.setUpdatedBy(VanillaConstants.SYSTEM_USER);
    }

    @Test
    public void testInsert() {
        transactionResult = transactionService.save(transaction);
        logger.info("transaction id : " + transactionResult.getId());
        Assert.assertNotNull(transactionResult);
    }
}
