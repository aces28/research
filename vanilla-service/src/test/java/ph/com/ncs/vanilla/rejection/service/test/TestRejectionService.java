package ph.com.ncs.vanilla.rejection.service.test;

import java.sql.Date;
import java.sql.Timestamp;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;
import ph.com.ncs.vanilla.rejection.domain.Rejection;
import ph.com.ncs.vanilla.rejection.service.RejectionService;
import ph.com.ncs.vanilla.application.domain.SubsApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = VanillaContextConfig.class)
@ComponentScan(basePackages = {"ph.com.ncs.vanilla.*.service"})
public class TestRejectionService {

    private static final Logger logger = LoggerFactory.getLogger(TestRejectionService.class);

    @Autowired
    private RejectionService rejectionService;

    private static Rejection rejection;
    private static Rejection rejectionResult;

    @BeforeClass
    public static void beforeTest() {
        rejection = new Rejection();

        SubsApplication subsApplication = new SubsApplication();
        subsApplication.setId(1);
        rejection.setReasonName("Requirements");
        rejection.setReasonDescription("Invalid Requirements");
        rejection.setCreatedBy("vanilla_user");
        rejection.setCreatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        rejection.setUpdatedBy("vanilla user");
    }

    @Test
    public void testInsert() {
        rejectionResult = rejectionService.save(rejection);
        logger.info("Test Insert Result :::::::: " + rejectionResult.toString());
        Assert.assertNotNull(rejectionResult);
    }

}
