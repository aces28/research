package ph.com.ncs.vanilla.subscriber.service.test;

import java.sql.Date;
import java.sql.Timestamp;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;
import ph.com.ncs.vanilla.subscriber.domain.Subscriber;
import ph.com.ncs.vanilla.subscriber.service.SubscriberService;

/**
 * @author edjohna 5/24/2016
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = VanillaContextConfig.class)
@ComponentScan(basePackages = {"ph.com.ncs.vanilla.*.service"})
public class TestSubscriberService {

    private static final Logger logger = LoggerFactory.getLogger(TestSubscriberService.class);

    private static Subscriber subscriber;
    private static Subscriber subscriberResult;

    @Autowired
    private SubscriberService subscriberService;


    @BeforeClass
    public static void beforeTest() {
        subscriber = new Subscriber();

        subscriber.setFirstName("Ferdinand");
        subscriber.setLastName("Marcos");
        subscriber.setMiddleName("MartialLaw");
        subscriber.setMothersName("Imelda");
        subscriber.setGender("Male");
        subscriber.setAddress("Ilocos");
        subscriber.setBirthday(new Date(VanillaDateUtils.longCurrentDate()));
        logger.info("Birthday : " + subscriber.getBirthday());
        subscriber.setCreatedBy("ferdinand");
        subscriber.setCreatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        subscriber.setUpdatedBy("ferdinand");

    }

    @Test
    public void testInsert() {
        subscriberResult = subscriberService.save(subscriber);
        logger.info("Subscriber ID: " + subscriber.getBirthday());
        Assert.assertNotNull(subscriberResult);
    }


}
