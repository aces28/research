package ph.com.ncs.vanilla.client.service.test;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;
import ph.com.ncs.vanilla.client.service.EsbClientException;
import ph.com.ncs.vanilla.client.service.VanillaClientFactory;
import ph.com.ncs.vanilla.client.service.VanillaEsbClientService;
import ph.com.ncs.vanilla.commons.constants.VanillaConstants;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;
import ph.com.ncs.vanilla.transaction.domain.Transaction;

import java.sql.Timestamp;

/**
 * Created by ace on 5/25/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = VanillaContextConfig.class)
@ComponentScan(basePackages = {"ph.com.ncs.vanilla.*.service"})
public class TestWsClientService {

    private static final Logger logger = LoggerFactory.getLogger(TestWsClientService.class);

    @Autowired
    private VanillaClientFactory clientFactory;
    private VanillaEsbClientService clientService;

    private static Transaction transaction;
    private static Transaction transactionRes;


    @BeforeClass
    public static void beforeTest() {
        transaction = new Transaction();

        transaction.setMsisdn("09753241954");
        transaction.setCreatedBy(VanillaConstants.SYSTEM_USER);
        transaction.setCreatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        transaction.setCode(0);
        transaction.setStatus(1);
        transaction.setType("LTP");
        transaction.setUpdatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        transaction.setUpdatedBy(VanillaConstants.SYSTEM_USER);
    }

    @Test
    public void testClientFactory() {
        Assert.assertNotNull(clientFactory);
    }

    @Test
    public void testWsClient() throws InstantiationException, IllegalAccessException, EsbClientException {
//        clientFactory = new VanillaClientFactory();
        clientService = clientFactory.getEsbService();
        transaction = clientService.getResult(transaction);
        logger.info("transaction type : " + transaction.getType());
        Assert.assertNotNull(clientService);
    }

    //    @Test
    public void testEsbClient() {
        Assert.assertEquals("09753241954", "09753241954");
    }

    //    @Test
//    public void testClientRequest() throws InstantiationException, IllegalAccessException {
//        int value = vanillaClientService.processRequest("09063241954");
//        Assert.assertEquals(1, value);
//    }
}
