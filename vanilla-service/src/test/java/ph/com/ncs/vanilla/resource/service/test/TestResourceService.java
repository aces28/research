package ph.com.ncs.vanilla.resource.service.test;

import java.sql.Date;
import java.sql.Timestamp;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;
import ph.com.ncs.vanilla.commons.constants.VanillaConstants;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;
import ph.com.ncs.vanilla.resource.domain.Resource;
import ph.com.ncs.vanilla.resource.service.ResourceService;
import ph.com.ncs.vanilla.application.domain.SubsApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = VanillaContextConfig.class)
@ComponentScan(basePackages = {"ph.com.ncs.vanilla.*.service"})
public class TestResourceService {

    private static final Logger logger = LoggerFactory.getLogger(TestResourceService.class);

    @Autowired
    private ResourceService resourceService;

    private static Resource resource;
    private static Resource resourceResult;

    @BeforeClass
    public static void beforeTest() {
        resource = new Resource();


        SubsApplication subsApplication = new SubsApplication();
        subsApplication.setId(1);

        resource.setSubsApplication(subsApplication);
        resource.setResourceUrl("http://blahblah.com");
        resource.setCreatedBy(VanillaConstants.SYSTEM_USER);
        resource.setCreatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        resource.setUpdatedBy(VanillaConstants.SYSTEM_USER);
    }

    @Test
    public void testInsert() {
        resourceResult = resourceService.save(resource);
        logger.info("Resource Insert:::::::::::" + resourceResult.toString());
        Assert.assertNotNull(resourceResult);
    }
}
