package ph.com.ncs.vanilla.batch;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Interval;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import ph.com.ncs.vanilla.base.security.service.LdapQueryService;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;
import ph.com.ncs.vanilla.user.domain.User;
import ph.com.ncs.vanilla.user.service.UserService;

@Component
public class ProcessExpiredAccounts implements CommandLineRunner {
	 
	 private static final DateTime LDAP_START_DATE  = new DateTime(1601, 1, 1, 0, 0, DateTimeZone.UTC);
	 
	 private static final Logger LOGGER = LoggerFactory.getLogger(ProcessExpiredAccounts.class);
	 
	 @Autowired
	 private UserService userService;
	 
	 @Autowired
	 private LdapQueryService ldapQueryService;
	 
	 @Autowired
	 private ConfigurableApplicationContext context;
	 
	 @Autowired
	 private Environment resource;
	 
	 @Override
	 public void run(String... args) throws Exception {
		 
		 Timestamp currentDate = new Timestamp(VanillaDateUtils.longCurrentDate());
		 Interval interval = new Interval(LDAP_START_DATE, DateTime.now());
		 String expiryDate = String.valueOf(interval.toDurationMillis() * 10000);
 		
		 List<User> userListFromLdap = ldapQueryService.getExpiredLdapUsers(expiryDate);
		 List<User> userListToVanillaDB =  Collections.synchronizedList(new ArrayList<User>());
		 
		 if (userListFromLdap.isEmpty())
			 LOGGER.info("ProcessExpiredAccounts: No expired accounts retrieved from LDAP!");
		 else {
			 for (User resultFromLDAP : userListFromLdap) {
				 if (userService.findByUserName(resultFromLDAP.getUserName()) != null) {
					 resultFromLDAP.setStatus(Integer.parseInt(resource.getRequiredProperty("batch.ustatus")));
					 resultFromLDAP.setIsLocked(Integer.parseInt(resource.getRequiredProperty("batch.islocked")));
					 resultFromLDAP.setUpdatedBy(resource.getRequiredProperty("batch.updatedby"));
					 resultFromLDAP.setUpdatedDate(currentDate);
					 userListToVanillaDB.add(resultFromLDAP);
				 }
			 }
			 
			 if (userListToVanillaDB.size() > 0)
				 userService.save(userListToVanillaDB);
			 
			 LOGGER.info("ProcessExpiredAccounts: " + userListToVanillaDB.size() + " expired accounts updated!");
		 }
		 
		 context.close();
	}
}
