package ph.com.ncs.vanilla.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.test.ImportAutoConfiguration;
import org.springframework.boot.context.web.SpringBootServletInitializer;

import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;

/**
 * Hello world!
 */
@SpringBootApplication
@ImportAutoConfiguration(VanillaContextConfig.class)
public class ExpiredAccountsBatchJob extends SpringBootServletInitializer {
	 
	public static void main(String[] args) {
		SpringApplication.run(ExpiredAccountsBatchJob.class, args);
	}
}
