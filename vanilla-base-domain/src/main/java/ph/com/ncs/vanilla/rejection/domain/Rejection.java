package ph.com.ncs.vanilla.rejection.domain;

import org.hibernate.validator.constraints.NotBlank;
import ph.com.ncs.vanilla.application.domain.SubsApplication;
import ph.com.ncs.vanilla.base.domain.Model;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(name = "TBL_REJECTION")
public class Rejection extends Model {

    private static final long serialVersionUID = 2584796044410142507L;

    private String reasonName;
    private String reasonDescription;
    private int applicationId;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "REJECTION_ID")
    @Override
    public Integer getId() {
        return super.getId();
    }

    @Override
    public void setId(Integer id) {
        super.setId(id);
    }

    @Column(name = "APPLICATION_ID")
    public int getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(int applicationId) {
        this.applicationId = applicationId;
    }

    @NotBlank
    @Column(name = "REASON_NAME")
    public String getReasonName() {
        return reasonName;
    }

    public void setReasonName(String reasonName) {
        this.reasonName = reasonName;
    }

    @Column(name = "REASON_DESC")
    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

    @Override
    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return super.getCreatedBy();
    }

    @Override
    public void setCreatedBy(String createdBy) {
        super.setCreatedBy(createdBy);
    }

    @Override
    @Column(name = "CREATED_DATE")
    public Timestamp getCreatedDate() {
        return super.getCreatedDate();
    }

    @Override
    public void setCreatedDate(Timestamp createdDate) {
        super.setCreatedDate(createdDate);
    }

    @Override
    @Column(name = "UPDATED_BY")
    public String getUpdatedBy() {
        return super.getUpdatedBy();
    }

    @Override
    public void setUpdatedBy(String updatedBy) {
        super.setUpdatedBy(updatedBy);
    }

    @Override
    @Column(name = "UPDATED_DATE")
    public Timestamp getUpdatedDate() {
        return super.getUpdatedDate();
    }

    @Override
    public void setUpdatedDate(Timestamp updatedDate) {
        super.setUpdatedDate(updatedDate);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((reasonDescription == null) ? 0 : reasonDescription.hashCode());
        result = prime * result + ((reasonName == null) ? 0 : reasonName.hashCode());
//        result = prime * result + ((subsApplication == null) ? 0 : subsApplication.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        Rejection other = (Rejection) obj;
        if (reasonDescription == null) {
            if (other.reasonDescription != null) return false;
        } else if (!reasonDescription.equals(other.reasonDescription)) return false;
        if (reasonName == null) {
            if (other.reasonName != null) return false;
        } else if (!reasonName.equals(other.reasonName)) return false;
//        if (subsApplication == null) {
//            if (other.subsApplication != null) return false;
//        } else if (!subsApplication.equals(other.subsApplication)) return false;
        return true;
    }

    @Override
    public String toString() {
        return "Rejection [getId()=" + getId() + ", getReasonName()=" + getReasonName() + ", getReasonDescription()=" + getReasonDescription() + ", getCreatedBy()=" + getCreatedBy() + ", getCreatedDate()=" + getCreatedDate() + ", getUpdatedBy()=" + getUpdatedBy() + ", getUpdatedDate()=" + getUpdatedDate() + "]";
    }


}
