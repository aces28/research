package ph.com.ncs.vanilla.audit.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ph.com.ncs.vanilla.base.domain.Model;

@Entity
@Table(name = "TBL_MODULE")
public class AuditModule extends Model {
	
	private static final long serialVersionUID = 2584796044410142507L;
	
    private int moduleId;
    private String moduleName;
    private String moduleDescription;
    private int auditType;
    
    @Id
    @Column(name = "MODULE_ID")	
	public int getModuleId() {
		return moduleId;
	}
    
	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}
	
    @Column(name = "MODULE_NAME")
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	
    @Column(name = "MODULE_DESCRIPTION")
	public String getModuleDescription() {
		return moduleDescription;
	}
	public void setModuleDescription(String moduleDescription) {
		this.moduleDescription = moduleDescription;
	}
	
	@Column(name = "AUDIT_TYPE")
	public int getAuditType() {
		return auditType;
	}

	public void setAuditType(int auditType) {
		this.auditType = auditType;
	}	
}
