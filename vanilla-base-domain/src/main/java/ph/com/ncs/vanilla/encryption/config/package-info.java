/**
 * Created by edjohna on 8/4/2016.
 */

package ph.com.ncs.vanilla.encryption.config;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.jasypt.hibernate4.type.EncryptedStringType;