package ph.com.ncs.vanilla.reports.domain;

/**
 * Created by edjohna on 7/6/2016.
 */
public class ApproverList {

    private int userId;
    private String approverName;
    private Long completed;
    private Long onGoing;


    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getApproverName() {
        return approverName;
    }

    public void setApproverName(String approverName) {
        this.approverName = approverName;
    }

    public Long getCompleted() {
        return completed;
    }

    public void setCompleted(Long completed) {
        this.completed = completed;
    }

    public Long getOnGoing() {
        return onGoing;
    }

    public void setOnGoing(Long onGoing) {
        this.onGoing = onGoing;
    }
}
