package ph.com.ncs.vanilla.base.domain;

/**
 * Created by ace on 5/19/16.
 */
public class WithStatusOption extends Model {

    private static final long serialVersionUID = -1930625129355768436L;

    private Status status;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
