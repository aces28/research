package ph.com.ncs.vanilla.email.domain;

/**
 * Created by ace on 6/19/16.
 */
public class Email {

    private String to;

    private String from;

    private String subject;

    private String body;

    private String smtpUsername;

    private String smtpPassword;

    private String host;

    private int port;

    private String protocol;

    private String tlsEnabled;

    private String tlsRequired;

    private String authEnabled;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSmtpUsername() {
        return smtpUsername;
    }

    public void setSmtpUsername(String smtpUsername) {
        this.smtpUsername = smtpUsername;
    }

    public String getSmtpPassword() {
        return smtpPassword;
    }

    public void setSmtpPassword(String smtpPassword) {
        this.smtpPassword = smtpPassword;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }


    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getTlsEnabled() {
        return tlsEnabled;
    }

    public void setTlsEnabled(String tlsEnabled) {
        this.tlsEnabled = tlsEnabled;
    }

    public String getTlsRequired() {
        return tlsRequired;
    }

    public void setTlsRequired(String tlsRequired) {
        this.tlsRequired = tlsRequired;
    }

    public String getAuthEnabled() {
        return authEnabled;
    }

    public void setAuthEnabled(String authEnabled) {
        this.authEnabled = authEnabled;
    }
}


