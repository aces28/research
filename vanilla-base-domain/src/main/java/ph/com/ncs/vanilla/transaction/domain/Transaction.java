package ph.com.ncs.vanilla.transaction.domain;

import org.hibernate.validator.constraints.NotBlank;
import ph.com.ncs.vanilla.base.domain.Status;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by ace on 5/19/16.
 */
@Entity
@Table(name = "TBL_CODE_TXN")
public class Transaction extends Status {

    private static final long serialVersionUID = 2584796044410142507L;

    private String msisdn;
    private String type;

    private int mode;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "TXN_ID")
    @Override
    public Integer getId() {
        return super.getId();
    }

    @Override
    public void setId(Integer id) {
        super.setId(id);
    }


    @NotBlank
    @Column(name = "SUBSCRIBER_MSISDN")
    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    @Column(name = "SUBSCRIBER_TYPE")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Column(name = "CREATED_DATE")
    @Override
    public Timestamp getCreatedDate() {
        return super.getCreatedDate();
    }

    @Override
    public void setCreatedBy(String createdBy) {
        super.setCreatedBy(createdBy);
    }

    @Column(name = "CREATED_BY")
    @Override
    public String getCreatedBy() {
        return super.getCreatedBy();
    }

    @Override
    public void setCreatedDate(Timestamp createdDate) {
        super.setCreatedDate(createdDate);
    }

    @Column(name = "UPDATED_BY")
    @Override
    public String getUpdatedBy() {
        return super.getUpdatedBy();
    }

    @Override
    public void setUpdatedBy(String updatedBy) {
        super.setUpdatedBy(updatedBy);
    }

    @Column(name = "UPDATED_DATE")
    @Override
    public Timestamp getUpdatedDate() {
        return super.getUpdatedDate();
    }

    @Override
    public void setUpdatedDate(Timestamp updatedDate) {
        super.setUpdatedDate(updatedDate);
    }

    @Override
    public void setCode(Integer code) {
        super.setCode(code);
    }

    @Override
    public Integer getCode() {
        return super.getCode();
    }

    @Override
    public Integer getStatus() {
        return super.getStatus();
    }

    @Override
    public void setStatus(Integer status) {
        super.setStatus(status);
    }

    @Transient
    @Column(name = "MODE")
    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                ", msisdn='" + msisdn + '\'' +
                ", type='" + type + '\'' +
                ", status='" + getStatus() + '\'' + '}';
    }
}
