package ph.com.ncs.vanilla.base.domain;


import javax.persistence.Id;

/**
 * Created by ace on 5/18/16.
 * <p>
 * Added equals and hashCode edjohna 5/24/2016
 */
public class Model extends Record implements Reference {

    public static final int DELETED = 1;
    public static final int NOT_DELETED = 0;
    public static final int ENABLED = 1;

    @Id
    private Integer id;
    private int version;
    private String referenceKey;

    public Integer getId() {
        return id;
    }

    public String getReferenceKey() {
        return referenceKey;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setReferenceKey(String referenceKey) {
        this.referenceKey = referenceKey;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((referenceKey == null) ? 0 : referenceKey.hashCode());
        result = prime * result + version;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        Model other = (Model) obj;
        if (id == null) {
            if (other.id != null) return false;
        } else if (!id.equals(other.id)) return false;
        if (referenceKey == null) {
            if (other.referenceKey != null) return false;
        } else if (!referenceKey.equals(other.referenceKey)) return false;
        if (version != other.version) return false;
        return true;
    }


}
