package ph.com.ncs.vanilla.resource.domain;

import org.hibernate.validator.constraints.NotBlank;
import ph.com.ncs.vanilla.application.domain.SubsApplication;
import ph.com.ncs.vanilla.base.domain.Model;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * @author edjohna 5/24/2016
 */
@Entity
@Table(name = "TBL_RESOURCE")
public class Resource extends Model {

    private static final long serialVersionUID = 2584796044410142507L;

    private SubsApplication subsApplication;
    private String resourceUrl;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "RESOURCE_ID")
    @Override
    public Integer getId() {
        return super.getId();
    }

    @ManyToOne
    @JoinColumn(name = "APPLICATION_ID")
    public SubsApplication getSubsApplication() {
        return subsApplication;
    }

    public void setSubsApplication(SubsApplication subsApplication) {
        this.subsApplication = subsApplication;
    }

    @NotBlank
    @Column(name = "RESOURCE_URL")
    public String getResourceUrl() {
        return resourceUrl;
    }

    public void setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl;
    }

    @Override
    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return super.getCreatedBy();
    }

    @Override
    public void setCreatedBy(String createdBy) {
        super.setCreatedBy(createdBy);
    }

    @Override
    @Column(name = "CREATED_DATE")
    public Timestamp getCreatedDate() {
        return super.getCreatedDate();
    }

    @Override
    public void setCreatedDate(Timestamp createdDate) {
        super.setCreatedDate(createdDate);
    }

    @Override
    @Column(name = "UPDATED_BY")
    public String getUpdatedBy() {
        return super.getUpdatedBy();
    }

    @Override
    public void setUpdatedBy(String updatedBy) {
        super.setUpdatedBy(updatedBy);
    }

    @Override
    @Column(name = "UPDATED_DATE")
    public Timestamp getUpdatedDate() {
        return super.getUpdatedDate();
    }

    @Override
    public void setUpdatedDate(Timestamp updatedDate) {
        super.setUpdatedDate(updatedDate);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((resourceUrl == null) ? 0 : resourceUrl.hashCode());
        result = prime * result + ((subsApplication == null) ? 0 : subsApplication.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        Resource other = (Resource) obj;
        if (resourceUrl == null) {
            if (other.resourceUrl != null) return false;
        } else if (!resourceUrl.equals(other.resourceUrl)) return false;
        if (subsApplication == null) {
            if (other.subsApplication != null) return false;
        } else if (!subsApplication.equals(other.subsApplication)) return false;
        return true;
    }

    @Override
    public String toString() {
        return "Resource [getId()=" + getId() + ", getCreatedBy()=" + getCreatedBy() + ", getSubsApplication()=" + getSubsApplication() + ", getResourceUrl()=" + getResourceUrl() + ", getCreatedDate()=" + getCreatedDate() + ", getUpdatedBy()=" + getUpdatedBy() + ", getUpdatedDate()=" + getUpdatedDate() + "]";
    }


}
