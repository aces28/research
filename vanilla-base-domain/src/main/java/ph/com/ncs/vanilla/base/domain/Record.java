package ph.com.ncs.vanilla.base.domain;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by ace on 5/18/16.
 * <p>
 * Added equals and hashCode edjohna 5/24/2016
 */
public class Record implements Serializable {

    private static final long serialVersionUID = -8577670756272354964L;

    private String createdBy;
    private Timestamp createdDate;
    private String updatedBy;
    private Timestamp updatedDate;
    private String modifiedApplicationDate;

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Timestamp getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Timestamp updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getModifiedApplicationDate() {
        return modifiedApplicationDate;
    }

    public void setModifiedApplicationDate(String modifiedApplicationDate) {
        this.modifiedApplicationDate = modifiedApplicationDate;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
        result = prime * result + ((createdDate == null) ? 0 : createdDate.hashCode());
        result = prime * result + ((updatedBy == null) ? 0 : updatedBy.hashCode());
        result = prime * result + ((updatedDate == null) ? 0 : updatedDate.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Record other = (Record) obj;
        if (createdBy == null) {
            if (other.createdBy != null) return false;
        } else if (!createdBy.equals(other.createdBy)) return false;
        if (createdDate == null) {
            if (other.createdDate != null) return false;
        } else if (!createdDate.equals(other.createdDate)) return false;
        if (updatedBy == null) {
            if (other.updatedBy != null) return false;
        } else if (!updatedBy.equals(other.updatedBy)) return false;
        if (updatedDate == null) {
            if (other.updatedDate != null) return false;
        } else if (!updatedDate.equals(other.updatedDate)) return false;
        return true;
    }


}
