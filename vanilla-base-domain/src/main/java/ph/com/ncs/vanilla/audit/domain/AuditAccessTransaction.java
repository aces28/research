package ph.com.ncs.vanilla.audit.domain;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import ph.com.ncs.vanilla.base.domain.Model;
import ph.com.ncs.vanilla.user.domain.User;

/**
 * Created by edjohna on 7/17/2016.
 */
@Entity
@Table(name = "TBL_ACCESS_TRANSACTION")
public class AuditAccessTransaction extends Model {
	
    private static final long serialVersionUID = 2584796044410142507L;
	
    private User user;
    private AuditModule module;
    private Timestamp dateAccessed;
    private String ipAddress;
    private String oldData;
    private String newData;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "TXN_ID")
    @Override
    public Integer getId() {
        return super.getId();
    }

    @Override
    public void setId(Integer id) {
        super.setId(id);
    }
    
    @ManyToOne
    @JoinColumn(name = "USER_ID")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    @ManyToOne
    @JoinColumn(name = "MODULE_ID")
	public AuditModule getModule() {
		return module;
	}

	public void setModule(AuditModule module) {
		this.module = module;
	}
    
    @Column(name = "DATE_ACCESSED")
    public Timestamp getDateAccessed() {
        return dateAccessed;
    }

    public void setDateAccessed(Timestamp dateAccessed) {
        this.dateAccessed = dateAccessed;
    }

    @Column(name = "IP_ADDRESS")
    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    @Override
    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return super.getCreatedBy();
    }

    @Override
    @Column(name = "CREATED_DATE")
    public Timestamp getCreatedDate() {
        return super.getCreatedDate();
    }

    @Override
    public void setCreatedBy(String createdBy) {
        super.setCreatedBy(createdBy);
    }

    @Override
    public void setCreatedDate(Timestamp createdDate) {
        super.setCreatedDate(createdDate);
    }

    @Override
    @Column(name = "UPDATED_BY")
    public String getUpdatedBy() {
        return super.getUpdatedBy();
    }

    @Override
    public void setUpdatedBy(String updatedBy) {
        super.setUpdatedBy(updatedBy);
    }

    @Override
    @Column(name = "UPDATED_DATE")
    public Timestamp getUpdatedDate() {
        return super.getUpdatedDate();
    }
    
    @Override
    public void setUpdatedDate(Timestamp updatedDate) {
        super.setUpdatedDate(updatedDate);
    }
    
    @Column(name = "OLD_DATA")
	public String getOldData() {
		return oldData;
	}

	public void setOldData(String oldData) {
		this.oldData = oldData;
	}
	
    @Column(name = "NEW_DATA")
	public String getNewData() {
		return newData;
	}

	public void setNewData(String newData) {
		this.newData = newData;
	}

    @Transient
    public String getAuditDate() {
        return new SimpleDateFormat("MM/dd/yyyy").format(dateAccessed);
    }

    public void setAuditDate(String auditDate) {
        this.setAuditDate(auditDate);
    }

    @Transient
    public String getAuditTime() {
        return new SimpleDateFormat("hh:mm:ss a").format(dateAccessed);
    }

    public void setAuditTime(String auditTime) {
        this.setAuditTime(auditTime);
    }

    @Override
    public String toString() {
        return "AuditAccess{" +
                "user=" + user +
                ", dateAccessed=" + dateAccessed +
                ", ipAddress='" + ipAddress + '\'' +
                ", moduleAccess='" + getModule().getModuleName() + '\'' +
                '}';
    }
}
