package ph.com.ncs.vanilla.base.domain;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;

/**
 * Created by ace on 5/19/16.
 */
public class Status extends Model {

    private static final long serialVersionUID = 3465798491708152104L;

    private Integer code;
    private Integer status;

    public Status() {
    }

    public Status(int code, int status) {
        this.code = code;
        this.status = status;
    }

    @Column(name = "CODE")
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(code).append(status).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Status)) {
            return false;
        }

        Status status = (Status) obj;
        return new EqualsBuilder().append(getCode(), status.getCode()).append(getStatus(), status.getStatus()).isEquals();
    }
}
