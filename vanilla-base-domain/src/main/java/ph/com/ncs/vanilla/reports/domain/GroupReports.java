package ph.com.ncs.vanilla.reports.domain;

/**
 * Created by edjohna on 7/12/2016.
 */
public class GroupReports {

    private String id;
    private String groupName;
    private String roleType;
    private String status;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
