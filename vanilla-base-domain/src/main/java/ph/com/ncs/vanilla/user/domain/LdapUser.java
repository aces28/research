package ph.com.ncs.vanilla.user.domain;

import java.io.Serializable;

/**
 * Created by ace on 7/18/16.
 */
public class LdapUser implements Serializable {

    private static final long serialVersionUID = 4080899220466305220L;

    private String uid;
    private String cn;
    private String sn;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getCn() {
        return cn;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }
}
