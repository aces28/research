package ph.com.ncs.vanilla.subscriber.domain;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import ph.com.ncs.vanilla.base.domain.Model;

/**
 * @author allango 7/13/2016
 */
@Entity
@Table(name = "TBL_ACCOUNT")
public class Account extends Model {

	private static final long serialVersionUID = 7814209811770800657L;


	private String msisdn;


	private Boolean status;

    public static final Account newInstance(String msisdn) {
    	Account target = new Account();

        target.msisdn = msisdn;
        target.status = true;
        target.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        target.setCreatedBy("SYSTEM");

        return target;
    }

    public String toString() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("msisdn", msisdn);
        map.put("status", status);
        map.put("createdTms", getCreatedDate());
        map.put("createdBy", getCreatedBy());
        map.put("updatedTms", getUpdatedDate());
        map.put("updatedBy", getUpdatedBy());

        return map.toString();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ACCOUNT_ID")
    @Override
    public Integer getId() {
        return super.getId();
    }

    @Override
    public void setId(Integer id) {
        super.setId(id);
    }
    @Column(name = "MSISDN", nullable = false)
	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
    @Column(name = "STATUS", nullable = false)
	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

    @Override
    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return super.getCreatedBy();
    }

    @Override
    public void setCreatedBy(String createdBy) {
        super.setCreatedBy(createdBy);
    }

    @Override
    @Column(name = "CREATED_DATE")
    public Timestamp getCreatedDate() {
        return super.getCreatedDate();
    }

    @Override
    public void setCreatedDate(Timestamp createdDate) {
        super.setCreatedDate(createdDate);
    }

    @Override
    @Column(name = "UPDATED_BY")
    public String getUpdatedBy() {
        return super.getUpdatedBy();
    }

    @Override
    public void setUpdatedBy(String updatedBy) {
        super.setUpdatedBy(updatedBy);
    }

    @Override
    @Column(name = "UPDATED_DATE")
    public Timestamp getUpdatedDate() {
        return super.getUpdatedDate();
    }

    @Override
    public void setUpdatedDate(Timestamp updatedDate) {
        super.setUpdatedDate(updatedDate);
    }
}
