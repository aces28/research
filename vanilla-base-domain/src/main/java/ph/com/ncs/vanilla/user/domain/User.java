package ph.com.ncs.vanilla.user.domain;

import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import ph.com.ncs.vanilla.base.domain.Status;
import ph.com.ncs.vanilla.group.domain.Group;
import ph.com.ncs.vanilla.role.domain.Role;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "TBL_USER")
public class User extends Status {

    private static final long serialVersionUID = 2584796044410142507L;

    private Group group;
    private Role role;
    private String userName;
    private String firstName;
    private String lastName;
    private String email;
    private int failedLoginAttempts;
    private int isLocked;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "USER_ID")
    @Override
    public Integer getId() {
        return super.getId();
    }

    @Override
    public void setId(Integer id) {
        super.setId(id);
    }

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "GROUP_ID")
    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    @ManyToOne
    @JoinColumn(name = "ROLE_ID")
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Column(columnDefinition = "VARCHAR", name = "USER_NAME")
    @ColumnTransformer(read = "AES_DECRYPT(USER_NAME, 'q2IfiQyifQSSbMV3Sht4jxjWjMwAAAAAAAAAAAAAAAA=')",
            write = "AES_ENCRYPT(?, 'q2IfiQyifQSSbMV3Sht4jxjWjMwAAAAAAAAAAAAAAAA=')")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Column(columnDefinition = "VARCHAR", name = "FIRST_NAME")
    @ColumnTransformer(read = "AES_DECRYPT(FIRST_NAME, 'q2IfiQyifQSSbMV3Sht4jxjWjMwAAAAAAAAAAAAAAAA=')",
            write = "AES_ENCRYPT(?, 'q2IfiQyifQSSbMV3Sht4jxjWjMwAAAAAAAAAAAAAAAA=')")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(columnDefinition = "VARCHAR", name = "LAST_NAME")
    @ColumnTransformer(read = "AES_DECRYPT(LAST_NAME, 'q2IfiQyifQSSbMV3Sht4jxjWjMwAAAAAAAAAAAAAAAA=')",
            write = "AES_ENCRYPT(?, 'q2IfiQyifQSSbMV3Sht4jxjWjMwAAAAAAAAAAAAAAAA=')")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Column(columnDefinition = "VARCHAR", name = "EMAIL")
    @ColumnTransformer(read = "AES_DECRYPT(EMAIL, 'q2IfiQyifQSSbMV3Sht4jxjWjMwAAAAAAAAAAAAAAAA=')",
            write = "AES_ENCRYPT(?, 'q2IfiQyifQSSbMV3Sht4jxjWjMwAAAAAAAAAAAAAAAA=')")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public Integer getStatus() {
        return super.getStatus();
    }

    @Override
    public void setStatus(Integer status) {
        super.setStatus(status);
    }

    @Override
    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return super.getCreatedBy();
    }

    @Override
    public void setCreatedBy(String createdBy) {
        super.setCreatedBy(createdBy);
    }

    @Override
    @Column(name = "CREATED_DATE")
    public Timestamp getCreatedDate() {
        return super.getCreatedDate();
    }

    @Override
    public void setCreatedDate(Timestamp createdDate) {
        super.setCreatedDate(createdDate);
    }

    @Override
    @Column(name = "UPDATED_BY")
    public String getUpdatedBy() {
        return super.getUpdatedBy();
    }

    @Override
    public void setUpdatedBy(String updatedBy) {
        super.setUpdatedBy(updatedBy);
    }

    @Override
    @Column(name = "UPDATED_DATE")
    public Timestamp getUpdatedDate() {
        return super.getUpdatedDate();
    }

    @Override
    public void setUpdatedDate(Timestamp updatedDate) {
        super.setUpdatedDate(updatedDate);
    }

    @Column(name = "FAILED_LOGIN_ATTEMPTS")
    public int getFailedLoginAttempts() {
        return failedLoginAttempts;
    }

    public void setFailedLoginAttempts(int failedLoginAttempts) {
        this.failedLoginAttempts = failedLoginAttempts;
    }

    @Column(name = "IS_LOCKED")
    public int getIsLocked() {
        return isLocked;
    }

    public void setIsLocked(int isLocked) {
        this.isLocked = isLocked;
    }

    @Override
    public String toString() {
        return "User{" +
                "group=" + group +
                ", role=" + role +
                ", userName='" + userName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
