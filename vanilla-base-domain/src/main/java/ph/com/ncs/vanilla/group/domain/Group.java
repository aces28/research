package ph.com.ncs.vanilla.group.domain;

import ph.com.ncs.vanilla.base.domain.Status;
import ph.com.ncs.vanilla.role.domain.Role;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(name = "TBL_GROUP")
public class Group extends Status {

    private static final long serialVersionUID = 2584796044410142507L;

    private String groupName;
    private Role role;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "GROUP_ID")
    @Override
    public Integer getId() {
        return super.getId();
    }

    @Override
    public void setId(Integer id) {
        super.setId(id);
    }

    @Override
    public void setStatus(Integer status) {
        super.setStatus(status);
    }

    @Override
    @Column(name = "STATUS")
    public Integer getStatus() {
        return super.getStatus();
    }

    @Column(name = "GROUP_NAME")
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @OneToOne
    @JoinColumn(name = "ROLE_ID")
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return super.getCreatedBy();
    }

    @Override
    public void setCreatedBy(String createdBy) {
        super.setCreatedBy(createdBy);
    }

    @Override
    @Column(name = "CREATED_DATE")
    public Timestamp getCreatedDate() {
        return super.getCreatedDate();
    }

    @Override
    public void setCreatedDate(Timestamp createdDate) {
        super.setCreatedDate(createdDate);
    }

    @Override
    @Column(name = "UPDATED_BY")
    public String getUpdatedBy() {
        return super.getUpdatedBy();
    }

    @Override
    public void setUpdatedBy(String updatedBy) {
        super.setUpdatedBy(updatedBy);
    }

    @Override
    @Column(name = "UPDATED_DATE")
    public Timestamp getUpdatedDate() {
        return super.getUpdatedDate();
    }

    @Override
    public void setUpdatedDate(Timestamp updatedDate) {
        super.setUpdatedDate(updatedDate);
    }

    @Override
    public String toString() {
        return "Group{" +
                "groupName='" + groupName + '\'' +
                ", role=" + role +
                '}';
    }
}
