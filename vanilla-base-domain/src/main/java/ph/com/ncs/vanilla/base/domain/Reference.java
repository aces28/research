package ph.com.ncs.vanilla.base.domain;

import java.io.Serializable;

/**
 * Created by ace on 5/18/16.
 */
public interface Reference extends Serializable {

    Integer getId();

    String getReferenceKey();

    void setId(Integer id);

    void setReferenceKey(String referenceKey);
}
