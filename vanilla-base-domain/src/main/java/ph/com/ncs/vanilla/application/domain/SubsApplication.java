package ph.com.ncs.vanilla.application.domain;

import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.validator.constraints.NotBlank;
import ph.com.ncs.vanilla.base.domain.Status;
import ph.com.ncs.vanilla.rejection.domain.Rejection;
import ph.com.ncs.vanilla.subscriber.domain.Subscriber;
import ph.com.ncs.vanilla.user.domain.User;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * @author edjohna 5/24/2016
 */
@Entity
@Table(name = "TBL_SUB_APPLICATION")
public class SubsApplication extends Status {

    private static final long serialVersionUID = 2584796044410142507L;

    private Subscriber subscriber;
    private User user;
    private String subscriberMsisdn;
    private Timestamp applicationDate;
    private String accountType;
    private String userEmail;
    private String attachment;
    private String termsConditions;
    private Date approvalDate;

    private Rejection rejection;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "APPLICATION_ID")
    @Override
    public Integer getId() {
        return super.getId();
    }

    @Override
    public void setId(Integer id) {
        super.setId(id);
    }

    @ManyToOne
    @JoinColumn(name = "SUBSCRIBER_ID")
    public Subscriber getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(Subscriber subscriber) {
        this.subscriber = subscriber;
    }

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "USER_ID")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @NotBlank
    @Column(columnDefinition = "VARCHAR", name = "SUBSCRIBER_MSISDN")
    @ColumnTransformer(read = "AES_DECRYPT(SUBSCRIBER_MSISDN, 'q2IfiQyifQSSbMV3Sht4jxjWjMwAAAAAAAAAAAAAAAA=')",
            write = "AES_ENCRYPT(?, 'q2IfiQyifQSSbMV3Sht4jxjWjMwAAAAAAAAAAAAAAAA=')")
    public String getSubscriberMsisdn() {
        return subscriberMsisdn;
    }

    public void setSubscriberMsisdn(String subscriberMsisdn) {
        this.subscriberMsisdn = subscriberMsisdn;
    }

    @Column(name = "APPLICATION_DATE")
    public Timestamp getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(Timestamp applicationDate) {
        this.applicationDate = applicationDate;
    }

    @NotBlank
    @Column(name = "ACCOUNT_TYPE")
    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    @Column(columnDefinition = "VARCHAR", name = "USER_EMAIL")
    @ColumnTransformer(read = "AES_DECRYPT(USER_EMAIL, 'q2IfiQyifQSSbMV3Sht4jxjWjMwAAAAAAAAAAAAAAAA=')",
            write = "AES_ENCRYPT(?, 'q2IfiQyifQSSbMV3Sht4jxjWjMwAAAAAAAAAAAAAAAA=')")
    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @NotBlank
    @Column(columnDefinition = "VARCHAR", name = "ATTACHMENT_URL")
    @ColumnTransformer(read = "AES_DECRYPT(ATTACHMENT_URL, 'q2IfiQyifQSSbMV3Sht4jxjWjMwAAAAAAAAAAAAAAAA=')",
            write = "AES_ENCRYPT(?, 'q2IfiQyifQSSbMV3Sht4jxjWjMwAAAAAAAAAAAAAAAA=')")
    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    @NotBlank
    @Column(columnDefinition = "VARCHAR", name = "TERMS_CONDITIONS")
    @ColumnTransformer(read = "AES_DECRYPT(TERMS_CONDITIONS, 'q2IfiQyifQSSbMV3Sht4jxjWjMwAAAAAAAAAAAAAAAA=')",
            write = "AES_ENCRYPT(?, 'q2IfiQyifQSSbMV3Sht4jxjWjMwAAAAAAAAAAAAAAAA=')")
    public String getTermsConditions() {
        return termsConditions;
    }

    public void setTermsConditions(String termsConditions) {
        this.termsConditions = termsConditions;
    }

    @Column(name = "APPROVAL_DATE")
    public Date getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }

    @Column(name = "APPROVAL_STATUS")
    @Override
    public Integer getStatus() {
        return super.getStatus();
    }

    @Override
    public void setStatus(Integer status) {
        super.setStatus(status);
    }

    @Override
    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return super.getCreatedBy();
    }

    @Override
    public void setCreatedBy(String createdBy) {
        super.setCreatedBy(createdBy);
    }

    @Override
    @Column(name = "CREATED_DATE")
    public Timestamp getCreatedDate() {
        return super.getCreatedDate();
    }

    @Override
    public void setCreatedDate(Timestamp createdDate) {
        super.setCreatedDate(createdDate);
    }

    @Override
    @Column(name = "UPDATED_BY")
    public String getUpdatedBy() {
        return super.getUpdatedBy();
    }

    @Override
    public void setUpdatedBy(String updatedBy) {
        super.setUpdatedBy(updatedBy);
    }

    @Override
    @Column(name = "UPDATED_DATE")
    public Timestamp getUpdatedDate() {
        return super.getUpdatedDate();
    }

    @Override
    public void setUpdatedDate(Timestamp updatedDate) {
        super.setUpdatedDate(updatedDate);
    }

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "REJECTION_ID")
    public Rejection getRejection() {
        return rejection;
    }

    public void setRejection(Rejection rejection) {
        this.rejection = rejection;
    }

    @Transient
    @Override
    public String getModifiedApplicationDate() {
        return new SimpleDateFormat("MM/dd/yyyy hh:mm a").format(applicationDate);
    }

    @Override
    public void setModifiedApplicationDate(String modifiedApplicationDate) {
        super.setModifiedApplicationDate(modifiedApplicationDate);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (subscriber != null ? subscriber.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (subscriberMsisdn != null ? subscriberMsisdn.hashCode() : 0);
        result = 31 * result + (applicationDate != null ? applicationDate.hashCode() : 0);
        result = 31 * result + (accountType != null ? accountType.hashCode() : 0);
        result = 31 * result + (userEmail != null ? userEmail.hashCode() : 0);
        result = 31 * result + (attachment != null ? attachment.hashCode() : 0);
        result = 31 * result + (termsConditions != null ? termsConditions.hashCode() : 0);
        result = 31 * result + (approvalDate != null ? approvalDate.hashCode() : 0);
        result = 31 * result + (rejection != null ? rejection.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        SubsApplication other = (SubsApplication) obj;
        if (accountType == null) {
            if (other.accountType != null) return false;
        } else if (!accountType.equals(other.accountType)) return false;
        if (applicationDate == null) {
            if (other.applicationDate != null) return false;
        } else if (!applicationDate.equals(other.applicationDate)) return false;
        if (approvalDate == null) {
            if (other.approvalDate != null) return false;
        } else if (!approvalDate.equals(other.approvalDate)) return false;
        if (attachment == null) {
            if (other.attachment != null) return false;
        } else if (!attachment.equals(other.attachment)) return false;
        if (subscriber == null) {
            if (other.subscriber != null) return false;
        } else if (!subscriber.equals(other.subscriber)) return false;
        if (subscriberMsisdn == null) {
            if (other.subscriberMsisdn != null) return false;
        } else if (!subscriberMsisdn.equals(other.subscriberMsisdn)) return false;
        if (termsConditions == null) {
            if (other.termsConditions != null) return false;
        } else if (!termsConditions.equals(other.termsConditions)) return false;
        if (userEmail == null) {
            if (other.userEmail != null) return false;
        } else if (!userEmail.equals(other.userEmail)) return false;
        if (user != other.user) return false;
        return true;
    }

    @Override
    public String toString() {
        return "SubsApplication [getId()=" + getId() + ", getSubscriber()=" + getSubscriber() + ", getUser()=" + getUser() +
                ", getSubscriberMsisdn()=" + getSubscriberMsisdn() + ", getApplicationDate()=" + getApplicationDate() +
                ", getAccountType()=" + getAccountType() + ", getUserEmail()=" + getUserEmail() + ", getAttachment()=" +
                getAttachment() + ", getTermsConditions()=" + getTermsConditions() + ", getApprovalDate()=" + getApprovalDate() +
                ", getStatus()=" + getStatus() + ", getCreatedBy()=" + getCreatedBy() + ", getCreatedDate()=" + getCreatedDate() +
                ", getUpdatedBy()=" + getUpdatedBy() + ", getUpdatedDate()=" + getUpdatedDate() + "]";
    }


}
