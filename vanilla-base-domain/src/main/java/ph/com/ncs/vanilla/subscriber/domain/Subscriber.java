package ph.com.ncs.vanilla.subscriber.domain;

import org.hibernate.annotations.*;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.NotBlank;
import org.jasypt.hibernate4.type.EncryptedStringType;
import ph.com.ncs.vanilla.base.domain.Model;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * @author edjohna 5/24/2015
 */
@Entity
@Table(name = "TBL_SUBSCRIBER")
public class Subscriber extends Model {

    private static final long serialVersionUID = 2584796044410142507L;

    private String firstName;
    private String middleName;
    private String lastName;
    private String gender;
    private String address;
    private Date birthday;
    private String mothersName;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "SUBSCRIBER_ID")
    @Override
    public Integer getId() {
        return super.getId();
    }

    @Override
    public void setId(Integer id) {
        super.setId(id);
    }

    @NotBlank
    @Column(columnDefinition = "VARCHAR", name = "FIRST_NAME")
    @ColumnTransformer(read = "AES_DECRYPT(FIRST_NAME, 'q2IfiQyifQSSbMV3Sht4jxjWjMwAAAAAAAAAAAAAAAA=')",
            write = "AES_ENCRYPT(?, 'q2IfiQyifQSSbMV3Sht4jxjWjMwAAAAAAAAAAAAAAAA=')")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @NotBlank
    @Column(columnDefinition = "VARCHAR", name = "MIDDLE_NAME")
    @ColumnTransformer(read = "AES_DECRYPT(MIDDLE_NAME, 'q2IfiQyifQSSbMV3Sht4jxjWjMwAAAAAAAAAAAAAAAA=')",
            write = "AES_ENCRYPT(?, 'q2IfiQyifQSSbMV3Sht4jxjWjMwAAAAAAAAAAAAAAAA=')")
    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    @NotBlank
//    @Type(type="encryptedString")
    @Column(columnDefinition = "VARCHAR", name = "LAST_NAME")
    @ColumnTransformer(read = "AES_DECRYPT(LAST_NAME, 'q2IfiQyifQSSbMV3Sht4jxjWjMwAAAAAAAAAAAAAAAA=')",
            write = "AES_ENCRYPT(?, 'q2IfiQyifQSSbMV3Sht4jxjWjMwAAAAAAAAAAAAAAAA=')")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @NotBlank
    @Column(name = "GENDER")
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    //	@NotBlank
//    @Type(type="encryptedString")
    @Column(columnDefinition = "VARCHAR", name = "ADDRESS")
    @ColumnTransformer(read = "AES_DECRYPT(ADDRESS, 'q2IfiQyifQSSbMV3Sht4jxjWjMwAAAAAAAAAAAAAAAA=')",
            write = "AES_ENCRYPT(?, 'q2IfiQyifQSSbMV3Sht4jxjWjMwAAAAAAAAAAAAAAAA=')")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Column(name = "BIRTHDAY")
    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    //    @Type(type="encryptedString")
    @Column(columnDefinition = "VARCHAR", name = "MOTHERS_NAME")
    @ColumnTransformer(read = "AES_DECRYPT(MOTHERS_NAME, 'q2IfiQyifQSSbMV3Sht4jxjWjMwAAAAAAAAAAAAAAAA=')",
            write = "AES_ENCRYPT(?, 'q2IfiQyifQSSbMV3Sht4jxjWjMwAAAAAAAAAAAAAAAA=')")
    public String getMothersName() {
        return mothersName;
    }

    public void setMothersName(String mothersName) {
        this.mothersName = mothersName;
    }


    @Override
    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return super.getCreatedBy();
    }

    @Override
    public void setCreatedBy(String createdBy) {
        super.setCreatedBy(createdBy);
    }

    @Override
    @Column(name = "CREATED_DATE")
    public Timestamp getCreatedDate() {
        return super.getCreatedDate();
    }

    @Override
    public void setCreatedDate(Timestamp createdDate) {
        super.setCreatedDate(createdDate);
    }

    @Override
    @Column(name = "UPDATED_BY")
    public String getUpdatedBy() {
        return super.getUpdatedBy();
    }

    @Override
    public void setUpdatedBy(String updatedBy) {
        super.setUpdatedBy(updatedBy);
    }

    @Override
    @Column(name = "UPDATED_DATE")
    public Timestamp getUpdatedDate() {
        return super.getUpdatedDate();
    }

    @Override
    public void setUpdatedDate(Timestamp updatedDate) {
        super.setUpdatedDate(updatedDate);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((address == null) ? 0 : address.hashCode());
        result = prime * result + ((birthday == null) ? 0 : birthday.hashCode());
        result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result + ((gender == null) ? 0 : gender.hashCode());
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result + ((middleName == null) ? 0 : middleName.hashCode());
        result = prime * result + ((mothersName == null) ? 0 : mothersName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        Subscriber other = (Subscriber) obj;
        if (address == null) {
            if (other.address != null) return false;
        } else if (!address.equals(other.address)) return false;
        if (birthday == null) {
            if (other.birthday != null) return false;
        } else if (!birthday.equals(other.birthday)) return false;
        if (firstName == null) {
            if (other.firstName != null) return false;
        } else if (!firstName.equals(other.firstName)) return false;
        if (gender == null) {
            if (other.gender != null) return false;
        } else if (!gender.equals(other.gender)) return false;
        if (lastName == null) {
            if (other.lastName != null) return false;
        } else if (!lastName.equals(other.lastName)) return false;
        if (middleName == null) {
            if (other.middleName != null) return false;
        } else if (!middleName.equals(other.middleName)) return false;
        if (mothersName == null) {
            if (other.mothersName != null) return false;
        } else if (!mothersName.equals(other.mothersName)) return false;
        return true;
    }

    @Override
    public String toString() {
        return "Subscriber [firstName=" + firstName + ", middleName=" + middleName + ", lastName=" + lastName + ", gender=" + gender + ", address=" + address + ", birthday=" + birthday + ", mothersName=" + mothersName + "]";
    }


}
