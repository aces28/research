package ph.com.ncs.vanilla.reports.domain;

/**
 * Created by edjohna on 7/19/2016.
 */
public class UserAccessHistoryReports {

    private String transactionId;
    private String user;
    private String lastName;
    private String firstName;
    private String date;
    private String time;
    private String ipAddress;
    private String moduleAccess;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getModuleAccess() {
        return moduleAccess;
    }

    public void setModuleAccess(String moduleAccess) {
        this.moduleAccess = moduleAccess;
    }
}
