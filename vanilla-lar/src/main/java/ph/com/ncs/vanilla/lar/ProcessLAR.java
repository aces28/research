package ph.com.ncs.vanilla.lar;
/**
 * Created by francism on 18/08/2016.
 */

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.support.ResourcePropertySource;
import org.springframework.stereotype.Component;

import ph.com.ncs.vanilla.audit.domain.AuditAccessTransaction;
import ph.com.ncs.vanilla.audit.service.AuditService;
import ph.com.ncs.vanilla.group.domain.Group;
import ph.com.ncs.vanilla.group.service.GroupService;
import ph.com.ncs.vanilla.reports.domain.UserLarReport;
import ph.com.ncs.vanilla.role.domain.Role;
import ph.com.ncs.vanilla.role.service.RoleService;


@Component
public class ProcessLAR implements CommandLineRunner{
	private static final Logger logger = LoggerFactory.getLogger(ProcessLAR.class);
	private static final SimpleDateFormat DATE_FILE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmssSSS");	
	 
	@Autowired
	private ConfigurableApplicationContext context;
    @Autowired
    private  AuditService auditService;
    @Autowired
    private  GroupService groupService;
    @Autowired
    private  RoleService roleService;
    
	 @Override
	 public void run(String... args) throws Exception {
		 
		 	logger.info("LAR extractReports() : Starting to extract");
			ResourcePropertySource propertySource = new ResourcePropertySource("resource",
					"classpath:application-lar.properties");

			String resultDir = (String) propertySource.getProperty("vanilla.lar.result_dir");
			if (!validateDir(resultDir)) {
				throw new Exception("Cannot find valid result directory location. ");
			}
		 
			File resultLoc = new File(resultDir);
			
			
		 List<UserLarReport> larReport = larExtractReports();
		 final String CSV_HEADER = "ID #,Full Name,Role Name,Role Description,Group Name, Group Description," +
		 "Action Permission,Account Status,Last Login,Create Date";
		 final String DELIMITER = ","; 
		 
		 String date = DATE_FILE_FORMAT.format(new Date());
		 logger.info("LAR extractReports() : Writing to FileWriter");
		 FileWriter fileWriter = null;
			 try{
				 logger.info("LAR extractReports() : location of result file is " + File.separator + resultLoc.getAbsolutePath());
				 fileWriter = new FileWriter( resultLoc.getAbsolutePath() + File.separator + "Vanilla.LAR." + date + ".csv");
				 	 fileWriter.append(CSV_HEADER.toString());
				 	 fileWriter.append("\n");
				 
				 	logger.info("LAR extractReports() : appending records");
				 	
				 for(UserLarReport user : larReport){			
					 logger.info("LAR extractReports() : appending user with user id: " + user.getUserId());
					 fileWriter.append(user.getUserId());
					 fileWriter.append(DELIMITER);
					 fileWriter.append(user.getFullName());
					 fileWriter.append(DELIMITER);
					 fileWriter.append(user.getRoleName());
					 fileWriter.append(DELIMITER);
					 fileWriter.append(user.getRoleDescription());
					 fileWriter.append(DELIMITER);
					 fileWriter.append(user.getGroupName());
					 fileWriter.append(DELIMITER);
					 fileWriter.append(user.getGroupDescription());
					 fileWriter.append(DELIMITER);
					 fileWriter.append(user.getActionPermission());
					 fileWriter.append(DELIMITER);
					 fileWriter.append(user.getAccountStatus());
					 fileWriter.append(DELIMITER);
					 fileWriter.append(user.getLastLogin());
					 fileWriter.append(DELIMITER);
					 fileWriter.append(user.getCreateDate());
					 fileWriter.append("\n");
				 }
				 
			 }catch (Exception e){
				 e.printStackTrace();
			 }finally{
				 fileWriter.flush();
				 fileWriter.close();
			 }
			 logger.info("LAR extractReports() : FileWriter process done ");
			 logger.info("LAR extractReports() : Check generated csv file in the result directory");
   
			 context.close();
	 }
	
	private List<UserLarReport> larExtractReports(){
        logger.info("LAR extractReports() : START");
        List<UserLarReport> userReportsList = new ArrayList<>();
        
        List<AuditAccessTransaction> auditList = auditService.generateLAR();
        for (AuditAccessTransaction audit : auditList) {
        	logger.info("LAR extractReports() : getting user information ");
        	UserLarReport userReports = new UserLarReport();
            userReports.setUserId(""+audit.getUser().getId()+"");
            userReports.setFullName(audit.getUser().getFirstName() + " " + audit.getUser().getLastName()+"" );
            userReports.setActionPermission("");            
            userReports.setLastLogin(audit.getDateAccessed().toString());
            userReports.setCreateDate(audit.getUser().getCreatedDate().toString());
            
            if (audit.getUser().getGroup() != null) {
                Group group = groupService.get(audit.getUser().getGroup());
                userReports.setGroupName(group.getGroupName());
                userReports.setGroupDescription(group.getGroupName());
            } else {
                userReports.setGroupName(" ");
                userReports.setGroupDescription(" ");
            }
            if (audit.getUser().getRole() != null) {
                Role role = roleService.get(audit.getUser().getRole());
                userReports.setRoleName(role.getRoleType());
                userReports.setRoleDescription(role.getRoleType());
            } else {
                userReports.setRoleName(" ");
                userReports.setRoleDescription(" ");
            }
            String availability;
            if (audit.getUser().getStatus() == 1) {
                availability = "active";
            } else {
                availability = "inactive";
            }
            userReports.setAccountStatus(availability);
            userReportsList.add(userReports);
        }
        logger.info("LAR extractReports() : Done getting information of users");
       
        logger.info("Number of Users extracted: " + userReportsList.size());

        	 return userReportsList;
        	 
         }
	
		private static boolean validateDir(String directory) {
		if (directory == null || directory.length() == 0) {
			return false;
		}
		if (directory.endsWith(File.separator)) {
			directory = directory.substring(0, directory.length() - 1);
		}
		return (new File(directory).isDirectory());
	}	
}
