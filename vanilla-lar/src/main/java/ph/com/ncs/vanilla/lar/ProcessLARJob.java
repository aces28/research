package ph.com.ncs.vanilla.lar;
/**
 * Created by francism on 18/08/2016.
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.test.ImportAutoConfiguration;
import org.springframework.boot.context.web.SpringBootServletInitializer;

import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;


@SpringBootApplication
@ImportAutoConfiguration(VanillaContextConfig.class)
public class ProcessLARJob extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(ProcessLARJob.class, args);
	}
}
