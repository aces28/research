package ph.com.ncs.vanilla.raven.ws.client.test;

import com.danateq.soap.link.pnm.PushNotification;
import com.danateq.soap.link.pnm.PushNotificationResp;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ph.com.ncs.vanilla.raven.ws.push.stubs.AsyncPushNotificationStub;
import ph.com.ncs.vanilla.raven.ws.push.stubs.NotificationCallBackHandler;
import ph.com.ncs.vanilla.raven.ws.push.stubs.SyncPushNotificationStub;

import java.rmi.RemoteException;

/**
 * Created by ace on 6/13/16.
 */
public class TestRavenClient {

    private static final Logger logger = LoggerFactory.getLogger(TestRavenClient.class);

    //    private static final String WSDL_RAVEN_URL = "http://10.225.38.147:8080/axis2/services/SyncPushNotificationService";
    private static final String WSDL_RAVEN_URL = "http://10.176.82.141:8080/axis2/services/SyncPushNotificationService";

    @Test
    public void testAsyncRavenClient() throws RemoteException, InterruptedException {

        logger.info("testAsyncRavenClient : START");
        AsyncPushNotificationStub asyncPushNotificationStub = new AsyncPushNotificationStub(WSDL_RAVEN_URL);
        PushNotification pushNotification = new PushNotification();
        pushNotification.setUserIdentity("639063241954");
        pushNotification.setUserIdentityType(0);
        pushNotification.setNotifPatternId(17);

        NotificationCallBackHandler ravenCallBackHandler = new NotificationCallBackHandler();
        asyncPushNotificationStub.startpushNotification(pushNotification, ravenCallBackHandler);

    }

    @Test
    public void testSyncRavenClient() throws RemoteException, InterruptedException {

        logger.info("testAsyncRavenClient : START");
        SyncPushNotificationStub syncPushNotificationStub = new SyncPushNotificationStub(WSDL_RAVEN_URL);
        PushNotification pushNotification = new PushNotification();
        pushNotification.setUserIdentity("639183241954");
        pushNotification.setUserIdentityType(0);
        pushNotification.setNotifPatternId(17);


        PushNotificationResp pushNotificationResp = syncPushNotificationStub.pushNotification(pushNotification);
        logger.info("notification : result : " + pushNotificationResp.getPushNotifResult());
    }
}
