/**
 * AsyncPushNotificationService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:08:57 BST)
 */
package ph.com.ncs.vanilla.raven.ws.push.stubs;


/*
 *  AsyncPushNotificationService java interface
 */
public interface AsyncPushNotificationService {
    /**
     * Auto generated method signature for Asynchronous Invocations
     *
     * @param pushNotification0
     */
    public void startpushNotification(
        com.danateq.soap.link.pnm.PushNotification pushNotification0,
        final AsyncPushNotificationHandler callback)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature for Asynchronous Invocations
     *
     * @param updateParameter2
     */
    public void startupdateParameter(
        com.danateq.soap.link.pnm.UpdateParameter updateParameter2,
        final AsyncPushNotificationHandler callback)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature for Asynchronous Invocations
     *
     * @param notifOptRequest4
     */
    public void startnotifOpt(
        com.danateq.soap.link.pnm.NotifOptRequest notifOptRequest4,
        final AsyncPushNotificationHandler callback)
        throws java.rmi.RemoteException;

    //
}
