package ph.com.ncs.vanilla.raven.ws.push.stubs;

import com.danateq.soap.link.pnm.NotifOptResponse;
import com.danateq.soap.link.pnm.PushNotificationResp;
import com.danateq.soap.link.pnm.UpdateParameterResp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by ace on 6/13/16.
 */
public class NotificationCallBackHandler extends AsyncPushNotificationHandler {

    private static final Logger logger = LoggerFactory.getLogger(NotificationCallBackHandler.class);

    @Override
    public Object getClientData() {
        return super.getClientData();
    }

    @Override
    public void receiveResultpushNotification(PushNotificationResp result) {
        super.receiveResultpushNotification(result);
        logger.info("receiveResultpushNotification : START : " + result.getPushNotifResult());
    }

    @Override
    public void receiveErrorpushNotification(Exception e) {
        super.receiveErrorpushNotification(e);
        logger.info("receiveErrorpushNotification : START");
    }

    @Override
    public void receiveResultupdateParameter(UpdateParameterResp result) {
        super.receiveResultupdateParameter(result);
        logger.info("receiveResultupdateParameter : START");
    }

    @Override
    public void receiveErrorupdateParameter(Exception e) {
        super.receiveErrorupdateParameter(e);
        logger.info("receiveErrorupdateParameter : START");
    }

    @Override
    public void receiveResultnotifOpt(NotifOptResponse result) {
        super.receiveResultnotifOpt(result);
        logger.info("receiveResultnotifOpt : START");
    }

    @Override
    public void receiveErrornotifOpt(Exception e) {
        super.receiveErrornotifOpt(e);
        logger.info("receiveErrornotifOpt : START");
    }
}
