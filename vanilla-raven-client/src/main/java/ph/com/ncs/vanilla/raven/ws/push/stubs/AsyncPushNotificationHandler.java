/**
 * AsyncPushNotificationHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:08:57 BST)
 */
package ph.com.ncs.vanilla.raven.ws.push.stubs;


/**
 *  AsyncPushNotificationHandler Callback class, Users can extend this class and implement
 *  their own receiveResult and receiveError methods.
 */
public abstract class AsyncPushNotificationHandler {
    protected Object clientData;

    /**
     * User can pass in any object that needs to be accessed once the NonBlocking
     * Web service call is finished and appropriate method of this CallBack is called.
     * @param clientData Object mechanism by which the user can pass in user data
     * that will be avilable at the time this callback is called.
     */
    public AsyncPushNotificationHandler(Object clientData) {
        this.clientData = clientData;
    }

    /**
     * Please use this constructor if you don't want to set any clientData
     */
    public AsyncPushNotificationHandler() {
        this.clientData = null;
    }

    /**
     * Get the client data
     */
    public Object getClientData() {
        return clientData;
    }

    /**
     * auto generated Axis2 call back method for pushNotification method
     * override this method for handling normal response from pushNotification operation
     */
    public void receiveResultpushNotification(
        com.danateq.soap.link.pnm.PushNotificationResp result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from pushNotification operation
     */
    public void receiveErrorpushNotification(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for updateParameter method
     * override this method for handling normal response from updateParameter operation
     */
    public void receiveResultupdateParameter(
        com.danateq.soap.link.pnm.UpdateParameterResp result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from updateParameter operation
     */
    public void receiveErrorupdateParameter(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for notifOpt method
     * override this method for handling normal response from notifOpt operation
     */
    public void receiveResultnotifOpt(
        com.danateq.soap.link.pnm.NotifOptResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from notifOpt operation
     */
    public void receiveErrornotifOpt(java.lang.Exception e) {
    }
}
