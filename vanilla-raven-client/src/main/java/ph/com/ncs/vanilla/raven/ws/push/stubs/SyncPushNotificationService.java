/**
 * SyncPushNotificationService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:08:57 BST)
 */
package ph.com.ncs.vanilla.raven.ws.push.stubs;


/*
 *  SyncPushNotificationService java interface
 */
public interface SyncPushNotificationService {
    /**
     * Auto generated method signature
     *
     * @param pushNotification
     */
    public com.danateq.soap.link.pnm.PushNotificationResp pushNotification(
        com.danateq.soap.link.pnm.PushNotification pushNotification)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param updateParameter
     */
    public com.danateq.soap.link.pnm.UpdateParameterResp updateParameter(
        com.danateq.soap.link.pnm.UpdateParameter updateParameter)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param notifOptRequest
     */
    public com.danateq.soap.link.pnm.NotifOptResponse notifOpt(
        com.danateq.soap.link.pnm.NotifOptRequest notifOptRequest)
        throws java.rmi.RemoteException;

    //
}
