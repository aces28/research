/**
 * PushNotification.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:09:26 BST)
 */
package com.danateq.soap.link.pnm;


/**
 *  PushNotification bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class PushNotification implements org.apache.axis2.databinding.ADBBean {
    public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName("http://DANATEQ.com/soap/link/pnm/",
            "pushNotification", "ns1");

    /**
     * field for UserIdentity
     */
    protected java.lang.String localUserIdentity;

    /**
     * field for UserIdentityType
     */
    protected int localUserIdentityType;

    /**
     * field for NotifPatternId
     */
    protected int localNotifPatternId;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNotifPatternIdTracker = false;

    /**
     * field for NotifGroupId
     */
    protected int localNotifGroupId;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNotifGroupIdTracker = false;

    /**
     * field for NotifPriority
     */
    protected int localNotifPriority;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNotifPriorityTracker = false;

    /**
     * field for NotifText
     */
    protected java.lang.String localNotifText;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNotifTextTracker = false;

    /**
     * field for NotifParam
     * This was an Array!
     */
    protected com.danateq.soap.link.pnm.TNotifParam[] localNotifParam;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNotifParamTracker = false;

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getUserIdentity() {
        return localUserIdentity;
    }

    /**
     * Auto generated setter method
     * @param param UserIdentity
     */
    public void setUserIdentity(java.lang.String param) {
        this.localUserIdentity = param;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getUserIdentityType() {
        return localUserIdentityType;
    }

    /**
     * Auto generated setter method
     * @param param UserIdentityType
     */
    public void setUserIdentityType(int param) {
        this.localUserIdentityType = param;
    }

    public boolean isNotifPatternIdSpecified() {
        return localNotifPatternIdTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNotifPatternId() {
        return localNotifPatternId;
    }

    /**
     * Auto generated setter method
     * @param param NotifPatternId
     */
    public void setNotifPatternId(int param) {
        // setting primitive attribute tracker to true
        localNotifPatternIdTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNotifPatternId = param;
    }

    public boolean isNotifGroupIdSpecified() {
        return localNotifGroupIdTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNotifGroupId() {
        return localNotifGroupId;
    }

    /**
     * Auto generated setter method
     * @param param NotifGroupId
     */
    public void setNotifGroupId(int param) {
        // setting primitive attribute tracker to true
        localNotifGroupIdTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNotifGroupId = param;
    }

    public boolean isNotifPrioritySpecified() {
        return localNotifPriorityTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNotifPriority() {
        return localNotifPriority;
    }

    /**
     * Auto generated setter method
     * @param param NotifPriority
     */
    public void setNotifPriority(int param) {
        // setting primitive attribute tracker to true
        localNotifPriorityTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNotifPriority = param;
    }

    public boolean isNotifTextSpecified() {
        return localNotifTextTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getNotifText() {
        return localNotifText;
    }

    /**
     * Auto generated setter method
     * @param param NotifText
     */
    public void setNotifText(java.lang.String param) {
        localNotifTextTracker = param != null;

        this.localNotifText = param;
    }

    public boolean isNotifParamSpecified() {
        return localNotifParamTracker;
    }

    /**
     * Auto generated getter method
     * @return com.danateq.soap.link.pnm.TNotifParam[]
     */
    public com.danateq.soap.link.pnm.TNotifParam[] getNotifParam() {
        return localNotifParam;
    }

    /**
     * validate the array for NotifParam
     */
    protected void validateNotifParam(
        com.danateq.soap.link.pnm.TNotifParam[] param) {
        if ((param != null) && (param.length > 15)) {
            throw new java.lang.RuntimeException(
                "Input values do not follow defined XSD restrictions");
        }
    }

    /**
     * Auto generated setter method
     * @param param NotifParam
     */
    public void setNotifParam(com.danateq.soap.link.pnm.TNotifParam[] param) {
        validateNotifParam(param);

        localNotifParamTracker = param != null;

        this.localNotifParam = param;
    }

    /**
     * Auto generated add method for the array for convenience
     * @param param com.danateq.soap.link.pnm.TNotifParam
     */
    public void addNotifParam(com.danateq.soap.link.pnm.TNotifParam param) {
        if (localNotifParam == null) {
            localNotifParam = new com.danateq.soap.link.pnm.TNotifParam[] {  };
        }

        //update the setting tracker
        localNotifParamTracker = true;

        java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localNotifParam);
        list.add(param);
        this.localNotifParam = (com.danateq.soap.link.pnm.TNotifParam[]) list.toArray(new com.danateq.soap.link.pnm.TNotifParam[list.size()]);
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, MY_QNAME));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "http://DANATEQ.com/soap/link/pnm/");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":pushNotification", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "pushNotification", xmlWriter);
            }
        }

        namespace = "";
        writeStartElement(null, namespace, "userIdentity", xmlWriter);

        if (localUserIdentity == null) {
            // write the nil attribute
            throw new org.apache.axis2.databinding.ADBException(
                "userIdentity cannot be null!!");
        } else {
            xmlWriter.writeCharacters(localUserIdentity);
        }

        xmlWriter.writeEndElement();

        namespace = "";
        writeStartElement(null, namespace, "userIdentityType", xmlWriter);

        if (localUserIdentityType == java.lang.Integer.MIN_VALUE) {
            throw new org.apache.axis2.databinding.ADBException(
                "userIdentityType cannot be null!!");
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    localUserIdentityType));
        }

        xmlWriter.writeEndElement();

        if (localNotifPatternIdTracker) {
            namespace = "";
            writeStartElement(null, namespace, "notifPatternId", xmlWriter);

            if (localNotifPatternId == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "notifPatternId cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNotifPatternId));
            }

            xmlWriter.writeEndElement();
        }

        if (localNotifGroupIdTracker) {
            namespace = "";
            writeStartElement(null, namespace, "notifGroupId", xmlWriter);

            if (localNotifGroupId == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "notifGroupId cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNotifGroupId));
            }

            xmlWriter.writeEndElement();
        }

        if (localNotifPriorityTracker) {
            namespace = "";
            writeStartElement(null, namespace, "notifPriority", xmlWriter);

            if (localNotifPriority == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "notifPriority cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNotifPriority));
            }

            xmlWriter.writeEndElement();
        }

        if (localNotifTextTracker) {
            namespace = "";
            writeStartElement(null, namespace, "notifText", xmlWriter);

            if (localNotifText == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "notifText cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localNotifText);
            }

            xmlWriter.writeEndElement();
        }

        if (localNotifParamTracker) {
            if (localNotifParam != null) {
                for (int i = 0; i < localNotifParam.length; i++) {
                    if (localNotifParam[i] != null) {
                        localNotifParam[i].serialize(new javax.xml.namespace.QName(
                                "", "notifParam"), xmlWriter);
                    } else {
                        // we don't have to do any thing since minOccures is zero
                    }
                }
            } else {
                throw new org.apache.axis2.databinding.ADBException(
                    "notifParam cannot be null!!");
            }
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals("http://DANATEQ.com/soap/link/pnm/")) {
            return "ns1";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static PushNotification parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            PushNotification object = new PushNotification();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"pushNotification".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (PushNotification) com.danateq.soap.link.pnm.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                java.util.ArrayList list7 = new java.util.ArrayList();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "userIdentity").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "userIdentity").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "userIdentity" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setUserIdentity(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "userIdentityType").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "userIdentityType").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "userIdentityType" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setUserIdentityType(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "notifPatternId").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "notifPatternId").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "notifPatternId" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNotifPatternId(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNotifPatternId(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "notifGroupId").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "notifGroupId").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "notifGroupId" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNotifGroupId(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNotifGroupId(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "notifPriority").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "notifPriority").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "notifPriority" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNotifPriority(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNotifPriority(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "notifText").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "notifText").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "notifText" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNotifText(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "notifParam").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "notifParam").equals(
                            reader.getName())) {
                    // Process the array and step past its final element's end.
                    list7.add(com.danateq.soap.link.pnm.TNotifParam.Factory.parse(
                            reader));

                    //loop until we find a start element that is not part of this array
                    boolean loopDone7 = false;

                    while (!loopDone7) {
                        // We should be at the end element, but make sure
                        while (!reader.isEndElement())
                            reader.next();

                        // Step out of this element
                        reader.next();

                        // Step to next element event.
                        while (!reader.isStartElement() &&
                                !reader.isEndElement())
                            reader.next();

                        if (reader.isEndElement()) {
                            //two continuous end elements means we are exiting the xml structure
                            loopDone7 = true;
                        } else {
                            if (new javax.xml.namespace.QName("", "notifParam").equals(
                                        reader.getName())) {
                                list7.add(com.danateq.soap.link.pnm.TNotifParam.Factory.parse(
                                        reader));
                            } else {
                                loopDone7 = true;
                            }
                        }
                    }

                    // call the converter utility  to convert and set the array
                    object.setNotifParam((com.danateq.soap.link.pnm.TNotifParam[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                            com.danateq.soap.link.pnm.TNotifParam.class, list7));
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
