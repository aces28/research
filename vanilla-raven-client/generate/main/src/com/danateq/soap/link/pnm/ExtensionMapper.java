/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:09:26 BST)
 */
package com.danateq.soap.link.pnm;


/**
 *  ExtensionMapper class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class ExtensionMapper {
    public static java.lang.Object getTypeObject(
        java.lang.String namespaceURI, java.lang.String typeName,
        javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
        if ("http://DANATEQ.com/soap/link/pnm/".equals(namespaceURI) &&
                "tNotifParam".equals(typeName)) {
            return com.danateq.soap.link.pnm.TNotifParam.Factory.parse(reader);
        }

        if ("http://DANATEQ.com/soap/link/pnm/".equals(namespaceURI) &&
                "tOperation".equals(typeName)) {
            return com.danateq.soap.link.pnm.TOperation.Factory.parse(reader);
        }

        if ("http://DANATEQ.com/soap/link/pnm/".equals(namespaceURI) &&
                "tParameter".equals(typeName)) {
            return com.danateq.soap.link.pnm.TParameter.Factory.parse(reader);
        }

        throw new org.apache.axis2.databinding.ADBException("Unsupported type " +
            namespaceURI + " " + typeName);
    }
}
