package ph.com.ncs.vanilla.ws.approval.controller;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ph.com.ncs.vanilla.account.service.AccountService;
import ph.com.ncs.vanilla.application.domain.SubsApplication;
import ph.com.ncs.vanilla.approval.service.ApprovalService;
import ph.com.ncs.vanilla.commons.constants.VanillaConstants;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;
import ph.com.ncs.vanilla.rejection.domain.Rejection;
import ph.com.ncs.vanilla.rejection.service.RejectionService;
import ph.com.ncs.vanilla.reports.domain.ApplicationReports;
import ph.com.ncs.vanilla.reports.domain.ApprovalReport;
import ph.com.ncs.vanilla.reports.domain.ApproveUnpaidReport;
import ph.com.ncs.vanilla.reports.domain.DisapproveReports;
import ph.com.ncs.vanilla.subscriber.domain.Subscriber;
import ph.com.ncs.vanilla.subscriber.service.SubscriberAppService;
import ph.com.ncs.vanilla.subscriber.service.SubscriberService;
import ph.com.ncs.vanilla.transaction.domain.Transaction;
import ph.com.ncs.vanilla.ws.audit.controller.AuditForm;

/**
 * Created by edjohna on 6/25/2016.
 */
@RestController
@RequestMapping("/approval")
public class ApprovalController {

    private static final Logger logger = LoggerFactory.getLogger(ApprovalController.class);

    @Autowired
    private AccountService accountService;
    @Autowired
    private ApprovalService approvalService;
    @Autowired
    private SubscriberAppService subsApplicationService;
    @Autowired
    private SubscriberService subscriberService;
    @Autowired
    private RejectionService rejectionService;
    @Autowired
    private ApproverForm approverForm;
    @Autowired
    private AuditForm auditForm;
    @Autowired
    private Environment environment;

    @RequestMapping(value = "/forApproval/{userName}/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<SubsApplication> forApproval(@PathVariable String userName, @PathVariable int pageNumber) {
        Page<SubsApplication> approvalsPage = approvalService.listForApprovalApprover(userName, pageNumber);
        approverForm.saveUserAccessAuditLogs(userName, Integer.parseInt(environment.getRequiredProperty("module.approver.for.approval")));
        return approvalsPage;
    }

    @RequestMapping(value = "/approved/{userName}/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<SubsApplication> approved(@PathVariable String userName, @PathVariable int pageNumber) {
        Page<SubsApplication> approvalsPage = approvalService.listApprovedApprover(userName, pageNumber);
        approverForm.saveUserAccessAuditLogs(userName, Integer.parseInt(environment.getRequiredProperty("module.approver.approved.unpaid")));
        return approvalsPage;
    }

    @RequestMapping(value = "/disapproved/{userName}/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<SubsApplication> disapproved(@PathVariable String userName, @PathVariable int pageNumber) {
        Page<SubsApplication> approvalsPage = approvalService.listDisapprovedApprover(userName, pageNumber);
        approverForm.saveUserAccessAuditLogs(userName, Integer.parseInt(environment.getRequiredProperty("module.approver.disapproved")));
        return approvalsPage;
    }

    @RequestMapping(value = "/filterApprovedApprover", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private Page<SubsApplication> filterApprovedApprover(@RequestBody Map<String, Object> map) {
        Date to = new Date(VanillaDateUtils.longDateToFormat(map.get("to").toString()));
        Date from = new Date(VanillaDateUtils.longDateFromFormat(map.get("from").toString()));
        String userName = map.get("userId").toString();
        int pageNumber = Integer.parseInt(map.get("pageNumber").toString());

        Page<SubsApplication> subsApplicationsPage = approvalService.filterApprovedApprover(userName, new Timestamp(to.getTime()), new Timestamp(from.getTime()), pageNumber);
        return subsApplicationsPage;
    }

    @RequestMapping(value = "/filterDisapprovedApprover", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private Page<SubsApplication> filterDisapprovedApprover(@RequestBody Map<String, Object> map) {
        Date to = new Date(VanillaDateUtils.longDateToFormat(map.get("to").toString()));
        Date from = new Date(VanillaDateUtils.longDateFromFormat(map.get("from").toString()));
        String userName = map.get("userId").toString();
        int pageNumber = Integer.parseInt(map.get("pageNumber").toString());

        Page<SubsApplication> subsApplicationsPage = approvalService.filterDisapprovedApprover(userName, new Timestamp(to.getTime()), new Timestamp(from.getTime()), pageNumber);
        return subsApplicationsPage;
    }

    @RequestMapping(value = "/filterForApprovalApprover", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private Page<SubsApplication> filterForApprovalApprover(@RequestBody Map<String, Object> map) {
        Date to = new Date(VanillaDateUtils.longDateToFormat(map.get("to").toString()));
        Date from = new Date(VanillaDateUtils.longDateFromFormat(map.get("from").toString()));
        logger.info("Date from : " + map.get("from").toString() + " : " + from + " : date to : " + map.get("to").toString() + " : " + to);
        String userName = map.get("userId").toString();
        int pageNumber = Integer.parseInt(map.get("pageNumber").toString());
        Page<SubsApplication> subsApplicationsPage = approvalService.filterForApprovalApprover(userName, new Timestamp(to.getTime()), new Timestamp(from.getTime()), pageNumber);
        return subsApplicationsPage;

    }

    @RequestMapping(value = "/approveApplication", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private SubsApplication approveApplication(@RequestBody Map<String, Object> map) {
        int appId = Integer.parseInt(map.get("appId").toString());
        logger.info("APPROVE APPLICATION : START : " + appId);
        SubsApplication subsApplication = new SubsApplication();
        Date approvalDate = new Date(VanillaDateUtils.longCurrentDate());
        Timestamp updateDate = new Timestamp(VanillaDateUtils.longCurrentDate());
        subsApplication.setId(appId);
        SubsApplication approveApplication = subsApplicationService.get(subsApplication);
        if (accountService.msisdnLookup(approveApplication.getSubscriberMsisdn())) {
            logger.info("===== MSISDN IS EXISTING IN ACCOUNT TABLE " + approveApplication.getSubscriberMsisdn() + " ======");
            if ("SUCCESS".equalsIgnoreCase(approverForm.invokeNF(approveApplication.getSubscriberMsisdn(), "plan2"))) {
//            if (true) {
                Transaction transaction = new Transaction();
                transaction.setMsisdn(approveApplication.getSubscriberMsisdn());
                transaction.setMode(8);

                if (approverForm.invokeRaven(transaction) == 0) {
//                if (true) {
                    approveApplication.setStatus(3);
                }

                approveApplication.setApprovalDate(approvalDate);
                approveApplication.setUpdatedDate(updateDate);
                return subsApplicationService.save(approveApplication);
            } else {
                approveApplication.setStatus(400);
                return approveApplication;
            }

        } else {
            logger.info("===== MSISDN IS NOT EXISTING IN ACCOUNT TABLE " + approveApplication.getSubscriberMsisdn() + " ======");
            if ("SUCCESS".equalsIgnoreCase(approverForm.invokeNF(approveApplication.getSubscriberMsisdn(), "plan1"))) {
//            if (true) {
                Transaction transaction = new Transaction();
                transaction.setMsisdn(approveApplication.getSubscriberMsisdn());
                transaction.setMode(7);

                if (approverForm.invokeRaven(transaction) == 0) {
//                if (true) {
                    approveApplication.setStatus(3);
                }

                approveApplication.setApprovalDate(approvalDate);
                approveApplication.setUpdatedDate(updateDate);
                return subsApplicationService.save(approveApplication);
            } else {
                approveApplication.setStatus(400);
                return approveApplication;
            }
        }
    }

    @RequestMapping(value = "/editApplication", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private SubsApplication editApplication(@RequestBody SubsApplication subsApplication) {
        logger.info("edit application : START");

        Timestamp timestamp = new Timestamp(VanillaDateUtils.longCurrentDate());
        SubsApplication application = subsApplicationService.get(subsApplication);

        Subscriber subscriberReq = subsApplication.getSubscriber();
        application.setUserEmail(subsApplication.getUserEmail());
        application.setUpdatedDate(timestamp);

        Subscriber subscriber = application.getSubscriber();
        subscriber.setGender(subscriberReq.getGender());
        subscriber.setFirstName(subscriberReq.getFirstName());
        subscriber.setMiddleName(subscriberReq.getMiddleName());
        subscriber.setLastName(subscriberReq.getLastName());
        subscriber.setBirthday(subscriberReq.getBirthday());
        subscriber.setAddress(subscriberReq.getAddress());
        subscriber.setUpdatedDate(timestamp);
        subscriber.setMothersName(subscriberReq.getMothersName());

        application.setSubscriber(subscriber);
        
        // application is edited by the user
        application.setStatus(2);
        
        return subsApplicationService.save(application);
    }

    @RequestMapping(value = "/disapproveApplication", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private SubsApplication disapproveApplication(@RequestBody Map<String, Object> map) {
        int applicationId = Integer.valueOf(String.valueOf(map.get("appId")));
        String rejectionName = String.valueOf(map.get("reason"));
        String rejectionDesc = "";
        Timestamp currentDate = new Timestamp(VanillaDateUtils.longCurrentDate());
        if (!StringUtils.isEmpty(String.valueOf(map.get("note")))) {
            rejectionDesc = String.valueOf(map.get("note"));
        }

        String user = VanillaConstants.SYSTEM_USER;

        logger.info("invokeRaven : " + applicationId + " : " + rejectionName + " : " + rejectionDesc);

        SubsApplication subsApplication = new SubsApplication();
        subsApplication.setId(applicationId);

        Rejection rejection = new Rejection();
        SubsApplication approveApplication = subsApplicationService.get(subsApplication);

        Transaction transaction = new Transaction();
        logger.info("retrieve application : msisdn : " + approveApplication.getSubscriberMsisdn());
        transaction.setMsisdn(approveApplication.getSubscriberMsisdn());
        transaction.setMode(6);
        if (approverForm.invokeRaven(transaction) == 0) {
//        if (true) {
            approveApplication.setStatus(4);
        }

        rejection.setReasonName(rejectionName);
        rejection.setReasonDescription(rejectionDesc);
        rejection.setApplicationId(applicationId);
        rejection.setCreatedDate(currentDate);
        rejection.setUpdatedDate(currentDate);

        rejection.setCreatedBy(user);
        rejection.setUpdatedBy(user);
        approveApplication.setRejection(rejection);
        approveApplication = subsApplicationService.save(approveApplication);

        return approveApplication;
    }

    @RequestMapping(value = "/searchForApproval/{keyword}/{userName}/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<SubsApplication> searchForApproval(@PathVariable String keyword, @PathVariable String userName, @PathVariable int pageNumber) {
        logger.info("searchForApproval() : START");
        return approvalService.searchForApprovalString(keyword, userName, pageNumber);
    }

    @RequestMapping(value = "/searchApproved/{keyword}/{userName}/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<SubsApplication> searchApproved(@PathVariable String keyword, @PathVariable String userName, @PathVariable int pageNumber) {
        logger.info("searchApproved() : START");
        return approvalService.searchApprovedString(keyword, userName, pageNumber);
    }

    @RequestMapping(value = "/searchDisapproved/{keyword}/{userName}/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<SubsApplication> searchDisapproved(@PathVariable String keyword, @PathVariable String userName, @PathVariable int pageNumber) {
        logger.info("searchDisapproved() : START");
        return approvalService.searchDispprovedString(keyword, userName, pageNumber);
    }

    @RequestMapping(value = "/extractApplicationReports", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private <T> T extractApplicationReports(@RequestBody Map<String, Object> map) {
        logger.info("extractApplicationReports() : START");
        String userName = map.get("userName").toString();
        logger.info("Approval user : " + userName);

        List<ApplicationReports> applicationReportsList = Collections.synchronizedList(new ArrayList<ApplicationReports>());
        List<DisapproveReports> disapproveReports = Collections.synchronizedList(new ArrayList<DisapproveReports>());
        List<ApprovalReport> approvalReports = Collections.synchronizedList(new ArrayList<ApprovalReport>());
        List<ApproveUnpaidReport> approveUnpaidReports = Collections.synchronizedList(new ArrayList<ApproveUnpaidReport>());

        ApplicationReports applicationHeader = createApplicationHeader();
        DisapproveReports disapproveHeader = createDisapproveHeader();
        ApprovalReport approvalHeader = createApprovalHeader();
        ApproveUnpaidReport approveUnpaidHeader = createApproveUnpaidHeader();

        if ("forApprovalString".equalsIgnoreCase(map.get("action").toString())) {
            logger.info("Extract Application Reports For Approval String : START");
            List<SubsApplication> applicationsPage = approvalService.extractForApprovalString(map.get("keyword").toString(), map.get("userName").toString());
            approvalReports.add(approvalHeader);

            for (SubsApplication subsApplication : applicationsPage) {
                Subscriber subscriber = subscriberService.get(subsApplication.getSubscriber());
                ApprovalReport approvalReport = createApprovalRow(subsApplication, subscriber);
                approvalReports.add(approvalReport);
            }
            return (T) approvalReports;
        } else if ("approvedString".equalsIgnoreCase(map.get("action").toString())) {
            logger.info("Extract Application Reports Approved String : START");
            List<SubsApplication> applicationsPage = approvalService.extractApprovedString(map.get("keyword").toString(), map.get("userName").toString());
            approveUnpaidReports.add(approveUnpaidHeader);

            for (SubsApplication subsApplication : applicationsPage) {
                Subscriber subscriber = subscriberService.get(subsApplication.getSubscriber());
                ApproveUnpaidReport approveUnpaidReport = createApproveUnpaidRow(subsApplication, subscriber);
                approveUnpaidReports.add(approveUnpaidReport);
            }

            return (T) approveUnpaidReports;
        } else if ("disapprovedString".equalsIgnoreCase(map.get("action").toString())) {
            logger.info("Extract Application Reports Disapproved String : START");
            List<SubsApplication> applicationsPage = approvalService.extractDispprovedString(map.get("keyword").toString(), map.get("userName").toString());
            disapproveReports.add(disapproveHeader);

            for (SubsApplication subsApplication : applicationsPage) {
                Subscriber subscriber = subscriberService.get(subsApplication.getSubscriber());
                DisapproveReports disapproveReport = createDisapproveRow(subsApplication, subscriber);
                disapproveReports.add(disapproveReport);
            }
            return (T) disapproveReports;

        } else if ("disapprovedDate".equalsIgnoreCase(map.get("action").toString())) {
            logger.info("Extract Application Reports Disapproved Date : START");
            Date to = new Date(VanillaDateUtils.longDateToFormat(map.get("to").toString()));
            Date from = new Date(VanillaDateUtils.longDateFromFormat(map.get("from").toString()));
            List<SubsApplication> applicationsPage = approvalService.extractDisapprovedApprover(map.get("userName").toString(), new Timestamp(to.getTime()), new Timestamp(from.getTime()));
            disapproveReports.add(disapproveHeader);

            for (SubsApplication subsApplication : applicationsPage) {
                Subscriber subscriber = subscriberService.get(subsApplication.getSubscriber());
                DisapproveReports disapproveReport = createDisapproveRow(subsApplication, subscriber);
                disapproveReports.add(disapproveReport);
            }
            return (T) disapproveReports;

        } else if ("approvedDate".equalsIgnoreCase(map.get("action").toString())) {

            logger.info("Extract Application Reports Approved Date : START");

            Date to = new Date(VanillaDateUtils.longDateToFormat(map.get("to").toString()));
            Date from = new Date(VanillaDateUtils.longDateFromFormat(map.get("from").toString()));
            List<SubsApplication> applicationsPage = approvalService.extractApprovedApprover(map.get("userName").toString(), new Timestamp(to.getTime()), new Timestamp(from.getTime()));
            approveUnpaidReports.add(approveUnpaidHeader);

            for (SubsApplication subsApplication : applicationsPage) {
                Subscriber subscriber = subscriberService.get(subsApplication.getSubscriber());
                ApproveUnpaidReport approveUnpaidReport = createApproveUnpaidRow(subsApplication, subscriber);
                approveUnpaidReports.add(approveUnpaidReport);
            }

            return (T) approveUnpaidReports;

        } else if ("forApprovalDate".equalsIgnoreCase(map.get("action").toString())) {
            logger.info("Extract Application Reports For Approval Date : START");
            Date to = new Date(VanillaDateUtils.longDateToFormat(map.get("to").toString()));
            Date from = new Date(VanillaDateUtils.longDateFromFormat(map.get("from").toString()));
            List<SubsApplication> applicationsPage = approvalService.extractForApprovalApprover(map.get("userName").toString(), new Timestamp(to.getTime()), new Timestamp(from.getTime()));
            approvalReports.add(approvalHeader);

            for (SubsApplication subsApplication : applicationsPage) {
                Subscriber subscriber = subscriberService.get(subsApplication.getSubscriber());
                ApprovalReport approvalReport = createApprovalRow(subsApplication, subscriber);
                approvalReports.add(approvalReport);
            }
            return (T) approvalReports;
        } else if ("allApproved".equalsIgnoreCase(map.get("action").toString())) {
            logger.info("Extract Application Reports All Approval : START");
            List<SubsApplication> applicationsPage = approvalService.extractAllApproved(map.get("userName").toString());
            approveUnpaidReports.add(approveUnpaidHeader);

            for (SubsApplication subsApplication : applicationsPage) {
                Subscriber subscriber = subscriberService.get(subsApplication.getSubscriber());
                ApproveUnpaidReport approveUnpaidReport = createApproveUnpaidRow(subsApplication, subscriber);
                approveUnpaidReports.add(approveUnpaidReport);
            }

            return (T) approveUnpaidReports;

        } else if ("allForApproval".equalsIgnoreCase(map.get("action").toString())) {
            logger.info("Extract Application Reports All Disapproved : START");
            List<SubsApplication> applicationsPage = approvalService.extractAllForApproval(userName);
            approvalReports.add(approvalHeader);

            for (SubsApplication subsApplication : applicationsPage) {
                Subscriber subscriber = subscriberService.get(subsApplication.getSubscriber());
                ApprovalReport approvalReport = createApprovalRow(subsApplication, subscriber);
                approvalReports.add(approvalReport);
            }
            return (T) approvalReports;
        } else if ("allDisapproved".equalsIgnoreCase(map.get("action").toString())) {
            logger.info("Extract Application Reports All Disapproved : START");
            List<SubsApplication> applicationsPage = approvalService.extractAllDisapproved(map.get("userName").toString());
            disapproveReports.add(disapproveHeader);

            for (SubsApplication subsApplication : applicationsPage) {
                Subscriber subscriber = subscriberService.get(subsApplication.getSubscriber());
                DisapproveReports disapproveReport = createDisapproveRow(subsApplication, subscriber);
                disapproveReports.add(disapproveReport);
            }
            return (T) disapproveReports;
        }

        return (T) applicationReportsList;

    }

    private ApproveUnpaidReport createApproveUnpaidRow(SubsApplication subsApplication, Subscriber subscriber) {
        ApproveUnpaidReport approveUnpaidReport = new ApproveUnpaidReport();
        approveUnpaidReport.setApplicationDate(VanillaDateUtils.formatDate(subsApplication.getApplicationDate()));
        approveUnpaidReport.setMsisdn(subsApplication.getSubscriberMsisdn());
        approveUnpaidReport.setFirstName(subscriber.getFirstName());
        approveUnpaidReport.setMiddleName(subscriber.getMiddleName());
        approveUnpaidReport.setLastName(subscriber.getLastName());
        approveUnpaidReport.setEmail(subsApplication.getUserEmail());
        approveUnpaidReport.setAddress(subscriber.getAddress());
        approveUnpaidReport.setBirthday(VanillaDateUtils.formatDate(subscriber.getBirthday()));
        approveUnpaidReport.setGender(subscriber.getGender());
        approveUnpaidReport.setMothersName(subscriber.getMothersName());
        approveUnpaidReport.setAccountType(subsApplication.getAccountType());
        approveUnpaidReport.setAttachment(environment.getRequiredProperty("cloud.aws.s3.bucket.url") + subsApplication.getAttachment());
        approveUnpaidReport.setTermsCondition(environment.getRequiredProperty("cloud.aws.s3.bucket.url") + subsApplication.getTermsConditions());
        approveUnpaidReport.setApprovedDate(VanillaDateUtils.formatDate(subsApplication.getApprovalDate()));
        String userName = "";
        if (subsApplication.getUser() != null) {
            userName = subsApplication.getUser().getUserName();
        }
        return approveUnpaidReport;
    }

    private ApprovalReport createApprovalRow(SubsApplication subsApplication, Subscriber subscriber) {
        ApprovalReport approvalReport = new ApprovalReport();
        approvalReport.setApplicationDate(VanillaDateUtils.formatDate(subsApplication.getApplicationDate()));
        approvalReport.setMsisdn(subsApplication.getSubscriberMsisdn());
        approvalReport.setFirstName(subscriber.getFirstName());
        approvalReport.setMiddleName(subscriber.getMiddleName());
        approvalReport.setLastName(subscriber.getLastName());
        approvalReport.setEmail(subsApplication.getUserEmail());
        approvalReport.setAddress(subscriber.getAddress());
        approvalReport.setBirthday(VanillaDateUtils.formatDate(subscriber.getBirthday()));
        approvalReport.setGender(subscriber.getGender());
        approvalReport.setMothersName(subscriber.getMothersName());
        approvalReport.setAccountType(subsApplication.getAccountType());
        approvalReport.setAttachment(environment.getRequiredProperty("cloud.aws.s3.bucket.url") + subsApplication.getAttachment());
        approvalReport.setTermsCondition(environment.getRequiredProperty("cloud.aws.s3.bucket.url") + subsApplication.getTermsConditions());

        return approvalReport;
    }

    private DisapproveReports createDisapproveRow(SubsApplication subsApplication, Subscriber subscriber) {
        DisapproveReports disapproveReports = new DisapproveReports();
        disapproveReports.setApplicationDate(VanillaDateUtils.formatDate(subsApplication.getApplicationDate()));
        disapproveReports.setMsisdn(subsApplication.getSubscriberMsisdn());
        disapproveReports.setFirstName(subscriber.getFirstName());
        disapproveReports.setMiddleName(subscriber.getMiddleName());
        disapproveReports.setLastName(subscriber.getLastName());
        disapproveReports.setEmail(subsApplication.getUserEmail());
        disapproveReports.setAddress(subscriber.getAddress());
        disapproveReports.setBirthday(VanillaDateUtils.formatDate(subscriber.getBirthday()));
        disapproveReports.setGender(subscriber.getGender());
        disapproveReports.setMothersName(subscriber.getMothersName());
        disapproveReports.setAccountType(subsApplication.getAccountType());
        disapproveReports.setAttachment(environment.getRequiredProperty("cloud.aws.s3.bucket.url") + subsApplication.getAttachment());
        disapproveReports.setTermsCondition(environment.getRequiredProperty("cloud.aws.s3.bucket.url") + subsApplication.getTermsConditions());
        String reasonDesc = "";
        String reasonName = "";
        if (subsApplication.getRejection() != null) {
            reasonDesc = subsApplication.getRejection().getReasonDescription();
            reasonName = subsApplication.getRejection().getReasonName();
        }
        disapproveReports.setAddNotes(reasonDesc);
        disapproveReports.setReason(reasonName);
        disapproveReports.setApprovedDate(VanillaDateUtils.formatDate(subsApplication.getApprovalDate()));
        String userName = "";
        if (subsApplication.getUser() != null) {
            userName = subsApplication.getUser().getUserName();
        }
        return disapproveReports;
    }

    private ApproveUnpaidReport createApproveUnpaidHeader() {
        ApproveUnpaidReport columnHeaders = new ApproveUnpaidReport();
        columnHeaders.setApplicationDate("Application Date");
        columnHeaders.setMsisdn("Mobile Number");
        columnHeaders.setFirstName("First Name");
        columnHeaders.setMiddleName("Middle Name");
        columnHeaders.setLastName("Last Name");
        columnHeaders.setEmail("Email Address");
        columnHeaders.setAddress("Home Address");
        columnHeaders.setBirthday("Birthday");
        columnHeaders.setGender("Gender");
        columnHeaders.setMothersName("Mother's Maiden Name");
        columnHeaders.setAccountType("Account Type");
        columnHeaders.setAttachment("Attachment");
        columnHeaders.setTermsCondition("T&C");
        columnHeaders.setApprovedDate("Approved Date");
        return columnHeaders;
    }

    private ApprovalReport createApprovalHeader() {
        ApprovalReport columnHeaders = new ApprovalReport();
        columnHeaders.setApplicationDate("Application Date");
        columnHeaders.setMsisdn("Mobile Number");
        columnHeaders.setFirstName("First Name");
        columnHeaders.setMiddleName("Middle Name");
        columnHeaders.setLastName("Last Name");
        columnHeaders.setEmail("Email Address");
        columnHeaders.setAddress("Home Address");
        columnHeaders.setBirthday("Birthday");
        columnHeaders.setGender("Gender");
        columnHeaders.setMothersName("Mother's Maiden Name");
        columnHeaders.setAccountType("Account Type");
        columnHeaders.setAttachment("Attachment");
        columnHeaders.setTermsCondition("T&C");
        return columnHeaders;
    }

    private DisapproveReports createDisapproveHeader() {
        DisapproveReports columnHeaders = new DisapproveReports();
        columnHeaders.setApplicationDate("Application Date");
        columnHeaders.setMsisdn("Mobile Number");
        columnHeaders.setFirstName("First Name");
        columnHeaders.setMiddleName("Middle Name");
        columnHeaders.setLastName("Last Name");
        columnHeaders.setEmail("Email Address");
        columnHeaders.setAddress("Home Address");
        columnHeaders.setBirthday("Birthday");
        columnHeaders.setGender("Gender");
        columnHeaders.setMothersName("Mother's Maiden Name");
        columnHeaders.setAccountType("Account Type");
        columnHeaders.setAttachment("Attachment");
        columnHeaders.setTermsCondition("T&C");
        columnHeaders.setAddNotes("Add Note");
        columnHeaders.setReason("Reason for Rejection");
        columnHeaders.setApprovedDate("Disapproved Date");
        return columnHeaders;
    }

    private ApplicationReports createApplicationHeader() {
        ApplicationReports columnHeaders = new ApplicationReports();
        columnHeaders.setApplicationDate("Application Date");
        columnHeaders.setMsisdn("Mobile Number");
        columnHeaders.setFirstName("First Name");
        columnHeaders.setMiddleName("Middle Name");
        columnHeaders.setLastName("Last Name");
        columnHeaders.setEmail("Email Address");
        columnHeaders.setAddress("Home Address");
        columnHeaders.setBirthday("Birthday");
        columnHeaders.setGender("Gender");
        columnHeaders.setMothersName("Mother's Maiden Name");
        columnHeaders.setAccountType("Account Type");
        columnHeaders.setAttachment("Attachment");
        columnHeaders.setTermsCondition("T&C");
        return columnHeaders;
    }
}