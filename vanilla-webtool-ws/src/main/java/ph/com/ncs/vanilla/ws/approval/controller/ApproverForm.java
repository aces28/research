package ph.com.ncs.vanilla.ws.approval.controller;

import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import ph.com.ncs.vanilla.audit.domain.AuditAccessTransaction;
import ph.com.ncs.vanilla.audit.domain.AuditModule;
import ph.com.ncs.vanilla.audit.service.AuditService;
import ph.com.ncs.vanilla.client.service.NfClientException;
import ph.com.ncs.vanilla.client.service.RavenClientException;
import ph.com.ncs.vanilla.client.service.VanillaNFClientService;
import ph.com.ncs.vanilla.client.service.VanillaRavenClientService;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;
import ph.com.ncs.vanilla.module.service.ModuleService;
import ph.com.ncs.vanilla.transaction.domain.Transaction;
import ph.com.ncs.vanilla.user.domain.User;
import ph.com.ncs.vanilla.user.service.UserService;

/**
 * Created by edjohna on 6/27/2016.
 */
@Component
public class ApproverForm{

    private static final Logger logger = LoggerFactory.getLogger(ApproverForm.class);

    @Autowired
    private VanillaNFClientService vanillaNFClientService;
    @Autowired
    private VanillaRavenClientService vanillaRavenClientService;
    @Autowired
    private AuditService auditService;
    @Autowired
    private UserService userService;
    @Autowired
    private ModuleService moduleService;

    public String invokeNF(String msisdn, String plan) {
        logger.info("send NF approval : START");
        Transaction transaction = new Transaction();
        if("plan1".equals(plan)) {
            logger.info("CREATING REQUEST FOR PLAN 1 STATUS");
            transaction.setMode(5);
            transaction.setMsisdn(msisdn);
            transaction.setId(1);
        }else if("plan2".equals(plan)){
            logger.info("CREATING REQUEST FOR PLAN 2 STATUS");
            transaction.setMode(6);
            transaction.setMsisdn(msisdn);
            transaction.setId(1);
        }
        String result = null;
        try {
            result = vanillaNFClientService.getResult(transaction);
        } catch (NfClientException e) {
            logger.error("Unable to send NF transaction : ");
        }
        return result;
    }

    public int invokeRaven(Transaction transaction) {
        logger.info("sending notification : " + transaction.getMode());
        int ravenResponse = 0;
        try {
            ravenResponse = vanillaRavenClientService.getResult(transaction);
        } catch (RavenClientException e) {
            logger.error("Unable to send raven transaction", e.getCause());
        }
        return ravenResponse;
    }

    public void saveUserAccessAuditLogs(String userName, int moduleId){
        AuditAccessTransaction auditAccess = new AuditAccessTransaction();
        User user = userService.findByUserName(userName);
        HttpServletRequest httpServletRequest = getHttpServletRequest();
        auditAccess.setUser(user);
        logger.info("IP ADDRESS: " + httpServletRequest.getRemoteAddr());
        auditAccess.setIpAddress(httpServletRequest.getRemoteAddr());
        AuditModule auditModule = moduleService.findByModuleId(moduleId);
        auditAccess.setModule(auditModule);
        auditAccess.setDateAccessed(new Timestamp(VanillaDateUtils.longCurrentDate()));
        auditAccess.setCreatedBy(userName);
        auditAccess.setCreatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        auditAccess.setUpdatedBy(userName);
        auditAccess.setUpdatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        auditService.save(auditAccess);
    }

    private HttpServletRequest getHttpServletRequest() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        return ((ServletRequestAttributes) requestAttributes).getRequest();
    }
}
