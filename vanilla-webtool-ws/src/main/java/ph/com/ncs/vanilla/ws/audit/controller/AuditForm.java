package ph.com.ncs.vanilla.ws.audit.controller;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import ph.com.ncs.vanilla.audit.domain.AuditAccessTransaction;
import ph.com.ncs.vanilla.audit.domain.AuditModule;
import ph.com.ncs.vanilla.audit.service.AuditService;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;
import ph.com.ncs.vanilla.module.service.ModuleService;
import ph.com.ncs.vanilla.reports.domain.TransactionLogsReports;
import ph.com.ncs.vanilla.reports.domain.UserAccessHistoryReports;
import ph.com.ncs.vanilla.user.domain.User;
import ph.com.ncs.vanilla.user.service.UserService;

/**
 * Created by edjohna on 7/19/2016.
 */
@Component
public class AuditForm {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuditForm.class);

    @Autowired
    private AuditService auditService;

    @Autowired
    private UserService userService;
    
    @Autowired
    private ModuleService moduleService;
    
    public Page<AuditAccessTransaction> listUserAccessHistory(Integer pageNumber){
        return auditService.listUserAccessHistory(pageNumber);
    }

    public Page<AuditAccessTransaction> listTransactionLogs(Integer pageNumber) {
        return auditService.listTransactionLogs(pageNumber);
    }

    public Page<AuditAccessTransaction> filterByModuleUserAccessHistory(int moduleId, Integer pageNumber){
        return auditService.filterByModuleUserAccessHistory(moduleId, pageNumber);
    }

    public Page<AuditAccessTransaction> filterByModuleTransactionLogs(int moduleId, Integer pageNumber){
        return auditService.filterByModuleTransactionLogs(moduleId, pageNumber);
    }

    public Page<AuditAccessTransaction> filterByDateUserAccessHistory(String from, String to, Integer pageNumber){
        Date toDate = new Date(VanillaDateUtils.formatToDate(to.toString()));
        Date fromDate = new Date(VanillaDateUtils.formatFromDate(from.toString()));

        return auditService.filterByDateUserAccessHistory(new Timestamp(fromDate.getTime()), new Timestamp(toDate.getTime()), pageNumber);
    }

    public Page<AuditAccessTransaction> filterByDateTransactionLogs(String from, String to, Integer pageNumber){
        Date toDate = new Date(VanillaDateUtils.formatToDate(to.toString()));
        Date fromDate = new Date(VanillaDateUtils.formatFromDate(from.toString()));

        return auditService.filterByDateTransactionLogs(new Timestamp(fromDate.getTime()), new Timestamp(toDate.getTime()), pageNumber);
    }

    public Page<AuditAccessTransaction> searchUserAccessHistory(String keyword, Integer pageNumber){
        return auditService.searchUserAccessHistory(keyword, pageNumber);
    }

    public Page<AuditAccessTransaction> searchTransactionLogs(String keyword, Integer pageNumber) {
        return auditService.searchTransactionLogs(keyword, pageNumber);
    }

    public List<UserAccessHistoryReports> extractUserAccessHistory(String keyword){
    	List<AuditAccessTransaction> auditAccessList = Collections.synchronizedList(new ArrayList<AuditAccessTransaction>());

    	if (keyword.equalsIgnoreCase("all")){
    		auditAccessList = auditService.extractUserAccessHistory(keyword);
    	} else {
    		auditAccessList = auditService.extractUserAccessHistory(keyword);
    	}
    	
        List<UserAccessHistoryReports> userAccessHistoryReportsList = new ArrayList<>();

        UserAccessHistoryReports headers = new UserAccessHistoryReports();
        headers.setLastName("Last Name");
        headers.setFirstName("First Name");
        headers.setDate("Date");
        headers.setTime("Time");
        headers.setModuleAccess("Module Access");
        headers.setUser("User ID");
        headers.setTransactionId("Transaction #");
        headers.setIpAddress("IP Address");

        userAccessHistoryReportsList.add(headers);

        int sequence = 1;

        for(AuditAccessTransaction auditAccess : auditAccessList){
            UserAccessHistoryReports userAccessHistoryReports = new UserAccessHistoryReports();

            userAccessHistoryReports.setTransactionId(String.valueOf(sequence));
            userAccessHistoryReports.setUser(auditAccess.getUser().getUserName());
            userAccessHistoryReports.setFirstName(auditAccess.getUser().getFirstName());
            userAccessHistoryReports.setLastName(auditAccess.getUser().getLastName());
            userAccessHistoryReports.setDate(auditAccess.getAuditDate());
            userAccessHistoryReports.setTime(auditAccess.getAuditTime());
            userAccessHistoryReports.setModuleAccess(auditAccess.getModule().getModuleName());
            userAccessHistoryReports.setIpAddress(auditAccess.getIpAddress());

            userAccessHistoryReportsList.add(userAccessHistoryReports);
            sequence++;
        }
        return userAccessHistoryReportsList;
    }

    public List<TransactionLogsReports> extractTransactionLogs(String keyword){
        List<AuditAccessTransaction> auditTxnList = auditService.extractTransactionLogs(keyword);
        List<TransactionLogsReports> transactionLogsReportsList = new ArrayList<>();

        TransactionLogsReports headers = new TransactionLogsReports();
        headers.setTransactionId("Transaction #");
        headers.setUser("User ID");
        headers.setLastName("Last Name");
        headers.setFirstName("First Name");
        headers.setDate("Date");
        headers.setTime("Time");
        headers.setIpAddress("IP Address");
        headers.setModuleAccess("Module");
        headers.setOldData("Old Data");
        headers.setNewData("New Data");

        transactionLogsReportsList.add(headers);

        int sequence = 1;
        for(AuditAccessTransaction auditTxn : auditTxnList){
            TransactionLogsReports transactionLogsReports = new TransactionLogsReports();
            
            transactionLogsReports.setTransactionId(String.valueOf(sequence));
            transactionLogsReports.setUser(auditTxn.getUser().getUserName());
            transactionLogsReports.setLastName(auditTxn.getUser().getLastName());
            transactionLogsReports.setFirstName(auditTxn.getUser().getFirstName());
            transactionLogsReports.setDate(auditTxn.getAuditDate());
            transactionLogsReports.setTime(auditTxn.getAuditTime());
            transactionLogsReports.setIpAddress(auditTxn.getIpAddress());
            transactionLogsReports.setModuleAccess(auditTxn.getModule().getModuleName());
            transactionLogsReports.setOldData(auditTxn.getOldData());
            transactionLogsReports.setNewData(auditTxn.getNewData());

            transactionLogsReportsList.add(transactionLogsReports);
            sequence++;

        }

        return transactionLogsReportsList;

    }
    
    public List<UserAccessHistoryReports> extractUserAccessHistoryByModule(int moduleId){
        List<AuditAccessTransaction> auditAccessList = auditService.extractUserAccessHistoryByModule(moduleId);
        List<UserAccessHistoryReports> userAccessHistoryReportsList = new ArrayList<>();

        UserAccessHistoryReports headers = new UserAccessHistoryReports();
        headers.setLastName("Last Name");
        headers.setFirstName("First Name");
        headers.setDate("Date");
        headers.setTime("Time");
        headers.setModuleAccess("Module Access");
        headers.setUser("User ID");
        headers.setTransactionId("Transaction #");
        headers.setIpAddress("IP Address");

        userAccessHistoryReportsList.add(headers);

        int sequence = 1;

        for(AuditAccessTransaction auditAccess : auditAccessList){
            UserAccessHistoryReports userAccessHistoryReports = new UserAccessHistoryReports();

            userAccessHistoryReports.setTransactionId(String.valueOf(sequence));
            userAccessHistoryReports.setUser(auditAccess.getUser().getUserName());
            userAccessHistoryReports.setFirstName(auditAccess.getUser().getFirstName());
            userAccessHistoryReports.setLastName(auditAccess.getUser().getLastName());
            userAccessHistoryReports.setDate(auditAccess.getAuditDate());
            userAccessHistoryReports.setTime(auditAccess.getAuditTime());
            userAccessHistoryReports.setModuleAccess(auditAccess.getModule().getModuleName());
            userAccessHistoryReports.setIpAddress(auditAccess.getIpAddress());

            userAccessHistoryReportsList.add(userAccessHistoryReports);
            sequence++;
        }
        return userAccessHistoryReportsList;
    }

    public List<TransactionLogsReports> extractTransactionLogsByModule(int moduleId){
        List<AuditAccessTransaction> auditTxnList = auditService.extractTransactionLogsByModule(moduleId);
        List<TransactionLogsReports> transactionLogsReportsList = new ArrayList<>();

        TransactionLogsReports headers = new TransactionLogsReports();
        headers.setTransactionId("Transaction #");
        headers.setUser("User ID");
        headers.setLastName("Last Name");
        headers.setFirstName("First Name");
        headers.setDate("Date");
        headers.setTime("Time");
        headers.setIpAddress("IP Address");
        headers.setModuleAccess("Module");
        headers.setOldData("Old Data");
        headers.setNewData("New Data");

        transactionLogsReportsList.add(headers);

        int sequence = 1;
        for(AuditAccessTransaction auditTxn : auditTxnList){
            TransactionLogsReports transactionLogsReports = new TransactionLogsReports();

            transactionLogsReports.setTransactionId(String.valueOf(sequence));
            transactionLogsReports.setUser(auditTxn.getUser().getUserName());
            transactionLogsReports.setLastName(auditTxn.getUser().getLastName());
            transactionLogsReports.setFirstName(auditTxn.getUser().getFirstName());
            transactionLogsReports.setDate(auditTxn.getAuditDate());
            transactionLogsReports.setTime(auditTxn.getAuditTime());
            transactionLogsReports.setIpAddress(auditTxn.getIpAddress());
            transactionLogsReports.setModuleAccess(auditTxn.getModule().getModuleName());
            transactionLogsReports.setOldData(auditTxn.getOldData());
            transactionLogsReports.setNewData(auditTxn.getNewData());

            transactionLogsReportsList.add(transactionLogsReports);
            sequence++;

        }

        return transactionLogsReportsList;


    }
    
    public List<UserAccessHistoryReports> extractUserAccessHistoryByDate(String from, String to){

        Date toDate = new Date(VanillaDateUtils.formatToDate(to.toString()));
        Date fromDate = new Date(VanillaDateUtils.formatFromDate(from.toString()));

        List<AuditAccessTransaction> auditAccessList = auditService.extractUserAccessHistoryByDate(new Timestamp(fromDate.getTime()), new Timestamp(toDate.getTime()));
        List<UserAccessHistoryReports> userAccessHistoryReportsList = new ArrayList<>();

        UserAccessHistoryReports headers = new UserAccessHistoryReports();
        headers.setLastName("Last Name");
        headers.setFirstName("First Name");
        headers.setDate("Date");
        headers.setTime("Time");
        headers.setModuleAccess("Module Access");
        headers.setUser("User ID");
        headers.setTransactionId("Transaction #");
        headers.setIpAddress("IP Address");

        userAccessHistoryReportsList.add(headers);

        int sequence = 1;

        for(AuditAccessTransaction auditAccess : auditAccessList){
            UserAccessHistoryReports userAccessHistoryReports = new UserAccessHistoryReports();
            
            userAccessHistoryReports.setTransactionId(String.valueOf(sequence));
            userAccessHistoryReports.setUser(auditAccess.getUser().getUserName());
            userAccessHistoryReports.setFirstName(auditAccess.getUser().getFirstName());
            userAccessHistoryReports.setLastName(auditAccess.getUser().getLastName());
            userAccessHistoryReports.setDate(auditAccess.getAuditDate());
            userAccessHistoryReports.setTime(auditAccess.getAuditTime());
            userAccessHistoryReports.setModuleAccess(auditAccess.getModule().getModuleName());
            userAccessHistoryReports.setIpAddress(auditAccess.getIpAddress());

            userAccessHistoryReportsList.add(userAccessHistoryReports);
            sequence++;
        }
        return userAccessHistoryReportsList;
    }

    public List<TransactionLogsReports> extractTransactionByDate(String from, String to){

        Date toDate = new Date(VanillaDateUtils.formatToDate(to.toString()));
        Date fromDate = new Date(VanillaDateUtils.formatFromDate(from.toString()));

        List<AuditAccessTransaction> auditTxnList = auditService.extractTransactionByDate(new Timestamp(fromDate.getTime()), new Timestamp(toDate.getTime()));
        List<TransactionLogsReports> transactionLogsReportsList = new ArrayList<>();

        TransactionLogsReports headers = new TransactionLogsReports();
        headers.setTransactionId("Transaction #");
        headers.setUser("User ID");
        headers.setLastName("Last Name");
        headers.setFirstName("First Name");
        headers.setDate("Date");
        headers.setTime("Time");
        headers.setIpAddress("IP Address");
        headers.setModuleAccess("Module");
        headers.setOldData("Old Data");
        headers.setNewData("New Data");

        transactionLogsReportsList.add(headers);

        int sequence = 1;
        for(AuditAccessTransaction auditTxn : auditTxnList){
            TransactionLogsReports transactionLogsReports = new TransactionLogsReports();

            transactionLogsReports.setTransactionId(String.valueOf(sequence));
            transactionLogsReports.setUser(auditTxn.getUser().getUserName());
            transactionLogsReports.setLastName(auditTxn.getUser().getLastName());
            transactionLogsReports.setFirstName(auditTxn.getUser().getFirstName());
            transactionLogsReports.setDate(auditTxn.getAuditDate());
            transactionLogsReports.setTime(auditTxn.getAuditTime());
            transactionLogsReports.setIpAddress(auditTxn.getIpAddress());
            transactionLogsReports.setModuleAccess(auditTxn.getModule().getModuleName());
            transactionLogsReports.setOldData(auditTxn.getOldData());
            transactionLogsReports.setNewData(auditTxn.getNewData());

            transactionLogsReportsList.add(transactionLogsReports);
            sequence++;
        }
        return transactionLogsReportsList;
    }

    /**
     * 
     * @param auditType - 1 : User Access History ; 2 : Transaction Logs
     * @param moduleId - values from TBL_MODULE
     * @param oldData
     * @param newData
     */
    public void saveAuditLogs(int auditType, int moduleId, String oldData, String newData){
    	LOGGER.info("ISG: saveAuditLogs() : START");
    	String userName = getLoggedInUser();
        AuditAccessTransaction auditAccess = new AuditAccessTransaction();
        User user = userService.findByUserName(userName);
        HttpServletRequest httpServletRequest = getHttpServletRequest();
        auditAccess.setUser(user);
        auditAccess.setIpAddress(httpServletRequest.getRemoteAddr());
        AuditModule auditModule = moduleService.findByModuleId(moduleId);
        auditAccess.setModule(auditModule);
        auditAccess.setDateAccessed(new Timestamp(VanillaDateUtils.longCurrentDate()));
        auditAccess.setCreatedBy(userName);
        auditAccess.setCreatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        auditAccess.setUpdatedBy(userName);
        auditAccess.setUpdatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        if (auditType == 2) {
        	auditAccess.setNewData(newData);
        	auditAccess.setOldData(oldData);
        }
        auditService.save(auditAccess);
    }

    private HttpServletRequest getHttpServletRequest() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        return ((ServletRequestAttributes) requestAttributes).getRequest();
    }
    
    private String getLoggedInUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userName = auth.getName();
        return userName;
    }
}