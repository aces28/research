package ph.com.ncs.vanilla.ws.isg.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ph.com.ncs.vanilla.base.security.service.LdapQueryServiceImpl;
import ph.com.ncs.vanilla.reports.domain.UserReports;
import ph.com.ncs.vanilla.user.domain.User;
import ph.com.ncs.vanilla.user.service.UserService;
import ph.com.ncs.vanilla.ws.audit.controller.AuditForm;

/**
 * Created by edjohna on 7/2/2016.
 */
@RestController
@RequestMapping("/isg")
public class ISGController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ISGController.class);

    @Autowired
    private UserService userService;
    @Autowired
    private LdapQueryServiceImpl ldapQueryService;
    
    @Autowired
    private AuditForm auditForm;
    @Autowired
    private ISGForm isgForm;
    @Autowired
    private Environment resource;

    @RequestMapping(value = "/listUser/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<User> listUser(@PathVariable int pageNumber) {
    	LOGGER.info("ISG: listUser() : START");
        Page<User> userPage = userService.listUser(pageNumber);
        auditForm.saveAuditLogs(1, Integer.parseInt(resource.getRequiredProperty("module.isg.users")), null, null);
        return userPage;
    }

    @RequestMapping(value = "/filterByRole/{roleType}/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<User> filterByRole(@PathVariable String roleType, @PathVariable int pageNumber) {
    	LOGGER.info("ISG: filterByRole() : START");
        Page<User> userPage = userService.filterByRole(roleType, pageNumber);
        return userPage;
    }

    @RequestMapping(value = "/filterByAvail/{status}/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<User> filterByStatus(@PathVariable int status, @PathVariable int pageNumber) {
    	LOGGER.info("ISG: filterByStatus() : START");
        Page<User> userPage = userService.filterByAvailability(status, pageNumber);
        return userPage;
    }

    @RequestMapping(value = "/saveUser", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private User saveUser(@RequestBody List<User> users) {
    	LOGGER.info("ISG: saveUser() : START");
        return isgForm.saveUser(users);
    }

    @RequestMapping(value = "/editUser", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private User editUser(@RequestBody Map<String, Object> map) {
    	LOGGER.info("ISG: editUser() : START");
    	return isgForm.editUser(map);
    }

    @RequestMapping(value = "/assignRoleToUser", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private User assignRoleToUser(@RequestBody Map<String, Object> map) {
    	LOGGER.info("ISG: assignRoleToUser() : START");
        return isgForm.assignRoleToUser(map);
    }

    @RequestMapping(value = "/searchUser/{keyword}/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<User> searchUser(@PathVariable String keyword, @PathVariable int pageNumber) {
    	LOGGER.info("ISG: searchUser() : START");
        return userService.searchUser(keyword, pageNumber);
    }

    @RequestMapping(value = "/extractReports/{keyword}/{action}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private List<UserReports> extractReports(@PathVariable String keyword, @PathVariable String action) {
    	LOGGER.info("ISG: extractReports() : START");
    	return isgForm.extractReports(keyword, action);
    }

    @RequestMapping(value = "/getAllLdapUsers", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private List<User> getAllLdapUsers() {
        return ldapQueryService.getAllUsers();
    }

    @RequestMapping(value = "/getLdapUsers/{keyword}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private List<User> getLdapUsers(@PathVariable String keyword) {
        List<User> userList = ldapQueryService.getLdapUsers(keyword);
        if (userList.isEmpty()) {
            User user = new User();
            user.setReferenceKey("No Users Found!");
            userList.add(user);
            return userList;
        } else {
            return userList;
        }
    }
}