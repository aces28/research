package ph.com.ncs.vanilla.ws.mis.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import ph.com.ncs.vanilla.application.domain.SubsApplication;
import ph.com.ncs.vanilla.base.service.BaseClientService;
import ph.com.ncs.vanilla.commons.constants.VanillaConstants;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;
import ph.com.ncs.vanilla.group.domain.Group;
import ph.com.ncs.vanilla.group.service.GroupService;
import ph.com.ncs.vanilla.role.domain.Role;
import ph.com.ncs.vanilla.role.service.RoleService;
import ph.com.ncs.vanilla.subscriber.service.SubscriberAppService;
import ph.com.ncs.vanilla.user.domain.User;
import ph.com.ncs.vanilla.user.service.UserService;
import ph.com.ncs.vanilla.ws.audit.controller.AuditForm;

/**
 * Created by edjohna on 7/29/2016.
 */
@Component
public class MISForm {
	
    private static final Logger LOGGER = LoggerFactory.getLogger(MISForm.class);

    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private GroupService groupService;
    @Autowired
    private SubscriberAppService subscriberAppService;
    
    @Autowired
    private AuditForm auditForm;
    @Autowired
    private Environment resource;

	public Group editGroup(Map<String, Object> map) {
        Integer newStatus = Integer.parseInt(map.get("status").toString());
        Group group = new Group();
        group.setId(Integer.parseInt(map.get("groupId").toString()));
        group = groupService.get(group);
        group.setStatus(newStatus);
        group = groupService.save(group);
        if (newStatus == 1)
        	auditForm.saveAuditLogs(2, getModuleId("module.mis.edit.group"), getOldNewData("Active", "No"), getOldNewData("Active", "Yes"));
        if (newStatus == 0)
        	auditForm.saveAuditLogs(2, getModuleId("module.mis.edit.group"), getOldNewData("Active", "Yes"), getOldNewData("Active", "No"));
        return group;
	}

	public Group saveGroup(Map<String, Object> map) {
		String groupName = String.valueOf(map.get("groupName"));
        Integer groupRole = Integer.valueOf(String.valueOf(map.get("groupRole")));
        ArrayList<Integer> addUserId = (ArrayList<Integer>) map.get("addUserId");
        Group group = new Group();
        Timestamp currentDate = new Timestamp(VanillaDateUtils.longCurrentDate());
        try {
            if (map.get("groupId") == null) {
                if (map.get("isGroupExist") != null) {
                    Group groupExist = groupService.findGroupByName(groupName);
                    if (groupExist != null) {
                        groupExist.setReferenceKey("Duplicate group name.");
                        return groupExist;
                    } else {
                        return new Group();
                    }
                }
                group.setGroupName(groupName);
                group.setCreatedDate(currentDate);
                group.setUpdatedDate(currentDate);
                group.setCreatedBy(VanillaConstants.SYSTEM_USER);
                group.setUpdatedBy(VanillaConstants.SYSTEM_USER);
                group.setStatus(1);
                Role role = new Role();
                role.setId(groupRole);
                group.setRole(role);
                group = groupService.save(group);
                auditForm.saveAuditLogs(2, getModuleId("module.mis.add.group"), null, getOldNewData("Group", group.getGroupName()));
                List<User> userList = new ArrayList<>();
                for (Integer userId : addUserId) {
                    User user = new User();
                    user.setId(userId);
                    user = userService.get(user);
                    user.setGroup(group);
                    userList.add(user);
                    auditForm.saveAuditLogs(2, getModuleId("module.mis.add.group"), null, getOldNewData("Added User", user.getUserName()));
                    LOGGER.info("Updated user : " + user.getUserName() + " : group id : " + group.getId() + " : role id : " + user.getRole().getId());
                }
                Iterable<User> userIterable = userList;
                userService.save(userIterable);
            } else {
                Integer groupId = Integer.valueOf(String.valueOf(map.get("groupId")));
                ArrayList<Integer> removeUserId = (ArrayList<Integer>) map.get("removeUserId");
                group.setId(groupId);
                group = groupService.get(group);
                List<User> removeUserList = userService.findAll(removeUserId);
                List<User> addUserList = userService.findAll(addUserId);
                List<User> totalUsers = new ArrayList<>();
                String err_msg = resource.getRequiredProperty(BaseClientService.UPDATE_GROUP_MEMBERS_ERROR_MESSAGE) + "\n";
                LOGGER.info("Update group members error message :: " + err_msg);
                int errCount = 0;
                for (User removeUsers : removeUserList) {
                	//re-assign :: User has edited for approval application
                	if(subscriberAppService.countApplicationUpdatedByApprover(2, removeUsers.getId()) == 0) {
                		Group removeGroup = new Group();
                        removeGroup.setId(0);
                        removeUsers.setGroup(removeGroup);
                        totalUsers.add(removeUsers);
                        auditForm.saveAuditLogs(2, getModuleId("module.mis.edit.group"), null, getOldNewData("Removed User", removeUsers.getUserName()));
                	} else {
                		LOGGER.info("Cannot remove from group -> User " + removeUsers.getUserName() + " has edited for approval application.");
                		errCount++;
                		err_msg = err_msg + "\n" + (errCount) + ". " + removeUsers.getFirstName() + " " + removeUsers.getLastName();
                	}
                }
                if(errCount == 0) {
                	for (User addUsers : addUserList) {
                        addUsers.setGroup(group);
                        totalUsers.add(addUsers);
                        auditForm.saveAuditLogs(2, getModuleId("module.mis.edit.group"), null, getOldNewData("Added User", addUsers.getUserName()));
                    }
                	Iterable<User> userIterable = totalUsers;
                    userService.save(userIterable);
                    LOGGER.info("Total number of user : " + addUserList.size() + " : user contents : " + Arrays.toString(addUserList.toArray()));
                } else {
                	group = new Group();
                    group.setReferenceKey(err_msg);
                }
            }
        } catch (Exception e) {
            LOGGER.error("Unable to add group : ", e);
            group = new Group();
            group.setReferenceKey("Duplicate group name.");
        }
        return group;
	}

	public User editUser(Map<String, Object> map) {
        int newGroup = Integer.parseInt(map.get("group").toString());
        int newStatus = Integer.parseInt(map.get("status").toString());
        int userId = Integer.parseInt(map.get("userId").toString());
        User userDetails = new User();
        userDetails.setId(userId);
        userDetails = userService.get(userDetails);
        Role role = new Role();
        role.setId(Integer.parseInt(map.get("role").toString()));
        role = roleService.get(role);
        userDetails.setRole(role);
        // re-assign : User must have no edited assignment
        boolean canbeUpdated = false;
        canbeUpdated = subscriberAppService.countApplicationUpdatedByApprover(2, userDetails.getId()) == 0;
        // update only if group was changed
        Group group = new Group();
        if (userDetails.getGroup() != null && userDetails.getGroup().getId() != newGroup) {
        	if (canbeUpdated) {
        		String oldData =  groupService.get(userDetails.getGroup()).getGroupName();
        		group.setId(newGroup);
        		group = groupService.get(group);
            	userDetails.setGroup(group);
            	userDetails.setUpdatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
            	auditForm.saveAuditLogs(2, getModuleId("module.mis.edit.user"), 
                		getOldNewData("Group", oldData), getOldNewData("Group", group.getGroupName()));
        	} else {
            	userDetails= new User();
            	userDetails.setReferenceKey(resource.getRequiredProperty(BaseClientService.REASSIGN_GROUP_ERROR_MESSAGE));  	
            	return userDetails;
            }
        } 
        // initial assign of group to user
        if (userDetails.getGroup() == null && newGroup != 0) {
        	group.setId(newGroup);
        	group = groupService.get(group);
        	userDetails.setGroup(group);
        	userDetails.setUpdatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        	auditForm.saveAuditLogs(2, getModuleId("module.mis.edit.user"), 
            		null, getOldNewData("Group", group.getGroupName()));
        }
        // update only if status was changed
        if (userDetails.getStatus() != newStatus) {
        	if (canbeUpdated) {
        		userDetails.setStatus(newStatus);
            	userDetails.setFailedLoginAttempts(0);
                userDetails.setUpdatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
                if (newStatus == 1)
                	auditForm.saveAuditLogs(2, getModuleId("module.mis.edit.user"), 
                			getOldNewData("Available", "No"), getOldNewData("Available", "Yes"));
                if (newStatus == 0)
                	auditForm.saveAuditLogs(2, getModuleId("module.mis.edit.user"), 
                			getOldNewData("Available", "Yes"), getOldNewData("Available", "No"));
        	} else {
        		userDetails = new User();
        		userDetails.setReferenceKey(resource.getRequiredProperty(BaseClientService.CHANGE_LOCKED_STATUS_ERROR_MESSAGE));
        		return userDetails;
        	}
        }
        return userService.save(userDetails);
	}

	public Iterable<SubsApplication> assignUser(Map<String, Object> map) {
        boolean isValidUpdate = false;
        ArrayList<Integer> appIdList = (ArrayList<Integer>) map.get("appId");
        Integer userId = Integer.valueOf(String.valueOf(map.get("userId")));
        List<SubsApplication> subsAppList = new ArrayList<>();
        Timestamp currentDate = new Timestamp(VanillaDateUtils.longCurrentDate());
        List<String> oldData =  Collections.synchronizedList(new ArrayList<String>());
        for (int appId : appIdList) {
            LOGGER.info("Get application id : " + appId);
            SubsApplication subsApplication = new SubsApplication();
            subsApplication.setId(appId);
            subsApplication = subscriberAppService.get(subsApplication);
            if(subsApplication.getStatus() == 2) {
            	LOGGER.info("Cannot re-assign application -> already updated by the current approver");
            	subsApplication = new SubsApplication();
            	subsApplication.setReferenceKey(resource.getRequiredProperty(BaseClientService.REASSIGN_APPROVER_ERROR_MESSAGE));
            	subsApplication.setApplicationDate(currentDate);
            	subsApplication.setUpdatedDate(currentDate);
	            subsApplication.setUpdatedBy(VanillaConstants.SYSTEM_USER);
	            subsAppList.add(subsApplication);
            } else {
            	subsApplication.setStatus(1);
	            subsApplication.setUpdatedDate(currentDate);
	            subsApplication.setUpdatedBy(VanillaConstants.SYSTEM_USER);
	            if (subsApplication.getUser() != null)
	            	oldData.add(subsApplication.getUser().getUserName());
	            else
	            	oldData.add(null);
	            
	            User user = new User();
	            user.setId(userId);
	            user = userService.get(user);
	            subsApplication.setUser(user);
	            subsAppList.add(subsApplication);
	            isValidUpdate = true;
            }
        }
        Iterable<SubsApplication> iterable = subsAppList;
		if (isValidUpdate)
			subscriberAppService.save(iterable);
		int auditCtr = 0;
		for (SubsApplication subsApp : subsAppList) {	
    		if (subsAppList.size() == 1)	
    			auditForm.saveAuditLogs(2, oldData.get(0) == null ? getModuleId("module.mis.assign.agent") : getModuleId("module.mis.edit.approver"), 
    					getOldNewData("User", oldData.get(0)), getOldNewData("User", subsApp.getUser().getUserName()));
    		else if (subsAppList.size() > 1) {
    			auditForm.saveAuditLogs(2, getModuleId("module.mis.assign.batch"), 
    						getOldNewData("User", oldData.get(auditCtr)), getOldNewData("User", subsApp.getUser().getUserName()));
    		}
    		auditCtr++;
    	}	
        return iterable;
	}
	
    private int getModuleId(String props) {
    	int moduleId = Integer.parseInt(resource.getRequiredProperty(props));
    	return moduleId;
    }
    
    private String getOldNewData(String label, String oldNewData) {    	
    	if (oldNewData != null)
    		return label + " : " + oldNewData;
    	else
    		return null;
    }
}