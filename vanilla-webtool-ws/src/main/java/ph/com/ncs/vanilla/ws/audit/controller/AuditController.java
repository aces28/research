package ph.com.ncs.vanilla.ws.audit.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ph.com.ncs.vanilla.audit.domain.AuditAccessTransaction;
import ph.com.ncs.vanilla.reports.domain.TransactionLogsReports;
import ph.com.ncs.vanilla.reports.domain.UserAccessHistoryReports;

/**
 * Created by edjohna on 7/20/2016.
 */
@RestController
@RequestMapping("/audit")
public class AuditController {

    @Autowired
    private AuditForm auditForm;
    
    @Autowired 
    private Environment resource;

    @RequestMapping(value = "/listUserAccessHistory/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<AuditAccessTransaction> listUserAccessHistory(@PathVariable int pageNumber){
        auditForm.saveAuditLogs(1, Integer.parseInt(resource.getRequiredProperty("module.user.access.history")), null, null);
        return auditForm.listUserAccessHistory(pageNumber);
    }

    @RequestMapping(value = "/listTransactionLogs/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<AuditAccessTransaction> listTransactionLogs(@PathVariable int pageNumber){
        auditForm.saveAuditLogs(1, Integer.parseInt(resource.getRequiredProperty("module.transaction.logs")), null, null);
        return auditForm.listTransactionLogs(pageNumber);
    }

    @RequestMapping(value = "/filterByModuleUserAccessHistory/{moduleId}/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<AuditAccessTransaction> filterByModuleUserAccessHistory(@PathVariable int moduleId, @PathVariable int pageNumber){
        return auditForm.filterByModuleUserAccessHistory(moduleId, pageNumber);
    }

    @RequestMapping(value = "/filterByModuleTransactionLogs/{moduleId}/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<AuditAccessTransaction> filterByModuleTransactionLogs(@PathVariable int moduleId, @PathVariable int pageNumber){
        return auditForm.filterByModuleTransactionLogs(moduleId, pageNumber);
    }

    @RequestMapping(value = "/filterByDateUserAccessHistory/{from}/{to}/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<AuditAccessTransaction> filterByDateUserAccessHistory(@PathVariable String from, @PathVariable String to, @PathVariable int pageNumber){
        return auditForm.filterByDateUserAccessHistory(from, to, pageNumber);
    }

    @RequestMapping(value = "/filterByDateTransactionLogs/{from}/{to}/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<AuditAccessTransaction> filterByDateTransactionLogs(@PathVariable String from, @PathVariable String to, @PathVariable int pageNumber){
        return auditForm.filterByDateTransactionLogs(from, to, pageNumber);
    }

    @RequestMapping(value = "/searchUserAccessHistory/{keyword}/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<AuditAccessTransaction> searchUserAccessHistory(@PathVariable String keyword, @PathVariable int pageNumber){
        return auditForm.searchUserAccessHistory(keyword, pageNumber);
    }

    @RequestMapping(value = "/searchTransactionLogs/{keyword}/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<AuditAccessTransaction> searchTransactionLogs(@PathVariable String keyword, @PathVariable int pageNumber){
        return auditForm.searchTransactionLogs(keyword,pageNumber);
    }

    @RequestMapping(value = "/extractUserAccessHistory", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private List<UserAccessHistoryReports> extractUserAccessHistory(@RequestBody Map<String, Object> map){
        List<UserAccessHistoryReports> userAccessHistoryReportsList = new ArrayList<>();
        
        if ("module".equalsIgnoreCase(map.get("action").toString()))
        	userAccessHistoryReportsList = auditForm.extractUserAccessHistoryByModule(Integer.parseInt(map.get("keyword").toString()));  
        else if("search".equalsIgnoreCase(map.get("action").toString()))
            userAccessHistoryReportsList = auditForm.extractUserAccessHistory(map.get("keyword").toString());
        else if ("date".equalsIgnoreCase(map.get("action").toString()))
            userAccessHistoryReportsList = auditForm.extractUserAccessHistoryByDate(map.get("from").toString(), map.get("to").toString());
        else if ("all".equalsIgnoreCase(map.get("action").toString()))
        	userAccessHistoryReportsList = auditForm.extractUserAccessHistory(map.get("keyword").toString());
        
        return userAccessHistoryReportsList;
    }

    @RequestMapping(value = "/extractTransactionLogs", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private List<TransactionLogsReports> extractTransactionLogs(@RequestBody Map<String, Object> map){
        List<TransactionLogsReports> transactionLogsReportsList = new ArrayList<>();
        if("module".equalsIgnoreCase(map.get("action").toString()))
            transactionLogsReportsList = auditForm.extractTransactionLogsByModule(Integer.parseInt(map.get("keyword").toString()));
        else if("search".equalsIgnoreCase(map.get("action").toString()))
            transactionLogsReportsList = auditForm.extractTransactionLogs(map.get("keyword").toString());
        else if ("date".equalsIgnoreCase(map.get("action").toString()))
            transactionLogsReportsList = auditForm.extractTransactionByDate(map.get("from").toString(), map.get("to").toString());
        else if ("all".equalsIgnoreCase(map.get("action").toString()))
        	transactionLogsReportsList = auditForm.extractTransactionLogs(map.get("keyword").toString());
        
        return transactionLogsReportsList;
    }
}
