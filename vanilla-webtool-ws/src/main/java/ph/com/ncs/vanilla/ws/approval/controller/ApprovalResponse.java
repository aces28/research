package ph.com.ncs.vanilla.ws.approval.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import ph.com.ncs.vanilla.application.domain.SubsApplication;

import java.util.List;

/**
 * Created by edjohna on 6/25/2016.
 */
public class ApprovalResponse<T extends SubsApplication> extends ResponseEntity {

    private HttpStatus statusCode;
    private List<SubsApplication> approvalList;

    protected ApprovalResponse(HttpStatus statusCode, List<SubsApplication> approvalList) {
        super(statusCode);
        this.approvalList = approvalList;
        this.statusCode = statusCode;

    }

    public List<SubsApplication> getBody() {
        return approvalList;
    }

    public HttpStatus getStatusCode() {
        return statusCode;
    }

}

