package ph.com.ncs.vanilla.ws.isg.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import ph.com.ncs.vanilla.base.service.BaseClientService;
import ph.com.ncs.vanilla.commons.constants.VanillaConstants;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;
import ph.com.ncs.vanilla.group.domain.Group;
import ph.com.ncs.vanilla.group.service.GroupService;
import ph.com.ncs.vanilla.reports.domain.UserReports;
import ph.com.ncs.vanilla.role.domain.Role;
import ph.com.ncs.vanilla.role.service.RoleService;
import ph.com.ncs.vanilla.subscriber.service.SubscriberAppService;
import ph.com.ncs.vanilla.user.domain.User;
import ph.com.ncs.vanilla.user.service.UserService;
import ph.com.ncs.vanilla.ws.audit.controller.AuditForm;

/**
 * Created by edjohna on 7/29/2016.
 */
@Component
public class ISGForm {

    private static final Logger LOGGER = LoggerFactory.getLogger(ISGForm.class);
    
    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private GroupService groupService;
    @Autowired
    private SubscriberAppService subscriberAppService;

    @Autowired
    private AuditForm auditForm;
    @Autowired
    private Environment resource;

    public User saveUser(List<User> users) {
        Timestamp currentDate = new Timestamp(VanillaDateUtils.longCurrentDate());
        try {
            if (users.size() < 1) {
                User user = users.get(0);
                LOGGER.info("saveUser() : user : " + user.getUserName() + " : " + user.getFirstName() + " : " + user.getLastName() +
                        " : " + user.getStatus() + " : " + user.getEmail());
                user.setCreatedDate(currentDate);
                user.setUpdatedDate(currentDate);
                user.setCreatedBy(VanillaConstants.SYSTEM_USER);
                user.setUpdatedBy(VanillaConstants.SYSTEM_USER);
                user.setIsLocked(0);
                Group defaultGrp = new Group();
                defaultGrp.setId(0);
                user.setGroup(defaultGrp);
                User addedUser = userService.save(user);
                return userService.get(addedUser);
            } else {
                List<User> userList = Collections.synchronizedList(new ArrayList<User>());
                for (User user : users) {
                    user.setCreatedDate(currentDate);
                    user.setUpdatedDate(currentDate);
                    user.setCreatedBy(VanillaConstants.SYSTEM_USER);
                    user.setUpdatedBy(VanillaConstants.SYSTEM_USER);
                    user.setIsLocked(0);
                    Group defaultGrp = new Group();
                    defaultGrp.setId(0);
                    user.setGroup(defaultGrp);
                    userList.add(user);
                }
                List<User> newUsersList = userService.save(userList);
                if (newUsersList.size() == 1) {
                	auditForm.saveAuditLogs(2, getModuleId("module.isg.add.user"), 
                			null, getOldNewData("New User", newUsersList.get(0).getUserName()));
                } else if (newUsersList.size() > 1) {
                	for (User newUser : newUsersList)
                		auditForm.saveAuditLogs(2, getModuleId("module.isg.add.many.users"), 
                			null, getOldNewData("New User", newUser.getUserName()));	
                }
                return newUsersList.get(0);
            }
        } catch (Exception e) {
        	LOGGER.error("Unable to add user : ", e);
            User user = new User();
            user.setReferenceKey("User already exist.");
            return user;
        }
    }

	public User editUser(Map<String, Object> map) {
        User user = new User();
        user.setId(Integer.parseInt(map.get("userId").toString()));
        
        int newStatus = Integer.parseInt(map.get("status").toString());
        int newRole = Integer.parseInt(map.get("role").toString());
        
        User editUser = userService.get(user);
        editUser.setStatus(newStatus);
        Role role = roleService.findRoleById(newRole);
        String oldData = editUser.getRole().getRoleType();
        editUser.setRole(role);
        
        auditForm.saveAuditLogs(2, getModuleId("module.isg.edit.user"), 
        		getOldNewData("Role", oldData), getOldNewData("Role", role.getRoleType()));
        
        return userService.save(editUser);
	}

	public User assignRoleToUser(Map<String, Object> map) {
		int newRole = Integer.parseInt(map.get("roleId").toString());
        int newLockStatus = Integer.parseInt(map.get("status").toString());
        int userId = Integer.parseInt(map.get("userId").toString());
        
        User userDetails = new User();
        userDetails.setId(userId);
        userDetails = userService.get(userDetails);
        
        // re-assign : User must have no edited assignment
        boolean canbeUpdated = false;
        canbeUpdated = subscriberAppService.countApplicationUpdatedByApprover(2, userDetails.getId()) == 0;
        
        // update only if role was changed
        if (userDetails.getRole() != null && userDetails.getRole().getId() != newRole) {
        	if (canbeUpdated) {
        		Role role = roleService.findRoleById(newRole);
        		String oldData = userDetails.getRole().getRoleType();
                userDetails.setRole(role);
                
//                if (newRole != 3) {
				Group group = new Group();
				group.setId(0);
				userDetails.setGroup(group);
//                }

                userDetails.setFailedLoginAttempts(0);
                userDetails.setUpdatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
                
                auditForm.saveAuditLogs(2, getModuleId("module.isg.edit.user"), 
                		getOldNewData("Role", oldData), getOldNewData("Role", role.getRoleType()));
        	} else {
        		userDetails = new User();
        		userDetails.setReferenceKey(resource.getRequiredProperty(BaseClientService.REASSIGN_ROLE_ERROR_MESSAGE));  		
        		return userDetails;
        	}
        }
        // update only if status was changed
        if (userDetails.getIsLocked() != newLockStatus) {
        	if (canbeUpdated) {
        		userDetails.setIsLocked(newLockStatus);
            	userDetails.setFailedLoginAttempts(0);
                userDetails.setUpdatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
                if (newLockStatus == 0)
                	auditForm.saveAuditLogs(2, getModuleId("module.isg.edit.user"), 
                			getOldNewData("Status", "No"), getOldNewData("Status", "Yes"));
                if (newLockStatus == 1)
                	auditForm.saveAuditLogs(2, getModuleId("module.isg.edit.user"), 
                			getOldNewData("Status", "Yes"), getOldNewData("Status", "No"));
        	} else {
        		userDetails = new User();
        		userDetails.setReferenceKey(resource.getRequiredProperty(BaseClientService.CHANGE_LOCKED_STATUS_ERROR_MESSAGE));
        		return userDetails;
        	}
        }  
        return userService.save(userDetails);
	}
	
    private int getModuleId(String props) {
    	int moduleId = Integer.parseInt(resource.getRequiredProperty(props));
    	return moduleId;
    }
    
    private String getOldNewData(String label, String oldNewData) {    	
    	if (oldNewData != null)
    		return label + " : " + oldNewData;
    	else
    		return null;
    }

	public List<UserReports> extractReports(String keyword, String action) {
		 List<UserReports> userReportsList = new ArrayList<>();
	        UserReports columnHeaders = new UserReports();
	        columnHeaders.setId("User #");
	        columnHeaders.setUserName("User ID");
	        columnHeaders.setFirstName("First Name");
	        columnHeaders.setLastName("Last Name");
	        columnHeaders.setGroupName("Group");
	        columnHeaders.setRole("Role");
	        columnHeaders.setStatus("Status");
	        userReportsList.add(columnHeaders);

	        if ("search".equalsIgnoreCase(action)) {
	            List<User> usersList = userService.extractUser(keyword);
	            int sequence = 1;
	            for (User user : usersList) {

	                UserReports userReports = new UserReports();
	                userReports.setId(String.valueOf(sequence));
	                userReports.setUserName(user.getUserName());
	                userReports.setFirstName(user.getFirstName());
	                userReports.setLastName(user.getLastName());
	                if (user.getGroup() != null) {
	                    Group group = groupService.get(user.getGroup());
	                    userReports.setGroupName(group.getGroupName());
	                } else {
	                    userReports.setGroupName(" ");
	                }
	                if (user.getRole() != null) {
	                    Role role = roleService.get(user.getRole());
	                    userReports.setRole(role.getRoleType());
	                } else {
	                    userReports.setRole(" ");
	                }
	                String status;
	                if (user.getStatus() == 1) {
	                    status = "available";
	                } else {
	                    status = "unavailable";
	                }
	                userReports.setStatus(status);
	                userReportsList.add(userReports);
	                sequence++;
	            }
	        } else if ("role".equalsIgnoreCase(action)) {
	            List<User> usersList = userService.extractByRole(keyword);
	            int sequence = 1;
	            for (User user : usersList) {

	                UserReports userReports = new UserReports();
	                userReports.setId(String.valueOf(sequence));
	                userReports.setUserName(user.getUserName());
	                userReports.setFirstName(user.getFirstName());
	                userReports.setLastName(user.getLastName());
	                if (user.getGroup() != null) {
	                    Group group = groupService.get(user.getGroup());
	                    userReports.setGroupName(group.getGroupName());
	                } else {
	                    userReports.setGroupName(" ");
	                }
	                if (user.getRole() != null) {
	                    Role role = roleService.get(user.getRole());
	                    userReports.setRole(role.getRoleType());
	                } else {
	                    userReports.setRole(" ");
	                }
	                String status;
	                if (user.getStatus() == 1) {
	                    status = "available";
	                } else {
	                    status = "unavailable";
	                }
	                userReports.setStatus(status);
	                userReportsList.add(userReports);
	                sequence++;
	            }

	        } else if ("status".equalsIgnoreCase(action)) {
	            int status = Integer.parseInt(keyword);
	            List<User> usersList = userService.extractByAvailability(status);
	            int sequence = 1;
	            for (User user : usersList) {

	                UserReports userReports = new UserReports();
	                userReports.setId(String.valueOf(sequence));
	                userReports.setUserName(user.getUserName());
	                userReports.setFirstName(user.getFirstName());
	                userReports.setLastName(user.getLastName());
	                if (user.getGroup() != null) {
	                    Group group = groupService.get(user.getGroup());
	                    userReports.setGroupName(group.getGroupName());
	                } else {
	                    userReports.setGroupName(" ");
	                }
	                if (user.getRole() != null) {
	                    Role role = roleService.get(user.getRole());
	                    userReports.setRole(role.getRoleType());
	                } else {
	                    userReports.setRole(" ");
	                }
	                String availability;
	                if (user.getStatus() == 1) {
	                    availability = "available";
	                } else {
	                    availability = "unavailable";
	                }
	                userReports.setStatus(availability);
	                userReportsList.add(userReports);
	                sequence++;
	            }
	        } else if ("all".equalsIgnoreCase(action)) {

	            List<User> usersList = userService.extractAllUser();
	            int sequence = 1;
	            for (User user : usersList) {

	                UserReports userReports = new UserReports();
	                userReports.setId(String.valueOf(sequence));
	                userReports.setUserName(user.getUserName());
	                userReports.setFirstName(user.getFirstName());
	                userReports.setLastName(user.getLastName());
	                if (user.getGroup() != null) {
	                    Group group = groupService.get(user.getGroup());
	                    userReports.setGroupName(group.getGroupName());
	                } else {
	                    userReports.setGroupName(" ");
	                }
	                if (user.getRole() != null) {
	                    Role role = roleService.get(user.getRole());
	                    userReports.setRole(role.getRoleType());
	                } else {
	                    userReports.setRole(" ");
	                }
	                String availability;
	                if (user.getStatus() == 1) {
	                    availability = "available";
	                } else {
	                    availability = "unavailable";
	                }
	                userReports.setStatus(availability);
	                userReportsList.add(userReports);
	                sequence++;
	            }
	        }
	        return userReportsList;
	}
}