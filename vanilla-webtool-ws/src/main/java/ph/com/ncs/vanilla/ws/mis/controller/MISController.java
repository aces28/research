package ph.com.ncs.vanilla.ws.mis.controller;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ph.com.ncs.vanilla.application.domain.SubsApplication;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;
import ph.com.ncs.vanilla.group.domain.Group;
import ph.com.ncs.vanilla.group.service.GroupService;
import ph.com.ncs.vanilla.mis.service.MISService;
import ph.com.ncs.vanilla.reports.domain.ApplicationReports;
import ph.com.ncs.vanilla.reports.domain.ApprovalReport;
import ph.com.ncs.vanilla.reports.domain.ApproveUnpaidReport;
import ph.com.ncs.vanilla.reports.domain.ApproverList;
import ph.com.ncs.vanilla.reports.domain.DisapproveReports;
import ph.com.ncs.vanilla.reports.domain.GroupReports;
import ph.com.ncs.vanilla.reports.domain.UserReports;
import ph.com.ncs.vanilla.role.domain.Role;
import ph.com.ncs.vanilla.role.service.RoleService;
import ph.com.ncs.vanilla.subscriber.domain.Subscriber;
import ph.com.ncs.vanilla.subscriber.service.SubscriberService;
import ph.com.ncs.vanilla.user.domain.User;
import ph.com.ncs.vanilla.user.service.UserService;
import ph.com.ncs.vanilla.ws.audit.controller.AuditForm;
import ph.com.ncs.vanilla.ws.common.ReportsHeader;

/**
 * Created by edjohna on 6/28/2016.
 */
@RestController
@RequestMapping("/mis")
public class MISController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MISController.class);

    @Autowired
    private MISService misService;
    @Autowired
    private UserService userService;
    @Autowired
    private GroupService groupService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private SubscriberService subscriberService;
    
    @Autowired
    private AuditForm auditForm;
    @Autowired
    private	MISForm misForm;
    @Autowired
    private ReportsHeader reportBuilder;
    @Autowired
    private Environment resource;

    @RequestMapping(value = "/forAssignment/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<SubsApplication> forAssignment(@PathVariable int pageNumber) {
        LOGGER.info("forAssignment() : START");
        Page<SubsApplication> assignmentPage = misService.listForAssignmentMIS(pageNumber);
        auditForm.saveAuditLogs(1, Integer.parseInt(resource.getRequiredProperty("module.mis.for.assignment")), null, null);
        return assignmentPage;
    }   

    @RequestMapping(value = "/forApproval/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<SubsApplication> forApproval(@PathVariable int pageNumber) {
        LOGGER.info("forApproval() : START");
        Page<SubsApplication> approvalPage = misService.listForApprovalMIS(pageNumber);
        auditForm.saveAuditLogs(1, Integer.parseInt(resource.getRequiredProperty("module.mis.for.approval")), null, null);
        return approvalPage;
    }

    @RequestMapping(value = "/approved/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<SubsApplication> approved(@PathVariable int pageNumber) {
        LOGGER.info("approved() : START");
        Page<SubsApplication> approvedPage = misService.listApprovedMIS(pageNumber);
        auditForm.saveAuditLogs(1, Integer.parseInt(resource.getRequiredProperty("module.mis.approved.unpaid")), null, null);
        return approvedPage;
    }

    @RequestMapping(value = "/disapproved/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<SubsApplication> disapproved(@PathVariable int pageNumber) {
        LOGGER.info("disapproved() : START");
        Page<SubsApplication> disapprovedPage = misService.listDisapprovedMIS(pageNumber);
        auditForm.saveAuditLogs(1, Integer.parseInt(resource.getRequiredProperty("module.mis.disapproved")), null, null);
        return disapprovedPage;
    }

    @RequestMapping(value = "/listApprover/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<User> listApprover(@PathVariable int pageNumber) {
        LOGGER.info("listApprover() : START");
        Page<User> userPage = userService.listApprover(pageNumber);
        auditForm.saveAuditLogs(1, Integer.parseInt(resource.getRequiredProperty("module.mis.users")), null, null);
        return userPage;
    }

    @RequestMapping(value = "/listAvailableGroup", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private List<Group> listAvailableGroup() {
        LOGGER.info("listAvailableGroup() : START");
        List<Group> groups = groupService.listAvailableGroup();
        return groups;
    }

    @RequestMapping(value = "/listGroup/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private <T> T listGroup(@PathVariable int pageNumber) {
        LOGGER.info("listGroup() : START");
        switch (pageNumber) {
            case 0:
                List<Group> groups = groupService.findAll();
                return (T) groups;
            default:
                Page<Group> groupPage = groupService.listGroup(pageNumber);
                auditForm.saveAuditLogs(1, Integer.parseInt(resource.getRequiredProperty("module.group")), null, null);
                return (T) groupPage;
        }
    }

    @RequestMapping(value = "/filterByGroup/{groupName}/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<User> filterByGroup(@PathVariable String groupName, @PathVariable int pageNumber) {
        LOGGER.info("filterByGroup() : START");
        Page<User> userPage = userService.filterByGroupName(groupName, pageNumber);
        return userPage;
    }

    @RequestMapping(value = "/approversForAssignment/{groupName}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private List<User> approversForAssignment(@PathVariable String groupName) {
        LOGGER.info("approversForAssignment() : START");
        return userService.listApproverForAssignment(groupName);
    }

    @RequestMapping(value = "/filterByStatus/{availability}/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<User> filterByAvail(@PathVariable int availability, @PathVariable int pageNumber) {
        LOGGER.info("filterByAvail() : START");
        Page<User> userPage = userService.filterByStatus(availability, pageNumber);
        return userPage;
    }

    @RequestMapping(value = "/filterForAssignmentMIS", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private Page<SubsApplication> filterForAssignmentMIS(@RequestBody Map<String, Object> map) {
        LOGGER.info("filterForAssignmentMIS() : START");
        Date to = new Date(VanillaDateUtils.longDateToFormat(map.get("to").toString()));
        Date from = new Date(VanillaDateUtils.longDateFromFormat(map.get("from").toString()));
        int pageNumber = Integer.parseInt(map.get("pageNumber").toString());
        Page<SubsApplication> applicationPage = misService.filterForAssignmentMIS(new Timestamp(to.getTime()), new Timestamp(from.getTime()), pageNumber);
        return applicationPage;
    }

    @RequestMapping(value = "/filterForApprovalMIS", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private Page<SubsApplication> filterForApprovalMIS(@RequestBody Map<String, Object> map) {
        LOGGER.info("filterForApprovalMIS() : START");
        Date to = new Date(VanillaDateUtils.longDateToFormat(map.get("to").toString()));
        Date from = new Date(VanillaDateUtils.longDateFromFormat(map.get("from").toString()));
        int pageNumber = Integer.parseInt(map.get("pageNumber").toString());
        Page<SubsApplication> applicationPage = misService.filterForApprovalMIS(new Timestamp(to.getTime()), new Timestamp(from.getTime()), pageNumber);
        return applicationPage;
    }

    @RequestMapping(value = "/filterApprovedMIS", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private Page<SubsApplication> filterApprovedMIS(@RequestBody Map<String, Object> map) {
        LOGGER.info("filterApprovedMIS() : START");
        Date to = new Date(VanillaDateUtils.longDateToFormat(map.get("to").toString()));
        Date from = new Date(VanillaDateUtils.longDateFromFormat(map.get("from").toString()));
        int pageNumber = Integer.parseInt(map.get("pageNumber").toString());
        Page<SubsApplication> applicationPage = misService.filterApprovedMIS(new Timestamp(to.getTime()), new Timestamp(from.getTime()), pageNumber);
        return applicationPage;
    }

    @RequestMapping(value = "/filterDisapprovedMIS", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private Page<SubsApplication> filterDisapprovedMIS(@RequestBody Map<String, Object> map) {
        LOGGER.info("filterDisapprovedMIS() : START");
        Date to = new Date(VanillaDateUtils.longDateToFormat(map.get("to").toString()));
        Date from = new Date(VanillaDateUtils.longDateFromFormat(map.get("from").toString()));
        int pageNumber = Integer.parseInt(map.get("pageNumber").toString());
        Page<SubsApplication> applicationPage = misService.filterDisapprovedMIS(new Timestamp(to.getTime()), new Timestamp(from.getTime()), pageNumber);
        return applicationPage;
    }

    @RequestMapping(value = "/editGroup", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private Group editGroup(@RequestBody Map<String, Object> map) {
        LOGGER.info("editGroup() : START");  
        return misForm.editGroup(map);
    }

    @RequestMapping(value = "/saveGroup", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private Group saveGroup(@RequestBody Map<String, Object> map) {
        LOGGER.info("saveGroup() : START");
        return misForm.saveGroup(map);
    }

    @RequestMapping(value = "/editUser", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private User editUser(@RequestBody Map<String, Object> map) {
        LOGGER.info("MIS: editUser() : START");
        return misForm.editUser(map);
    }

    @RequestMapping(value = "/assignUser", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private Iterable<SubsApplication> assignUser(@RequestBody Map<String, Object> map) {
        LOGGER.info("assignUser() : START");
        return misForm.assignUser(map);
    }

    @RequestMapping(value = "/assignUserToGroup", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private User assignUserToGroup(@RequestBody Map<String, Object> map) {
        LOGGER.info("assignUserToGroup() : START");
        User user = new User();
        user.setId(Integer.parseInt(map.get("userId").toString()));
        Group group = new Group();
        group.setId(Integer.parseInt(map.get("groupId").toString()));
        User userDetails = userService.get(user);
        userDetails.setGroup(group);
        return userService.save(userDetails);
    }

    @RequestMapping(value = "/viewMembers/{groupId}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Map<String, String> viewMembers(@PathVariable int groupId) {
        LOGGER.info("viewMembers() : START");
        Group group = new Group();
        Role role = new Role();
        HashMap<String, String> response = new HashMap<>();
        group.setId(groupId);
        group = groupService.get(group);
        String roleName = role.getRoleType();
        response.put("groupName", group.getGroupName());
        response.put("role", roleName);
        response.put("groupId", group.getId().toString());
        return response;
    }

    @RequestMapping(value = "/getApprovers", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private List<ApproverList> getApprover() {
        LOGGER.info("getApprover() : START");
        List<User> users = userService.getApproversNoGroup();
        List<ApproverList> approverLists = new ArrayList<>();
        for (User user : users) {
            ApproverList approverList = new ApproverList();
            String name = user.getFirstName() + " " + user.getLastName();
            approverList.setUserId(user.getId());
            approverList.setApproverName(name);
            approverLists.add(approverList);
        }
        return approverLists;
    }

    @RequestMapping(value = "/getMembers/{groupId}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private List<ApproverList> getMembers(@PathVariable int groupId) {
        LOGGER.info("getMembers() : START");
        List<User> users = userService.getMembersOfGroup(groupId);
        List<ApproverList> approverLists = new ArrayList<>();
        for (User user : users) {
            ApproverList approverList = new ApproverList();
            String name = user.getFirstName() + " " + user.getLastName();
            approverList.setUserId(user.getId());
            approverList.setApproverName(name);
            approverLists.add(approverList);
        }
        return approverLists;
    }

    @RequestMapping(value = "/listApproversForAssignment/{groupName}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private List<ApproverList> listApproversForAssignment(@PathVariable String groupName) {
    	LOGGER.info("listApproversForAssignment() : START");
    	List<ApproverList> approverLists = new ArrayList<>();
        List<User> userList = userService.listApproverForAssignment(groupName);
        for (User user : userList) {
            ApproverList approverList = new ApproverList();
            String name = user.getFirstName() + " " + user.getLastName();
            approverList.setUserId(user.getId());
            approverList.setApproverName(name);
            approverList.setCompleted(misService.approverCompleted(user.getId()));
            approverList.setOnGoing(misService.approverOngoing(user.getId()));
            approverLists.add(approverList);
        }
        return approverLists;
    }

    @RequestMapping(value = "/searchForAssignment/{keyword}/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<SubsApplication> searchForAssignment(@PathVariable String keyword, @PathVariable int pageNumber) {
        LOGGER.info("searchForApproval() : START");
        return misService.searchForAssignmentString(keyword, pageNumber);
    }

    @RequestMapping(value = "/searchForApproval/{keyword}/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<SubsApplication> searchForApproval(@PathVariable String keyword, @PathVariable int pageNumber) {
        LOGGER.info("searchForApproval() : START");
        return misService.searchForApprovalString(keyword, pageNumber);
    }

    @RequestMapping(value = "/searchApproved/{keyword}/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<SubsApplication> searchApproved(@PathVariable String keyword, @PathVariable int pageNumber) {
        LOGGER.info("searchApproved() : START");
        return misService.searchApprovedString(keyword, pageNumber);
    }

    @RequestMapping(value = "/searchDisapproved/{keyword}/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<SubsApplication> searchDisapproved(@PathVariable String keyword, @PathVariable int pageNumber) {
        LOGGER.info("searchDisapproved() : START");
        return misService.searchDisapprovedString(keyword, pageNumber);
    }

    @RequestMapping(value = "/searchApprover/{keyword}/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<User> searchApprover(@PathVariable String keyword, @PathVariable int pageNumber) {
        LOGGER.info("searchApprover() : START");
        return userService.searchApprover(keyword, pageNumber);
    }

    @RequestMapping(value = "/searchGroup/{keyword}/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private Page<Group> searchGroup(@PathVariable String keyword, @PathVariable int pageNumber) {
        LOGGER.info("searchGroup() : START");
        return groupService.searchGroup(keyword, pageNumber);
    }

    @RequestMapping(value = "/extractReports/{keyword}/{action}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private List<UserReports> extractReports(@PathVariable String keyword, @PathVariable String action) {
        LOGGER.info("extractReports() : START");
        List<UserReports> userReportsList = new ArrayList<>();
        UserReports columnHeaders = new UserReports();
        columnHeaders.setId("User #");
        columnHeaders.setUserName("User ID");
        columnHeaders.setFirstName("First Name");
        columnHeaders.setLastName("Last Name");
        columnHeaders.setGroupName("Group");
        columnHeaders.setRole("Role");
        columnHeaders.setStatus("Status");
        userReportsList.add(columnHeaders);

        if ("search".equalsIgnoreCase(action)) {
            LOGGER.info("Extract Reports By Search : START");
            List<User> usersList = userService.extractApprover(keyword);
            int sequence = 1;
            for (User user : usersList) {
                UserReports userReports = new UserReports();
                userReports.setId(String.valueOf(sequence));
                userReports.setUserName(user.getUserName());
                userReports.setFirstName(user.getFirstName());
                userReports.setLastName(user.getLastName());
                if (user.getGroup() != null) {
                    Group group = groupService.get(user.getGroup());
                    userReports.setGroupName(group.getGroupName());
                } else
                    userReports.setGroupName(" ");
                if (user.getRole() != null) {
                    Role role = roleService.get(user.getRole());
                    userReports.setRole(role.getRoleType());
                } else
                    userReports.setRole(" ");
                String status;
                if (user.getStatus() == 1)
                    status = "available";
                else
                    status = "unavailable";
                userReports.setStatus(status);
                userReportsList.add(userReports);
                sequence++;
            }
        } else if ("group".equalsIgnoreCase(action)) {
            LOGGER.info("Extract Reports By Group : START");
            List<User> usersList = userService.extractByGroupName(keyword);
            int sequence = 1;
            for (User user : usersList) {
                UserReports userReports = new UserReports();
                userReports.setId(String.valueOf(sequence));
                userReports.setUserName(user.getUserName());
                userReports.setFirstName(user.getFirstName());
                userReports.setLastName(user.getLastName());
                if (user.getGroup() != null) {
                    Group group = groupService.get(user.getGroup());
                    userReports.setGroupName(group.getGroupName());
                } else
                    userReports.setGroupName(" ");
                if (user.getRole() != null) {
                    Role role = roleService.get(user.getRole());
                    userReports.setRole(role.getRoleType());
                } else
                    userReports.setRole(" ");
                String status;
                if (user.getStatus() == 1) {
                    status = "available";
                } else
                    status = "unavailable";
                userReports.setStatus(status);
                userReportsList.add(userReports);
                sequence++;
            }
        } else if ("status".equalsIgnoreCase(action)) {
            LOGGER.info("Extract Reports By Status : START");
            int status = Integer.parseInt(keyword);
            List<User> usersList = userService.extractByStatus(status);
            int sequence = 1;
            for (User user : usersList) {
                UserReports userReports = new UserReports();
                userReports.setId(String.valueOf(sequence));
                userReports.setUserName(user.getUserName());
                userReports.setFirstName(user.getFirstName());
                userReports.setLastName(user.getLastName());
                if (user.getGroup() != null) {
                    Group group = groupService.get(user.getGroup());
                    userReports.setGroupName(group.getGroupName());
                } else
                    userReports.setGroupName(" ");
                if (user.getRole() != null) {
                    Role role = roleService.get(user.getRole());
                    userReports.setRole(role.getRoleType());
                } else
                    userReports.setRole(" ");
                String availability;
                if (user.getStatus() == 1) {
                    availability = "available";
                } else
                    availability = "unavailable";
                userReports.setStatus(availability);
                userReportsList.add(userReports);
                sequence++;
            }
        } else if ("all".equalsIgnoreCase(action)) {
            LOGGER.info("Extract All Reports : START");
            List<User> usersList = userService.extractAllApprover();
            int sequence = 1;
            for (User user : usersList) {
                UserReports userReports = new UserReports();
                userReports.setId(String.valueOf(sequence));
                userReports.setUserName(user.getUserName());
                userReports.setFirstName(user.getFirstName());
                userReports.setLastName(user.getLastName());
                if (user.getGroup() != null) {
                    Group group = groupService.get(user.getGroup());
                    userReports.setGroupName(group.getGroupName());
                } else 
                	userReports.setGroupName(" ");
                if (user.getRole() != null) {
                    Role role = roleService.get(user.getRole());
                    userReports.setRole(role.getRoleType());
                } else 
                	userReports.setRole(" ");
                String status;
                if (user.getStatus() == 1) {
                    status = "available";
                } else {
                    status = "unavailable";
                }
                userReports.setStatus(status);
                userReportsList.add(userReports);
                sequence++;
            }
        }
        return userReportsList;
    }

    @RequestMapping(value = "/extractGroupReports/{keyword}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    private List<GroupReports> extractGroupReports(@PathVariable String keyword) {

        LOGGER.info("extractGroupReports() : START");
        List<GroupReports> groupReportList = new ArrayList<>();
        List<Group> groupList = groupService.extractGroup(keyword);
        GroupReports columnHeaders = new GroupReports();
        columnHeaders.setId("Group #");
        columnHeaders.setGroupName("Group Name");
        columnHeaders.setStatus("Status");
        columnHeaders.setRoleType("Group Role");
        groupReportList.add(columnHeaders);
        int sequence = 1;
        for (Group group : groupList) {
            GroupReports groupReports = new GroupReports();
            String status;
            groupReports.setId(String.valueOf(sequence));
            groupReports.setGroupName(group.getGroupName());
            if (group.getStatus() == 1) {
                status = "available";
            } else
                status = "unavailable";
            groupReports.setStatus(status);
            if (group.getRole() != null) {
                Role role = roleService.get(group.getRole());
                groupReports.setRoleType(role.getRoleType());
            } else
                groupReports.setRoleType(" ");
            groupReportList.add(groupReports);
            sequence++;
        }
        return groupReportList;
    }

    @RequestMapping(value = "/extractAllGroupReports", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private List<GroupReports> extractAllGroupReports() {
        LOGGER.info("extractAllGroupReports() : START");
        List<GroupReports> groupReportList = new ArrayList<>();
        List<Group> groupList = groupService.extractAllGroup();
        GroupReports columnHeaders = new GroupReports();
        columnHeaders.setId("Group #");
        columnHeaders.setGroupName("Group Name");
        columnHeaders.setStatus("Status");
        columnHeaders.setRoleType("Group Role");
        groupReportList.add(columnHeaders);
        int sequence = 1;
        for (Group group : groupList) {
            GroupReports groupReports = new GroupReports();
            String status;
            groupReports.setId(String.valueOf(sequence));
            groupReports.setGroupName(group.getGroupName());
            if (group.getStatus() == 1) {
                status = "available";
            } else
                status = "unavailable";
            groupReports.setStatus(status);
            if (group.getRole() != null) {
                Role role = roleService.get(group.getRole());
                groupReports.setRoleType(role.getRoleType());
            } else
                groupReports.setRoleType(" ");
            groupReportList.add(groupReports);
            sequence++;
        }
        return groupReportList;
    }

    @RequestMapping(value = "/extractApplicationReports", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private <T> T extractApplicationReports(@RequestBody Map<String, Object> map) {
        LOGGER.info("extractApplicationReports() : START");
        List<ApplicationReports> applicationReportsList = Collections.synchronizedList(new ArrayList<ApplicationReports>());
        List<DisapproveReports> disapproveReports = Collections.synchronizedList(new ArrayList<DisapproveReports>());
        List<ApprovalReport> approvalReports = Collections.synchronizedList(new ArrayList<ApprovalReport>());
        List<ApproveUnpaidReport> approveUnpaidReports = Collections.synchronizedList(new ArrayList<ApproveUnpaidReport>());

        ApplicationReports applicationHeader = reportBuilder.createApplicationHeader();
        DisapproveReports disapproveHeader = reportBuilder.createDisapproveHeader();
        ApprovalReport approvalHeader = reportBuilder.createApprovalHeader();
        ApproveUnpaidReport approveUnpaidHeader = reportBuilder.createApproveUnpaidHeader();

        if ("forApprovalString".equalsIgnoreCase(map.get("action").toString())) {
            LOGGER.info("Extract Application Reports For Approval By String: START");
            List<SubsApplication> applicationsPage = misService.extractForApprovalString(map.get("keyword").toString());
            approvalReports.add(approvalHeader);
            for (SubsApplication subsApplication : applicationsPage) {
                Subscriber subscriber = subscriberService.get(subsApplication.getSubscriber());
                ApprovalReport approvalReport = reportBuilder.createApprovalRow(subsApplication, subscriber);
                approvalReports.add(approvalReport);
            }
            return (T) approvalReports;
        } else if ("approvedString".equalsIgnoreCase(map.get("action").toString())) {
            LOGGER.info("Extract Application Reports Approved By String: START");
            List<SubsApplication> applicationsPage = misService.extractApprovedString(map.get("keyword").toString());
            approveUnpaidReports.add(approveUnpaidHeader);
            for (SubsApplication subsApplication : applicationsPage) {
                Subscriber subscriber = subscriberService.get(subsApplication.getSubscriber());
                ApproveUnpaidReport approveUnpaidReport = reportBuilder.createApproveUnpaidRow(subsApplication, subscriber);
                approveUnpaidReports.add(approveUnpaidReport);
            }
            return (T) approveUnpaidReports;
        } else if ("disapprovedString".equalsIgnoreCase(map.get("action").toString())) {
            LOGGER.info("Extract Application Reports Disapproved By String: START");
            List<SubsApplication> applicationsPage = misService.extractDisapprovedString(map.get("keyword").toString());
            disapproveReports.add(disapproveHeader);
            for (SubsApplication subsApplication : applicationsPage) {
                Subscriber subscriber = subscriberService.get(subsApplication.getSubscriber());
                DisapproveReports disapproveReport = reportBuilder.createDisapproveRow(subsApplication, subscriber);
                disapproveReports.add(disapproveReport);
            }
            return (T) disapproveReports;
        } else if ("forAssignmentString".equalsIgnoreCase(map.get("action").toString())) {
            LOGGER.info("Extract Application Reports For Assignment By String: START");
            List<SubsApplication> applicationsPage = misService.extractForAssignmentString(map.get("keyword").toString());
            applicationReportsList.add(applicationHeader);
            for (SubsApplication subsApplication : applicationsPage) {
                Subscriber subscriber = subscriberService.get(subsApplication.getSubscriber());
                ApplicationReports applicationReports = reportBuilder.createRow(subsApplication, subscriber);
                applicationReportsList.add(applicationReports);
            }
            return (T) applicationReportsList;
        } else if ("forApprovalDate".equalsIgnoreCase(map.get("action").toString())) {
            LOGGER.info("Extract Application Reports For Approval By Date : START");
            Date to = new Date(VanillaDateUtils.longDateToFormat(map.get("to").toString()));
            Date from = new Date(VanillaDateUtils.longDateFromFormat(map.get("from").toString()));
            List<SubsApplication> applicationsPage = misService.extractForApprovalMIS(new Timestamp(to.getTime()), new Timestamp(from.getTime()));
            approvalReports.add(approvalHeader);
            for (SubsApplication subsApplication : applicationsPage) {
                Subscriber subscriber = subscriberService.get(subsApplication.getSubscriber());
                ApprovalReport approvalReport = reportBuilder.createApprovalRow(subsApplication, subscriber);
                approvalReports.add(approvalReport);
            }
            return (T) approvalReports;
        } else if ("forAssignmentDate".equalsIgnoreCase(map.get("action").toString())) {
            LOGGER.info("Extract Application Reports For Assignment By Date : START");
            Date to = new Date(VanillaDateUtils.longDateToFormat(map.get("to").toString()));
            Date from = new Date(VanillaDateUtils.longDateFromFormat(map.get("from").toString()));
            List<SubsApplication> applicationsPage = misService.extractForAssignmentMIS(new Timestamp(to.getTime()), new Timestamp(from.getTime()));
            applicationReportsList.add(applicationHeader);
            for (SubsApplication subsApplication : applicationsPage) {
                Subscriber subscriber = subscriberService.get(subsApplication.getSubscriber());
                ApplicationReports applicationReports = reportBuilder.createRow(subsApplication, subscriber);
                applicationReportsList.add(applicationReports);
            }
            return (T) applicationReportsList;
        } else if ("approvedDate".equalsIgnoreCase(map.get("action").toString())) {
            LOGGER.info("Extract Application Reports For Approved By Date : START");
            Date to = new Date(VanillaDateUtils.longDateToFormat(map.get("to").toString()));
            Date from = new Date(VanillaDateUtils.longDateFromFormat(map.get("from").toString()));
            List<SubsApplication> applicationsPage = misService.extractApprovedMIS(new Timestamp(to.getTime()), new Timestamp(from.getTime()));
            approveUnpaidReports.add(approveUnpaidHeader);
            for (SubsApplication subsApplication : applicationsPage) {
                Subscriber subscriber = subscriberService.get(subsApplication.getSubscriber());
                ApproveUnpaidReport approveUnpaidReport = reportBuilder.createApproveUnpaidRow(subsApplication, subscriber);
                approveUnpaidReports.add(approveUnpaidReport);
            }
            return (T) approveUnpaidReports;
        } else if ("disapprovedDate".equalsIgnoreCase(map.get("action").toString())) {
            LOGGER.info("Extract Application Reports Disapproved By Date : START");
            Date to = new Date(VanillaDateUtils.longDateToFormat(map.get("to").toString()));
            Date from = new Date(VanillaDateUtils.longDateFromFormat(map.get("from").toString()));
            LOGGER.info("date from : " + from + " : date to : " + to);
            List<SubsApplication> applicationsPage = misService.extractDisapprovedMIS(new Timestamp(to.getTime()), new Timestamp(from.getTime()));
            disapproveReports.add(disapproveHeader);
            for (SubsApplication subsApplication : applicationsPage) {
                Subscriber subscriber = subscriberService.get(subsApplication.getSubscriber());
                DisapproveReports disapproveReport = reportBuilder.createDisapproveRow(subsApplication, subscriber);
                disapproveReports.add(disapproveReport);
            }
            return (T) disapproveReports;
        } else if ("allApproved".equalsIgnoreCase(map.get("action").toString())) {
            LOGGER.info("Extract Application Reports All Approved : START");
            List<SubsApplication> applicationsPage = misService.extractAllApproved();
            approveUnpaidReports.add(approveUnpaidHeader);
            for (SubsApplication subsApplication : applicationsPage) {
                Subscriber subscriber = subscriberService.get(subsApplication.getSubscriber());
                ApproveUnpaidReport approveUnpaidReport = reportBuilder.createApproveUnpaidRow(subsApplication, subscriber);
                approveUnpaidReports.add(approveUnpaidReport);
            }
            return (T) approveUnpaidReports;
        } else if ("allDisapproved".equalsIgnoreCase(map.get("action").toString())) {
            LOGGER.info("Extract Application Reports All Disapproved : START");
            List<SubsApplication> applicationsPage = misService.extractAllDisapproved();
            disapproveReports.add(disapproveHeader);
            for (SubsApplication subsApplication : applicationsPage) {
                Subscriber subscriber = subscriberService.get(subsApplication.getSubscriber());
                DisapproveReports disapproveReport = reportBuilder.createDisapproveRow(subsApplication, subscriber);
                disapproveReports.add(disapproveReport);
            }
            return (T) disapproveReports;
        } else if ("allForApproval".equalsIgnoreCase(map.get("action").toString())) {
            LOGGER.info("Extract Application Reports All Disapproved : START");
            List<SubsApplication> applicationsPage = misService.extractAllForApproval();
            approvalReports.add(approvalHeader);
            for (SubsApplication subsApplication : applicationsPage) {
                Subscriber subscriber = subscriberService.get(subsApplication.getSubscriber());
                ApprovalReport approvalReport = reportBuilder.createApprovalRow(subsApplication, subscriber);
                approvalReports.add(approvalReport);
            }
            return (T) approvalReports;
        } else if ("allForAssignment".equalsIgnoreCase(map.get("action").toString())) {
            LOGGER.info("Extract Application Reports All Disapproved : START");
            List<SubsApplication> applicationsPage = misService.extractAllForAssignment();
            applicationReportsList.add(applicationHeader);
            for (SubsApplication subsApplication : applicationsPage) {
                Subscriber subscriber = subscriberService.get(subsApplication.getSubscriber());
                ApplicationReports applicationReports = reportBuilder.createRow(subsApplication, subscriber);
                applicationReportsList.add(applicationReports);
            }
            return (T) applicationReportsList;
        }
        return (T) applicationReportsList;
    }
}