package ph.com.ncs.vanilla.ws.home.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * Created by ace on 6/28/16.
 */
@RestController
public class HomeController {


    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

    @RequestMapping("/user")
    @ResponseBody
    public Principal user(Principal user) {
        return user;
    }
    //@RequestMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public String login() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        logger.info("user name : " + authentication.getName());
        return "success";
    }
}
