package ph.com.ncs.vanilla.ws.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import ph.com.ncs.vanilla.application.domain.SubsApplication;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;
import ph.com.ncs.vanilla.reports.domain.ApplicationReports;
import ph.com.ncs.vanilla.reports.domain.ApprovalReport;
import ph.com.ncs.vanilla.reports.domain.ApproveUnpaidReport;
import ph.com.ncs.vanilla.reports.domain.DisapproveReports;
import ph.com.ncs.vanilla.subscriber.domain.Subscriber;

@Component
public class ReportsHeader {
	
    @Autowired
    private Environment resource;
	
    public ApproveUnpaidReport createApproveUnpaidRow(SubsApplication subsApplication, Subscriber subscriber) {
        ApproveUnpaidReport approveUnpaidReport = new ApproveUnpaidReport();
        approveUnpaidReport.setApplicationDate(VanillaDateUtils.formatDate(subsApplication.getApplicationDate()));
        approveUnpaidReport.setMsisdn(subsApplication.getSubscriberMsisdn());
        approveUnpaidReport.setFirstName(subscriber.getFirstName());
        approveUnpaidReport.setMiddleName(subscriber.getMiddleName());
        approveUnpaidReport.setLastName(subscriber.getLastName());
        approveUnpaidReport.setEmail(subsApplication.getUserEmail());
        approveUnpaidReport.setAddress(subscriber.getAddress());
        approveUnpaidReport.setBirthday(VanillaDateUtils.formatDate(subscriber.getBirthday()));
        approveUnpaidReport.setGender(subscriber.getGender());
        approveUnpaidReport.setMothersName(subscriber.getMothersName());
        approveUnpaidReport.setAccountType(subsApplication.getAccountType());
        approveUnpaidReport.setAttachment(resource.getRequiredProperty("cloud.aws.s3.bucket.url") + subsApplication.getAttachment());
        approveUnpaidReport.setTermsCondition(resource.getRequiredProperty("cloud.aws.s3.bucket.url") + subsApplication.getTermsConditions());
        approveUnpaidReport.setApprovedDate(VanillaDateUtils.formatDate(subsApplication.getApprovalDate()));
        String userName = "";
        if (subsApplication.getUser() != null) {
            userName = subsApplication.getUser().getUserName();
        }
        approveUnpaidReport.setApproverId(userName);
        return approveUnpaidReport;
    }

	public ApprovalReport createApprovalRow(SubsApplication subsApplication, Subscriber subscriber) {
        ApprovalReport approvalReport = new ApprovalReport();
        approvalReport.setApplicationDate(VanillaDateUtils.formatDate(subsApplication.getApplicationDate()));
        approvalReport.setMsisdn(subsApplication.getSubscriberMsisdn());
        approvalReport.setFirstName(subscriber.getFirstName());
        approvalReport.setMiddleName(subscriber.getMiddleName());
        approvalReport.setLastName(subscriber.getLastName());
        approvalReport.setEmail(subsApplication.getUserEmail());
        approvalReport.setAddress(subscriber.getAddress());
        approvalReport.setBirthday(VanillaDateUtils.formatDate(subscriber.getBirthday()));
        approvalReport.setGender(subscriber.getGender());
        approvalReport.setMothersName(subscriber.getMothersName());
        approvalReport.setAccountType(subsApplication.getAccountType());
        approvalReport.setAttachment(resource.getRequiredProperty("cloud.aws.s3.bucket.url") + subsApplication.getAttachment());
        approvalReport.setTermsCondition(resource.getRequiredProperty("cloud.aws.s3.bucket.url") + subsApplication.getTermsConditions());
        String userName = "";
        if (subsApplication.getUser() != null)
            userName = subsApplication.getUser().getUserName();
        approvalReport.setApproverId(userName);
        return approvalReport;
    }

    public DisapproveReports createDisapproveRow(SubsApplication subsApplication, Subscriber subscriber) {
        DisapproveReports disapproveReports = new DisapproveReports();
        disapproveReports.setApplicationDate(VanillaDateUtils.formatDate(subsApplication.getApplicationDate()));
        disapproveReports.setMsisdn(subsApplication.getSubscriberMsisdn());
        disapproveReports.setFirstName(subscriber.getFirstName());
        disapproveReports.setMiddleName(subscriber.getMiddleName());
        disapproveReports.setLastName(subscriber.getLastName());
        disapproveReports.setEmail(subsApplication.getUserEmail());
        disapproveReports.setAddress(subscriber.getAddress());
        disapproveReports.setBirthday(VanillaDateUtils.formatDate(subscriber.getBirthday()));
        disapproveReports.setGender(subscriber.getGender());
        disapproveReports.setMothersName(subscriber.getMothersName());
        disapproveReports.setAccountType(subsApplication.getAccountType());
        disapproveReports.setAttachment(resource.getRequiredProperty("cloud.aws.s3.bucket.url") + subsApplication.getAttachment());
        disapproveReports.setTermsCondition(resource.getRequiredProperty("cloud.aws.s3.bucket.url") + subsApplication.getTermsConditions());
        String reasonDesc = "";
        String reasonName = "";
        if (subsApplication.getRejection() != null) {
            reasonDesc = subsApplication.getRejection().getReasonDescription();
            reasonName = subsApplication.getRejection().getReasonName();
        }
        disapproveReports.setAddNotes(reasonDesc);
        disapproveReports.setReason(reasonName);
        disapproveReports.setApprovedDate(VanillaDateUtils.formatDate(subsApplication.getApprovalDate()));
        String userName = "";
        if (subsApplication.getUser() != null)
            userName = subsApplication.getUser().getUserName();
        disapproveReports.setApproverId(userName);
        return disapproveReports;
    }

    public ApplicationReports createRow(SubsApplication subsApplication, Subscriber subscriber) {
        ApplicationReports applicationReports = new ApplicationReports();
        applicationReports.setApplicationDate(VanillaDateUtils.formatDate(subsApplication.getApplicationDate()));
        applicationReports.setMsisdn(subsApplication.getSubscriberMsisdn());
        applicationReports.setFirstName(subscriber.getFirstName());
        applicationReports.setMiddleName(subscriber.getMiddleName());
        applicationReports.setLastName(subscriber.getLastName());
        applicationReports.setEmail(subsApplication.getUserEmail());
        applicationReports.setAddress(subscriber.getAddress());
        applicationReports.setBirthday(VanillaDateUtils.formatDate(subscriber.getBirthday()));
        applicationReports.setGender(subscriber.getGender());
        applicationReports.setMothersName(subscriber.getMothersName());
        applicationReports.setAccountType(subsApplication.getAccountType());
        applicationReports.setAttachment(resource.getRequiredProperty("cloud.aws.s3.bucket.url") + subsApplication.getAttachment());
        applicationReports.setTermsCondition(resource.getRequiredProperty("cloud.aws.s3.bucket.url") + subsApplication.getTermsConditions());
        return applicationReports;
    }

    public ApproveUnpaidReport createApproveUnpaidHeader() {
        ApproveUnpaidReport columnHeaders = new ApproveUnpaidReport();
        columnHeaders.setApplicationDate("Application Date");
        columnHeaders.setMsisdn("Mobile Number");
        columnHeaders.setFirstName("First Name");
        columnHeaders.setMiddleName("Middle Name");
        columnHeaders.setLastName("Last Name");
        columnHeaders.setEmail("Email Address");
        columnHeaders.setAddress("Home Address");
        columnHeaders.setBirthday("Birthday");
        columnHeaders.setGender("Gender");
        columnHeaders.setMothersName("Mother's Maiden Name");
        columnHeaders.setAccountType("Account Type");
        columnHeaders.setAttachment("Attachment");
        columnHeaders.setTermsCondition("T&C");
        columnHeaders.setApprovedDate("Approved Date");
        columnHeaders.setApproverId("Approver");
        return columnHeaders;
    }

    public ApprovalReport createApprovalHeader() {
        ApprovalReport columnHeaders = new ApprovalReport();
        columnHeaders.setApplicationDate("Application Date");
        columnHeaders.setMsisdn("Mobile Number");
        columnHeaders.setFirstName("First Name");
        columnHeaders.setMiddleName("Middle Name");
        columnHeaders.setLastName("Last Name");
        columnHeaders.setEmail("Email Address");
        columnHeaders.setAddress("Home Address");
        columnHeaders.setBirthday("Birthday");
        columnHeaders.setGender("Gender");
        columnHeaders.setMothersName("Mother's Maiden Name");
        columnHeaders.setAccountType("Account Type");
        columnHeaders.setAttachment("Attachment");
        columnHeaders.setTermsCondition("T&C");
        columnHeaders.setApproverId("Approver");
        return columnHeaders;
    }

    public DisapproveReports createDisapproveHeader() {
        DisapproveReports columnHeaders = new DisapproveReports();
        columnHeaders.setApplicationDate("Application Date");
        columnHeaders.setMsisdn("Mobile Number");
        columnHeaders.setFirstName("First Name");
        columnHeaders.setMiddleName("Middle Name");
        columnHeaders.setLastName("Last Name");
        columnHeaders.setEmail("Email Address");
        columnHeaders.setAddress("Home Address");
        columnHeaders.setBirthday("Birthday");
        columnHeaders.setGender("Gender");
        columnHeaders.setMothersName("Mother's Maiden Name");
        columnHeaders.setAccountType("Account Type");
        columnHeaders.setAttachment("Attachment");
        columnHeaders.setTermsCondition("T&C");
        columnHeaders.setAddNotes("Add Note");
        columnHeaders.setReason("Reason for Rejection");
        columnHeaders.setApprovedDate("Disapproved Date");
        columnHeaders.setApproverId("Approver");
        return columnHeaders;
    }

    public ApplicationReports createApplicationHeader() {
        ApplicationReports columnHeaders = new ApplicationReports();
        columnHeaders.setApplicationDate("Application Date");
        columnHeaders.setMsisdn("Mobile Number");
        columnHeaders.setFirstName("First Name");
        columnHeaders.setMiddleName("Middle Name");
        columnHeaders.setLastName("Last Name");
        columnHeaders.setEmail("Email Address");
        columnHeaders.setAddress("Home Address");
        columnHeaders.setBirthday("Birthday");
        columnHeaders.setGender("Gender");
        columnHeaders.setMothersName("Mother's Maiden Name");
        columnHeaders.setAccountType("Account Type");
        columnHeaders.setAttachment("Attachment");
        columnHeaders.setTermsCondition("T&C");
        return columnHeaders;
    }
}