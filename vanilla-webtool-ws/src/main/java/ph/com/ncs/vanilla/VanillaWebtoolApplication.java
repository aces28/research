package ph.com.ncs.vanilla;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.test.ImportAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;
import ph.com.ncs.vanilla.base.security.listener.TimeOutListener;

/**
 * Hello world!
 */
@SpringBootApplication
@ImportAutoConfiguration(VanillaContextConfig.class)
public class VanillaWebtoolApplication extends SpringBootServletInitializer {
	
	@Bean
	public TimeOutListener executorListener() {
	   return new TimeOutListener();
	}
	
    public static void main(String[] args) {
    	SpringApplication.run(VanillaWebtoolApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(VanillaWebtoolApplication.class);
    }
    
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
    	super.onStartup(servletContext);
        servletContext.addListener(executorListener());
    }
}
