package ph.com.ncs.vanilla.ws.transaction.controller;

import ph.com.ncs.vanilla.transaction.domain.Transaction;
import ph.com.ncs.vanilla.base.listener.BaseListener;

/**
 * Created by ace on 5/29/16.
 */
public interface TransactionListener<T> extends BaseListener<Transaction> {

    /**
     * Updates transaction to set error code
     *
     * @param t
     */
    void onError(T t);
}
