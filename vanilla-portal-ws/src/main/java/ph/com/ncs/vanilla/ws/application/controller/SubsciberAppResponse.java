package ph.com.ncs.vanilla.ws.application.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import ph.com.ncs.vanilla.application.domain.SubsApplication;

/**
 * @author edjohna 5/31/2016
 */
public class SubsciberAppResponse<T extends SubsApplication> extends ResponseEntity {

    private HttpStatus statusCode;
    private SubsApplication subsApplication;

    protected SubsciberAppResponse(HttpStatus statusCode, SubsApplication subsApplication) {
        super(statusCode);
        this.statusCode = statusCode;
        this.subsApplication = subsApplication;
    }

    @Override
    public SubsApplication getBody() {
        SubsApplication subsApplicationResponse = new SubsApplication();

        subsApplicationResponse.setSubscriber(subsApplication.getSubscriber());
        subsApplicationResponse.setUser(subsApplication.getUser());
        subsApplicationResponse.setSubscriberMsisdn(subsApplication.getSubscriberMsisdn());
        subsApplicationResponse.setApplicationDate(subsApplication.getApplicationDate());
        subsApplicationResponse.setAccountType(subsApplication.getAccountType());
        subsApplicationResponse.setUserEmail(subsApplication.getUserEmail());
        subsApplicationResponse.setAttachment(subsApplication.getAttachment());
        subsApplicationResponse.setTermsConditions(subsApplication.getTermsConditions());
        subsApplicationResponse.setApprovalDate(subsApplication.getApprovalDate());

        return subsApplicationResponse;
    }

    @Override
    public HttpStatus getStatusCode() {
        return statusCode;
    }

}
