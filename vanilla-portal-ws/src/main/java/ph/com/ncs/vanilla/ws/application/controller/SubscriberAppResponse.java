package ph.com.ncs.vanilla.ws.application.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import ph.com.ncs.vanilla.subscriber.domain.Subscriber;

public class SubscriberAppResponse extends ResponseEntity {

    private HttpStatus statusCode;
    private Subscriber subscriber;

    protected SubscriberAppResponse(HttpStatus statusCode, Subscriber subscriber) {
        super(statusCode);
        this.statusCode = statusCode;
        this.subscriber = subscriber;
    }

    @Override
    public Subscriber getBody() {
        Subscriber subscriberResponse = new Subscriber();
        subscriberResponse.setFirstName(subscriber.getFirstName());
        subscriberResponse.setMiddleName(subscriber.getMiddleName());
        subscriberResponse.setLastName(subscriber.getLastName());
        subscriberResponse.setGender(subscriber.getGender());
        subscriberResponse.setAddress(subscriber.getAddress());
        subscriberResponse.setBirthday(subscriber.getBirthday());
        subscriberResponse.setMothersName(subscriber.getMothersName());
        return subscriberResponse;
    }

    @Override
    public HttpStatus getStatusCode() {
        return statusCode;
    }

}
