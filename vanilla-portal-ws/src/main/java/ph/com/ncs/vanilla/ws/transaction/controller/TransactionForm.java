package ph.com.ncs.vanilla.ws.transaction.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import ph.com.ncs.vanilla.base.form.BaseForm;
import ph.com.ncs.vanilla.base.listener.BaseListener;
import ph.com.ncs.vanilla.base.service.BaseClientService;
import ph.com.ncs.vanilla.client.service.*;
import ph.com.ncs.vanilla.commons.constants.VanillaConstants;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;
import ph.com.ncs.vanilla.commons.utils.encryption.VanillaAESEncryption;
import ph.com.ncs.vanilla.transaction.domain.Transaction;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Random;

/**
 * Created by ace on 5/29/16.
 */
@Component
public class TransactionForm extends BaseForm<Transaction> {

    private static final Logger logger = LoggerFactory.getLogger(TransactionForm.class);

    @Autowired
    private VanillaClientFactory clientFactory;
    private TransactionListener listener;

    private String encryptedMsisdn;
    private String decryptedMsisdn;

    @Autowired
    private Environment environment;

    @Override
    public <T> T handleRequest(Transaction transaction) {

        Timestamp timestamp = new Timestamp(VanillaDateUtils.longCurrentDate());
        transaction.setCreatedBy(VanillaConstants.SYSTEM_USER);
        transaction.setCreatedDate(timestamp);
        transaction.setUpdatedDate(timestamp);
        transaction.setUpdatedBy(VanillaConstants.SYSTEM_USER);
        VanillaNFClientService nfClientService;
        VanillaRavenClientService ravenClientService;

        try {
            if (transaction.getStatus() == null) {

                nfClientService = clientFactory.getNFService();
                ravenClientService = clientFactory.getRavenService();
                setDecryptMsisdn(transaction.getMsisdn());
                transaction.setMsisdn(decryptedMsisdn);
//                transaction.setStatus(0);
//                transaction.setType("TM");
                transaction = clientFactory.getEsbService().getResult(transaction);
                if (!StringUtils.isEmpty(transaction.getType())) {
//                if (true) {
                    logger.info("transaction start with ESB : " + decryptedMsisdn);
                    setEncryptedMsisdn(transaction.getMsisdn());
                    transaction.setMode(0);
                    transaction.setMsisdn(encryptedMsisdn);
                    listener.onStatusComplete(transaction);
                    logger.info("transaction done with ESB : " + decryptedMsisdn);
                    transaction.setMsisdn(decryptedMsisdn);
                    String nfResponse = nfClientService.getResult(transaction);
                    logger.info("transaction done with NF : " + decryptedMsisdn);
                    if (!StringUtils.isEmpty(nfResponse) && nfResponse.contains("FAILED")) {
//                    if (true) {
                        transaction.setMode(1);
                        transaction.setMsisdn(encryptedMsisdn);
                        listener.onStatusComplete(transaction);
                        logger.info("generate random code for RAVEN : " + decryptedMsisdn);
                        transaction.setCode(100000 + (new Random().nextInt(900000)));
                        transaction.setMsisdn(decryptedMsisdn);
                        if (ravenClientService.getResult(transaction) == Integer.valueOf(0)) {
//                        if (true) {
                            logger.info("send raven sms with msisdn :  " + decryptedMsisdn);
                            transaction.setMode(2);
                            transaction.setMsisdn(encryptedMsisdn);
                            listener.onStatusComplete(transaction);
                            logger.info("sent raven sms with msisdn :  " + decryptedMsisdn);
                        }
                    } else {
                        transaction.setReferenceKey(environment.getRequiredProperty(BaseClientService.NFBUS_ERROR_MESSAGE));
                    }
                }
            } else if (transaction.getStatus() == 2) {
                ravenClientService = clientFactory.getRavenService();
                transaction.setCode(100000 + (new Random().nextInt(900000)));
                transaction.setMsisdn(decryptedMsisdn);
                transaction.setMode(2);
                if (ravenClientService.getResult(transaction) == Integer.valueOf(0)) {
//                if (true) {
                    transaction.setMsisdn(encryptedMsisdn);
                    listener.onStatusComplete(transaction);
                }
            }
        } catch (Exception e) {
            logger.error("Unable to send request : " + e.getClass().getSimpleName(), e.getCause());
            if (e instanceof EsbClientException) {
                transaction.setReferenceKey(environment.getRequiredProperty(BaseClientService.ESB_TIMEOUT_MESSAGE));
            } else if (e instanceof NfClientException) {
                transaction.setReferenceKey(environment.getRequiredProperty(BaseClientService.NFBUS_TIMEOUT_MESSAGE));
            } else if (e instanceof RavenClientException) {
                transaction.setReferenceKey(environment.getRequiredProperty(BaseClientService.RAVEN_TIMEOUT_MESSAGE));
            }

            transaction.setStatus(500);
            listener.onError(transaction);
        }

        transaction.setMsisdn(decryptedMsisdn);
        return (T) transaction;
    }

    @Override
    public void setListener(BaseListener listener) {
        this.listener = (TransactionListener) listener;
    }

    private void setDecryptMsisdn(String msisdn) {
        this.decryptedMsisdn = VanillaAESEncryption.decrypt(msisdn);
    }

    private void setEncryptedMsisdn(String msisdn) {
        this.encryptedMsisdn = VanillaAESEncryption.encrypt(msisdn);
    }
}
