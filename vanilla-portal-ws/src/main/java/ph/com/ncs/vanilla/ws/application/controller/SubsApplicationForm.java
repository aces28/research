package ph.com.ncs.vanilla.ws.application.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ph.com.ncs.vanilla.application.domain.SubsApplication;
import ph.com.ncs.vanilla.base.form.BaseForm;
import ph.com.ncs.vanilla.base.listener.BaseListener;
import ph.com.ncs.vanilla.client.service.RavenClientException;
import ph.com.ncs.vanilla.client.service.VanillaClientFactory;
import ph.com.ncs.vanilla.client.service.VanillaRavenClientService;
import ph.com.ncs.vanilla.commons.constants.VanillaConstants;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;
import ph.com.ncs.vanilla.resource.domain.Resource;
import ph.com.ncs.vanilla.subscriber.domain.Subscriber;
import ph.com.ncs.vanilla.transaction.domain.Transaction;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author edjohna 5/31/2016
 */
@Component
public class SubsApplicationForm extends BaseForm<Map<String, Object>> {

    private static final Logger logger = LoggerFactory.getLogger(SubsApplicationForm.class);

    @Autowired
    private VanillaClientFactory clientFactory;
    @Autowired
    private VanillaRavenClientService ravenClientService;

    private Subscriber subscriber;
    private SubsApplication subsApplication;
    private Resource resource;

    public <T> T handleSubscriberRequest(Map<String, Object> map) throws SubscriberAppException, ParseException {
        if (map != null) {
            subscriber = setSubscriber(map);
            logger.info("Subscriber :: " + subscriber.toString());
        }
        return (T) subscriber;
    }

    public <T> T handleSubscriberAppRequest(Map<String, Object> map) throws SubscriberAppException {
        if (map != null) {
            subsApplication = setSubsApplication(map);
            logger.info("Subscriber App :: " + subsApplication.toString());
        }
        return (T) subsApplication;
    }

    public <T> T handleResourceRequest(Map<String, Object> map) throws SubscriberAppException {
        if (map != null) {
            resource = setResource(map);
            logger.info("Resource :: " + resource.toString());
        }
        return (T) resource;
    }

    public void sendConfirmationMessage(Transaction transaction) {
        try {
            ravenClientService.getResult(transaction);
        } catch (RavenClientException e) {
            logger.error("Unable to send sms : ", e);
        }
    }

    private Subscriber setSubscriber(Map<String, Object> map) throws SubscriberAppException, ParseException {
        logger.info("Setting Map to Subscriber");
        Subscriber subscriber = new Subscriber();
        for (Entry<String, Object> entry : map.entrySet()) {
            if (entry.getKey().equalsIgnoreCase("firstName")) {
                logger.info("firstname:: " + entry.getValue().toString());
                subscriber.setFirstName(entry.getValue().toString());
            } else if (entry.getKey().equalsIgnoreCase("middleName")) {
                logger.info("middleName:: " + entry.getValue().toString());
                subscriber.setMiddleName(entry.getValue().toString());
            } else if (entry.getKey().equalsIgnoreCase("lastName")) {
                logger.info("lastName:: " + entry.getValue().toString());
                subscriber.setLastName(entry.getValue().toString());
            } else if (entry.getKey().equalsIgnoreCase("gender")) {
                logger.info("gender:: " + entry.getValue().toString());
                subscriber.setGender(entry.getValue().toString());
            } else if (entry.getKey().equalsIgnoreCase("motherName")) {
                logger.info("motherName:: " + entry.getValue().toString());
                subscriber.setMothersName(entry.getValue().toString());
            } else if (entry.getKey().equalsIgnoreCase("homeAddress")) {
                logger.info("homeAddress:: " + entry.getValue().toString());
                subscriber.setAddress(entry.getValue().toString());
            } else if (entry.getKey().equalsIgnoreCase("birthday")) {
                Date birthDate = new Date(VanillaDateUtils.longSqlFormat(entry.getValue().toString()));
                logger.info("birthday:: " + birthDate);
                subscriber.setBirthday(birthDate);
            } else if (entry.getKey().equalsIgnoreCase("resourceUrl")) {
                logger.info("================= resourceUrl:: " + entry.getValue().toString());
            }

        }

        subscriber.setCreatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        subscriber.setUpdatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        subscriber.setCreatedBy(VanillaConstants.SYSTEM_USER);
        subscriber.setUpdatedBy(VanillaConstants.SYSTEM_USER);

        logger.info("Done Setting Map to Subscriber");
        return subscriber;
    }

    private SubsApplication setSubsApplication(Map<String, Object> map) throws SubscriberAppException {
        logger.info("Setting Map to SubsApplication");
        SubsApplication subsApplication = new SubsApplication();

        for (Entry<String, Object> entry : map.entrySet()) {
            if (entry.getKey().equalsIgnoreCase("mobile")) {
                logger.info("mobile:: " + entry.getValue().toString());
                subsApplication.setSubscriberMsisdn(entry.getValue().toString());
            } else if (entry.getKey().equalsIgnoreCase("emailAdd")) {
                logger.info("emailAdd:: " + entry.getValue().toString());
                subsApplication.setUserEmail(entry.getValue().toString());
            } else if (entry.getKey().equalsIgnoreCase("resourceUrl")) {
                logger.info("resourceUrl:: " + entry.getValue().toString());
                subsApplication.setTermsConditions(entry.getValue().toString());
            } else if (entry.getKey().equalsIgnoreCase("type")) {
                logger.info("type:: " + entry.getValue().toString());
                subsApplication.setAccountType(entry.getValue().toString());
            } else if (entry.getKey().equalsIgnoreCase("attachment")) {
                logger.info("attachment:: " + entry.getValue().toString());
                subsApplication.setAttachment(entry.getValue().toString());
            }
        }

        subsApplication.setApprovalDate(new Date(VanillaDateUtils.longCurrentDate()));
        subsApplication.setApplicationDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        subsApplication.setStatus(0);
        subsApplication.setCreatedBy(VanillaConstants.SYSTEM_USER);
        subsApplication.setUpdatedBy(VanillaConstants.SYSTEM_USER);
        subsApplication.setUpdatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        subsApplication.setCreatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        return subsApplication;


    }

    private Resource setResource(Map<String, Object> map) throws SubscriberAppException {
        logger.info("Setting Map to Resource");
        Resource resource = new Resource();

        for (Entry<String, Object> entry : map.entrySet()) {
            if (entry.getKey().equalsIgnoreCase("resourceUrl")) {
                logger.info("resourceUrl:: " + entry.getValue().toString());
                resource.setResourceUrl(entry.getValue().toString());
            }
        }

        resource.setCreatedBy(VanillaConstants.SYSTEM_USER);
        resource.setUpdatedBy(VanillaConstants.SYSTEM_USER);
        resource.setCreatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
        resource.setUpdatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));

        logger.info("Done Setting Map to Resource");
        return resource;
    }

    @Override
    public <T extends BaseListener> void setListener(T t) {
    }

}
