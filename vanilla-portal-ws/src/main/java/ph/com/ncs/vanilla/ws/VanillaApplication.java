package ph.com.ncs.vanilla.ws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.test.ImportAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import ph.com.ncs.vanilla.base.dao.VanillaContextConfig;

/**
 * Created by ace on 5/22/16.
 */
@SpringBootApplication
@ImportAutoConfiguration(VanillaContextConfig.class)
@ComponentScan(basePackages = {"ph.com.ncs.vanilla"})
public class VanillaApplication {

    public static void main(String[] args) {
        SpringApplication.run(VanillaApplication.class, args);
    }

}
