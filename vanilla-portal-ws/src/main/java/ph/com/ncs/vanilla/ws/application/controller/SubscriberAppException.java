package ph.com.ncs.vanilla.ws.application.controller;

/**
 * @author edjohna
 */
public class SubscriberAppException extends Exception {

    public SubscriberAppException() {
        super();
    }

    public SubscriberAppException(String message) {
        super(message);
    }

    public SubscriberAppException(Throwable cause) {
        super(cause);
    }

    public SubscriberAppException(String message, Throwable cause) {
        super(message, cause);
    }

}
