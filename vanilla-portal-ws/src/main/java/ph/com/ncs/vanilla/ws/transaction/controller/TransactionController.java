package ph.com.ncs.vanilla.ws.transaction.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ph.com.ncs.vanilla.application.domain.SubsApplication;
import ph.com.ncs.vanilla.commons.utils.VanillaDateUtils;
import ph.com.ncs.vanilla.commons.utils.encryption.VanillaAESEncryption;
import ph.com.ncs.vanilla.subscriber.service.SubscriberAppService;
import ph.com.ncs.vanilla.transaction.domain.Transaction;
import ph.com.ncs.vanilla.transaction.service.TransactionService;
import ph.com.ncs.vanilla.transaction.service.TransactionServiceException;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by ace on 5/20/16.
 */
@RestController
@RequestMapping("/register")
public class TransactionController implements TransactionListener<Transaction> {

    private static final Logger logger = LoggerFactory.getLogger(TransactionController.class);
    private String encryptedMsisdn;

    @Autowired
    private TransactionForm transactionForm;
    @Autowired
    private TransactionService transactionService;
    @Autowired
    private SubscriberAppService appService;
    @Autowired
    private Environment environment;

    @RequestMapping(value = "/verify/{msisdn}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public TransactionResponse<Transaction> verify(@PathVariable String msisdn) {
        logger.info("check msisdn : " + msisdn);
        setEncryptedMsisdn(msisdn);
        logger.info("check encrypted msisdn : " + encryptedMsisdn);
        Transaction transaction = new Transaction();
        transaction.setMsisdn(encryptedMsisdn);
        transactionForm.setListener(this);

        List<SubsApplication> subsApplications = appService.findApplicationByMsisdn(msisdn);

        if (subsApplications != null && (!subsApplications.isEmpty() && subsApplications.size() > 0)) {
            SubsApplication application = subsApplications.get(0);
            String message;
            logger.info("application status : " + application.getStatus());
            switch (application.getStatus()) {
                case 0:
                case 1:
                case 2:
                    message = environment.getRequiredProperty("error.pending.message");
                    break;
                case 3:
                default:
                    transaction = transactionForm.handleRequest(transaction);
                    if (transaction.getCode() != null) {
                        return new TransactionResponse<>(HttpStatus.OK, transaction);
                    } else {
                        return new TransactionResponse<>(HttpStatus.NOT_ACCEPTABLE, transaction);
                    }
            }

            logger.info("pending application size : " + subsApplications.size());
            transaction.setReferenceKey(message);
            return new TransactionResponse<>(HttpStatus.NOT_ACCEPTABLE, transaction);
        }

        transaction = transactionForm.handleRequest(transaction);
        if (transaction.getCode() != null) {
            return new TransactionResponse<>(HttpStatus.OK, transaction);
        } else {
            return new TransactionResponse<>(HttpStatus.NOT_ACCEPTABLE, transaction);
        }

    }

    @RequestMapping(value = "/validate/{code}/{msisdn}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public boolean validate(@PathVariable int code, @PathVariable String msisdn) {
        logger.info("validate : code " + code + " : msisdn : " + msisdn);
        try {
            setEncryptedMsisdn(msisdn);
            Transaction transaction = transactionService.findByCodeAndMsisdn(code, encryptedMsisdn);
            long currentDate = new Date(System.currentTimeMillis()).getTime();

            if (transaction != null) {
                long transactionDate = transaction.getUpdatedDate().getTime();
                long timeDiff = currentDate - transactionDate;
                long diffMinutes = timeDiff / (60 * 1000) % 60;

                if (diffMinutes >= 5L) {
                    logger.info("Subscriber verification code has expired : " + diffMinutes);
                    transactionService.save(transaction);
                    return false;
                }

                transaction.setStatus(-1);
                transaction.setUpdatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
                transactionService.save(transaction);
            } else {
                return false;
            }
        } catch (TransactionServiceException e) {
            logger.error("Invalid code or value does not exists ", e);
            return false;
        }
        return true;
    }

    @RequestMapping(value = "/resend/{msisdn}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.PUT)
    public TransactionResponse<Transaction> resend(@PathVariable String msisdn) {

        setEncryptedMsisdn(msisdn);
        logger.info("resend msisdn : " + msisdn + " : encrypted msisdn : " + encryptedMsisdn);
        Transaction transaction = new Transaction();
        transaction.setMsisdn(encryptedMsisdn);
        transaction.setStatus(2);
        transactionForm.setListener(this);

        transaction = transactionForm.handleRequest(transaction);
        return new TransactionResponse<>(HttpStatus.OK, transaction);
    }

    @Override
    public void onStatusComplete(Transaction transaction) {

        try {
            if (transaction != null)
                logger.info("Current search msisdn : " + transaction.toString() + " : " + transaction.getMode());
            Transaction modifiedTxn = transactionService.findByFailedMsisdn(transaction.getMsisdn());

            if (modifiedTxn != null) logger.info("================== modified size : " + modifiedTxn.toString());

            if (transaction != null)
                logger.info("Current search msisdn : " + transaction.toString() + " END =======================");

            if (transaction.getStatus() != null) {
                if (transaction.getMode() == 0 && modifiedTxn == null) {
                    logger.info("========== DONE WITH ESB INSERT ========" + transaction.getMsisdn());
                    transaction.setStatus(transaction.getMode());
                    transactionService.save(transaction);
                } else if ((modifiedTxn.getStatus() == 0 && transaction.getMode() == 1) || (modifiedTxn.getStatus() == 1 && transaction.getMode() == 2)) {
                    logger.info("========== START WITH NF/RAVEN INSERT ========" + transaction.getMsisdn() + transaction.getMode());
                    logger.info("Current msisdn : " + transaction.getMsisdn() + " : modified msisdn : " + modifiedTxn.getMsisdn());
                    transaction.setStatus(transaction.getMode());
                    transaction.setUpdatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
                    transactionService.save(transaction);
                    logger.info("Current msisdn : " + transaction.getMsisdn() + " : modified msisdn : " + modifiedTxn.getMsisdn());
                    logger.info("========== DONE WITH NF/RAVEN INSERT ========" + transaction.getMsisdn());
                } else if ((modifiedTxn.getStatus() == 0 && transaction.getMode() == 0) || (modifiedTxn.getStatus() == 1 && transaction.getMode() == 0) || (modifiedTxn.getStatus() == 2 && transaction.getMode() == 0)) {

                    logger.info("Current msisdn : " + transaction.getMsisdn() + " : modified msisdn : " + modifiedTxn.getMsisdn());
                    logger.info("Negative scenario ========" + transaction.getMsisdn() + " modified status : " +
                            modifiedTxn.getStatus() + " : current : " + transaction.getMode());

                    modifiedTxn.setStatus(-1);
                    modifiedTxn.setUpdatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
                    transactionService.save(modifiedTxn);
                    transaction.setStatus(transaction.getMode());
                    transaction.setUpdatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
                    transactionService.save(transaction);
                    logger.info("Current msisdn : " + transaction.getMsisdn() + " : modified msisdn : " + modifiedTxn.getMsisdn());
                } else if (modifiedTxn.getStatus() == 2 && transaction.getMode() == 2) {
                    logger.info("Resend SVC : START");
                    logger.info("Current msisdn : " + transaction.getMsisdn() + " : modified msisdn : " + modifiedTxn.getMsisdn());
                    modifiedTxn.setCode(transaction.getCode());
                    modifiedTxn.setUpdatedDate(new Timestamp(VanillaDateUtils.longCurrentDate()));
                    transactionService.save(modifiedTxn);
                    logger.info("Current msisdn : " + transaction.getMsisdn() + " : modified msisdn : " + modifiedTxn.getMsisdn());
                } else {
                    logger.info("Invalid transaction : START");
                }
            } else {
                logger.info("Invalid transaction : EMPTY");
            }

            logger.info("id : " + transaction.getId() + " msisdn : " + transaction.getMsisdn() + " code : " +
                    transaction.getCode() + " status : " + transaction.getMode());
        } catch (TransactionServiceException e) {
            logger.error("Unable to update transaction ", e);
        }
    }

    private void setEncryptedMsisdn(String msisdn) {
        this.encryptedMsisdn = VanillaAESEncryption.encrypt(msisdn);
    }

    @Override
    public void onError(Transaction transaction) {
    }
}
