package ph.com.ncs.vanilla.ws.application.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import ph.com.ncs.vanilla.application.domain.SubsApplication;
import ph.com.ncs.vanilla.aws.service.AwsUploadService;
import ph.com.ncs.vanilla.email.service.EmailService;
import ph.com.ncs.vanilla.resource.domain.Resource;
import ph.com.ncs.vanilla.resource.service.ResourceService;
import ph.com.ncs.vanilla.subscriber.domain.Subscriber;
import ph.com.ncs.vanilla.subscriber.service.SubscriberAppService;
import ph.com.ncs.vanilla.subscriber.service.SubscriberService;
import ph.com.ncs.vanilla.transaction.domain.Transaction;

import javax.servlet.ServletException;
import javax.servlet.http.Part;
import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Map;

/**
 * Created by ace on 5/26/16.
 */
@RestController
@RequestMapping("/subscriber")
public class SubscriberAppController {

    private static final Logger logger = LoggerFactory.getLogger(SubscriberAppController.class);

    @Autowired
    private SubsApplicationForm subsApplicationForm;
    @Autowired
    private SubscriberService subscriberService;
    @Autowired
    private SubscriberAppService subscriberAppService;
    @Autowired
    private EmailService emailService;
    @Autowired
    private ResourceService resourceService;
    @Autowired
    private AwsUploadService awsUploadService;

    private Subscriber subscriber;
    private SubsApplication subsApplication;
    private Resource resource;


    @RequestMapping(value = "/application", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private SubscriberAppResponse application(@RequestBody Map<String, Object> map) throws ParseException {
        logger.info("Starting Subscriber Application Validation");
        subscriber = new Subscriber();
        subsApplication = new SubsApplication();
        resource = new Resource();

        try {
            logger.info("Initializing handleSubscriberRequest");
            subscriber = subsApplicationForm.handleSubscriberRequest(map);
            subsApplication = subsApplicationForm.handleSubscriberAppRequest(map);
            resource = subsApplicationForm.handleResourceRequest(map);
        } catch (SubscriberAppException e) {
            logger.error("Http Status = Not Acceptable", e.getMessage());
            return (new SubscriberAppResponse(HttpStatus.NOT_ACCEPTABLE, subscriber));
        }

        Subscriber subId = subscriberService.save(subscriber);
        subsApplication.setSubscriber(subId);
        SubsApplication subsAppId = subscriberAppService.save(subsApplication);
        resource.setSubsApplication(subsAppId);
        resourceService.save(resource);
        Transaction transaction = new Transaction();
        transaction.setMode(5);
        transaction.setMsisdn(subsApplication.getSubscriberMsisdn());
        subsApplicationForm.sendConfirmationMessage(transaction);

        return (new SubscriberAppResponse(HttpStatus.OK, subscriber));

    }

    @RequestMapping(value = "/sendEmail", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private boolean sendEmail(@RequestBody Map<String, Object> map) {
        String email = String.valueOf(map.get("emailAdd"));
        if (StringUtils.isEmpty(email)) {
            return false;
        } else {
            logger.info("send email message : " + email);
            emailService.sendEmail(email);
            return true;
        }

    }

    @RequestMapping(value = "/upload", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    private Map<String, String> upload(MultipartHttpServletRequest multipartRequest) {
        Collection<Part> multipartFiles = null;
        try {
            multipartFiles = multipartRequest.getParts();
            logger.info("upload : START : " + multipartFiles.size());
        } catch (IOException e) {
            logger.error("IOException :: ", e);
        } catch (ServletException e) {
            logger.error("ServletException :: ", e);
        }

        return awsUploadService.uploadMultipart(multipartFiles);
    }

}
