package ph.com.ncs.vanilla.ws.application.controller;

import ph.com.ncs.vanilla.base.listener.BaseListener;
import ph.com.ncs.vanilla.subscriber.domain.Subscriber;

/**
 * @author edjohna 6/3/2016
 */
public interface SubsAppListener<T> extends BaseListener<Subscriber> {

    void onError(T t);
}
