package ph.com.ncs.vanilla.ws.transaction.controller;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.NoSuchPaddingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import ph.com.ncs.vanilla.commons.utils.encryption.VanillaAESEncryption;
import ph.com.ncs.vanilla.transaction.domain.Transaction;

/**
 * Created by ace on 5/29/16.
 */
public class TransactionResponse<T extends Transaction> extends ResponseEntity {

    private HttpStatus statusCode;
    private Transaction transaction;

    protected TransactionResponse(HttpStatus statusCode, Transaction transaction) {
        super(statusCode);
        this.statusCode = statusCode;
        this.transaction = transaction;
    }

    @Override
    public Transaction getBody() {
        return transaction;
    }

    @Override
    public HttpStatus getStatusCode() {
        return statusCode;
    }
}
