package com.globe.vanilla.batch.service.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import com.globe.vanilla.batch.service.AccountDAO;

import ph.com.ncs.vanilla.subscriber.domain.Account;

public class AccountDAOImpl implements AccountDAO {

    protected SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    protected final Session session() {
        return sessionFactory.openSession();
    }

	@Override
	@Transactional(readOnly = false)
	public void createOrUpdate(Account account) {
		Session session = session();
		session.saveOrUpdate(account);
		session.flush();
		session.close();
	}

	@Override
	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public Account get(String msisdn) {
		Session session = session();
		Criteria criteria = session.createCriteria(Account.class);
		criteria.add(Restrictions.eq("msisdn", msisdn));
		List<Account> list = criteria.list();
		if (list != null && list.size() == 1) {
			return list.get(0);
		}
		return null;
	}

	@Override
	@Transactional(readOnly = false)
	public void delete(String msisdn) {
		Session session = session();
		Account account = get(msisdn);
		session.delete(account);
		session.flush();
		session.close();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<Account> list() {
		Session session = session();
		Criteria criteria = session.createCriteria(Account.class);
		List<Account> list = criteria.list();
		return list;
	}

}
