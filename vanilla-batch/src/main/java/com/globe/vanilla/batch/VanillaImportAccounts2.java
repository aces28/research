package com.globe.vanilla.batch;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.support.ResourcePropertySource;

import com.globe.vanilla.batch.service.AccountDAO;
import com.globe.vanilla.batch.util.VanillaFileUtil;

import ph.com.ncs.vanilla.subscriber.domain.Account;

public class VanillaImportAccounts2 {

    private static final Logger LOGGER = LoggerFactory.getLogger(VanillaImportAccounts2.class);
	
	private static final SimpleDateFormat DATE_LOG_FORMAT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");

	private static final SimpleDateFormat DATE_FILE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmssSSS");

	private static AccountDAO accountDAO;
	static {
		accountDAO = new ClassPathXmlApplicationContext(new String[] { "META-INF/spring/datasource-context.xml" })
				.getBean("accountDAO", AccountDAO.class);
	}
	
	public static void main(String args[]) {
		try {
			VanillaImportAccounts2 vanilla = new VanillaImportAccounts2();
			vanilla.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void execute() throws Exception {

		ResourcePropertySource propertySource = new ResourcePropertySource("resource",
				"classpath:vanilla-batch.properties");

		String logDir = (String) propertySource.getProperty("vanilla.batch.logs_dir");
		if (!validateDir(logDir)) {
			throw new Exception("Cannot find valid log directory location. ");
		}
		String sourceDir = (String) propertySource.getProperty("vanilla.batch.source_dir");
		if (!validateDir(sourceDir)) {
			throw new Exception("Cannot find valid source directory location. ");
		}
		String backupDir = (String) propertySource.getProperty("vanilla.batch.backup_dir");
		if (!validateDir(backupDir)) {
			throw new Exception("Cannot find valid backup directory location. ");
		}
		String errorDir = (String) propertySource.getProperty("vanilla.batch.error_dir");
		if (!validateDir(errorDir)) {
			throw new Exception("Cannot find valid error directory location. ");
		}
		ArrayList<String> logContents = new ArrayList<String>();
		LOGGER.info("Start of Vanilla import batch. Time:" + DATE_LOG_FORMAT.format(new Date()));
		logContents.add("Start of Vanilla import batch. Time:" + DATE_LOG_FORMAT.format(new Date()));
		String[] filenames = new File(sourceDir).list();
		File file = null;
		for (String filename : filenames) {
			file = new File(sourceDir + File.separator + filename);
			if (file.isFile()) {
				processFile(file, logContents, backupDir, errorDir);
			}
		}
		logContents.add("");
		LOGGER.info("End of Vanilla import batch. Time:" + DATE_LOG_FORMAT.format(new Date()));
		logContents.add("End of Vanilla import batch. Time:" + DATE_LOG_FORMAT.format(new Date()));
		String date = DATE_FILE_FORMAT.format(new Date());
		File location = new File(logDir);
		File logFile = new File(location.getAbsolutePath() + File.separator + "vanilla_import_" + date + ".log");
		VanillaFileUtil.writeFile(logFile, logContents);
	}

	private void processFile(File file, ArrayList<String> logContents, String backupDir, String errorDir) {
		logContents.add("");
		LOGGER.info("Start processing file:" + file.getName() + " " + DATE_LOG_FORMAT.format(new Date()));
		logContents.add("Start processing file:" + file.getName() + " " + DATE_LOG_FORMAT.format(new Date()));
		ArrayList<Account> accounts = VanillaFileUtil.readFile(file, logContents);
		if (accounts != null) {
			for (Account account : accounts) {
				//accountService.save(account);
				if (accountDAO.get(account.getMsisdn()) == null) {
					accountDAO.createOrUpdate(account);
				}
			}
			logContents.add("File:" + file.getName() + " is valid.");
			logContents.add("Records saved:" + accounts.size());
			moveFile(backupDir, file, "backup");
		} else {
			logContents.add("File:" + file.getName() + " is invalid.");
			moveFile(errorDir, file, "error");
		}
		LOGGER.info("End processing file:" + file.getName() + " " + DATE_LOG_FORMAT.format(new Date()));
		logContents.add("End processing file:" + file.getName() + " " + DATE_LOG_FORMAT.format(new Date()));
	}

	private static void moveFile(String directory, File file, String type) {
		String date = DATE_FILE_FORMAT.format(new Date());
		File location = new File(directory);
		File destination = new File(
				location.getAbsolutePath() + File.separator + file.getName() + "_" + type + "_" + date);
		file.renameTo(destination);
	}

	private static boolean validateDir(String directory) {
		if (directory == null || directory.length() == 0) {
			return false;
		}
		if (directory.endsWith(File.separator)) {
			directory = directory.substring(0, directory.length() - 1);
		}
		return (new File(directory).isDirectory());
	}
}