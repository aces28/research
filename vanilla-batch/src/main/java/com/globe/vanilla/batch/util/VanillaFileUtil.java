package com.globe.vanilla.batch.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ph.com.ncs.vanilla.subscriber.domain.Account;

public class VanillaFileUtil {

	public static ArrayList<Account> readFile(File file, ArrayList<String> logContents) {
		ArrayList<Account> accountList = new ArrayList<Account>();
		ArrayList<String> msisdnList = new ArrayList<String>();
		BufferedReader reader = null;
		boolean hasInvalid = false;
		try {
			reader = new BufferedReader(new FileReader(file));
			String msisdn;
			while ((msisdn = reader.readLine()) != null) {
				if (msisdnList.contains(msisdn)) {
					logContents.add("DUPLICATE MSISDN:" + msisdn + " found in file.");
				}
				if (isValidMsisdn(msisdn)) {
					msisdnList.add(msisdn);
					accountList.add(Account.newInstance(msisdn));
				} else {
					logContents.add("MSISDN:" + msisdn + " with invalid format found in file.");
					hasInvalid = true;
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Error:"+e.getMessage());
		} catch (IOException e) {
			System.out.println("Error:"+e.getMessage());
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
				}
			}
		}
		if (hasInvalid) {
			return null;
		}
		return accountList;
	}

	public static void writeFile(File file, ArrayList<String> logContents) {
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			for (String log : logContents) {
				bw.write(log + "\n");
			}
			bw.close();
			fw.close();
		} catch (IOException e) {
			System.out.println("Error:"+e.getMessage());
		}
	}	
	
	private static boolean isValidMsisdn(String msisdn) {
		Pattern pattern = Pattern.compile("^0[0-9]{10}");
		Matcher matcher = pattern.matcher(msisdn);
		return (matcher.matches());
	}
	
	public static void main(String args[]) {
		SimpleDateFormat DATE_LOG_FORMAT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
		try {
			Date date1 = DATE_LOG_FORMAT.parse("2016/07/12 12:43:50.603");
			Date date2 = DATE_LOG_FORMAT.parse("2016/07/12 13:29:51.226");
			long diff = date2.getTime() - date1.getTime();
			long diffMinutes = diff / (60 * 1000) % 60;
			System.out.println(diffMinutes + " minutes");

			date1 = DATE_LOG_FORMAT.parse("2016/07/12 13:29:51.226");
			date2 = DATE_LOG_FORMAT.parse("2016/07/12 15:14:07.665");
			diff = date2.getTime() - date1.getTime();
			diffMinutes = diff / (60 * 1000) % 60;
			System.out.println(diffMinutes + " minutes");

		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	
//	public static void main(String args[]) {
//		long index = 9190000000l;
//		long value = 0;
//		ArrayList<String> msisdns = new ArrayList<String>(); 
//		for (int i = 1; i <= 200000; i++) {
//			value = index + i;
//			msisdns.add("0" + value);
//		}
//		
//		VanillaFileUtil.writeFile(new File("C:\\vanilla-test\\stress3.txt"), msisdns);
//	}
}
