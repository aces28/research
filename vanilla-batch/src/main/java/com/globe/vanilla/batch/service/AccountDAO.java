package com.globe.vanilla.batch.service;

import java.util.List;

import ph.com.ncs.vanilla.subscriber.domain.Account;

public interface AccountDAO {

	public void createOrUpdate(Account account);
	
	public List<Account> list();
	
	public Account get(String msisdn);
	
	public void delete(String msisdn);
	
}
