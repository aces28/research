package ph.com.ncs.vanilla.aws.service;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * Created by jomerp on 8/4/2016.
 */
@Configuration
@PropertySource(value = {"classpath:application-aws.properties"})
public class AwsConfig {

    @Value("${cloud.aws.credentials.accessKey}")
    private String accessKey;

    @Value("${cloud.aws.credentials.secretKey}")
    private String secretKey;

    @Value("${cloud.aws.region}")
    private String region;

    @Value("${cloud.aws.credentials.enable}")
    private boolean enabled;


    @Bean
    public BasicAWSCredentials basicAWSCredentials() {
        return new BasicAWSCredentials(accessKey, secretKey);
    }

    @Bean
    public AmazonS3Client amazonS3Client() {
        AmazonS3Client amazonS3;
        if (enabled) {
            amazonS3 = new AmazonS3Client(basicAWSCredentials());
        } else {
            amazonS3 = new AmazonS3Client();
        }

//        amazonS3.setRegion(Region.getRegion(Regions.fromName(region)));
        return amazonS3;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
