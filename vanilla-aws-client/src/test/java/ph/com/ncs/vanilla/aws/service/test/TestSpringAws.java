package ph.com.ncs.vanilla.aws.service.test;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.*;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import ph.com.ncs.vanilla.aws.service.AwsConfig;

import java.io.File;
import java.util.List;
import java.util.UUID;

/**
 * Created by jomerp on 8/4/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AwsConfig.class)
public class TestSpringAws {

    private static final Logger logger = LoggerFactory.getLogger(TestSpringAws.class);

    @Value("${cloud.aws.region}")
    private String region;

    @Value("${cloud.aws.credentials.enable}")
    private boolean enabled;

    @Autowired
    private AmazonS3Client amazonS3Client;

    @Autowired
    private ResourceLoader resourceLoader;

    @Value("${cloud.aws.s3.bucket}")
    private String bucket;

    @Value("${cloud.aws.s3.bucket.url}")
    private String awsUrl;

    @Test
    public void testAwsProperties() {
        logger.info("test aws values : " + awsUrl + " : enabled : " + enabled);
        Assert.assertEquals("https://vnls3dv01.s3.amazonaws.com/", awsUrl);
    }

    @Test
    public void testAwsS3Client() {
        Assert.assertNotNull(amazonS3Client);
        BucketPolicy bucketPolicy = amazonS3Client.getBucketPolicy(bucket);
        logger.info("get details : " + bucketPolicy.getPolicyText());
    }

    @Test
    public void testUploadFile() {
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, UUID.randomUUID().toString() + "_aws-test-file.txt", new File("aws-test-file.txt"));
        PutObjectResult objectResult = amazonS3Client.putObject(putObjectRequest);
        logger.info("file version : " + objectResult.getVersionId());
    }

    @Test
    public void testMultipart() {
        ObjectListing objectListing = amazonS3Client.listObjects(bucket);
        List<S3ObjectSummary> summaries = objectListing.getObjectSummaries();
        for (S3ObjectSummary s3ObjectSummary : summaries) {
            logger.info("============ : file name : ============ " + s3ObjectSummary.getKey());
        }
    }

}
